//
//  RentViewRentViewInteractor.swift
//  turaQshare
//
//  Created by Yesbol Kulanbekov on 28/12/2018.
//  Copyright © 2018 turaQshare. All rights reserved.
//
import Result

class RentViewInteractor: RentViewInteractorInput {

    weak var output: RentViewInteractorOutput!
    
    
    
    func post(_ freetime: FreeTime) {
        
        let onReturn = { (result: PostFreeTimesResult) in

            switch result {
            case let .success(value) :
                print("Post free time result is :", value)

                DispatchQueue.main.async {
                    self.output.didPostSuccessfully()
                }
            case let .failure(error):
                DispatchQueue.main.async {
                    self.output.didPostFail(with: error)
                }
            }
        }
        
        let postFreetimeRequest = PostFreeTimesReq(freetime: freetime, onReturn: onReturn)
        postFreetimeRequest.dispatch()
    
    }

}
