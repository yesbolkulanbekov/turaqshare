//
//  RentViewRentViewInteractorIO.swift
//  turaQshare
//
//  Created by Yesbol Kulanbekov on 28/12/2018.
//  Copyright © 2018 turaQshare. All rights reserved.
//

import Foundation

protocol RentViewInteractorInput {
    func post(_ freetime: FreeTime)
}

protocol RentViewInteractorOutput: class {
    func didPostSuccessfully()
    func didPostFail(with error: TuraqError)
}
