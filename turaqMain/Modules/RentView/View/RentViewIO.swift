//
//  RentViewRentViewViewIO.swift
//  turaQshare
//
//  Created by Yesbol Kulanbekov on 28/12/2018.
//  Copyright © 2018 turaQshare. All rights reserved.
//
import Foundation

protocol RentViewOutput {

    /**
        @author Yesbol Kulanbekov
        Notify presenter that view is ready
    */

    func viewIsReady()
    func initView(with parkingPlace: ParkingPlace)
}


protocol RentViewInput: class, DisplaysAlert {

    /**
        @author Yesbol Kulanbekov
        Setup initial state of the view
    */
    func showWeekTable(with weekDays: [WeekDay], _ onWeekPick: OnWeekPick)
    func showPickerView(with onPick: OnPick, currentFreq: Freq)
    func presentDatePicker(date: Date, minDate: Date, _ datePicked: CommandWith<Date>)
    func setupInitialState()
    
    var props: RentViewController.Props { get set }
    
}

typealias OnWeekPick = CommandWith<[WeekDay]>
