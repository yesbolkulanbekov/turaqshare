//
//  RentView.swift
//  turaQshare
//
//  Created by Yesbol Kulanbekov on 12/28/18.
//  Copyright © 2018 turaQshare. All rights reserved.
//

// TODO: Reorganize the methods and properties
// in GenericView as in RentView

import UIKit

class RentView: UIScrollView {
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupSelf()
        configureSubViews()
        styleSubviews()
        renderConstantData()
        layout()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
    }
    
    /// Properties
    
    var sv = RentView.Subviews()

    var props = Props.zero {
        didSet {
            sv.beginDataLbl.text = props.startDate.formatted
            sv.endDataLbl.text = props.endDate.formatted
            sv.freqDataLbl.text = props.freq.rawValue
            
            sv.begRepDataLbl.text = props.startRepDate.formatted
            sv.endRepDataLbl.text = props.endRepDate.formatted
            
            //self.sv.begRepRect.isHidden = self.props.freq.isOnce
            self.sv.endRepRect.isHidden = self.props.freq.isOnce
            self.sv.weekRect.isHidden = self.props.freq.isOnce || !(self.props.freq.isWeekly)
            
           
            guard !(props.weekDays.isEmpty) else { return }
            let firstDay = props.weekDays[0].rawValue
            let count = props.weekDays.count
            sv.weekDataLbl.text = props.weekDays[1..<count].reduce(firstDay) { (res,weekDay) in
                return res + ", " + weekDay.rawValue
            }
        }
    }
    
    @objc func injected() {
        #if DEBUG

        backgroundColor = UIColor.green
        
        sv.contentView.removeFromSuperview()
        sv.buildingTitle.removeFromSuperview()
        sv.chooseDateInfoLbl.removeFromSuperview()
        sv.beginRect.removeFromSuperview()
        sv.beginLbl.removeFromSuperview()
        sv.buildingTitle.removeFromSuperview()
        
        sv.endRect.removeFromSuperview()
        sv.endLbl.removeFromSuperview()
        sv.endDataLbl.removeFromSuperview()
        
        sv.freqRect.removeFromSuperview()
        sv.freqLbl.removeFromSuperview()
        sv.freqDataLbl.removeFromSuperview()
        
        sv.confirmBtn.removeFromSuperview()
        
        
        
        RentViewLayout(rootView: self).paint()
        #endif

    }
    

    
}

/// View configuration, content and styling

extension RentView {
    
    
    struct Props {
        var startDate: Date
        var endDate: Date
        var freq: Freq
        var startRepDate: Date
        var endRepDate: Date
        var weekDays: [WeekDay]
        
        let selectDate: CommandWith<Date>
        let selectEndDate: CommandWith<Date>
        
        let selStartRepDate: CommandWith<Date>
        let selEndRepEndDate: CommandWith<Date>
        let selWeek: CommandWith<[WeekDay]>
        
        let selectFreq: CommandWith<String>
        
        let onConfirm: CommandWith<Props>
        
        static let zero = Props(startDate: Date(),
                                endDate: Date(),
                                freq: Freq.once,
                                startRepDate: Date(),
                                endRepDate: Date(),
                                weekDays: [],
                                selectDate: CommandWith<Date> { _ in },
                                selectEndDate: CommandWith<Date> { _ in },
                                selStartRepDate: CommandWith<Date> { _ in },
                                selEndRepEndDate: CommandWith<Date> { _ in },
                                selWeek: CommandWith<[WeekDay]> { _ in },
                                selectFreq: CommandWith<String> { _ in },
                                onConfirm: CommandWith<Props> { _ in })
    }

    struct Subviews {
        let contentView   = UIView()
        let buildingTitle = UILabel()
        let chooseDateInfoLbl = UILabel()
        
        let beginRect        = UIView()
        let beginLbl     = UILabel()
        var beginDataLbl = UILabel()
        
        let endRect      = UIView()
        let endLbl     = UILabel()
        let endDataLbl = UILabel()
        
        let freqRect      = UIView()
        let freqLbl     = UILabel()
        let freqDataLbl = UILabel()
        
        let begRepRect    = UIView()
        let begRepLbl     = UILabel()
        let begRepDataLbl = UILabel()
        
        let endRepRect    = UIView()
        let endRepLbl     = UILabel()
        let endRepDataLbl = UILabel()
        
        let weekRect    = UIView()
        let weekLbl     = UILabel()
        let weekDataLbl = UILabel()
        
        let confirmBtn = UIButton(type: .system)
        
        var rootStack = UIStackView()
    }
    
    func renderConstantData() {
        self.sv.begRepRect.isHidden = true

        sv.buildingTitle.text     = ""//"#245 ЖК “Нурсая-1”"
        sv.chooseDateInfoLbl.text = "Пожалуйста выберите даты"
        
        sv.beginLbl.text      = "  Начало  "
        sv.beginDataLbl.text  = Date().formatted
        
        sv.endLbl.text      = "  Конец  "
        sv.endDataLbl.text  = Date().formatted
        
        sv.freqLbl.text      = "  Частота  "
        sv.freqDataLbl.text  = Freq.day.rawValue
        
        sv.begRepLbl.text     = "  Начало повторений  "
        sv.begRepDataLbl.text = Date().formatted

        sv.endRepLbl.text     = "  Конец повторений  "
        sv.endRepDataLbl.text = Date().formatted
        
        sv.weekLbl.text     = "  Дни недели  "
        sv.weekDataLbl.text = WeekDay.mon.rawValue
        
        sv.confirmBtn.setTitle("Подтвердить", for: .normal)
    }
    
    func styleSubviews() {
        sv.buildingTitle.textColor = .pine
        sv.buildingTitle.font = UIFont(name: "Helvetica", size: 30)
        sv.buildingTitle.adjustsFontSizeToFitWidth = true
        sv.buildingTitle.numberOfLines = 2
        
        sv.chooseDateInfoLbl.textColor = .pine
        sv.chooseDateInfoLbl.font = UIFont(name: "Helvetica", size: 14)
        sv.chooseDateInfoLbl.numberOfLines = 1
        
        sv.beginRect.backgroundColor = .whiteTwo
        sv.beginLbl.backgroundColor = UIColor.blueGreen
        sv.beginLbl.textColor = .white
        sv.beginDataLbl.font = UIFont.NunitoLight(ofSize: 12)
        sv.beginDataLbl.textColor = .pine
        
        sv.endRect.backgroundColor = .whiteTwo
        sv.endLbl.backgroundColor = UIColor.blueGreen
        sv.endLbl.textColor = .white
        sv.endDataLbl.font = UIFont.NunitoLight(ofSize: 12)
        sv.endDataLbl.textColor = .pine
        
        sv.freqRect.backgroundColor = .whiteTwo
        sv.freqLbl.backgroundColor = UIColor.blueGreen
        sv.freqLbl.textColor = .white
        sv.freqDataLbl.font = UIFont.NunitoLight(ofSize: 12)
        sv.freqDataLbl.textColor = .pine
        
        

        sv.begRepRect.backgroundColor = .whiteTwo
        sv.begRepLbl.backgroundColor = UIColor.blueGreen
        sv.begRepLbl.textColor = .white
        sv.begRepDataLbl.font = UIFont.NunitoLight(ofSize: 12)
        sv.begRepDataLbl.textColor = .pine
        
        sv.endRepRect.backgroundColor = .whiteTwo
        sv.endRepLbl.backgroundColor = UIColor.blueGreen
        sv.endRepLbl.textColor = .white
        sv.endRepDataLbl.font = UIFont.NunitoLight(ofSize: 12)
        sv.endRepDataLbl.textColor = .pine
        
        
        sv.weekRect.backgroundColor = .whiteTwo
        sv.weekLbl.backgroundColor = UIColor.blueGreen
        sv.weekLbl.textColor = .white
        sv.weekDataLbl.font = UIFont.NunitoLight(ofSize: 12)
        sv.weekDataLbl.textColor = .pine
        sv.weekDataLbl.numberOfLines = 0
        
        sv.confirmBtn.setTitleColor(UIColor.blueGreen, for: .normal)
        sv.confirmBtn.titleLabel?.font = UIFont.LucidaGrandeBold(ofSize: 18)
        sv.confirmBtn.backgroundColor = UIColor.blueGreen.withAlphaComponent(0.08)

    }
    
    private func configureSubViews() {
        let rect1GestRecognizer = UITapGestureRecognizer(target: self, action: #selector(selectStartDate))
        sv.beginRect.addGestureRecognizer(rect1GestRecognizer)
        
        let rect2GestRecognizer = UITapGestureRecognizer(target: self, action: #selector(selectEndDate))
        sv.endRect.addGestureRecognizer(rect2GestRecognizer)
        
        let rect3GestRecognizer = UITapGestureRecognizer(target: self, action: #selector(selectFrequency))
        sv.freqRect.addGestureRecognizer(rect3GestRecognizer)
        
        let begRepRecognizer = UITapGestureRecognizer(target: self, action: #selector(selBegRep))
        sv.begRepRect.addGestureRecognizer(begRepRecognizer)
        
        let endRepRecognizer = UITapGestureRecognizer(target: self, action: #selector(selEndRep))
        sv.endRepRect.addGestureRecognizer(endRepRecognizer)
        
        let weekRecognizer = UITapGestureRecognizer(target: self, action: #selector(selectWeek))
        sv.weekRect.addGestureRecognizer(weekRecognizer)
        
        sv.confirmBtn.addTarget(self, action: #selector(confirm), for: .touchUpInside)
    }
    
    private func setupSelf() {
        alwaysBounceVertical = true
        translatesAutoresizingMaskIntoConstraints = false
    }
}


extension RentView {
    
    func layout() {
        RentViewLayout(rootView: self).paint()
    }

}


extension RentView {
    @objc
    func selectStartDate() {
        print("Select start date pressed")
        props.selectDate.perform(with: props.startDate)
    }
    
    @objc
    func selectEndDate() {
        print("Select end date pressed")
        props.selectEndDate.perform(with: props.endDate)
    }
    
    @objc
    func selectFrequency() {
        print("Select end date pressed")
        props.selectFreq.perform(with: sv.freqLbl.text!)
    }
    
    @objc
    func selBegRep() {
        props.selStartRepDate.perform(with: props.startRepDate)
    }
    
    @objc
    func selEndRep() {
        props.selEndRepEndDate.perform(with: props.endRepDate)
    }
    
    @objc
    func selectWeek() {
        props.selWeek.perform(with: props.weekDays)
    }
    
    @objc
    func confirm() {
        props.onConfirm.perform(with: props)
        
    }
}




/// View layout

struct RentViewLayout {
    
    var rootView: RentView
    var sv: RentView.Subviews
    var conv: UIView
    
    init(rootView: RentView) {
        self.rootView = rootView
        self.sv = rootView.sv
        self.conv = rootView.sv.contentView
        
        sv.rootStack = UIStackView() { stack in
            stack.axis = .vertical
            stack.distribution = .fill
            stack.alignment = .fill
            stack.spacing = 30
        }
    
    }
    
    func paint() {
        addSubViews()
        addConstraints()
    }
    
    func addSubViews() {
        rootView.addSubview(sv.contentView)
        
        conv.addSubview(sv.buildingTitle)
        conv.addSubview(sv.chooseDateInfoLbl)
        
        conv.addSubview(sv.rootStack)
        
        sv.rootStack.addArrangedSubview(sv.beginRect)
        sv.rootStack.addArrangedSubview(sv.endRect)
        sv.rootStack.addArrangedSubview(sv.freqRect)
        sv.rootStack.addArrangedSubview(sv.begRepRect)
        sv.rootStack.addArrangedSubview(sv.endRepRect)
        sv.rootStack.addArrangedSubview(sv.weekRect)
        
        addSubsForRect1()
        addSubsForRect2()
        addSubsForRect3()
        addSubsForBegRep()
        addSubsForEndRep()
        addSubsForWeeks()

        conv.addSubview(sv.confirmBtn)
    }
    
    func addSubsForRect1() {
        sv.beginRect.addSubview(sv.beginLbl)
        sv.beginRect.addSubview(sv.beginDataLbl)
        addConstraintsForRect1()

    }
    
    func addSubsForRect2() {
        sv.endRect.addSubview(sv.endLbl)
        sv.endRect.addSubview(sv.endDataLbl)
        addConstraintsForRect2()

    }
    
    func addSubsForRect3() {
        sv.freqRect.addSubview(sv.freqLbl)
        sv.freqRect.addSubview(sv.freqDataLbl)
        addConstraintsForRect3()

    }
    
    func addSubsForBegRep() {
        sv.begRepRect.addSubview(sv.begRepLbl)
        sv.begRepRect.addSubview(sv.begRepDataLbl)
        
        sv.begRepRect.addConstraints([
            equal(\.heightAnchor, to: 60)
            ])
        
        sv.begRepLbl.addConstraints([
            equal(sv.begRepRect, \.leadingAnchor, constant: 10),
            equal(sv.begRepRect, \.centerYAnchor, \.topAnchor, constant: 0)
            ])
        
        sv.begRepDataLbl.addConstraints([
            equal(sv.begRepRect, \.leadingAnchor, constant: 15),
            equal(sv.begRepRect, \.bottomAnchor, constant: -15),
        ])
        
    }
    
    func addSubsForEndRep() {
        sv.endRepRect.addSubview(sv.endRepLbl)
        sv.endRepRect.addSubview(sv.endRepDataLbl)
        
        sv.endRepRect.addConstraints([
            equal(\.heightAnchor, to: 60)
            ])
        
        sv.endRepLbl.addConstraints([
            equal(sv.endRepRect, \.leadingAnchor, constant: 10),
            equal(sv.endRepRect, \.centerYAnchor, \.topAnchor, constant: 0)
            ])
        
        sv.endRepDataLbl.addConstraints([
            equal(sv.endRepRect, \.leadingAnchor, constant: 15),
            equal(sv.endRepRect, \.bottomAnchor, constant: -15),
            ])
        
    }
    
    func addSubsForWeeks() {
        sv.weekRect.addSubview(sv.weekLbl)
        sv.weekRect.addSubview(sv.weekDataLbl)
        
        
        sv.weekLbl.addConstraints([
            equal(sv.weekRect, \.leadingAnchor, constant: 10),
            equal(sv.weekRect, \.centerYAnchor, \.topAnchor, constant: 0)
            ])
        
        sv.weekDataLbl.addConstraints([
            equal(sv.weekRect, \.topAnchor, constant: 30),
            equal(sv.weekRect, \.leadingAnchor, constant: 15),
            equal(sv.weekRect, \.bottomAnchor, constant: -15),
            equal(sv.weekRect, \.trailingAnchor, constant: -10),
        ])
        
    }
    
    func addConstraints() {
        sv.contentView.addConstraints(equalTo(superView: rootView))
        sv.contentView.addConstraints([
            equal(rootView, \.widthAnchor),
            equal(\.heightAnchor, to: 900)
        ])
        
        sv.buildingTitle.addConstraints([
            equal(conv, \.leadingAnchor, constant: 20),
            equal(conv, \.topAnchor, constant: 20),
            equal(conv, \.trailingAnchor, constant: -20)
        ])
        
        sv.chooseDateInfoLbl.addConstraints([
            equal(conv, \.leadingAnchor, constant: 20),
            equal(conv, \.trailingAnchor, constant: -20),
            equal(sv.buildingTitle,
                  \.topAnchor,
                  \.bottomAnchor,
                  constant: 8)
        ])
        
        sv.rootStack.addConstraints([
            equal(conv, \.leadingAnchor, constant: 20),
            equal(conv, \.trailingAnchor, constant: -20),
            equal(sv.chooseDateInfoLbl,
                  \.topAnchor,
                  \.bottomAnchor,
                  constant: 20)
        ])
        
        
        sv.confirmBtn.addConstraints([
            equal(conv, \.leadingAnchor, constant: 98),
            equal(conv, \.trailingAnchor, constant: -98),
            equal(\.heightAnchor, to: 44),
            equal(sv.rootStack,
                  \.topAnchor,
                  \.bottomAnchor,
                  constant: 35)
            ])
    }
    
    func addConstraintsForRect1() {
        sv.beginRect.addConstraints([
            equal(\.heightAnchor, to: 60)
        ])
        
        sv.beginLbl.addConstraints([
            equal(sv.beginRect, \.leadingAnchor, constant: 10),
            equal(sv.beginRect, \.centerYAnchor, \.topAnchor, constant: 0)
        ])
        
        sv.beginDataLbl.addConstraints([
            equal(sv.beginRect, \.leadingAnchor, constant: 15),
            equal(sv.beginRect, \.bottomAnchor, constant: -15),
        ])
    }
    
    func addConstraintsForRect2() {
        sv.endRect.addConstraints([
            equal(\.heightAnchor, to: 60)
            ])
        
        sv.endLbl.addConstraints([
            equal(sv.endRect, \.leadingAnchor, constant: 10),
            equal(sv.endRect, \.centerYAnchor, \.topAnchor, constant: 0)
            ])
        
        sv.endDataLbl.addConstraints([
            equal(sv.endRect, \.leadingAnchor, constant: 15),
            equal(sv.endRect, \.bottomAnchor, constant: -15),
            ])
    }
    
    func addConstraintsForRect3() {
        sv.freqRect.addConstraints([
            equal(\.heightAnchor, to: 60)
            ])
        
        sv.freqLbl.addConstraints([
            equal(sv.freqRect, \.leadingAnchor, constant: 10),
            equal(sv.freqRect, \.centerYAnchor, \.topAnchor, constant: 0)
            ])
        
        sv.freqDataLbl.addConstraints([
            equal(sv.freqRect, \.leadingAnchor, constant: 15),
            equal(sv.freqRect, \.bottomAnchor, constant: -15),
        ])
    }
    

    
}
