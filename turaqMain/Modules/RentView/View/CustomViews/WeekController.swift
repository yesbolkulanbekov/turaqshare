//
//  WeekController.swift
//  turaQshare
//
//  Created by Yesbol Kulanbekov on 3/5/19.
//  Copyright © 2019 turaQshare. All rights reserved.
//

import UIKit

class WeekController: UIViewController {
    
    // MARK: Public
    
    var selectedDays = [WeekDay]()
    
    var onWeekPick = OnWeekPick { _ in }
    
    
    // MARK: Private props
    
    private var weekTV = UITableView()
    
    private var weekDays = WeekDay.allCases
    
    override func viewDidLoad() {
        self.navigationItem.title = "Выбор дня"
        setupTableView()
    }

    private func setupTableView() {
        weekTV.register(UITableViewCell.self, forCellReuseIdentifier: "CellID")
        weekTV.dataSource = self
        weekTV.delegate = self
        weekTV.rowHeight = 44
        weekTV.tableFooterView = UIView()
        weekTV.allowsMultipleSelection = true
        
        view.addSubview(weekTV)
        weekTV.addConstraints(equalToSafeArea(superView: view))
    }
    
    
}


extension WeekController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return weekDays.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = UITableViewCell()
        cell.textLabel?.text = weekDays[indexPath.row].rawValue
        cell.selectionStyle = .none
        cell.tintColor = UIColor.blueGreen
        
        for day in selectedDays {
            guard let chosenIndex = weekDays.firstIndex(of: day) else {
                cell.accessoryType = .none
                return cell
            }
            
            if chosenIndex == indexPath.row {
                cell.accessoryType = .checkmark
            }
            
        }
        

        return cell
    }
}

extension WeekController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let selectedDay = weekDays[indexPath.row]
        
        if selectedDays.contains(selectedDay) {
            let index = selectedDays.firstIndex(of: selectedDay)!
            guard selectedDays.count > 1 else { return }
            selectedDays.remove(at: index)
        } else {
            selectedDays.append(selectedDay)
        }
        
        tableView.reloadData()
        onWeekPick.perform(with: selectedDays)
    }
}
