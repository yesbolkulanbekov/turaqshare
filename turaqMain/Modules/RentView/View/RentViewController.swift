//
//  RentViewRentViewViewController.swift
//  turaQshare
//
//  Created by Yesbol Kulanbekov on 28/12/2018.
//  Copyright © 2018 turaQshare. All rights reserved.
//

import UIKit

class RentViewController: UIViewController {
    
    struct Props {
        var rentVProps: RentView.Props
        
        static var zero = Props(rentVProps: .zero)
    }
    
    var props = Props.zero {
        didSet {
            rentView.props = props.rentVProps
        }
    }

    var output: RentViewOutput!

    let spinnerView = UIActivityIndicatorView(style: .gray)
    let rentView    = RentView()
    
    // MARK: Life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .white
        output.viewIsReady()
    }

}

extension RentViewController:  RentViewInput {

    // MARK: RentViewViewInput
    func setupInitialState() {
        setupInitViewState()
    }
    
    func presentDatePicker(date: Date, minDate: Date, _ datePicked: CommandWith<Date>) {

        let datePickerController = MonsterDatePicker()
        datePickerController.selectedDate = date
        datePickerController.minDate = minDate
        
        let onDatePicked = CommandWith<Date> { date in
            datePicked.perform(with: date)
        }
        
        datePickerController.datePicked = onDatePicked
        
        
        datePickerController.modalTransitionStyle = .crossDissolve
        datePickerController.modalPresentationStyle = .overCurrentContext
        self.present(datePickerController, animated: true, completion: nil)
    }
    
    func showWeekTable(with weekDays: [WeekDay], _ onWeekPick: OnWeekPick) {
        let weekVC = WeekController()
        weekVC.selectedDays = weekDays
        weekVC.onWeekPick   = onWeekPick
        self.navigationController?.pushViewController(weekVC, animated: true)
    }

    
    
    func showPickerView(with onPick: OnPick, currentFreq: Freq) {
        let picker = FreqPicker()
        picker.onPick = onPick
        picker.selectedFreq = currentFreq
        picker.modalTransitionStyle = .crossDissolve
        picker.modalPresentationStyle = .overCurrentContext
        self.present(picker, animated: true, completion: nil)

    }
}



extension RentViewController {
    private func setupInitViewState() {
        navigationItem.title = "Сдать место"
        view.backgroundColor = .white
        view.addSubview(rentView)
        rentView.addConstraints(equalToSafeArea(superView: view))
        view.addSubview(spinnerView)
        spinnerView.addConstraints([
            equal(view, \UIView.safeAreaLayoutGuide.centerXAnchor),
            equal(view, \UIView.safeAreaLayoutGuide.centerYAnchor)
        ])
    }
    
    
}
