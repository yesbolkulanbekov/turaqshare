//
//  RentViewRentViewPresenter.swift
//  turaQshare
//
//  Created by Yesbol Kulanbekov on 28/12/2018.
//  Copyright © 2018 turaQshare. All rights reserved.
//

import Foundation

class RentViewPresenter: RentViewModuleInput {

    weak var view: RentViewInput!
    var interactor: RentViewInteractorInput!
    var router: RentViewRouterInput!

    var parkingPlace: ParkingPlace!
    
    
    var startDate = Date()
    var endDate = Date().adding(minutes: 30)
    
    var startRepDate = Date()
    var endRepDate = Date().adding(minutes: 30)
    
    var weekDays = [WeekDay.mon]
    
    var freq = Freq.once
}

extension RentViewPresenter: RentViewOutput {
    func initView(with parkingPlace: ParkingPlace) {
        self.parkingPlace = parkingPlace
    }

    func viewIsReady() {
        view.setupInitialState()
        view.props = vcProps()
        print("Parking Place is ", parkingPlace)
    }
    
    private func postFreetime(with props: RentView.Props) {
        var weekDays: [String]? = nil
        if freq.isWeekly {
            weekDays = self.weekDays.map { $0.description }
        }
        
        let freetime = FreeTime(uuid: nil,
                                parkingPlaceNumber: parkingPlace.placeNumber!,
                                parkingZone: parkingPlace.parkingZone!,
                                startTime: self.startDate.iso8601,
                                endTime: self.endDate.iso8601,
                                frequency: self.freq.intForm,
                                repeatStartTime: self.startRepDate.iso8601,
                                repeatEndTime: self.endRepDate.iso8601,
                                weekDays: weekDays)
        
    
        self.interactor.post(freetime)
    }
}

extension RentViewPresenter: RentViewInteractorOutput {
    func didPostSuccessfully() {
        view.displayAlert(with: "Место сдано!")
    }
    
    func didPostFail(with error: TuraqError) {
        view.displayAlert(with: error.localizedDescription)
    }

}
extension RentViewPresenter {
    func vcProps() ->  RentViewController.Props {

        let onStartDatePick = CommandWith<Date> { (date) in
            self.startDate = date
            self.startRepDate = date
            if self.startDate >= self.endDate {
                let dateAdvanced = date.adding(minutes: 30)
                self.endRepDate = dateAdvanced
                self.endDate = dateAdvanced
            }

            self.view.props = self.vcProps()
        }

        let selectDate = CommandWith<Date> { date in
            self.view.presentDatePicker(date: date, minDate: Date(), onStartDatePick)
        }


        let onEndDatePick = CommandWith<Date> { (date) in
            self.endDate = date
            if self.endDate >= self.endRepDate {
                self.endRepDate = date
            }
            self.view.props = self.vcProps()
        }

        let onSelectEndDate = CommandWith<Date> { endDate in
            let minEndDate = self.startDate.adding(minutes: 30)
            self.view.presentDatePicker(date: endDate, minDate: minEndDate , onEndDatePick)
        }
        
        
        let onPick = OnPick { freq in
            self.freq = freq
            self.view.props = self.vcProps()
        }

        let onSelectFreq = CommandWith<String> { freq in
            self.view.showPickerView(with: onPick, currentFreq: self.freq)
        }


        let onPickStartRepDate = CommandWith<Date> { date in
            self.startRepDate = date
            self.view.props = self.vcProps()
        }
        
        let onSelStartRepDate = CommandWith<Date> { date in
            self.view.presentDatePicker(date: date, minDate: Date(), onPickStartRepDate)

        }
        
        let onPickEndRepDate = CommandWith<Date> { date in
            self.endRepDate = date
            self.view.props = self.vcProps()
        }
        
        let onSelEndRepDate = CommandWith<Date> { date in
            let minDate = self.endDate
            self.view.presentDatePicker(date: date, minDate: minDate, onPickEndRepDate)
        }
        
        let onWeekPick = OnWeekPick { pickedWeekDays in
            self.weekDays = pickedWeekDays
            self.view.props = self.vcProps()
        }
        
        let onSelectWeekday = CommandWith<[WeekDay]> { weekDays in
            self.view.showWeekTable(with: weekDays, onWeekPick)
        }
        
        let onConfirm = CommandWith<RentView.Props> { props in
            self.postFreetime(with: props)
        }

        let rentVProps = RentView.Props(
            startDate: self.startDate,
            endDate: self.endDate,
            freq: self.freq,
            startRepDate: self.startRepDate,
            endRepDate: self.endRepDate,
            weekDays: self.weekDays,
            selectDate: selectDate,
            selectEndDate: onSelectEndDate,
            selStartRepDate: onSelStartRepDate,
            selEndRepEndDate: onSelEndRepDate,
            selWeek: onSelectWeekday,
            selectFreq: onSelectFreq,
            onConfirm: onConfirm
        )

        let vcProps = RentViewController.Props(rentVProps: rentVProps)

        return vcProps
    }
    

}
