//
//  RentViewRentViewConfigurator.swift
//  turaQshare
//
//  Created by Yesbol Kulanbekov on 28/12/2018.
//  Copyright © 2018 turaQshare. All rights reserved.
//

import UIKit

struct RentViewModuleConfigurator {

    func assembleModule() -> RentViewController {
        let viewController = RentViewController()
        configure(viewController: viewController)
        return viewController
    }

    private func configure(viewController: RentViewController) {

        let router = RentViewRouter()

        let presenter = RentViewPresenter()
        presenter.view = viewController
        presenter.router = router

        let interactor = RentViewInteractor()
        interactor.output = presenter

        presenter.interactor = interactor
        viewController.output = presenter
    }

}
