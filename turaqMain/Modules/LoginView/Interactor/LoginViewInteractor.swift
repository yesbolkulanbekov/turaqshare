//
//  LoginViewLoginViewInteractor.swift
//  turaQshare
//
//  Created by Yesbol Kulanbekov on 06/12/2018.
//  Copyright © 2018 turaQshare. All rights reserved.
//
import Foundation

// TODO: Strongify

class LoginViewInteractor: LoginViewInteractorInput {

    let api = TuraqAPI.shared
    
    let phoneProvider = FirebaseServices.shared.phoneProvider
    let auth          = FirebaseServices.shared.auth
    let ref           = FirebaseServices.shared.ref
    
    let userDefs      = UserDefaultsUserStorageService.shared
    
    weak var output: LoginViewInteractorOutput!

    func login(with loginInfo: User.Login) {
        api.login(loginInfo, onSuccess: { [weak self] (user: User) in
            guard let strongSelf = self else { return }
            guard let output = strongSelf.output else { return }
            
            DispatchQueue.main.async {
                output.didLoginSuccessfully()
            }
            
        }, onFailure: { [weak self] (error) in
            guard let strongSelf = self else { return }
            guard let output = strongSelf.output else { return }
            
            DispatchQueue.main.async {
                output.didFailLogin(with: error)
            }
        })
    }
    
}


extension LoginViewInteractor {

    func verify(phone number: String) {

        
        let onVerificationSuccess = { [weak self] (verificationID: String?, error: Error?) in
            guard let strongSelf = self else { return }
            guard let output = strongSelf.output else { return }
            
            strongSelf.userDefs.setPhoneAuthID(verificationID)
            
            guard let error = error else {
                output.didVerifySuccessfully()
                return
            }
            
            output.verifyDidFail(with: error)
            
        }
        
        
        phoneProvider.verifyPhoneNumber(number,
                                        uiDelegate: nil,
                                        completion: onVerificationSuccess)

    }
    

    
    func signUp(email: String, password: String) {
        auth.createUser(withEmail: email, password: password) { (user, error) in
            //guard let strongSelf = self else { return }
            //guard let output = strongSelf.output else { return }
            
            // TODO: handle create user error
            
            guard let uid = user?.uid else {
                // TODO: handle no user id
                return
            }
            
            
            let usersRef = self.ref.child("users").child(uid)
            let values = ["car_number": "A001KAZ",
                          "name": "Nursultan",
                          "phone": "+77072023773"]
            usersRef.updateChildValues(values, withCompletionBlock: { (error, ref) in
                // TODO: handle user database update  error

                
            })

        }
    }
    
}
