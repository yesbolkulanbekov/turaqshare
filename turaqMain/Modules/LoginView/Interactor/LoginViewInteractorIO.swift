//
//  LoginViewLoginViewInteractorIO.swift
//  turaQshare
//
//  Created by Yesbol Kulanbekov on 06/12/2018.
//  Copyright © 2018 turaQshare. All rights reserved.
//

import Foundation

protocol LoginViewInteractorInput {
    func signUp(email: String, password: String)
    func verify(phone number: String)
    
    func login(with loginInfo: User.Login)

}

protocol LoginViewInteractorOutput: class {
    func didVerifySuccessfully()
    func verifyDidFail(with error: Error)
    
    func didLoginSuccessfully()
    func didFailLogin(with error: Error)
}
