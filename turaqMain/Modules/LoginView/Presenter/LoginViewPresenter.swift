//
//  LoginViewLoginViewPresenter.swift
//  turaQshare
//
//  Created by Yesbol Kulanbekov on 06/12/2018.
//  Copyright © 2018 turaQshare. All rights reserved.
//

class LoginViewPresenter: LoginViewModuleInput {

    weak var view: LoginViewViewInput!
    var interactor: LoginViewInteractorInput!
    var router: LoginViewRouterInput!

}

extension LoginViewPresenter: LoginViewViewOutput {
    
    func viewIsReady() {
        view.setupInitialState()
        
        let loginPossibleAction = LoginInitialView.Props.PhoneLoginAction.possible(signInCommand())
        renderVCProps(with: loginPossibleAction)
    }
    
    func didPressSignIn(with code: String) {
    }
    
    func didPressVerify() {
        //interactor.verify(phone: "+77023028263")
        interactor.signUp(email: "crazyemail@hunter.com", password: "password123")
    }

}

extension LoginViewPresenter: LoginViewInteractorOutput {
    func didLoginSuccessfully() {
        let loginPossibleAction = LoginInitialView.Props.PhoneLoginAction.possible(signInCommand())
        renderVCProps(with: loginPossibleAction)
        router.opentabbarVC(from: view.getVC())
    }
    
    func didFailLogin(with error: Error) {
        let loginPossibleAction = LoginInitialView.Props.PhoneLoginAction.possible(signInCommand())
        renderVCProps(with: loginPossibleAction)
        view.displayAlert(with: error.localizedDescription)
    }
    
    
    /// Firebase
    
    func didVerifySuccessfully() {
        let loginPossibleAction = LoginInitialView.Props.PhoneLoginAction.possible(signInCommand())
        renderVCProps(with: loginPossibleAction)
        router.openVerificationVC(from: view.getVC())
    }
    
    func verifyDidFail(with error: Error) {
        let loginPossibleAction = LoginInitialView.Props.PhoneLoginAction.possible(signInCommand())
        renderVCProps(with: loginPossibleAction)
        view.displayAlert(with: error.localizedDescription)
    }
    
}



/// Private methods

extension LoginViewPresenter {
    func renderVCProps(with action: LoginInitialView.Props.PhoneLoginAction){
        let props = LoginInitialView.Props.init(phoneLoginAction: action,
                                                signInWithEmail: signInWithEmailCommand(),
                                                regWithEmail: regWithEmailCommand())
        
        let vcProps = LoginViewViewController.Props(rootViewProps: props)
        view.render(with: vcProps)
    }
    
    func signInCommand() -> CommandWith<User.Login> {
        let signInCommand = CommandWith { (loginInfo: User.Login) in
            self.interactor.login(with: loginInfo)
            let loginInProgessAction = LoginInitialView.Props.PhoneLoginAction.inProgress
            self.renderVCProps(with: loginInProgessAction)
        }
        return signInCommand
    }
    
    func signInWithEmailCommand() -> Command {
        let command = CommandWith{
            self.router.openEmailReg(from: self.view.getVC())
        }
        return command
    }
    
    func regWithEmailCommand() -> Command {
        let command = CommandWith {
            self.router.openEmailReg(from: self.view.getVC())
        }
        return command
    }
}
