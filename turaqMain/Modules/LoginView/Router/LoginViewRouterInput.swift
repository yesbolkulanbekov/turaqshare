//
//  LoginViewLoginViewRouterInput.swift
//  turaQshare
//
//  Created by Yesbol Kulanbekov on 06/12/2018.
//  Copyright © 2018 turaQshare. All rights reserved.
//

import UIKit

protocol LoginViewRouterInput {
    func openVerificationVC(from vc: UIViewController)
    func openEmailReg(from vc: UIViewController)
    func opentabbarVC(from vc: UIViewController)

}
