//
//  LoginViewLoginViewRouter.swift
//  turaQshare
//
//  Created by Yesbol Kulanbekov on 06/12/2018.
//  Copyright © 2018 turaQshare. All rights reserved.
//
import UIKit

class LoginViewRouter: LoginViewRouterInput {
    func openVerificationVC(from vc: UIViewController) {
        let verificationVC = VerificationModuleConfigurator().assembleModule()
        vc.navigationController?.pushViewController(verificationVC, animated: true)
    }
    
    func openEmailReg(from vc: UIViewController){
        let emailRegVC = EmailRegModuleConfigurator().assembleModule()
        vc.navigationController?.pushViewController(emailRegVC, animated: true)
    }

    func opentabbarVC(from vc: UIViewController) {
        let tabbarVC = TabBarViewController()
        let delegate = UIApplication.shared.delegate as! AppDelegate
        delegate.window?.rootViewController = tabbarVC
    }
}
