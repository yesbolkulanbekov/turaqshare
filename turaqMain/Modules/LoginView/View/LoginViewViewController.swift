//
//  LoginViewLoginViewViewController.swift
//  turaQshare
//
//  Created by Yesbol Kulanbekov on 06/12/2018.
//  Copyright © 2018 turaQshare. All rights reserved.
//

import UIKit

// TODO: Viewcontroller props?

class LoginViewViewController: UIViewController {
    
    struct Props {
        let rootViewProps: LoginInitialView.Props
    }

    var output: LoginViewViewOutput!
    
    let loginInitView = LoginInitialView()
    let spinnerView   = UIActivityIndicatorView(style: .gray)

    // MARK: Life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        output.viewIsReady()
    }
    
    private func setupInitViewState() {
        navigationController?.isNavigationBarHidden = true
        view.backgroundColor = .white
        view.addSubview(loginInitView)
        loginInitView.addConstraints(equalToSafeArea(superView: view))
        view.addSubview(spinnerView)
        spinnerView.addConstraints([
            equal(view, \UIView.safeAreaLayoutGuide.centerXAnchor),
            equal(view, \UIView.safeAreaLayoutGuide.centerYAnchor)
        ])
    }
    
    var isLoginInProgress: Bool = false {
        didSet {
            switch isLoginInProgress {
            case true:
                spinnerView.startAnimating()
            case false:
                spinnerView.stopAnimating()
            }
        }
    }

}

extension LoginViewViewController:  LoginViewViewInput {

    // MARK: LoginViewViewInput
    func setupInitialState() {
        setupInitViewState()
    }
    
    func render(with props: Props) {
        let loginAction = props.rootViewProps.phoneLoginAction
        self.isLoginInProgress = loginAction.isInProgress
        
        loginInitView.render(with: props.rootViewProps)
    }

    
}
    
extension LoginViewViewController {

    private func setNavButtons() {
        let verifyButton = UIBarButtonItem(title: "Verify",
                                           style: .plain,
                                           target: self, action: #selector(pushVerify))
        
        let signInButton = UIBarButtonItem(title: "Sign In",
                                           style: .plain,
                                           target: self, action: #selector(pushSignIn))
        
        navigationItem.rightBarButtonItem = signInButton
        navigationItem.leftBarButtonItem = verifyButton
    }
    
    @objc
    func pushSignIn() {
        output.didPressSignIn(with: "123456")
    }
    
    @objc
    func pushVerify() {
        output.didPressVerify()
    }

}
