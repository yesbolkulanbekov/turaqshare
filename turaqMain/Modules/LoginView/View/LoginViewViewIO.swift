//
//  LoginViewLoginViewViewIO.swift
//  turaQshare
//
//  Created by Yesbol Kulanbekov on 06/12/2018.
//  Copyright © 2018 turaQshare. All rights reserved.
//

protocol LoginViewViewOutput {

    /**
        @author Yesbol Kulanbekov
        Notify presenter that view is ready
    */

    func viewIsReady()
    func didPressSignIn(with code: String)
    func didPressVerify()
}


protocol LoginViewViewInput: class, DisplaysAlert, HasViewController {

    /**
        @author Yesbol Kulanbekov
        Setup initial state of the view
    */

    func setupInitialState()
    func render(with props: LoginViewViewController.Props) 

}
