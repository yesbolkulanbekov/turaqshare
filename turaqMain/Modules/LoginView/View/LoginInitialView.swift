//
//  LoginInitialView.swift
//  turaQshare
//
//  Created by Yesbol Kulanbekov on 12/12/18.
//  Copyright © 2018 Galym Anuarbek. All rights reserved.
//

 import UIKit


class LoginInitialView: UIScrollView {
    
    // TODO: write this to Shared Code
    
    var props: Props = Props() {
        didSet {
            let phLogAction = props.phoneLoginAction
            let isPossible = phLogAction.possible != nil
            
            subViews.signInButton.isEnabled = isPossible
            alpha = phLogAction.isInProgress ? 0.3 : 1
            
        }
    }
    
    struct Subviews {
        let contentView          = UIView()
        let turaqLogo            = UIImageView()
        let backGroundView       = UIImageView()
        let welcomeLabel         = UILabel()
        let phoneTF              = UITextField()
        let passwordTF           = UITextField()
        let signInButton         = UIButton(type: .system)
        let needAccountLabel     = UILabel()
        let sigInEmailButton     = UIButton(type: .system)
        let registerButton       = UIButton(type: .system)
    }
    
    let subViews = LoginInitialView.Subviews()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupSelf()
        configureSubViews()
        styleSubviews()
        renderConstantData()     // TODO: write this to Shared Code
        render(with: Props())
        layout()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
    }
}


/// Props configuration and init

extension LoginInitialView {
    struct Props {
        let phoneLoginAction: PhoneLoginAction
        let signInWithEmail: Command
        let regWithEmail: Command
        
        enum PhoneLoginAction {
            case disabled
            case inProgress
            case possible(CommandWith<User.Login>)
            
            var possible: CommandWith<User.Login>? {
                guard case let .possible(command) = self else { return nil }
                return command
            }
            
            var isDisabled: Bool {
                guard case .disabled = self else { return false }
                return true
            }
            
            var isInProgress: Bool {
                guard case .inProgress = self else { return false }
                return true
            }
        }
    }
}

extension LoginInitialView.Props {
    init() {
        self.phoneLoginAction = .possible(CommandWith<User.Login> { _ in })
        self.signInWithEmail  = CommandWith { }
        self.regWithEmail     = CommandWith { }
    }
}

/// View configuration, content and styling

extension LoginInitialView: UITextFieldDelegate {
    
    private func setupSelf() {
        translatesAutoresizingMaskIntoConstraints = false
        backgroundColor = .white
        
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(dissMissKeyboard))
        addGestureRecognizer(tapGesture)
    }
    
    private func configureSubViews() {
        subViews.signInButton.addTarget(self, action: #selector(signIn), for: .touchUpInside)
        subViews.sigInEmailButton.addTarget(self, action: #selector(signInWithEmail), for: .touchUpInside)
        subViews.registerButton.addTarget(self, action: #selector(regWithEmail), for: .touchUpInside)
        
        subViews.welcomeLabel.adjustsFontSizeToFitWidth = true
        subViews.phoneTF.keyboardType = .phonePad
        
        subViews.passwordTF.delegate = self
        subViews.phoneTF.delegate = self
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        self.contentInset.bottom = 50
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        self.contentInset.bottom = 0
    }
    
    
    func styleSubviews() {
        
        subViews.passwordTF.isSecureTextEntry = true
        
        subViews.welcomeLabel.font       = UIFont.LucidaGrandeBold(ofSize: 30)
        subViews.welcomeLabel.textColor  = UIColor.pine
        
        let leftViewPhone = UIView(frame: CGRect(x: 0, y: 0, width: 15, height: 15))
        leftViewPhone.backgroundColor = .whiteTwo
        
        let leftViewPassword = UIView(frame: CGRect(x: 0, y: 0, width: 15, height: 15))
        leftViewPassword.backgroundColor = .whiteTwo

        subViews.phoneTF.backgroundColor = UIColor.whiteTwo
        subViews.phoneTF.textColor       = UIColor.pine
        subViews.phoneTF.font            = UIFont.NunitoRegular(ofSize: 16)
        subViews.phoneTF.leftView = leftViewPhone
        subViews.phoneTF.leftViewMode = .always
        
        subViews.passwordTF.backgroundColor = UIColor.whiteTwo
        subViews.passwordTF.textColor       = UIColor.pine
        subViews.passwordTF.font            = UIFont.NunitoRegular(ofSize: 16)
        subViews.passwordTF.leftView = leftViewPassword
        subViews.passwordTF.leftViewMode = .always

        subViews.signInButton.setTitleColor(UIColor.denim, for: .normal)
        subViews.signInButton.titleLabel?.font = UIFont.LucidaGrandeBold(ofSize: 16)
        subViews.signInButton.backgroundColor = UIColor.blueGreen.withAlphaComponent(0.08)
        
        subViews.needAccountLabel.font      = UIFont.LucidaGrandeRegular(ofSize: 16)
        subViews.needAccountLabel.textColor = UIColor.pine
        
        subViews.registerButton.setTitleColor(UIColor.white, for: .normal)
        subViews.registerButton.titleLabel?.font = UIFont.LucidaGrandeBold(ofSize: 18)
        subViews.registerButton.backgroundColor = UIColor.denim
        
        subViews.sigInEmailButton.setTitleColor(UIColor.white, for: .normal)
        subViews.sigInEmailButton.titleLabel?.font = UIFont.LucidaGrandeBold(ofSize: 18)
        subViews.sigInEmailButton.backgroundColor = UIColor.denim

    }
    
    func render(with props: LoginInitialView.Props) {
        self.props = props     // TODO: write this to Shared Code
    }
    
    func renderConstantData() {
        subViews.turaqLogo.image            = UIImage(named: "logo")
        subViews.welcomeLabel.text          = "Добро пожаловать!"
        subViews.phoneTF.placeholder        = "+7 (XXX) XXX XX XX"
        
        subViews.phoneTF.text        = "+1234567890"
        // TODO: Format th phone number field
        
        subViews.passwordTF.placeholder = "**********"
        subViews.passwordTF.text = "12345"

        subViews.signInButton.setTitle("Войти", for: .normal)
        
        subViews.needAccountLabel.text = "Нужен аккаунт?"
        subViews.registerButton.setTitle("Зарегистрироваться", for: .normal)
        subViews.sigInEmailButton.setTitle("Войти через Email", for: .normal)

        subViews.backGroundView.image = UIImage(named: "backgroundfig")
    }
    
    func layout() {
        LoginInitialViewLayout(rootView: self).paint()
    }
    
}

extension LoginInitialView {
    @objc
    func signIn() {
        let phoneSignInCommand = props.phoneLoginAction.possible
        let loginInfo = User.Login.init(phone: subViews.phoneTF.text!,
                                        password: subViews.passwordTF.text!)
        phoneSignInCommand?.perform(with: loginInfo)
    }
    
    @objc
    func dissMissKeyboard() {
        subViews.phoneTF.resignFirstResponder()
        subViews.passwordTF.resignFirstResponder()
    }
    
    @objc
    func signInWithEmail() {
        props.signInWithEmail.perform()
    }
    
    @objc
    func regWithEmail() {
        props.regWithEmail.perform()
    }
}


/// View layout

struct LoginInitialViewLayout {
    
    var rootView: LoginInitialView
    var contentView: UIView
    var sv: LoginInitialView.Subviews
    
    init(rootView: LoginInitialView) {
        self.rootView = rootView
        self.sv = rootView.subViews
        self.contentView = sv.contentView
    }
    
    func paint() {
        addSubViews()
        addConstraints()
    }
    
    func addSubViews() {
        rootView.addSubview(sv.contentView)
        contentView.addSubview(sv.backGroundView)
        contentView.addSubview(sv.turaqLogo)
        contentView.addSubview(sv.welcomeLabel)
        contentView.addSubview(entryStack)
        contentView.addSubview(sv.signInButton)
        contentView.addSubview(sv.needAccountLabel)
        
        contentView.addSubview(sv.registerButton)
        
        entryStack.addArrangedSubview(sv.phoneTF)
        entryStack.addArrangedSubview(sv.passwordTF)
    }
    
    func addConstraints() {
        sv.contentView.addConstraints(equalTo(superView: rootView))
        sv.contentView.addConstraints([
            equal(rootView, \.widthAnchor),
            equal(rootView, \.heightAnchor),
        ])
        
        sv.turaqLogo.addConstraints([
            equal(contentView, \UIView.safeAreaLayoutGuide.topAnchor, constant: 30),
            equal(contentView, \UIView.safeAreaLayoutGuide.leadingAnchor, constant: 20)
        ])

        sv.welcomeLabel.addConstraints([
            equal(contentView, \UIView.safeAreaLayoutGuide.leadingAnchor, constant: 20),
            equal(contentView, \UIView.safeAreaLayoutGuide.trailingAnchor, constant: -45),

            equal(sv.turaqLogo,
                  \UIView.safeAreaLayoutGuide.topAnchor,
                  \UIView.safeAreaLayoutGuide.bottomAnchor,
                  constant: 28)
        ])
        
        entryStack.addConstraints([
            equal(contentView, \UIView.safeAreaLayoutGuide.leadingAnchor, constant: 20),
            equal(sv.welcomeLabel,
                  \UIView.safeAreaLayoutGuide.topAnchor,
                  \UIView.safeAreaLayoutGuide.bottomAnchor,
                  constant: 28),
            equal(contentView, \UIView.safeAreaLayoutGuide.trailingAnchor, constant: -20),
        ])
        
        sv.phoneTF.addConstraints([
            equal(\UIView.heightAnchor, to: 46)
        ])
        
        sv.signInButton.addConstraints([
            equal(\UIView.heightAnchor, to: 44),
            equal(contentView, \UIView.safeAreaLayoutGuide.leadingAnchor, constant: 98),
            equal(contentView, \UIView.safeAreaLayoutGuide.trailingAnchor, constant: -98),
            equal(entryStack,
                  \UIView.safeAreaLayoutGuide.topAnchor,
                  \UIView.safeAreaLayoutGuide.bottomAnchor,
                  constant: 20),
        ])
        
        sv.needAccountLabel.addConstraints([
            equal(contentView, \UIView.centerXAnchor)
        ])
        
        sv.registerButton.addConstraints([
            equal(contentView, \UIView.safeAreaLayoutGuide.leadingAnchor),
            equal(contentView, \UIView.safeAreaLayoutGuide.trailingAnchor),
            equal(contentView, \UIView.safeAreaLayoutGuide.bottomAnchor),
            equal(\UIView.heightAnchor, to: 50),
            equal(sv.needAccountLabel,
                  \UIView.safeAreaLayoutGuide.topAnchor,
                  \UIView.safeAreaLayoutGuide.bottomAnchor,
                  constant:20)
            
        ])
        
        sv.backGroundView.addConstraints([
            equal(contentView, \UIView.safeAreaLayoutGuide.topAnchor, constant: 23),
            equal(contentView, \UIView.safeAreaLayoutGuide.trailingAnchor),
        ])
    }
    
 
    let entryStack = UIStackView() { stack in
        stack.axis = .vertical
        stack.distribution = .fillEqually
        stack.alignment = .fill
        stack.spacing = 15
    }
    
}

// TODO: remove safeAreaLayoutGuide from constraints
