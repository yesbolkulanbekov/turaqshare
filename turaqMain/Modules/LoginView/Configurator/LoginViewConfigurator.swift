//
//  LoginViewLoginViewConfigurator.swift
//  turaQshare
//
//  Created by Yesbol Kulanbekov on 06/12/2018.
//  Copyright © 2018 turaQshare. All rights reserved.
//

import UIKit

struct LoginViewModuleConfigurator {

    func assembleModule() -> LoginViewViewController {
        let viewController = LoginViewViewController()
        configure(viewController: viewController)
        return viewController
    }

    private func configure(viewController: LoginViewViewController) {

        let router = LoginViewRouter()

        let presenter = LoginViewPresenter()
        presenter.view = viewController
        presenter.router = router

        let interactor = LoginViewInteractor()
        interactor.output = presenter

        presenter.interactor = interactor
        viewController.output = presenter
    }

}
