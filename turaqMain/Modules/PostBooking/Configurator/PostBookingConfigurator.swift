//
//  PostBookingPostBookingConfigurator.swift
//  turaQshare
//
//  Created by Yesbol Kulanbekov on 10/03/2019.
//  Copyright © 2019 turaQshare. All rights reserved.
//

import UIKit

// TODO: create navController property in Router and set it with assembleModule(with nav: UINavigationController)

struct PostBookingModuleConfigurator {

    func assembleModule() -> PostBookingViewController {
        let viewController = PostBookingViewController()
        configure(viewController: viewController)
        return viewController
    }

    func assembleModule(with storyBoardName: String) -> PostBookingViewController {
        let storyboard = UIStoryboard(name: storyBoardName, bundle: .main)
        let viewController = storyboard.instantiateInitialViewController() as! PostBookingViewController
        configure(viewController: viewController)
        return viewController
    }

    private func configure(viewController: PostBookingViewController) {

        let router = PostBookingRouter()

        let presenter = PostBookingPresenter()
        presenter.view = viewController
        presenter.router = router

        let interactor = PostBookingInteractor()
        interactor.output = presenter

        presenter.interactor = interactor
        viewController.output = presenter
    }

}
