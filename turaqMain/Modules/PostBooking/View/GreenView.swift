//
//  GreenView.swift
//  turaQshare
//
//  Created by Yesbol Kulanbekov on 4/2/19.
//  Copyright © 2019 turaQshare. All rights reserved.
//
import UIKit

class GreenView: UIView {
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupSelf()
        configureSubViews()
        styleSubviews()
        sv.render(with: Props.zero)
        sv.renderConstantData()
        GreenViewLayout(for: self).paint()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
    }
    
    /// Properties
    
    let sv = Subviews()
    
    var props = Props.zero {
        didSet {
            sv.render(with: props)
        }
    }
    
    func setTitle(_ title: String) {
        sv.titleLbl.text = "  \(title)  "
    }
    
    var data: String {
        return sv.dataTextF.text!
    }
    
    var rect1GestRecognizer: UITapGestureRecognizer!

}

/// View configuration, content and styling

extension GreenView {
    
    struct Props {
        
        let data: String
        let select: CommandWith<String?>
        
        static let zero = Props(
            data: "",
            select: CommandWith<String?>.nop
        )
    }
    
    struct Subviews {
        
        let rootRect     = UIView()
        let titleLbl     = UILabel()
        var dataTextF = UITextField()
        
        
        func render(with props: Props) {
            dataTextF.text  = props.data
        }
        
        func renderConstantData() {
            dataTextF.placeholder = "Номер"
            titleLbl.text = "  Начало  "

        }
    }
    
    func styleSubviews() {
        style(sv.rootRect, sv.titleLbl, sv.dataTextF)
    }
    
    private func style(_ rect: UIView, _ lbl: UILabel, _ dataTextView: UITextField) {
        rect.backgroundColor = .whiteTwo
        lbl.backgroundColor = UIColor.blueGreen
        lbl.textColor = .white
        dataTextView.font = UIFont.NunitoLight(ofSize: 12)
        dataTextView.textColor = .pine
    }
    
    private func configureSubViews() {
        self.rect1GestRecognizer = UITapGestureRecognizer(target: self, action: #selector(selectStartDate))
        sv.rootRect.addGestureRecognizer(rect1GestRecognizer)
    }
    
    @objc
    func selectStartDate() {
        print("Select start date pressed")
        props.select.perform(with: sv.dataTextF.text)
    }
    
    private func setupSelf() {
        translatesAutoresizingMaskIntoConstraints = false
    }
}



/// View layout

struct GreenViewLayout {
    
    var rootView: GreenView
    var sv: GreenView.Subviews
    
    init(for rootView: GreenView) {
        self.rootView = rootView
        self.sv = rootView.sv
    }
    
    func paint() {
        addSubViews()
        addConstraints()
    }
    
    func addSubViews() {
        rootView.addSubview(sv.rootRect)
        sv.rootRect.addSubview(sv.titleLbl)
        sv.rootRect.addSubview(sv.dataTextF)
    }
    
    func addConstraints() {
        sv.rootRect.addConstraints(equalTo(superView: rootView))
        sv.rootRect.addConstraints([
            equal(\.heightAnchor, to: 60)
        ])
        
        sv.titleLbl.addConstraints([
            equal(sv.rootRect, \.leadingAnchor, constant: 10),
            equal(sv.rootRect, \.centerYAnchor, \.topAnchor, constant: 0)
        ])
        
        sv.dataTextF.addConstraints([
            equal(sv.rootRect, \.leadingAnchor, constant: 15),
            equal(sv.rootRect, \.bottomAnchor, constant: -15),
        ])
    }
    
    let rootStack = UIStackView() { stack in
        stack.axis = .vertical
        stack.distribution = .fill
        stack.alignment = .fill
        stack.spacing = 8
    }
    
}



