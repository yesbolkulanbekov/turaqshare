//
//  CalBookingView.swift
//  turaQshare
//
//  Created by Yesbol Kulanbekov on 3/31/19.
//  Copyright © 2019 turaQshare. All rights reserved.
//

import UIKit
import FSCalendar
import RangeSeekSlider


class CalBookingView: UIScrollView {
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupSelf()
        configureSubViews()
        styleSubviews()
        sv.render(with: Props.zero)
        sv.renderConstantData()
        CalBookingViewLayout(for: self).paint()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
    }
    
    /// Properties
    
    let sv = Subviews()
    
    var props = Props.zero {
        didSet {
            sv.render(with: props)
        }
    }
    
    func renderOnce(with props: Props) {
        let minStartDate = props.scheds.map { $0.startD.secondsInADay }.min()
        let maxEndDate = props.scheds.map { $0.endD.secondsInADay }.max()
        
        sv.rangeSlider.minValue = CGFloat(minStartDate ?? 0)
        sv.rangeSlider.maxValue = CGFloat(maxEndDate ?? 0)
        sv.rangeSlider.selectedMinValue = CGFloat(minStartDate ?? 0)
        sv.rangeSlider.selectedMaxValue = CGFloat(maxEndDate ?? 0)
    }
    
    func renderFreqAndCarValues(with props: Props) {
        sv.freqView.props = props.freqProps
        let freq = Freq(rawValue: props.freqProps.data)!
        
        switch freq {
        case .day: sv.calendar.allowsMultipleSelection = true
        case .weekly: sv.calendar.allowsMultipleSelection = true
        case .once: sv.calendar.allowsMultipleSelection = false
        }
    }
    
    func deselectAll() {
        sv.calendar.selectedDates.forEach { sv.calendar.deselect($0) }
    }
    
    var onSlide = CommandWith<(Int,Int)>.nop
    
    private var selectedStartDate: Date!
    private var selectedEndDate: Date!
}

/// View configuration, content and styling

extension CalBookingView {
    struct Sched {
        let startD: Date
        let endD: Date
        let scheduleModel: Schedule
    }
    
    struct Props {
        
        let scheds: [Sched]
        let onChoosingSchedsFromCalendar: CommandWith<[Schedule]>
        let freqProps: GreenView.Props
        let carProps: GreenView.Props
        let onConfirm: CommandWith<String>
    
        func scheds(for date: Date) -> [Sched] {
            let schedsOnThisDate = self.scheds.filter { sched in
                var gregCalendar = Calendar(identifier: Calendar.Identifier.gregorian)
                gregCalendar.timeZone = TimeZone(secondsFromGMT: 0)!
                let hasEventOnDate =  gregCalendar.isDate(sched.startD, inSameDayAs: date)
                return hasEventOnDate
            }
            return schedsOnThisDate
        }
        
        func countScheds(for date: Date) -> Int {
            return scheds(for: date).count
        }
        
        static let zero = Props(
            scheds: [Sched](),
            onChoosingSchedsFromCalendar: CommandWith<[Schedule]>.nop,
            freqProps: .zero,
            carProps: .zero,
            onConfirm: CommandWith<String>.nop
        )
    }
    
    struct Subviews {
        
        let calendar = FSCalendar()
        let rangeSlider = RangeSeekSlider()
        let freqView = GreenView()
        let carNumber = GreenView()
        
        let confirmBtn = UIButton(type: .system)

        
        func render(with props: Props) {
            calendar.reloadData()
        }
        
        func renderConstantData() {
            freqView.setTitle("Частота")
            carNumber.setTitle("Номер машины")
            confirmBtn.setTitle("Подтвердить", for: .normal)

        }
        
    }
    
    func styleSubviews() {
        sv.rangeSlider.handleColor = UIColor.blueGreen
        sv.rangeSlider.tintColor = UIColor.blueGreen
        
        sv.confirmBtn.setTitleColor(UIColor.blueGreen, for: .normal)
        sv.confirmBtn.titleLabel?.font = UIFont.LucidaGrandeBold(ofSize: 18)
        sv.confirmBtn.backgroundColor = UIColor.blueGreen.withAlphaComponent(0.08)
    }
    
    private func configureSubViews() {
        sv.calendar.locale = Locale(identifier: "ru_RU")
        sv.calendar.allowsMultipleSelection = false
        sv.calendar.swipeToChooseGesture.isEnabled = false
        sv.calendar.isMultipleTouchEnabled = true
        
        
        sv.rangeSlider.delegate = self
        sv.freqView.sv.dataTextF.isUserInteractionEnabled = false
        sv.carNumber.rect1GestRecognizer.isEnabled = false
        
        sv.confirmBtn.addTarget(self, action: #selector(confirm), for: .touchUpInside)
    }
    
    @objc
    func confirm() {
        props.onConfirm.perform(with: sv.carNumber.data)
    }
    
    private func setupSelf() {
        sv.calendar.dataSource = self
        sv.calendar.delegate   = self
        self.translatesAutoresizingMaskIntoConstraints = false
        self.alwaysBounceVertical = true
    }
}

extension CalBookingView: FSCalendarDataSource, FSCalendarDelegate {
    
    func minimumDate(for calendar: FSCalendar) -> Date {
        guard !props.scheds.isEmpty else { return Date() }
        let minDate = props.scheds.first!.startD
        return minDate.toGlobalTime()
    }
    
    func calendar(_ calendar: FSCalendar, subtitleFor date: Date) -> String? {
        guard !props.scheds.isEmpty else { return nil }
        let count = props.countScheds(for:date.toLocalTime())
        return (count == 0) ? nil : "Свб."
    }
    
    func calendar(_ calendar: FSCalendar, numberOfEventsFor date: Date) -> Int {
        guard !props.scheds.isEmpty else { return 0 }
        return props.countScheds(for:date.toLocalTime())
    }
    
    func calendarCurrentPageDidChange(_ calendar: FSCalendar) {
        calendar.reloadData()
    }
    
    func calendar(_ calendar: FSCalendar, shouldSelect date: Date, at monthPosition: FSCalendarMonthPosition) -> Bool {
        guard !props.scheds.isEmpty else { return false }
        let count = props.countScheds(for:date.toLocalTime())
        return (count == 0) ? false : true
    }
    
    func calendar(_ calendar: FSCalendar, didSelect date: Date, at monthPosition: FSCalendarMonthPosition) {

        let selectedDatesOrdered = calendar.selectedDates.map{ $0.toLocalTime() }.sorted()
        self.selectedStartDate = selectedDatesOrdered.first!
        self.selectedEndDate = selectedDatesOrdered.last!

        updateSelection()
        
        filterScheds()
    }
    
    func calendar(_ calendar: FSCalendar, didDeselect date: Date, at monthPosition: FSCalendarMonthPosition) {
        
        let selectedDatesOrdered = calendar.selectedDates.map{ $0.toLocalTime() }.sorted()
        guard let middle = selectedDatesOrdered.middle().last else { return }
        
        if date >= middle {
            self.selectedStartDate = date
        } else {
            self.selectedEndDate = date
        }

        updateSelection()
        
        filterScheds()
    }
    
    
    private func updateSelection() {
        sv.calendar.selectedDates.map{ $0.toLocalTime() }.forEach { sv.calendar.deselect($0) }
        let dates = Date.dates(from: self.selectedStartDate, to: self.selectedEndDate)
        dates.forEach { sv.calendar.select($0)}
    }
    
    private func filterScheds() {
        let selectedDates = sv.calendar.selectedDates.map{ $0.toLocalTime() }.sorted()
        let scheduleModels = selectedDates.flatMap { props.scheds(for: $0).map { $0.scheduleModel } }
        props.onChoosingSchedsFromCalendar.perform(with: scheduleModels)
    }
    
}

extension CalBookingView: RangeSeekSliderDelegate {
    func rangeSeekSlider(_ slider: RangeSeekSlider, didChange minValue: CGFloat, maxValue: CGFloat) {
//        print("Standard slider updated. Min Value: \(minValue) Max Value: \(maxValue)")
        onSlide.perform(with: (Int(minValue),Int(maxValue)))
    }
    
    func rangeSeekSlider(_ slider: RangeSeekSlider, stringForMinValue minValue: CGFloat) -> String? {
        let hms = secondsToHoursMinutesSeconds(seconds: Int(minValue))
        return hms
    }

    func rangeSeekSlider(_ slider: RangeSeekSlider, stringForMaxValue maxValue: CGFloat) -> String? {
        let hms = secondsToHoursMinutesSeconds(seconds: Int(maxValue))
        return hms
    }

    func secondsToHoursMinutesSeconds (seconds : Int) -> String { //}(hour: Int, min: Int, Int) {
        let interval = seconds
        let formatter = DateComponentsFormatter()
        formatter.allowedUnits = [.hour, .minute]
        formatter.unitsStyle = .brief
        formatter.calendar?.locale = Locale(identifier: "ru_RU")
        let formattedString = formatter.string(from: TimeInterval(interval))!
        return formattedString
    }
}



/// View layout

struct CalBookingViewLayout {
    
    var rootView: CalBookingView
    var sv: CalBookingView.Subviews
    
    init(for rootView: CalBookingView) {
        self.rootView = rootView
        self.sv = rootView.sv
    }
    
    func paint() {
        addSubViews()
        addConstraints()
    }
    
    func addSubViews() {
        rootView.addSubview(rootStackView)
        rootStackView.addArrangedSubview(sv.rangeSlider)
        rootStackView.addArrangedSubview(sv.calendar)
        rootStackView.addArrangedSubview(sv.freqView)
        rootStackView.addArrangedSubview(sv.carNumber)
        buttonPadStackView.addArrangedSubview(sv.confirmBtn)
        rootStackView.addArrangedSubview(buttonPadStackView)
    }
    
    func addConstraints() {
        rootStackView.addConstraints(equalTo(superView: rootView))
        rootStackView.addCons(equal(rootView, Dims.width))
        
        
        sv.calendar.addCons(
            equal(Dims.height, to: 300)
        )
        
        sv.confirmBtn.addConstraints([
            equal(\.heightAnchor, to: 44)
        ])

    }
    
    let rootStackView = UIStackView() { stack in
        stack.axis = .vertical
        stack.distribution = .fill
        stack.alignment = .fill
        stack.spacing = 16
        stack.isLayoutMarginsRelativeArrangement = true
        stack.layoutMargins = UIEdgeInsets(top: 8, left: 8, bottom: 8, right: 8)
    }
    
    let buttonPadStackView = UIStackView() { stack in
        stack.axis = .vertical
        stack.distribution = .fill
        stack.alignment = .fill
        stack.isLayoutMarginsRelativeArrangement = true
        stack.layoutMargins = UIEdgeInsets(top: 8, left: 90, bottom: 8, right: 90)
    }
    
}




