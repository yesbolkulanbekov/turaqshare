//
//  PostBookingPostBookingViewController.swift
//  turaQshare
//
//  Created by Yesbol Kulanbekov on 10/03/2019.
//  Copyright © 2019 turaQshare. All rights reserved.
//



import UIKit

// MARK: View input protocol

protocol PostBookingViewInput: class, DisplaysAlert, HasViewController {
    func setupInitialState()
    
    func showWeekTable(with weekDays: [WeekDay], _ onWeekPick: OnWeekPick)
    func showPickerView(with onPick: OnPick, currentFreq: Freq)
    func presentDatePicker(date: Date, minDate: Date, _ datePicked: CommandWith<Date>)
    
    var props: PostBookingViewController.Props { get set }
    var onSlide: CommandWith<(Int,Int)> { get set }
    func renderOnce(with props: PostBookingViewController.Props)
    func renderFreqAndCarValues(with props: PostBookingViewController.Props)
    
    func popVC()
    func deselectAll() 

}

// MARK: View controller that implements viewinput protocol

class PostBookingViewController: UIViewController {
    
    var props = Props.zero {
        didSet {
            bookingView.props = props.bookVProps
        }
    }
    
    var onSlide = CommandWith<(Int, Int)>.nop {
        didSet {
            bookingView.onSlide = onSlide
        }
    }

    var output: PostBookingViewOutput!
    let spinnerView = UIActivityIndicatorView(style: .gray)
    let bookingView = CalBookingView()
    
    
    // MARK: Life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        output.viewIsReady()
    }

}

extension PostBookingViewController {
    struct Props {
        var bookVProps: CalBookingView.Props
        
        static var zero = Props(bookVProps: .zero)
    }
}

extension PostBookingViewController:  PostBookingViewInput {

    // MARK: PostBookingViewInput
    func setupInitialState() {
        navigationController?.interactivePopGestureRecognizer?.isEnabled = false
        view.backgroundColor = .white
        navigationItem.title = "Новое бронирование"
        view.addSubview(bookingView)
        bookingView.addConstraints(equalToSafeArea(superView: view))
        view.addSubview(spinnerView)
        spinnerView.addConstraints([
            equal(view, \UIView.safeAreaLayoutGuide.centerXAnchor),
            equal(view, \UIView.safeAreaLayoutGuide.centerYAnchor)
        ])
    }
    
    func renderOnce(with props: PostBookingViewController.Props) {
        bookingView.renderOnce(with: props.bookVProps)
    }
    
    func renderFreqAndCarValues(with props: PostBookingViewController.Props) {
        bookingView.renderFreqAndCarValues(with: props.bookVProps)
    }
    
    func popVC() {
        self.navigationController?.popViewController(animated: true)
    }

    func deselectAll() {
        self.bookingView.deselectAll()
    }
}

extension PostBookingViewController {
    
    func presentDatePicker(date: Date, minDate: Date, _ datePicked: CommandWith<Date>) {
        let datePickerController = MonsterDatePicker()
        datePickerController.selectedDate = date
        datePickerController.minDate = minDate
        
        let onDatePicked = CommandWith<Date> { date in
            datePicked.perform(with: date)
        }
        
        datePickerController.datePicked = onDatePicked
        
        datePickerController.modalTransitionStyle = .crossDissolve
        datePickerController.modalPresentationStyle = .overCurrentContext
        self.present(datePickerController, animated: true, completion: nil)
    }
    
    func showWeekTable(with weekDays: [WeekDay], _ onWeekPick: OnWeekPick) {
        let weekVC = WeekController()
        weekVC.selectedDays = weekDays
        weekVC.onWeekPick   = onWeekPick
        self.navigationController?.pushViewController(weekVC, animated: true)
    }
    
    func showPickerView(with onPick: OnPick, currentFreq: Freq) {
        let picker = FreqPicker()
        picker.onPick = onPick
        picker.choices = [Freq.once, Freq.day]
        picker.selectedFreq = currentFreq
        picker.modalTransitionStyle = .crossDissolve
        picker.modalPresentationStyle = .overCurrentContext
        self.present(picker, animated: true, completion: nil)
    }
    
}
