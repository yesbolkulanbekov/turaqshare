//
//  PostBookingPostBookingPresenter.swift
//  turaQshare
//
//  Created by Yesbol Kulanbekov on 10/03/2019.
//  Copyright © 2019 turaQshare. All rights reserved.
//
import Foundation

class PostBookingPresenter: PostBookingModuleInput {

    weak var view: PostBookingViewInput!
    var interactor: PostBookingInteractorInput!
    var router: PostBookingRouterInput!
    
    
    var parkingPlace: ParkingPlace!
    var freetime: FreeTime!
    

//    var weekDays = [WeekDay.mon]

    var freq: Freq!
    
    var freeSchedules = [Schedule]()
    var chosenScheds  = [Schedule]()
    
    var sliderMinSec: Int!
    var sliderMaxSec: Int!
}

extension PostBookingPresenter: PostBookingViewOutput {
    func initView(with parkingPlace: ParkingPlace) {
        self.parkingPlace = parkingPlace
    }
    
    func initView(with freetime: FreeTime) {
        self.freetime = freetime
        let tempFreq = Freq.instance(from: freetime.frequency)!
        if tempFreq == Freq.weekly {
            self.freq = Freq.day
        } else {
            self.freq = tempFreq
        }
    }
    
    func viewIsReady() {
        view.setupInitialState()
        view.renderFreqAndCarValues(with: vcProps(with: []))
        view.renderOnce(with: vcProps(with: freeSchedules))
        interactor.getPublicSchedules(for: self.freetime)
        
        let onSlide = CommandWith<(Int,Int)> { (minSec,maxSec) in
            self.sliderMinSec = minSec
            self.sliderMaxSec = maxSec
            let partialResult = self.freeSchedules.filter { return $0.endTime.secondsInADay >= maxSec }
            let finalResult   = partialResult.filter { $0.startTime.secondsInADay <= minSec }
            self.view.props = self.vcProps(with: finalResult)
        }
        
        view.onSlide = onSlide
    }
    
    private func postBooking(with carNumber: String) {
        
        var weekDays: [String]? = nil

        if self.freq.isWeekly {
            //weekDays = self.weekDays.map { $0.description }
        }
        guard !chosenScheds.isEmpty else {
            view.displayAlert(with: "Пожалуйста, выберите даты")
            return
        }
        let sortedChosen = chosenScheds.sorted {$0.startTime < $1.startTime}
        let startTime = sortedChosen.first!.startTime!
        
        let finalStartTime = startTime.setTime(comps: sliderMinSec.dateComps)
        let finalEndTime   = startTime.setTime(comps: (sliderMaxSec).dateComps)
        
        let sortedChosenByEnd = chosenScheds.sorted {$0.endTime < $1.endTime}
        let endRepDate = sortedChosenByEnd.last!.endTime
        
        let booking = Booking(
            uuid: nil,
            customer: nil,
            owner: nil,
            parkingPlaceNumber: freetime.parkingPlaceNumber!,
            parkingZone: freetime.parkingZone!,
            startTime: finalStartTime?.iso8601,
            endTime: finalEndTime?.iso8601,
            status: nil,
            frequency: self.freq.intForm,
            repeatStartTime: finalStartTime?.iso8601,
            repeatEndTime: endRepDate?.iso8601,
            weekDays: weekDays,
            carNumber: carNumber
        )

        self.interactor.post(booking)
    }
}

extension PostBookingPresenter: PostBookingInteractorOutput {
    
    func didLoad(_ freeSchedules: [Schedule]) {
        if freeSchedules.isEmpty {
            view.disPlayAlert(with: "Нет мест.") {
                self.view.popVC()
            }
        }
        view.renderOnce(with: vcProps(with: freeSchedules))
        self.freeSchedules = freeSchedules
        guard let minStartDate = (self.freeSchedules.map { $0.startTime.secondsInADay }.min()) else { return }
        guard let maxEndDate = (self.freeSchedules.map { $0.endTime.secondsInADay }.max()) else { return }
        self.sliderMinSec = minStartDate
        self.sliderMaxSec = maxEndDate
        let partialResult = self.freeSchedules.filter { return $0.endTime.secondsInADay >= maxEndDate }
        let finalResult   = partialResult.filter { $0.startTime.secondsInADay <= minStartDate }
        view.props = vcProps(with: finalResult)
    }
    
    func didFailLoadSchedules(with error: TuraqError) {
        view.displayAlert(with: error.localizedDescription)
    }
    
    func didPostSuccessfully() {
        view.disPlayAlert(with: "Забронировано!") {
            self.view.popVC()
        }
    }
    
    func didPostFail(with error: TuraqError) {
        view.displayAlert(with: error.localizedDescription)
    }

}


extension PostBookingPresenter {
    
    func vcProps(with schedules: [Schedule]) ->  PostBookingViewController.Props {
        
        let scheds: [CalBookingView.Sched] = schedules.map { schedule in
            let sched = CalBookingView.Sched(
                startD: schedule.startTime,
                endD: schedule.endTime,
                scheduleModel: schedule
            )
            return sched
        }
        
        let onChoosingScheds = CommandWith<[Schedule]> { chosenScheds in
            print("Chosen Ones", chosenScheds)
            self.chosenScheds = chosenScheds
        }
        
        let onPick = OnPick { freq in
            self.view.deselectAll()
            self.freq = freq
            self.view.renderFreqAndCarValues(with: self.vcProps(with: []))
        }
        
        
        let freqProps = GreenView.Props(
            data: self.freq.rawValue,
            select: CommandWith<String?> { _ in
                self.view.showPickerView(with: onPick, currentFreq: self.freq)
            }
        )
        
        let carProps = GreenView.Props(
            data: "",
            select: CommandWith<String?>.nop
        )
        
        let onConfirm = CommandWith<String> { carNumber in
            guard !(carNumber.isEmpty) else {
                self.view.displayAlert(with: "Нужно заполнить номер авто!")
                return
            }
            
            self.postBooking(with: carNumber)
        }
        
        let bookVProps = CalBookingView.Props(
            scheds:scheds,
            onChoosingSchedsFromCalendar: onChoosingScheds,
            freqProps: freqProps,
            carProps: carProps,
            onConfirm: onConfirm
        )
 
        let vcProps = PostBookingViewController.Props(bookVProps: bookVProps)
        
        return vcProps
    }

}



// MARK: ViewController output protocol

protocol PostBookingViewOutput {
    func viewIsReady()
    func initView(with parkingPlace: ParkingPlace)
    func initView(with freetime: FreeTime)
}
