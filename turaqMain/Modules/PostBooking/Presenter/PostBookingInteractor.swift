//
//  PostBookingPostBookingInteractor.swift
//  turaQshare
//
//  Created by Yesbol Kulanbekov on 10/03/2019.
//  Copyright © 2019 turaQshare. All rights reserved.
//
import Foundation

protocol PostBookingInteractorInput {
    func post(_ booking: Booking)
    func getPublicSchedules(for freetime: FreeTime!)

}

protocol PostBookingInteractorOutput: class {
    func didPostSuccessfully()
    func didPostFail(with error: TuraqError)
    
    func didLoad(_ freeSchedules: [Schedule])
    func didFailLoadSchedules(with error: TuraqError)
}

// MARK: Implementation

class PostBookingInteractor: PostBookingInteractorInput {

    weak var output: PostBookingInteractorOutput!

    func post(_ booking: Booking) {
        
        let onReturn = { (result: PostBookingResult) in
            
            switch result {
            case let .success(value) :
                print("Post booking result is :", value)
                
                DispatchQueue.main.async {
                    self.output.didPostSuccessfully()
                }
            case let .failure(error):
                DispatchQueue.main.async {
                    self.output.didPostFail(with: error)
                }
            }
        }
        
        let postBookingRequest = PostBookingReq(booking: booking, onReturn: onReturn)
        postBookingRequest.dispatch()
        
    }
    
    
    func getPublicSchedules(for freetime: FreeTime!) {
        let onReturn: (GetPublicSchedulesResult) -> Void = { result in
            switch result {
            case let .success(schedules) :
                
                DispatchQueue.main.async {
                    let freeSchedules = schedules.filter { $0.status == BookingStatus.free}
                    let sortedFreeSchedules = freeSchedules.sorted(by: { $0.startTime < $1.startTime })
                    self.output.didLoad(sortedFreeSchedules)
                }
                
            case let .failure(error):
                
                DispatchQueue.main.async {
                    self.output.didFailLoadSchedules(with: error)
                }
                
            }
        }
        
        let schedReq = GetPublicSchedules(
            onReturn: onReturn,
            freetimeID: freetime.uuid
        )
        
        schedReq.dispatch()
    }
}
