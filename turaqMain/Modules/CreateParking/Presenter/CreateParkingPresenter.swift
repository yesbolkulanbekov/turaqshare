//
//  CreateParkingCreateParkingPresenter.swift
//  turaQshare
//
//  Created by Yesbol Kulanbekov on 25/12/2018.
//  Copyright © 2018 turaQshare. All rights reserved.
//
import Foundation
class CreateParkingPresenter: CreateParkingModuleInput {

    weak var view: CreateParkingViewInput!
    var interactor: CreateParkingInteractorInput!
    var router: CreateParkingRouterInput!

    var pickedDocumentURL: URL?
}

extension CreateParkingPresenter: CreateParkingViewOutput {
    func viewIsReady() {
        view.setupInitialState()
        view.render(with: placeCreationPossibleVCProps())
    }
}

extension CreateParkingPresenter: CreateParkingInteractorOutput {
    func didSucceedCreate(with message: String) {
        view.render(with: placeCreationPossibleVCProps())
        view.disPlayAlert(with: message, action: {
            self.router.pop(the: self.view.getVC())
        })
    }
    
    func didCreateFail(with error: TuraqError) {
        view.displayAlert(with: error.localizedDescription)
    }
    

}


extension CreateParkingPresenter {
    func placeCreationPossibleVCProps() -> CreateParkingViewController.Props {
        let command = CommandWith<ParkingPlace> { parkingPlace in
            guard let chosenFileURL = self.pickedDocumentURL else {
                self.view.displayAlert(with: "Выберите документ, пожалуйста.")
                return
            }
            self.interactor.createParkingPlace(placeNum: parkingPlace.placeNumber,
                                               parkingZone: 1,
                                               chosenFile: chosenFileURL)
            self.view.render(with: self.placeCreationInProgressVCProps())
        }
        
        let showPicker = Command {
            self.router.showPicker(from: self.view.getVC())
        }
        
        let chooseDoc = CommandWith<URL> { localURL in
            self.pickedDocumentURL = localURL
        }
        
        let showInfo = Command {
            self.view.displayAlert(with: "Подтверждение вашего места занимает от 3 до 6 часов.")
        }
        
        let action = CreateParkingView.Props.CreatePropsAction.possible(command)
        let parkingProps = CreateParkingView.Props(createPostAction: action, showPicker: showPicker, showInfo: showInfo)
        let vcProps = CreateParkingViewController.Props(rootProps: parkingProps, chooseDoc: chooseDoc)
        return vcProps
    }
    
    func placeCreationInProgressVCProps() -> CreateParkingViewController.Props {
        let action = CreateParkingView.Props.CreatePropsAction.inProgress
        let parkingProps = CreateParkingView.Props(createPostAction: action, showPicker: Command.nop, showInfo: Command.nop)
        let vcProps = CreateParkingViewController.Props(rootProps: parkingProps, chooseDoc: CommandWith<URL>{ _ in })
        return vcProps
    }
}
