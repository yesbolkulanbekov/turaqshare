//
//  CreateParkingCreateParkingConfigurator.swift
//  turaQshare
//
//  Created by Yesbol Kulanbekov on 25/12/2018.
//  Copyright © 2018 turaQshare. All rights reserved.
//

import UIKit

struct CreateParkingModuleConfigurator {

    func assembleModule() -> CreateParkingViewController {
        let viewController = CreateParkingViewController()
        configure(viewController: viewController)
        return viewController
    }

    private func configure(viewController: CreateParkingViewController) {

        let router = CreateParkingRouter()

        let presenter = CreateParkingPresenter()
        presenter.view = viewController
        presenter.router = router

        let interactor = CreateParkingInteractor()
        interactor.output = presenter

        presenter.interactor = interactor
        viewController.output = presenter
    }

}
