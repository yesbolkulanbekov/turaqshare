//
//  CreateParkingCreateParkingRouterInput.swift
//  turaQshare
//
//  Created by Yesbol Kulanbekov on 25/12/2018.
//  Copyright © 2018 turaQshare. All rights reserved.
//

import UIKit

protocol CreateParkingRouterInput {
    func pop(the vc: UIViewController)
    func showPicker(from vc: UIViewController)

}
