//
//  CreateParkingCreateParkingRouter.swift
//  turaQshare
//
//  Created by Yesbol Kulanbekov on 25/12/2018.
//  Copyright © 2018 turaQshare. All rights reserved.
//

import UIKit
import MobileCoreServices

class CreateParkingRouter: CreateParkingRouterInput {
    func pop(the vc: UIViewController) {
        vc.navigationController?.popViewController(animated: true)
    }
    
    func showPicker(from vc: UIViewController) {
        let docType = [String(kUTTypePDF)]
        let docPicker = UIDocumentPickerViewController(documentTypes: docType, in: UIDocumentPickerMode.import)
        docPicker.delegate = vc as? UIDocumentPickerDelegate
        docPicker.modalPresentationStyle = .fullScreen
        vc.present(docPicker, animated: true, completion: nil)
    }
}
