//
//  CreateParkingCreateParkingInteractorIO.swift
//  turaQshare
//
//  Created by Yesbol Kulanbekov on 25/12/2018.
//  Copyright © 2018 turaQshare. All rights reserved.
//

import Foundation

protocol CreateParkingInteractorInput {
    func createParkingPlace(placeNum: String, parkingZone: Int, chosenFile: URL)

}

protocol CreateParkingInteractorOutput: class {
    func didSucceedCreate(with message: String)
    func didCreateFail(with error: TuraqError)
}
