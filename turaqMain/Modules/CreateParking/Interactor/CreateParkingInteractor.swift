//
//  CreateParkingCreateParkingInteractor.swift
//  turaQshare
//
//  Created by Yesbol Kulanbekov on 25/12/2018.
//  Copyright © 2018 turaQshare. All rights reserved.
//
import Foundation

class CreateParkingInteractor: CreateParkingInteractorInput {

    weak var output: CreateParkingInteractorOutput!
    
    func createParkingPlace(placeNum: String, parkingZone: Int, chosenFile: URL) {
        let postPlaces = PostPlacesRequest(placeNumber: placeNum, parkingZone: 1, onReturn: { [weak self] result in
            guard let self = self else { return }
            guard let output = self.output else { return }
            
            switch result {
            case .success(let message):
                

                
                let postDocRequest = PostDocRequest(placeNumber: placeNum, parkingZone: 1, fileURL: chosenFile) { (result) in
                    
                    DispatchQueue.main.async {
                        output.didSucceedCreate(with: message)
                    }
                }
                
                postDocRequest.dispatch()

                
            case .failure(let error):
                DispatchQueue.main.async {
                    output.didCreateFail(with: error)
                }
            }
        })
        postPlaces.dispatch()
        
    }

}
