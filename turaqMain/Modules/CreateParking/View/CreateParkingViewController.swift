//
//  CreateParkingCreateParkingViewController.swift
//  turaQshare
//
//  Created by Yesbol Kulanbekov on 25/12/2018.
//  Copyright © 2018 turaQshare. All rights reserved.
//

import UIKit
import MobileCoreServices

class CreateParkingViewController: UIViewController {

    struct Props {
        let rootProps: CreateParkingView.Props
        let chooseDoc: CommandWith<URL>
    }
    
    var props: Props! {
        didSet {
            createParkingView.render(with: props.rootProps)
            isActionInProgress = props.rootProps.createPostAction.isInProgress
        }
    }
    
    var output: CreateParkingViewOutput!
    
    let spinnerView        = UIActivityIndicatorView(style: .gray)
    let createParkingView  = CreateParkingView()

    // MARK: Life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        output.viewIsReady()
    }
    var isActionInProgress: Bool = false {
        didSet {
            switch isActionInProgress {
            case true:
                spinnerView.startAnimating()
            case false:
                spinnerView.stopAnimating()
            }
        }
    }
}

extension CreateParkingViewController:  CreateParkingViewInput {

    // MARK: CreateParkingViewInput
    func setupInitialState() {
        setupInitViewState()
    }
    
    func render(with props: Props) {
        self.props = props

    }
    
    func showPicker() {
        let docType = [String(kUTTypePDF)]
        let docPicker = UIDocumentPickerViewController(documentTypes: docType, in: UIDocumentPickerMode.import)
        docPicker.delegate = self
        docPicker.modalPresentationStyle = .fullScreen
        self.present(docPicker, animated: true, completion: nil)
        
    }

}

extension CreateParkingViewController: UIDocumentPickerDelegate{

    func documentPicker(_ controller: UIDocumentPickerViewController, didPickDocumentsAt urls: [URL]) {
        let localURL = urls[0]
        props.chooseDoc.perform(with: localURL)



    }
    


}

extension CreateParkingViewController {
    private func setupInitViewState() {
        navigationItem.title = "Новый паркинг"
        view.backgroundColor = .white
        view.addSubview(createParkingView)
        createParkingView.addConstraints(equalToSafeArea(superView: view))
        view.addSubview(spinnerView)
        spinnerView.addConstraints([
            equal(view, \UIView.safeAreaLayoutGuide.centerXAnchor),
            equal(view, \UIView.safeAreaLayoutGuide.centerYAnchor)
        ])
    }
    

}
