//
//  CreateParkingCreateParkingViewIO.swift
//  turaQshare
//
//  Created by Yesbol Kulanbekov on 25/12/2018.
//  Copyright © 2018 turaQshare. All rights reserved.
//
import Foundation

protocol CreateParkingViewOutput {

    /**
        @author Yesbol Kulanbekov
        Notify presenter that view is ready
    */

    func viewIsReady()
}


protocol CreateParkingViewInput: class, DisplaysAlert, HasViewController {

    /**
        @author Yesbol Kulanbekov
        Setup initial state of the view
    */
    func render(with props: CreateParkingViewController.Props)
    func setupInitialState()
    func showPicker()
}
