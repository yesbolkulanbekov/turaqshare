//
//  CreateParkingView.swift
//  turaQshare
//
//  Created by Yesbol Kulanbekov on 12/25/18.
//  Copyright © 2018 turaQshare. All rights reserved.
//

import UIKit


class CreateParkingView: UIScrollView {
    
    var props: Props! {
        didSet {
            let isPossible = props.createPostAction.possible != nil
            self.isUserInteractionEnabled = isPossible
            self.alpha = isPossible ? 1 : 0.2
        }
    }
    
    struct Subviews {
        let mainLabel          = UILabel()
        let contentView        = UIView()
        let parkingSpaceNum    = UITextField()
        let addressTV          = UITextView()
        let showOnMapBtn       = UIButton(type: .system)
        let backGroundView     = UIImageView()
        let pinView            = UIImageView()
        let attachDocInfoLabel = UILabel()
        
        let attachBtn  = UIButton()
        let attachInfoBtn = UIButton()
        
        let checkBox = UIButton()
        let conditionsLabel = UILabel()
        
        let continueBtn = UIButton(type: .system)
        let termsBtn    = UIButton(type: .system)
    }
    
    let subViews = CreateParkingView.Subviews()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupSelf()
        configureSubViews()
        styleSubviews()
        renderConstantData()
        layout()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
    }
}


/// Props configuration and init

extension CreateParkingView {
    struct Props {
        let createPostAction: CreatePropsAction
        let showPicker: Command
        let showInfo: Command
        
        enum CreatePropsAction {
            case disabled
            case inProgress
            case possible(CommandWith<ParkingPlace>)
            
            var possible: CommandWith<ParkingPlace>? {
                guard case let .possible(command) = self else { return nil }
                return command
            }
            
            var isDisabled: Bool {
                guard case .disabled = self else { return false }
                return true
            }
            
            var isInProgress: Bool {
                guard case .inProgress = self else { return false }
                return true
            }
        }
    }
}

extension CreateParkingView.Props {
    
}

/// View configuration, content and styling

extension CreateParkingView {
    
    private func setupSelf() {
        translatesAutoresizingMaskIntoConstraints = false
        alwaysBounceVertical = true
        let tapRecog = UITapGestureRecognizer(target: self, action: #selector(dissmissKey))
        self.addGestureRecognizer(tapRecog)
    }
    
    @objc
    func dissmissKey() {
        self.endEditing(true)
    }
    
    private func configureSubViews() {
        subViews.continueBtn.addTarget(self, action: #selector(createParkingPlace), for: .touchUpInside)
        subViews.attachBtn.addTarget(self, action: #selector(pickDoc), for: .touchUpInside)
        
        subViews.attachInfoBtn.addTarget(self, action: #selector(showInfo), for: .touchUpInside)
        
        subViews.addressTV.delegate = self
    }
    
    @objc
    func showInfo() {
        props.showInfo.perform()
    }
    
    @objc
    func createParkingPlace() {
        guard let command = props.createPostAction.possible else { return }
        let number = subViews.parkingSpaceNum.text!
        let parkingPlace = ParkingPlace(placeNumber: number, parkingZone: 1, owner: nil, status: nil)
        command.perform(with: parkingPlace)
        
    }
    
    @objc
    func pickDoc() {
        props.showPicker.perform()
    }
    
    func styleSubviews() {
        subViews.mainLabel.font = UIFont(name: "Helvetica", size: 30)
        subViews.mainLabel.textColor = UIColor.pine
        subViews.mainLabel.numberOfLines = 0
        
        subViews.parkingSpaceNum.font = UIFont.LucidaGrandeRegular(ofSize: 16)
        subViews.parkingSpaceNum.textColor = .pine
        subViews.parkingSpaceNum.backgroundColor = UIColor.whiteTwo
        
        subViews.addressTV.font = UIFont.LucidaGrandeRegular(ofSize: 16)
        subViews.addressTV.textColor = .pine
        subViews.addressTV.backgroundColor = .whiteTwo
        
        subViews.showOnMapBtn.titleLabel?.font = UIFont.LucidaGrandeRegular(ofSize: 16)
        subViews.showOnMapBtn.setTitleColor(.denim, for: .normal)
        subViews.showOnMapBtn.backgroundColor = .clear
        
        subViews.attachDocInfoLabel.font = UIFont.LucidaGrandeRegular(ofSize: 14)
        subViews.attachDocInfoLabel.textColor = UIColor.pine
        subViews.attachDocInfoLabel.numberOfLines = 0
        
        subViews.attachBtn.backgroundColor = UIColor.blueGreen.withAlphaComponent(0.08)
        
        subViews.conditionsLabel.font = UIFont.LucidaGrandeRegular(ofSize: 16)
        subViews.conditionsLabel.textColor = UIColor.pine
        
        subViews.continueBtn.titleLabel?.font = UIFont.LucidaGrandeBold(ofSize: 18)
        subViews.continueBtn.setTitleColor(UIColor.blueGreen, for: .normal)
        subViews.continueBtn.backgroundColor = UIColor.blueGreen.withAlphaComponent(0.08)
        
        subViews.termsBtn.titleLabel?.font = UIFont.NunitoRegular(ofSize: 16)
        subViews.termsBtn.setTitleColor(UIColor.denim, for: .normal)
        subViews.termsBtn.backgroundColor = .clear
        
    }
    
    func renderConstantData() {
        subViews.mainLabel.text = "Укажите ваше парковочное место"
        subViews.parkingSpaceNum.placeholder = "Введите номер парковочного места"
        subViews.addressTV.text = "Адрес"
        subViews.addressTV.textColor = UIColor.lightGray
        subViews.showOnMapBtn.setTitle("Указать на карте", for: .normal)
        subViews.backGroundView.image = UIImage(named: "backgroundfig")
        subViews.pinView.image = UIImage(named: "locationGrid")
        subViews.attachDocInfoLabel.text = "Прикрепите официальный документ"

        subViews.attachBtn.setImage(UIImage(named: "file"), for: .normal)
        subViews.attachInfoBtn.setImage(UIImage(named: "information"), for: .normal)
        
        subViews.checkBox.setImage(UIImage(named: "empty"), for: .normal)
        subViews.conditionsLabel.text = "Принять Правила & Условия"
        
        subViews.continueBtn.setTitle("Далее", for: .normal)
        subViews.termsBtn.setTitle("Правила & Условия", for: .normal)
        
        
        subViews.showOnMapBtn.isHidden = true
        subViews.pinView.isHidden = true
        subViews.conditionsLabel.isHidden = true
        subViews.checkBox.isHidden = true
        subViews.termsBtn.isHidden = true
    }
    
    func render(with props: CreateParkingView.Props) {
        self.props = props
    }
    
    func layout() {
        CreateParkingViewLayout(rootView: self).paint()
    }
    
}

extension CreateParkingView: UITextViewDelegate {
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView.textColor == UIColor.lightGray {
            textView.text = nil
            textView.textColor = UIColor.black
        }
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text.isEmpty {
            textView.text = "Адрес"
            textView.textColor = UIColor.lightGray
        }
    }
}


/// View layout

struct CreateParkingViewLayout {
    
    var rootView: CreateParkingView
    var sv: CreateParkingView.Subviews
    
    init(rootView: CreateParkingView) {
        self.rootView = rootView
        self.sv = rootView.subViews
    }
    
    func paint() {
        addSubViews()
        addConstraints()
    }
    
    func addSubViews() {
        rootView.addSubview(sv.contentView)
        sv.contentView.addSubview(sv.backGroundView)
        sv.contentView.addSubview(sv.mainLabel)
        sv.contentView.addSubview(sv.parkingSpaceNum)
        sv.contentView.addSubview(sv.addressTV)
        sv.contentView.addSubview(sv.showOnMapBtn)
        sv.contentView.addSubview(sv.pinView)
        sv.contentView.addSubview(sv.attachDocInfoLabel)
        sv.contentView.addSubview(attachBtnStack)
        sv.contentView.addSubview(checkboxStack)
        
        attachBtnStack.addArrangedSubview(sv.attachBtn)
        attachBtnStack.addArrangedSubview(sv.attachInfoBtn)
        
        checkboxStack.addArrangedSubview(sv.checkBox)
        checkboxStack.addArrangedSubview(sv.conditionsLabel)
        
        sv.contentView.addSubview(sv.continueBtn)
        sv.contentView.addSubview(sv.termsBtn)
    }
    
    func addConstraints() {
        sv.contentView.addConstraints(equalTo(superView: rootView))
        sv.contentView.addConstraints([
            equal(rootView, \.widthAnchor),
            equal(sv.termsBtn, \UIView.bottomAnchor, constant: 30)
        ])

        
        sv.mainLabel.addConstraints([
            equal(sv.contentView, \UIView.topAnchor, constant: 20),
            equal(sv.contentView, \UIView.trailingAnchor, constant: -20),
            equal(sv.contentView, \UIView.leadingAnchor, constant: 20)
        ])
        
        sv.parkingSpaceNum.addConstraints([
            equal(sv.contentView, \UIView.leadingAnchor, constant: 20),
            equal(sv.contentView, \UIView.trailingAnchor, constant: -20),
            equal(sv.mainLabel,
                  \UIView.topAnchor,
                  \UIView.bottomAnchor,
                  constant: 20),
            equal(\UIView.heightAnchor, to: 46)
        ])
        
        sv.addressTV.addConstraints([
            equal(sv.contentView, \UIView.leadingAnchor, constant: 20),
            equal(sv.contentView, \UIView.trailingAnchor, constant: -20),
            equal(sv.parkingSpaceNum,
                  \UIView.topAnchor,
                  \UIView.bottomAnchor,
                  constant: 8),
            equal(\UIView.heightAnchor, to: 98)
        ])
        
        sv.showOnMapBtn.addConstraints([
            equal(sv.contentView, \UIView.trailingAnchor, constant: -30),
            equal(sv.addressTV,
                  \UIView.topAnchor,
                  \UIView.bottomAnchor,
                  constant: 12)
        ])
        
        sv.backGroundView.addConstraints([
            equal(sv.contentView, \UIView.topAnchor, constant: 23),
            equal(sv.contentView, \UIView.trailingAnchor),
        ])
        
        sv.pinView.addConstraints([
            equal(sv.showOnMapBtn,
                  \UIView.trailingAnchor,
                  \UIView.leadingAnchor,
                  constant: -8),
            equal(sv.showOnMapBtn, \UIView.centerYAnchor)
        ])
        
        sv.attachDocInfoLabel.addConstraints([
            equal(sv.contentView, \UIView.leadingAnchor, constant: 20),
            equal(sv.contentView, \UIView.trailingAnchor, constant: -20),
            equal(sv.pinView,
                  \UIView.topAnchor,
                  \UIView.bottomAnchor,
                  constant: 11),
        ])
        
        attachBtnStack.addConstraints([
            equal(sv.contentView, \UIView.leadingAnchor, constant: 20),
            equal(\UIView.heightAnchor, to: 44),
            equal(sv.attachDocInfoLabel,
                  \UIView.topAnchor,
                  \UIView.bottomAnchor,
                  constant: 12),
        ])
        
        sv.attachBtn.addConstraints([
            equal(\UIView.widthAnchor, to: 180)
        ])
        
        checkboxStack.addConstraints([
            equal(sv.contentView, \UIView.leadingAnchor, constant: 20),
            equal(attachBtnStack,
                  \UIView.topAnchor,
                  \UIView.bottomAnchor,
                  constant: 26)
        ])
        
        sv.continueBtn.addConstraints([
            equal(sv.contentView, \UIView.leadingAnchor, constant: 98),
            equal(sv.contentView, \UIView.trailingAnchor, constant: -98),
            equal(\UIView.heightAnchor, to: 44),
            equal(checkboxStack,
                  \UIView.topAnchor,
                  \UIView.bottomAnchor,
                  constant: 39),
        ])
        
        sv.termsBtn.addConstraints([
            equal(sv.contentView, \UIView.centerXAnchor),
            equal(sv.continueBtn,
                  \UIView.topAnchor,
                  \UIView.bottomAnchor,
                  constant: 50),
        ])
        
    }
    
    
    let attachBtnStack = UIStackView() { stack in
        stack.axis = .horizontal
        stack.distribution = .fill
        stack.alignment = .fill
        stack.spacing = 8
    }
    
    let checkboxStack = UIStackView() { stack in
        stack.axis = .horizontal
        stack.distribution = .fill
        stack.alignment = .fill
        stack.spacing = 15
    }
}
