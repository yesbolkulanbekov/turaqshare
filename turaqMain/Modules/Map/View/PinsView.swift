//
//  PinsView.swift
//  turaQshare
//
//  Created by Yesbol Kulanbekov on 12/16/18.
//  Copyright © 2018 turaQshare. All rights reserved.
//

import UIKit
import MapKit

class PinsView: UIView {
    
    var props: Props! {
        didSet {
            let annos = props.annotations
            subViews.mapView.addAnnotations(annos)
            
            let isPossible = props.searchAction.possible != nil
            subViews.searchBar.isEnabled = isPossible
            subViews.mapView.isUserInteractionEnabled = isPossible
            
            self.alpha = props.searchAction.isInProgress ? 0.3 : 1
            props.mapItems.forEach {
                dropPinZoomIn(placemark: $0.placemark)
            }
        }
    }
    
    func dropPinZoomIn(placemark:MKPlacemark){
        // cache the pin
        //selectedPin = placemark
        // clear existing pins
        subViews.mapView.removeAnnotations(subViews.mapView.annotations)
        let annotation = MKPointAnnotation()
        annotation.coordinate = placemark.coordinate
        annotation.title = placemark.name
        if let city = placemark.locality,
            let state = placemark.administrativeArea {
            annotation.subtitle = "\(city) \(state)"
        }
        subViews.mapView.addAnnotation(annotation)
        let span = MKCoordinateSpan(latitudeDelta: 0.03, longitudeDelta: 0.03)
        let region = MKCoordinateRegion(center: placemark.coordinate, span: span)
        subViews.mapView.setRegion(region, animated: true)
    }
    
    struct Subviews {
        let mapView = MKMapView()
        let searchBar = UITextField()
        let imageView = UIImageView()

    }
    
    let subViews = PinsView.Subviews()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupSelf()
        configureSubViews()
        styleSubviews()
        renderConstantData()
        layout()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
    }

}


/// Props configuration and init

extension PinsView {
    struct Props {
        let mapItems: [MKMapItem]
        let annotations: [Pin]
        let selectParking: CommandWith<Pin>
        let searchAction: SearchAction
        
        enum SearchAction {
            case disabled
            case inProgress
            case possible(CommandWith<String>)
            
            var possible: CommandWith<String>? {
                guard case let .possible(command) = self else { return nil }
                return command
            }
            
            var isDisabled: Bool {
                guard case .disabled = self else { return false }
                return true
            }
            
            var isInProgress: Bool {
                guard case .inProgress = self else { return false }
                return true
            }
        }
    }
}

/// View configuration, content and styling

extension PinsView {
    
    private func setupSelf() {
        translatesAutoresizingMaskIntoConstraints = false
        let tapRecognizer = UITapGestureRecognizer(target: self, action: #selector(resignKeyB))
        self.addGestureRecognizer(tapRecognizer)
    }
    
    private func configureSubViews() {
        subViews.searchBar.leftViewMode = .always
        subViews.searchBar.delegate = self
        subViews.mapView.delegate = self

        subViews.mapView.register(MKMarkerAnnotationView.self, forAnnotationViewWithReuseIdentifier: "Annotation")

    }
    
    func styleSubviews() {
        subViews.searchBar.backgroundColor = .whiteTwo
    }
    
    func render(with props: PinsView.Props) {
        self.props = props
    }

    func renderConstantData() {
        subViews.searchBar.placeholder = "Керуен"

        let image = UIImage(named: "searchPin")
        subViews.imageView.image = image
        subViews.imageView.contentMode = .center
        subViews.imageView.clipsToBounds = true
        
        subViews.searchBar.leftView = subViews.imageView
        
        let initialLocation = CLLocation(latitude: 51.128295, longitude: 71.430416)
        self.centerMapOnLocation(location: initialLocation)
    }
    
    func layout() {
        PinsViewLayout(rootView: self).paint()
    }
    
    override func layoutSubviews() {
        let origin = CGPoint(x: 0, y: 0)
        let size = CGSize(width: 40, height: 27)
        let imageFrame = CGRect(origin: origin, size: size)
        subViews.imageView.frame = imageFrame
    }
    
}

extension PinsView {
    func centerMapOnLocation(location: CLLocation) {
        let regionRadius: CLLocationDistance = 1000

        let coordinateRegion = MKCoordinateRegion(center: location.coordinate,
                                                  latitudinalMeters: regionRadius,
                                                  longitudinalMeters: regionRadius)
        subViews.mapView.setRegion(coordinateRegion, animated: true)
        
    }
    
    @objc
    func resignKeyB() {
        subViews.searchBar.resignFirstResponder()
    }

}

extension PinsView: UITextFieldDelegate {
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        print("Return in pinsview")
        textField.resignFirstResponder()
        let searchCommand = props.searchAction.possible
        searchCommand?.perform(with: subViews.searchBar.text!)
        return true
    }
    
}


extension PinsView: MKMapViewDelegate {
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        
        guard let annotation = annotation as? Pin else { return nil }
        
        let identifier = "Annotation"
        var view: MKMarkerAnnotationView

        guard let dequeuedView = mapView.dequeueReusableAnnotationView(withIdentifier: identifier) as? MKMarkerAnnotationView else {
            view = MKMarkerAnnotationView(annotation: annotation, reuseIdentifier: identifier)
            return view
        }
        
        dequeuedView.annotation = annotation
        dequeuedView.canShowCallout = true
        dequeuedView.calloutOffset = CGPoint(x: -5, y: 5)
        let btn = UIButton(type: .detailDisclosure)
        dequeuedView.rightCalloutAccessoryView = btn
        view = dequeuedView
        return view

    }
    
    func mapView(_ mapView: MKMapView, annotationView view: MKAnnotationView, calloutAccessoryControlTapped control: UIControl) {
        let parking = view.annotation as! Pin
        props.selectParking.perform(with: parking)
    }
    
    func mapView(_ mapView: MKMapView, regionDidChangeAnimated animated: Bool) {
        subViews.searchBar.resignFirstResponder()
    }
    
    
}




/// View layout

struct PinsViewLayout {
    
    var rootView: PinsView
    var sv: PinsView.Subviews
    
    init(rootView: PinsView) {
        self.rootView = rootView
        self.sv = rootView.subViews
    }
    
    func paint() {
        addSubViews()
        addConstraints()
    }
    
    func addSubViews() {
        rootView.addSubview(sv.mapView)
        rootView.addSubview(sv.searchBar)
        //sv.searchBar.addSubview(sv.imageView)
    }
    
    func addConstraints() {
        sv.mapView.addConstraints([
            equal(rootView, \UIView.safeAreaLayoutGuide.bottomAnchor),
            equal(rootView, \UIView.safeAreaLayoutGuide.leadingAnchor),
            equal(rootView, \UIView.safeAreaLayoutGuide.trailingAnchor),
            equal(sv.searchBar,
                  \UIView.safeAreaLayoutGuide.topAnchor,
                  \UIView.safeAreaLayoutGuide.bottomAnchor,
                  constant: 15)

        ])
        
        sv.searchBar.addConstraints([
            equal(rootView, \.topAnchor, constant: 20),
            equal(rootView, \.leadingAnchor, constant: 20),
            equal(rootView, \.trailingAnchor, constant: -20),
            equal(\.heightAnchor, to: 46)
        ])
        
    
    }
    
    
    
}
