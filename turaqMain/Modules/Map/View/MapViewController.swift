//
//  MapMapViewController.swift
//  turaQshare
//
//  Created by Yesbol Kulanbekov on 16/12/2018.
//  Copyright © 2018 turaQshare. All rights reserved.
//

import UIKit

class MapViewController: UIViewController {
    
    struct Props {
        let mapProps: PinsView.Props
    }
    

    var output: MapViewOutput!
    
    let spinnerView   = UIActivityIndicatorView(style: .gray)
    let pinsView      = PinsView()

    // MARK: Life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .white
        output.viewIsReady()
    }

    private func setupInitViewState() {
        view.backgroundColor = .white
        navigationItem.title = "Выберите локацию"
        view.addSubview(pinsView)
        pinsView.addConstraints(equalToSafeArea(superView: view))
        view.addSubview(spinnerView)
        spinnerView.addConstraints([
            equal(view, \UIView.safeAreaLayoutGuide.centerXAnchor),
            equal(view, \UIView.safeAreaLayoutGuide.centerYAnchor)
        ])
    }
    
    var isSearchInProgress: Bool = false {
        didSet {
            switch isSearchInProgress {
            case true:
                spinnerView.startAnimating()
            case false:
                spinnerView.stopAnimating()
            }
        }
    }
}

extension MapViewController:  MapViewInput {

    // MARK: MapViewInput
    func setupInitialState() {
        setupInitViewState()
    }
    
    func render(with props: Props) {
        let searchAction = props.mapProps.searchAction
        self.isSearchInProgress = searchAction.isInProgress
        pinsView.render(with: props.mapProps)
    }

}
