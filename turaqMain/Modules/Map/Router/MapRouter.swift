//
//  MapMapRouter.swift
//  turaQshare
//
//  Created by Yesbol Kulanbekov on 16/12/2018.
//  Copyright © 2018 turaQshare. All rights reserved.
//
import UIKit

class MapRouter: MapRouterInput {
    func openParkingList(with zone: Zone, from vc: UIViewController) {
        let buildingParkingListVC = BuildingParkingListModuleConfigurator().assembleModule()
        buildingParkingListVC.output.initView(with: zone)
        vc.navigationController?.pushViewController(buildingParkingListVC, animated: true)
    }

}
