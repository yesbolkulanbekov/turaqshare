//
//  MapMapRouterInput.swift
//  turaQshare
//
//  Created by Yesbol Kulanbekov on 16/12/2018.
//  Copyright © 2018 turaQshare. All rights reserved.
//

import UIKit

protocol MapRouterInput {
    func openParkingList(with zone: Zone, from vc: UIViewController)
}
