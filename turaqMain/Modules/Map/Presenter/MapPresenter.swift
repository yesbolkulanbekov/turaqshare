//
//  MapMapPresenter.swift
//  turaQshare
//
//  Created by Yesbol Kulanbekov on 16/12/2018.
//  Copyright © 2018 turaQshare. All rights reserved.
//
import MapKit

class MapPresenter: MapModuleInput {

    weak var view: MapViewInput!
    var interactor: MapInteractorInput!
    var router: MapRouterInput!

}

extension MapPresenter: MapViewOutput {
    func viewIsReady() {
        view.setupInitialState()
        interactor.getParkings()
    }
}

extension MapPresenter: MapInteractorOutput {
    func didFind(_ items: [MKMapItem]) {
        view.render(with: searchPossibleProps(items, [Pin]()))
    }
    
    func didLoadParkings(_ parkings: [Pin]) {
        view.render(with: searchPossibleProps([MKMapItem](), parkings))
    }

}


extension MapPresenter {
    func searchPossibleProps(_ items: [MKMapItem], _ parkings: [Pin]) -> MapViewController.Props {
        let selectParking = CommandWith<Pin> { parking in
            guard let zone = parking.zone else { return }
            self.router.openParkingList(with: zone, from: self.view.getVC())
        }
        
        let search = CommandWith<String> { searchText in
            print("Search in Presenter")
            self.interactor.search(for: searchText)
            self.view.render(with: self.searchInProgresProps())
        }
        
        let searchAction = PinsView.Props.SearchAction.possible(search)
        
        let mapProps = PinsView.Props(mapItems: items,
                                      annotations: parkings,
                                      selectParking: selectParking,
                                      searchAction: searchAction)
        let vcProps = MapViewController.Props(mapProps: mapProps)
        return vcProps
    }
    
    func searchInProgresProps() -> MapViewController.Props {
        let selectParking = CommandWith<Pin> { _ in }
        let searchAction = PinsView.Props.SearchAction.inProgress
        
        let mapProps = PinsView.Props(mapItems: [MKMapItem](), annotations: [Pin](), selectParking: selectParking, searchAction: searchAction)
        let vcProps = MapViewController.Props(mapProps: mapProps)
        return vcProps
    }


}
