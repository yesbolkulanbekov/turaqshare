//
//  MapMapInteractorIO.swift
//  turaQshare
//
//  Created by Yesbol Kulanbekov on 16/12/2018.
//  Copyright © 2018 turaQshare. All rights reserved.
//

import MapKit

protocol MapInteractorInput {
    func getParkings()
    func search(for searchBarText: String)

}

protocol MapInteractorOutput: class {
    func didLoadParkings(_ parkings: [Pin])
    func didFind(_ items: [MKMapItem])
}
