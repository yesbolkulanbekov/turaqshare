//
//  MapMapInteractor.swift
//  turaQshare
//
//  Created by Yesbol Kulanbekov on 16/12/2018.
//  Copyright © 2018 turaQshare. All rights reserved.
//

import FirebaseDatabase
import MapKit
import CoreLocation
import Result

class MapInteractor: MapInteractorInput {
    let ref           = FirebaseServices.shared.ref

    weak var output: MapInteractorOutput!

    func getParkings() {
        
        let onReturn  = { (result: Result<[Zone], TuraqError>)  in
            switch result {
            case .success(let zones):
                
                DispatchQueue.main.async {
                    
                    var pins = [Pin]()
                    
                    for zone in zones {
                        let latD = CLLocationDegrees(zone.latitude)
                        let lonD = CLLocationDegrees(zone.longitude)
                        let house = Pin(locationName: zone.name,
                                        coordinate: CLLocationCoordinate2D(latitude: latD, longitude: lonD))
                        house.zone = zone
                        pins.append(house)
                    }
                    

                    self.output.didLoadParkings(pins)
                }
                
            case .failure(let error):
                DispatchQueue.main.async {
                    
                }
            }
        }
        
        let zoneReq = Zones(onReturn: onReturn)
        
        zoneReq.dispatch()
        

    }
    
    
    func search(for searchBarText: String) {
        let request = MKLocalSearch.Request()
        request.naturalLanguageQuery = searchBarText
        let centerLocation = CLLocationCoordinate2D(latitude: 51.128295, longitude: 71.430416)
        let span = MKCoordinateSpan(latitudeDelta: CLLocationDegrees(exactly: 0.01)!, longitudeDelta: CLLocationDegrees(exactly: 0.01)!)
        let region = MKCoordinateRegion(center: centerLocation, span: span)
        request.region = region//mapView.region
        let search = MKLocalSearch(request: request)
        search.start { response, _ in
            guard let response = response else {
                return
            }
            let mapItems = response.mapItems
            self.output.didFind(mapItems)
        }
    }

}

class Pin: NSObject, MKAnnotation {
    let title: String?
    let locationName: String
    let coordinate: CLLocationCoordinate2D
    var zone: Zone?
    
    init(locationName: String, coordinate: CLLocationCoordinate2D) {
        
        self.title = locationName
        
        self.locationName = locationName
        
        self.coordinate = coordinate
        
        super.init()
    }
    
    var subtitle: String? {
        return locationName
    }
}



















//        ref.child("parkings").observeSingleEvent(of: .value) { (snap) in
//            for child in snap.children {
//                let snap = child as! DataSnapshot
//                let key = snap.key
//                let value = snap.value
//                //print("key = \(key)  value = \(value!)")
//
//                let one = value as? [String: Any]
//                guard let parkings = one?.values else { return }
//                for parking in parkings {
//                    guard let parkD = parking as? [String: Any] else { return }
//                    let actParkings = parkD.values
//
//                    for actParking in actParkings {
//                        guard let actPark = actParking as? [String: Any] else { return }
//                        print("actPark", actPark )
//                        if let lat = actPark["latitude"] as? Double {
//                            if let lon = actPark["longitude"]  as? Double  {
//                                print("Lat and long", lat, lon)
//                                let latD = CLLocationDegrees(lat)
//                                let lonD = CLLocationDegrees(lon)
//                                let house = Parking(locationName: key,
//                                                    coordinate: CLLocationCoordinate2D(latitude: latD, longitude: lonD))
//                                pins.append(house)
//                            }
//                        }
//                    }
//                    self.output.didLoadParkings(pins)
//
//                }
//            }
//        }
