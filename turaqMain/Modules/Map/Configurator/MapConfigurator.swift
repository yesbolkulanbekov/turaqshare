//
//  MapMapConfigurator.swift
//  turaQshare
//
//  Created by Yesbol Kulanbekov on 16/12/2018.
//  Copyright © 2018 turaQshare. All rights reserved.
//

import UIKit

struct MapModuleConfigurator {

    func assembleModule() -> MapViewController {
        let viewController = MapViewController()
        configure(viewController: viewController)
        return viewController
    }

    private func configure(viewController: MapViewController) {

        let router = MapRouter()

        let presenter = MapPresenter()
        presenter.view = viewController
        presenter.router = router

        let interactor = MapInteractor()
        interactor.output = presenter

        presenter.interactor = interactor
        viewController.output = presenter
    }

}
