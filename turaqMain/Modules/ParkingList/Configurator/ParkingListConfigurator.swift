//
//  ParkingListParkingListConfigurator.swift
//  turaQshare
//
//  Created by Yesbol Kulanbekov on 27/12/2018.
//  Copyright © 2018 turaQshare. All rights reserved.
//

import UIKit

struct ParkingListModuleConfigurator {

    func assembleModule() -> ParkingListViewController {
        let viewController = ParkingListViewController()
        configure(viewController: viewController)
        return viewController
    }

    private func configure(viewController: ParkingListViewController) {

        let router = ParkingListRouter()

        let presenter = ParkingListPresenter()
        presenter.view = viewController
        presenter.router = router

        let interactor = ParkingListInteractor()
        interactor.output = presenter

        presenter.interactor = interactor
        viewController.output = presenter
    }

}
