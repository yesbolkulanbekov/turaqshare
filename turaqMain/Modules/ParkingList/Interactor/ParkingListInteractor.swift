//
//  ParkingListParkingListInteractor.swift
//  turaQshare
//
//  Created by Yesbol Kulanbekov on 27/12/2018.
//  Copyright © 2018 turaQshare. All rights reserved.
//
import Foundation

class ParkingListInteractor: ParkingListInteractorInput {

    weak var output: ParkingListInteractorOutput!
    
    func listParkings() {
        let placesRequest = GetPlacesRequest { [weak self] (result) in
            
            guard let self = self else { return }
            guard let output = self.output else { return }
            
            switch result {
            case .success(let places):
            
                DispatchQueue.main.async {
                    output.didLoadPlacesWithSuccess(places)
                }
                
            case .failure(let error):
                DispatchQueue.main.async {
                    output.didFailLoadingPlaces(with: error)
                }
            }
        }
        placesRequest.dispatch()
    }

}
