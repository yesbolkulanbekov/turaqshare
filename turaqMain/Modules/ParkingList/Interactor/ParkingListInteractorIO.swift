//
//  ParkingListParkingListInteractorIO.swift
//  turaQshare
//
//  Created by Yesbol Kulanbekov on 27/12/2018.
//  Copyright © 2018 turaQshare. All rights reserved.
//

import Foundation

protocol ParkingListInteractorInput {
    func listParkings() 

}

protocol ParkingListInteractorOutput: class {
    func didLoadPlacesWithSuccess(_ places: [ParkingPlace])
    func didFailLoadingPlaces(with error: TuraqError)
}
