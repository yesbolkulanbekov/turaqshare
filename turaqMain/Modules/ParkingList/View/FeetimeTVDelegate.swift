//
//  FeetimeTVDelegate.swift
//  turaQshare
//
//  Created by Yesbol Kulanbekov on 3/7/19.
//  Copyright © 2019 turaQshare. All rights reserved.
//

import UIKit




class FeetimeTVDelegate: NSObject, UITableViewDataSource, UITableViewDelegate {
    
    var freetimes = [FreeTime]()
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return freetimes.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "FreetimeCellID", for: indexPath) as! FreeTimeCell
        let freetime = freetimes[indexPath.row]
        cell.configure(with: freetime)
        return cell
    }
    
}




class FreeTimeCell: UITableViewCell {
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
    }
    
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    
    func configure(with freeTime: FreeTime) {
        var placeNum = ""
        if let number = freeTime.parkingPlaceNumber {
            placeNum = String(number)
        }
        
        let name = "Место: " + placeNum + "  Зона: " + String(freeTime.parkingZone)
        textLabel?.text = name

    }
}
