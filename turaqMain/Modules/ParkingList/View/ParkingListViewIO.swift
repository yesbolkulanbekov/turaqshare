//
//  ParkingListParkingListViewIO.swift
//  turaQshare
//
//  Created by Yesbol Kulanbekov on 27/12/2018.
//  Copyright © 2018 turaQshare. All rights reserved.
//

protocol ParkingListViewOutput {

    /**
        @author Yesbol Kulanbekov
        Notify presenter that view is ready
    */

    func viewIsReady()
    func viewWillAppear()
}


protocol ParkingListViewInput: class, HasViewController, DisplaysAlert {

    /**
        @author Yesbol Kulanbekov
        Setup initial state of the view
    */

    func setupInitialState()
    func render(with props: ParkListVCProps)
    
    func render(with freetimes: [FreeTime])
}
