//
//  ParkingListParkingListViewController.swift
//  turaQshare
//
//  Created by Yesbol Kulanbekov on 27/12/2018.
//  Copyright © 2018 turaQshare. All rights reserved.
//

import UIKit
typealias ParkListVCProps = ParkingListViewController.Props
typealias ParkTableProps = ParkingListViewController.ParkingTableProps

extension ParkListVCProps {
    init() {
        self.rootTableProps = ParkTableProps(cells: [ParkTableProps.CellProps]())
        self.addParking     = Command.nop
        self.loadingAction  = LoadAction.inProgress
        self.refresh        = Command.nop
    }
}

class ParkingListViewController: UIViewController {
    
    struct Props {
        let rootTableProps: ParkingTableProps
        let addParking: Command
        let refresh: Command
        
        let loadingAction: LoadAction
        
        enum LoadAction {
            case disabled
            case inProgress
            case possible(Command)

            
            var possible: Command? {
                guard case let .possible(command) = self else { return nil }
                return command
            }
            
            var isDisabled: Bool {
                guard case .disabled = self else { return false }
                return true
            }
            
            var isInProgress: Bool {
                guard case .inProgress = self else { return false }
                return true
            }
        }
    }
    
    struct ParkingTableProps {
        let cells: [CellProps]
        
        struct CellProps {
            let name: String
            let select: Command
        }
    }
    
    var props = Props() {
        didSet {
            let isPossible = props.loadingAction.possible != nil
            self.parkListTV.isUserInteractionEnabled = isPossible
            self.view.alpha = isPossible ? 1 : 0.4
            
            isActionInProgress = props.loadingAction.isInProgress

        }
    }

    var output: ParkingListViewOutput!
    
    var parkListTV = UITableView()
    let spinnerView   = UIActivityIndicatorView(style: .gray)
    let newParkingBtn = UIButton(type: .system)
    let refreshControl = UIRefreshControl()
    
    let freetimeTV = UITableView()
    var tvDel = FeetimeTVDelegate()
    
    let segment = UISegmentedControl()

    // MARK: Life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .white
        output.viewIsReady()
        props.loadingAction.possible?.perform()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        output.viewWillAppear()
        // TODO: Load with pull to refresh
        props.loadingAction.possible?.perform()

    }
    
    var isActionInProgress: Bool = false {
        didSet {
            switch isActionInProgress {
            case true:
                spinnerView.startAnimating()
            case false:
                spinnerView.stopAnimating()
            }
        }
    }
}

extension ParkingListViewController:  ParkingListViewInput {

    // MARK: ParkingListViewInput
    func setupInitialState() {
        setupInitViewState()
        setupTableView()
        setupAddParkingButton()
        setupSegmentControl()
    }
    
    func render(with props: ParkListVCProps) {
        self.props = props

        parkListTV.reloadData()
    }
    
    func render(with freetimes: [FreeTime]) {
        tvDel.freetimes = freetimes
        freetimeTV.reloadData()
    }

}



extension ParkingListViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return props.rootTableProps.cells.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ParkingCellID", for: indexPath)
        let currentParking = props.rootTableProps.cells[indexPath.row]
        cell.selectionStyle = .none
        cell.textLabel?.text = currentParking.name
        return cell
    }
}


extension ParkingListViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
        let deleteAction = UIContextualAction(style: .destructive, title: "Удалить") { (action, view, completion) in

            completion(true)
        }
        
        deleteAction.image = UIImage(named: "trash")
        deleteAction.backgroundColor = UIColor.blueGreen
        
        return UISwipeActionsConfiguration(actions: [deleteAction])
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let parking = props.rootTableProps.cells[indexPath.row]
        parking.select.perform()
    }
}



extension ParkingListViewController {
    
    private func setupAddParkingButton() {
        newParkingBtn.setTitle("+ Новый адрес", for: .normal)
        newParkingBtn.backgroundColor = UIColor.blueGreen.withAlphaComponent(0.08)
        newParkingBtn.titleLabel?.font = UIFont.LucidaGrandeBold(ofSize: 18)
        newParkingBtn.setTitleColor(UIColor.blueGreen, for: .normal)
        newParkingBtn.addTarget(self, action: #selector(addNewParking), for: .touchUpInside)
    }
    
    @objc
    func addNewParking() {
        props.addParking.perform()
    }
    
    private func setupTableView() {
        parkListTV.register(UITableViewCell.self, forCellReuseIdentifier: "ParkingCellID")
        parkListTV.dataSource = self
        parkListTV.delegate = self
        parkListTV.rowHeight = 60
        parkListTV.tableFooterView = UIView()
        
        refreshControl.tintColor = .red
        refreshControl.attributedTitle = NSAttributedString.init(string: "Refresh", attributes: nil)
        refreshControl.addTarget(self, action: #selector(refreshTable), for: .valueChanged)
        
        //parkListTV.refreshControl = refreshControl
        
        freetimeTV.register(FreeTimeCell.self, forCellReuseIdentifier: "FreetimeCellID")
        freetimeTV.dataSource = tvDel
        freetimeTV.delegate = tvDel
        freetimeTV.rowHeight = 60
        freetimeTV.tableFooterView = UIView()
        
    }
    
    @objc
    func refreshTable() {
        props.refresh.perform()
    }
    
    private func setupSegmentControl() {
        segment.insertSegment(withTitle: "Места", at: 0, animated: true)
        segment.insertSegment(withTitle: "Активные", at: 1, animated: true)
        segment.tintColor = UIColor.blueGreen
        segment.selectedSegmentIndex = 0

        segment.addTarget(self, action: #selector(indexChanged(_:)), for: .valueChanged)
        freetimeTV.isHidden = true
    }

    
    @objc
    func indexChanged(_ sender: UISegmentedControl) {
        switch sender.selectedSegmentIndex{
        case 0:
            parkListTV.isHidden = false
            freetimeTV.isHidden = true
        case 1:
            parkListTV.isHidden = true
            freetimeTV.isHidden = false
        default:
            break
        }
    }
    
    private func setupInitViewState() {
        navigationController?.navigationBar.prefersLargeTitles = false
        navigationItem.title = "Мои Паркинги"
        view.backgroundColor = .white
        
        view.addSubview(freetimeTV)

        view.addSubview(parkListTV)
        view.addSubview(newParkingBtn)
        view.addSubview(segment)
        
        
        freetimeTV.addConstraints([
            equal(segment, \UIView.topAnchor, \UIView.bottomAnchor, constant: 8),
            equal(view, \UIView.leadingAnchor, \UIView.safeAreaLayoutGuide.leadingAnchor),
            equal(view, \UIView.trailingAnchor, \UIView.safeAreaLayoutGuide.trailingAnchor),
            equal(newParkingBtn, \.bottomAnchor, \.topAnchor, constant: -20)
            ])
        
        segment.addConstraints([
            equal(view, \UIView.topAnchor,   \UIView.safeAreaLayoutGuide.topAnchor, constant: 10),
            equal(view, \UIView.leadingAnchor, \UIView.safeAreaLayoutGuide.leadingAnchor, constant: 20),
            equal(view, \UIView.trailingAnchor, \UIView.safeAreaLayoutGuide.trailingAnchor, constant: -20),
            equal(\.heightAnchor, to: 40)
        ])
        
    
        parkListTV.addConstraints([
            equal(segment, \UIView.topAnchor, \UIView.bottomAnchor, constant: 8),
            equal(view, \UIView.leadingAnchor, \UIView.safeAreaLayoutGuide.leadingAnchor),
            equal(view, \UIView.trailingAnchor, \UIView.safeAreaLayoutGuide.trailingAnchor),
            equal(newParkingBtn, \.bottomAnchor, \.topAnchor, constant: -20)
        ])
        
        view.addSubview(spinnerView)
        spinnerView.addConstraints([
            equal(view, \UIView.safeAreaLayoutGuide.centerXAnchor),
            equal(view, \UIView.safeAreaLayoutGuide.centerYAnchor)
        ])
        
        newParkingBtn.addConstraints([
            equal(view,
                  \.bottomAnchor,
                  \UIView.safeAreaLayoutGuide.bottomAnchor,
                  constant: -20),
            equal(\.heightAnchor, to: 44),
            equal(view, \UIView.safeAreaLayoutGuide.leadingAnchor, constant: 85),
            equal(view, \UIView.safeAreaLayoutGuide.trailingAnchor, constant: -85)
        ])

    }
}
