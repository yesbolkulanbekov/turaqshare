//
//  ParkingListParkingListPresenter.swift
//  turaQshare
//
//  Created by Yesbol Kulanbekov on 27/12/2018.
//  Copyright © 2018 turaQshare. All rights reserved.
//
import Foundation

class ParkingListPresenter: ParkingListModuleInput {

    weak var view: ParkingListViewInput!
    var interactor: ParkingListInteractorInput!
    var router: ParkingListRouterInput!

}

extension ParkingListPresenter: ParkingListViewOutput {
    func viewIsReady() {
        view.setupInitialState()
        view.render(with: parkingsLoadPossibleVCProps(for: [ParkingPlace]()))
        self.getFreetimes()
    }
    
    func viewWillAppear() {
        view.render(with: parkingsLoadPossibleVCProps(for: [ParkingPlace]()))
    }
}

extension ParkingListPresenter: ParkingListInteractorOutput {
    func didLoadPlacesWithSuccess(_ places: [ParkingPlace]) {
        view.render(with: parkingsLoadPossibleVCProps(for: places))
    }
    
    func didFailLoadingPlaces(with error: TuraqError) {
        view.displayAlert(with: error.localizedDescription)
        view.render(with: parkingsLoadPossibleVCProps(for: [ParkingPlace]()))
    }
}


extension ParkingListPresenter {
    func parkingsLoadPossibleVCProps(for places: [ParkingPlace]) -> ParkListVCProps {
    
        let cells: [ParkTableProps.CellProps] = places.map { (place: ParkingPlace) in
            let name = "Место: " + place.placeNumber + "  Зона: " + String(place.parkingZone)
            let cell = ParkTableProps.CellProps(name: name, select: Command {
                self.router.openRentVC(with: place, from: self.view.getVC())
                //self.view.displayAlert(with: "Backend is not ready")

            })
            return cell
        }
        
        let tableProps = ParkTableProps(cells: cells)
        let addParkingCommand = Command {
            self.router.openCreateParking(from: self.view.getVC())
        }
        
        let refresh = Command {
            
        }
        
        let loadParking = Command {
            self.view.render(with: self.parkingsLoadInProgressVCProps())
            self.interactor.listParkings()
        }
        
        let props = ParkListVCProps(rootTableProps: tableProps,
                                    addParking: addParkingCommand,
                                    refresh: refresh,
                                    loadingAction: ParkingListViewController.Props.LoadAction.possible(loadParking))
        return props
    }
    
    func parkingsLoadInProgressVCProps() -> ParkListVCProps {
        
        let emptyCells = [ParkTableProps.CellProps]()
        
        let emptyTableProps = ParkTableProps(cells: emptyCells)
        let emptyAddParkingCommand = Command { _ in }
        
        let refresh = Command { _ in }
        
        let props = ParkListVCProps(rootTableProps: emptyTableProps,
                                    addParking: emptyAddParkingCommand,
                                    refresh: refresh,
                                    loadingAction: ParkingListViewController.Props.LoadAction.inProgress)
        return props
    }
}




extension ParkingListPresenter {
    func getFreetimes() {
        let freetimes = GetFreeTimes { (result) in
            
            switch result {
            case .success(let freetimes):
                
                DispatchQueue.main.async {
                    self.view.render(with: freetimes)
                }
                
            case .failure(let error):
                
                DispatchQueue.main.async {
                    self.view.displayAlert(with: error.localizedDescription)
                }
            }
        }
        
        freetimes.dispatch()
    }
}
