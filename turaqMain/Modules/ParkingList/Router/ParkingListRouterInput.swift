//
//  ParkingListParkingListRouterInput.swift
//  turaQshare
//
//  Created by Yesbol Kulanbekov on 27/12/2018.
//  Copyright © 2018 turaQshare. All rights reserved.
//

import UIKit

protocol ParkingListRouterInput {
    func openCreateParking(from vc: UIViewController) 
    func openRentVC(with parkingPlace: ParkingPlace, from vc: UIViewController)

}
