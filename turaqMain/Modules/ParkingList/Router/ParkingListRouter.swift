//
//  ParkingListParkingListRouter.swift
//  turaQshare
//
//  Created by Yesbol Kulanbekov on 27/12/2018.
//  Copyright © 2018 turaQshare. All rights reserved.
//

import UIKit

class ParkingListRouter: ParkingListRouterInput {
    func openCreateParking(from vc: UIViewController) {
        let createParkingVC = CreateParkingModuleConfigurator().assembleModule()
        vc.navigationController?.pushViewController(createParkingVC, animated: true)
    }
    
    func openRentVC(with parkingPlace: ParkingPlace, from vc: UIViewController){
        let rentVC = RentViewModuleConfigurator().assembleModule()
        rentVC.output.initView(with: parkingPlace)
        vc.navigationController?.pushViewController(rentVC, animated: true)
    }
}
