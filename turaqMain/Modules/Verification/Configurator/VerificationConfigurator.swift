//
//  VerificationVerificationConfigurator.swift
//  turaQshare
//
//  Created by Yesbol Kulanbekov on 15/12/2018.
//  Copyright © 2018 turaQshare. All rights reserved.
//

import UIKit

struct VerificationModuleConfigurator {

    func assembleModule() -> VerificationViewController {
        let viewController = VerificationViewController()
        configure(viewController: viewController)
        return viewController
    }

    private func configure(viewController: VerificationViewController) {

        let router = VerificationRouter()

        let presenter = VerificationPresenter()
        presenter.view = viewController
        presenter.router = router

        let interactor = VerificationInteractor()
        interactor.output = presenter

        presenter.interactor = interactor
        viewController.output = presenter
    }

}
