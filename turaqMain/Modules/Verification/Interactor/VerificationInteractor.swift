//
//  VerificationVerificationInteractor.swift
//  turaQshare
//
//  Created by Yesbol Kulanbekov on 15/12/2018.
//  Copyright © 2018 turaQshare. All rights reserved.
//
import Foundation

class VerificationInteractor: VerificationInteractorInput {

    weak var output: VerificationInteractorOutput!
    
    let phoneProvider = FirebaseServices.shared.phoneProvider
    let auth          = FirebaseServices.shared.auth
    let ref           = FirebaseServices.shared.ref
    
    let userDefs      = UserDefaultsUserStorageService.shared
    
    
    
    func signIn(with verificationCode: String) {
        
        guard let verificationID = userDefs.getPhoneAuthID() else {
            // TODO: alert no verification ID
            return
        }
        
        let credential = phoneProvider.credential(
            withVerificationID: verificationID,
            verificationCode: verificationCode)
        
        auth.signInAndRetrieveData(with: credential) { [weak self] (authResult, error) in
            
            guard let strongSelf = self else { return }
            guard let output = strongSelf.output else { return }
            
            guard let error = error else {
            
                let uid = authResult!.user.uid
                strongSelf.ref.child("users").observeSingleEvent(of: .value) { (snapshot) in
                    if snapshot.hasChild(uid) {
                        output.didSignInWithExistingUser()
                    } else {
                        output.didSignUpWithPhone()
                    }
                }
                
                return
            }
            
            output.signInDidFail(with: error)
            
        }
    }
    
}
