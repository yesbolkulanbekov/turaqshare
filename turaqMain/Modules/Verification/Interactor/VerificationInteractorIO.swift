//
//  VerificationVerificationInteractorIO.swift
//  turaQshare
//
//  Created by Yesbol Kulanbekov on 15/12/2018.
//  Copyright © 2018 turaQshare. All rights reserved.
//

import Foundation

protocol VerificationInteractorInput {
    func signIn(with verificationCode: String)
}

protocol VerificationInteractorOutput: class {
    func didSignUpWithPhone()
    func didSignInWithExistingUser()
    func signInDidFail(with error: Error)
}
