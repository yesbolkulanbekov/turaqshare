//
//  VerifyView.swift
//  turaQshare
//
//  Created by Yesbol Kulanbekov on 12/15/18.
//  Copyright © 2018 Galym Anuarbek. All rights reserved.
//

import Foundation


import UIKit


class VerifyView: UIScrollView {
    
    var prevContentOffset: CGPoint!
    
    var props: Props = Props() {
        didSet {
            let phVerAction = props.phoneVerifyAction
            let isPossible = phVerAction.possible != nil
            subViews.confirmButton.isEnabled = isPossible
            alpha = phVerAction.isInProgress ? 0.3 : 1
        }
    }
    
    struct Subviews {
        let turaqLogo    = UIImageView()
        let verifyLB     = UILabel()
        let infoLB       = UILabel()
        let verifyCodeLB = UILabel()

        let backGroundView       = UIImageView()
        let phonePicture         = UIImageView()
        
        let tf1 = UITextField()

        
        let resendButton  = UIButton(type: .system)
        let confirmButton = UIButton(type: .system)

    }
    
    let subViews = VerifyView.Subviews()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupSelf()
        configureSubViews()
        styleSubviews()
        renderConstantData()
        render(with: Props())
        layout()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
    }
}


/// Props configuration and init

extension VerifyView {
    struct Props {
        let phoneVerifyAction: PhoneVerifyAction
        
        enum PhoneVerifyAction {
            case disabled
            case inProgress
            case possible(CommandWith<String>)
            
            var possible: CommandWith<String>? {
                guard case let .possible(command) = self else { return nil }
                return command
            }
            
            var isDisabled: Bool {
                guard case .disabled = self else { return false }
                return true
            }
            
            var isInProgress: Bool {
                guard case .inProgress = self else { return false }
                return true
            }
        }
    }
}

extension VerifyView.Props {
    init() {
        self.phoneVerifyAction = .possible(CommandWith<String> { _ in })
    }
}

/// View configuration, content and styling

extension VerifyView {
    
    private func setupSelf() {
        
        translatesAutoresizingMaskIntoConstraints = false
        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(dissmissK))
        addGestureRecognizer(tapGestureRecognizer)
        subViews.tf1.delegate = self


    }
    
    private func configureSubViews() {
        subViews.verifyLB.adjustsFontSizeToFitWidth = true
        subViews.verifyCodeLB.adjustsFontSizeToFitWidth = true
        subViews.verifyCodeLB.textAlignment = .center
        subViews.infoLB.numberOfLines = 0
        subViews.resendButton.titleLabel?.numberOfLines = 0
        subViews.tf1.textAlignment = .center
        subViews.tf1.keyboardType = .numberPad
        
        subViews.confirmButton.addTarget(self, action: #selector(confirm), for: .touchUpInside)
    }
    
    func styleSubviews() {
        subViews.verifyLB.font       = UIFont.LucidaGrandeBold(ofSize: 30)
        subViews.verifyLB.textColor  = UIColor.pine
        
        subViews.infoLB.font       = UIFont.LucidaGrandeRegular(ofSize: 14)
        subViews.infoLB.textColor  = UIColor.pine
        
        subViews.verifyCodeLB.font       = UIFont.LucidaGrandeBold(ofSize: 22)
        subViews.verifyCodeLB.textColor  = UIColor.pine
        
        subViews.tf1.backgroundColor = UIColor.whiteTwo

        subViews.resendButton.titleLabel?.font = UIFont.LucidaGrandeRegular(ofSize: 16)
        subViews.resendButton.setTitleColor(.denim, for: .normal)

        subViews.confirmButton.setTitleColor(UIColor.denim, for: .normal)
        subViews.confirmButton.titleLabel?.font = UIFont.LucidaGrandeBold(ofSize: 18)
        subViews.confirmButton.backgroundColor = UIColor.blueGreen.withAlphaComponent(0.08)
    }
    
    func render(with props: VerifyView.Props) {
        self.props = props
    }
    
    // TODO: Add this to generic view and shared code
    func renderConstantData() {
        subViews.turaqLogo.image = UIImage(named: "logo")
        subViews.verifyLB.text   = "Верификация…"
        subViews.infoLB.text   = "Введите 6-и значный код, высланный вам по СМС."
        subViews.backGroundView.image = UIImage(named: "backgroundfig")
        subViews.phonePicture.image = UIImage(named: "verificationPhone")
        subViews.verifyCodeLB.text = "Код подтверждения"
        subViews.resendButton.setTitle("Переотправить\n(timer: each 1 min)", for: .normal)
        subViews.confirmButton.setTitle("Подтвердить", for: .normal)
        subViews.tf1.text = "123456"
    }
    
    func layout() {
        VerifyViewLayout(rootView: self).paint()
    }
    
}

extension VerifyView: UITextFieldDelegate {
    func textFieldDidBeginEditing(_ textField: UITextField) {
        self.prevContentOffset = self.contentOffset
        let point = CGPoint(x: 0, y: subViews.verifyCodeLB.frame.origin.y - 50)
        self.setContentOffset(point, animated: true)
    }
    

}

extension VerifyView {
    
    @objc
    func confirm() {
        guard let command = props.phoneVerifyAction.possible else { return }
        command.perform(with: subViews.tf1.text!)
    }
    
    
    @objc
    func dissmissK() {
        subViews.tf1.resignFirstResponder()

        guard let offset = self.prevContentOffset else { return }
        self.setContentOffset(offset, animated: true)

        
    }
}


/// View layout
// TODO: rename subviews to sv in shared code
struct VerifyViewLayout {
    
    var rootView: VerifyView
    var sv: VerifyView.Subviews
    
    init(rootView: VerifyView) {
        self.rootView = rootView
        self.sv = rootView.subViews
    }
    
    func paint() {
        addSubViews()
        addConstraints()
    
        
    }
    
    func addSubViews() {
        rootView.addSubview(contentView)
        contentView.addSubview(sv.turaqLogo)
        contentView.addSubview(sv.verifyLB)
        contentView.addSubview(sv.infoLB)
        contentView.addSubview(sv.backGroundView)
        contentView.addSubview(sv.phonePicture)
        contentView.addSubview(sv.verifyCodeLB)
        contentView.addSubview(sv.tf1)
        contentView.addSubview(sv.resendButton)
        contentView.addSubview(sv.confirmButton)
        

    }
    

    func addConstraints() {
        contentView.addConstraints(equalTo(superView: rootView))
        contentView.addConstraints([
            equal(rootView, \UIView.widthAnchor)

        ])
        
        sv.turaqLogo.addConstraints([
            equal(contentView, \UIView.topAnchor, constant: 30),
            equal(contentView, \UIView.leadingAnchor, constant: 20)
        ])
        
        sv.verifyLB.addConstraints([
            equal(sv.turaqLogo,
                  \UIView.topAnchor,
                  \UIView.bottomAnchor,
                  constant: 30),
            equal(contentView, \UIView.leadingAnchor, constant: 20),
            equal(contentView, \UIView.trailingAnchor, constant: -20),
        ])
        
        sv.infoLB.addConstraints([
            equal(sv.verifyLB, \.topAnchor, \.bottomAnchor, constant: 5),
            equal(contentView, \UIView.leadingAnchor, constant: 20),
            equal(contentView, \UIView.trailingAnchor, constant: -64),
        ])
        
        sv.backGroundView.addConstraints([
            equal(rootView, \UIView.safeAreaLayoutGuide.topAnchor, constant: 23),
            equal(rootView, \UIView.safeAreaLayoutGuide.trailingAnchor),
        ])

        sv.phonePicture.addConstraints([
            equal(sv.infoLB,
                  \UIView.topAnchor,
                  \UIView.bottomAnchor,
                  constant: 20),
            equal(contentView, \UIView.leadingAnchor, constant: 169)
        ])
        
        sv.verifyCodeLB.addConstraints([
            equal(sv.phonePicture,
                  \UIView.topAnchor,
                  \UIView.bottomAnchor,
                  constant: 35),
            equal(contentView, \UIView.leadingAnchor, constant: 37),
            equal(contentView, \UIView.trailingAnchor, constant: -37)
        ])
        
        sv.tf1.addConstraints([
            equal(sv.verifyCodeLB,
                  \UIView.topAnchor,
                  \UIView.bottomAnchor,
                  constant: 10),
            equal(contentView, \UIView.leadingAnchor, constant: 23),
            equal(contentView, \UIView.trailingAnchor, constant: -23),
            equal(\.heightAnchor, to: 50)
        ])
        
        sv.resendButton.addConstraints([
            equal(sv.tf1,
                  \UIView.topAnchor,
                  \UIView.bottomAnchor,
                  constant: 10),
            equal(contentView, \UIView.trailingAnchor, constant: -14),
            

        ])
        
        sv.confirmButton.addConstraints([
            equal(sv.resendButton,
                  \UIView.topAnchor,
                  \UIView.bottomAnchor,
                  constant: 26),
            
            equal(contentView, \UIView.bottomAnchor, constant: -46),
            equal(contentView, \UIView.trailingAnchor, constant: -97),
            equal(contentView, \UIView.leadingAnchor, constant: 97),
            equal(\UIView.heightAnchor, to: 44)
        ])
        
        
    }
    
    let contentView = UIView()

    

    

}



