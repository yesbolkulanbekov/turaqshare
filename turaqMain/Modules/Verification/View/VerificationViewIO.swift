//
//  VerificationVerificationViewIO.swift
//  turaQshare
//
//  Created by Yesbol Kulanbekov on 15/12/2018.
//  Copyright © 2018 turaQshare. All rights reserved.
//

protocol VerificationViewOutput {

    /**
        @author Yesbol Kulanbekov
        Notify presenter that view is ready
    */

    func viewIsReady()
}


protocol VerificationViewInput: class, DisplaysAlert, HasViewController {

    /**
        @author Yesbol Kulanbekov
        Setup initial state of the view
    */
    func render(with props: VerificationViewController.Props)
    func setupInitialState()
}
