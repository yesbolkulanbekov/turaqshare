//
//  VerificationVerificationViewController.swift
//  turaQshare
//
//  Created by Yesbol Kulanbekov on 15/12/2018.
//  Copyright © 2018 turaQshare. All rights reserved.
//

import UIKit

class VerificationViewController: UIViewController {
    
    struct Props {
        let rootViewProps: VerifyView.Props
    }

    var output: VerificationViewOutput!

    let verifyView = VerifyView()
    let spinnerView = UIActivityIndicatorView(style: UIActivityIndicatorView.Style.gray)
    
    // MARK: Life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .white
        output.viewIsReady()
    }

    private func setupInitViewState() {
        navigationController?.isNavigationBarHidden = false

        view.backgroundColor = .white
        view.addSubview(verifyView)
        verifyView.addConstraints(equalToSafeArea(superView: view))
        view.addSubview(spinnerView)
        spinnerView.addConstraints([
            equal(view, \UIView.safeAreaLayoutGuide.centerXAnchor),
            equal(view, \UIView.safeAreaLayoutGuide.centerYAnchor)
        ])
    }
    
    var isLoginInProgress: Bool = false {
        didSet {
            switch isLoginInProgress {
            case true:
                spinnerView.startAnimating()
            case false:
                spinnerView.stopAnimating()
            }
        }
    }
}

extension VerificationViewController:  VerificationViewInput {

    // MARK: VerificationViewInput
    func setupInitialState() {
        setupInitViewState()
    }

    func render(with props: Props) {
        let verifyAction = props.rootViewProps.phoneVerifyAction
        self.isLoginInProgress = verifyAction.isInProgress
        verifyView.render(with: props.rootViewProps)
    }
}
