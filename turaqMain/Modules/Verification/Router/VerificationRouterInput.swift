//
//  VerificationVerificationRouterInput.swift
//  turaQshare
//
//  Created by Yesbol Kulanbekov on 15/12/2018.
//  Copyright © 2018 turaQshare. All rights reserved.
//

import UIKit

protocol VerificationRouterInput {
    func opentabbarVC(from vc: UIViewController)
    func openRegVC(from vc: UIViewController) 

}
