//
//  VerificationVerificationRouter.swift
//  turaQshare
//
//  Created by Yesbol Kulanbekov on 15/12/2018.
//  Copyright © 2018 turaQshare. All rights reserved.
//
import UIKit

class VerificationRouter: VerificationRouterInput {
    
    func opentabbarVC(from vc: UIViewController) {
        let tabbarVC = TabBarViewController()
        let delegate = UIApplication.shared.delegate as! AppDelegate
        delegate.window?.rootViewController = tabbarVC
    }
    
    func openRegVC(from vc: UIViewController) {
        let regVC = EmailRegModuleConfigurator().assembleModule()
        vc.navigationController?.pushViewController(regVC, animated: true)
    }
}
