//
//  VerificationVerificationPresenter.swift
//  turaQshare
//
//  Created by Yesbol Kulanbekov on 15/12/2018.
//  Copyright © 2018 turaQshare. All rights reserved.
//

class VerificationPresenter: VerificationModuleInput {

    weak var view: VerificationViewInput!
    var interactor: VerificationInteractorInput!
    var router: VerificationRouterInput!

}

extension VerificationPresenter: VerificationViewOutput {
    func viewIsReady() {
        view.setupInitialState()
        
        let verifyPossibleAction = VerifyView.Props.PhoneVerifyAction.possible(verifyCommand())
        renderVCProps(with: verifyPossibleAction)
    }
}

extension VerificationPresenter: VerificationInteractorOutput {
    func didSignUpWithPhone() {
        let verifyPossibleAction = VerifyView.Props.PhoneVerifyAction.possible(verifyCommand())
        renderVCProps(with: verifyPossibleAction)
        print("Successful signUP")
        router.openRegVC(from: view.getVC())
    }
    
    func didSignInWithExistingUser() {
        let verifyPossibleAction = VerifyView.Props.PhoneVerifyAction.possible(verifyCommand())
        renderVCProps(with: verifyPossibleAction)
        print("Successful signin ")
        router.opentabbarVC(from: view.getVC())
    }
    

    func signInDidFail(with error: Error) {
        let verifyPossibleAction = VerifyView.Props.PhoneVerifyAction.possible(verifyCommand())
        renderVCProps(with: verifyPossibleAction)
        view.displayAlert(with: error.localizedDescription)
    }
    
}

extension VerificationPresenter {
    func verifyCommand() -> CommandWith<String> {
        let verifyCommand = CommandWith<String> { verificationCode in
            self.interactor.signIn(with: verificationCode)
            print("Hellooooooooooooo")
            let verifyInProgessAction = VerifyView.Props.PhoneVerifyAction.inProgress
            self.renderVCProps(with: verifyInProgessAction)
        }
        return verifyCommand
    }
    
    func renderVCProps(with action: VerifyView.Props.PhoneVerifyAction) {
        let props = VerifyView.Props(phoneVerifyAction: action)
        let vcProps = VerificationViewController.Props(rootViewProps: props)
        view.render(with: vcProps)
    }
}



