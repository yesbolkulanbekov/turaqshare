//
//  TabBarViewController.swift
//  turaQshare
//
//  Created by Yesbol Kulanbekov on 12/25/18.
//  Copyright © 2018 turaQshare. All rights reserved.
//

import UIKit

class TabBarViewController: UITabBarController {
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tabBar.barTintColor = UIColor.white.withAlphaComponent(0.8)
        
        let mapTab      = configureMapTab()
        let parkListTab = configureParkingListTab()
        //let historyTab  = configureHistoryTab()
        let settingsTab = configureSettingsTab()
        
        viewControllers = [mapTab,parkListTab,settingsTab]

        guard let items = tabBar.items else { return }
        for item in items {
            item.imageInsets = UIEdgeInsets(top: 4, left: 0, bottom: -4, right: 0)
        }
    
    }
    
    private func configureMapTab() -> UIViewController {
        let mapVC = MapModuleConfigurator().assembleModule()
        let mapVCInNav = UINavigationController(rootViewController: mapVC)
        let mapVCSelectedImage = UIImage(named: "icon1a")?.withRenderingMode(UIImage.RenderingMode.alwaysOriginal)
        let mapVCUnSelectedImage = UIImage(named: "icon1")?.withRenderingMode(UIImage.RenderingMode.alwaysOriginal)
        mapVCInNav.tabBarItem = UITabBarItem(title: nil, image: mapVCUnSelectedImage, selectedImage: mapVCSelectedImage)
        mapVCInNav.tabBarItem.titlePositionAdjustment = UIOffset(horizontal: 0, vertical: 50)
        return mapVCInNav
    }
    
    private func configureParkingListTab() ->  UIViewController {
        let parkListVC = ParkingListModuleConfigurator().assembleModule()
        let parkListVCInNav = UINavigationController(rootViewController: parkListVC)
        let selectedImage = UIImage(named: "icon2a")?.withRenderingMode(UIImage.RenderingMode.alwaysOriginal)
        let unSelectedImage = UIImage(named: "icon2")?.withRenderingMode(UIImage.RenderingMode.alwaysOriginal)
        parkListVCInNav.tabBarItem = UITabBarItem(title: nil, image: unSelectedImage, selectedImage: selectedImage)
        parkListVCInNav.tabBarItem.titlePositionAdjustment = UIOffset(horizontal: 0, vertical: 50)
        return parkListVCInNav
    }
    
    private func configureHistoryTab() ->  UIViewController {
        let historyVC = HistoryViewController()
        let historyVCInNav = UINavigationController(rootViewController: historyVC)
        let parkingVCSelectedImage = UIImage(named: "icon3a")?.withRenderingMode(UIImage.RenderingMode.alwaysOriginal)
        let parkingVCUnSelctedImage = UIImage(named: "icon3")?.withRenderingMode(UIImage.RenderingMode.alwaysOriginal)
        historyVCInNav.tabBarItem = UITabBarItem(title: nil, image: parkingVCUnSelctedImage, selectedImage: parkingVCSelectedImage)
        historyVCInNav.tabBarItem.titlePositionAdjustment = UIOffset(horizontal: 0, vertical: 50)
        return historyVCInNav
    }
    
    private func configureSettingsTab() ->  UIViewController {
        let settingsVC = TSettingsModuleConfigurator().assembleModule()
        let settingsVCInNav = UINavigationController(rootViewController: settingsVC)
        let parkingVCSelectedImage = UIImage(named: "icon4a")?.withRenderingMode(UIImage.RenderingMode.alwaysOriginal)
        let parkingVCUnSelctedImage = UIImage(named: "icon4")?.withRenderingMode(UIImage.RenderingMode.alwaysOriginal)
        settingsVCInNav.tabBarItem = UITabBarItem(title: nil, image: parkingVCUnSelctedImage, selectedImage: parkingVCSelectedImage)
        settingsVCInNav.tabBarItem.titlePositionAdjustment = UIOffset(horizontal: 0, vertical: 50)
        return settingsVCInNav
    }
}
