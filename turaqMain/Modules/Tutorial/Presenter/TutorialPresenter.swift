//
//  TutorialTutorialPresenter.swift
//  turaQshare
//
//  Created by Yesbol Kulanbekov on 24/12/2018.
//  Copyright © 2018 turaQshare. All rights reserved.
//

class TutorialPresenter: TutorialModuleInput {

    weak var view: TutorialViewInput!
    var interactor: TutorialInteractorInput!
    var router: TutorialRouterInput!

}

extension TutorialPresenter: TutorialViewOutput {
    func viewIsReady() {
        view.setupInitialState()
        view.render(with: vcProps())
    }
}

extension TutorialPresenter: TutorialInteractorOutput {

}

extension TutorialPresenter {
    func vcProps() -> TutVCProps {
        let command = CommandWith <Int> { currentPage in
            guard currentPage == 1 else { return }
            self.router.openLogin(from: self.view.getVC())
        }
        
        let tutProps = TutProps(nextPageCommand: command)
        let vcProps = TutVCProps(rootViewProps: tutProps)
        return vcProps
    }
}
