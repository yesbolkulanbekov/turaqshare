//
//  TutorialTutorialViewIO.swift
//  turaQshare
//
//  Created by Yesbol Kulanbekov on 24/12/2018.
//  Copyright © 2018 turaQshare. All rights reserved.
//

protocol TutorialViewOutput {

    /**
        @author Yesbol Kulanbekov
        Notify presenter that view is ready
    */

    func viewIsReady()
}


protocol TutorialViewInput: class, HasViewController {

    /**
        @author Yesbol Kulanbekov
        Setup initial state of the view
    */

    func setupInitialState()
    func render(with props: TutVCProps)
}
