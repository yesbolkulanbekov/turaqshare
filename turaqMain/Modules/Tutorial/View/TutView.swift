//
//  TutView.swift
//  turaQshare
//
//  Created by Yesbol Kulanbekov on 12/24/18.
//  Copyright © 2018 turaQshare. All rights reserved.
//

import Foundation

import UIKit

typealias TutProps = TutView.Props

class TutView: UIScrollView {
    
    var currentPage: Int = 0

    var props: Props = Props() {
        didSet {

        }
    }
    
    struct Subviews {
        let page1 = UIView()
        let page2 = UIView()
        let mainLabelOne = UILabel()
        let mainLabelTwo = UILabel()
        
        let subtitleOne = UILabel()
        let subtitleTwo = UILabel()
        
        let detailOne = UILabel()
        let detailTwo = UILabel()
        
        let logo1 = UIImageView()
        let logo2 = UIImageView()
        
        let image1 = UIImageView()
        let image2 = UIImageView()
        
        let nextBtn = UIButton(type: .system)
        
        let pageControl = UIPageControl()
    }
    
    let subViews = TutView.Subviews()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupSelf()
        configureSubViews()
        styleSubviews()
        renderConstantData()
        render(with: Props())
        layout()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
    }
}


/// Props configuration and init

extension TutView {
    struct Props {
        let nextPageCommand: CommandWith <Int>
    }
}

extension TutView.Props {
    init() {
        self.nextPageCommand = CommandWith <Int> { _ in }
    }
}

/// View configuration, content and styling

extension TutView {
    
    private func setupSelf() {
        translatesAutoresizingMaskIntoConstraints = false
        alwaysBounceHorizontal = true
        isPagingEnabled = true
        showsHorizontalScrollIndicator = false
        delegate = self
    }
    
    private func configureSubViews() {
        subViews.mainLabelOne.adjustsFontSizeToFitWidth = true
        subViews.mainLabelOne.numberOfLines = 0
        subViews.detailOne.numberOfLines = 0
        
        subViews.mainLabelTwo.adjustsFontSizeToFitWidth = true
        subViews.mainLabelTwo.numberOfLines = 0
        subViews.detailTwo.numberOfLines = 0
        
        subViews.nextBtn.addTarget(self, action: #selector(nextPage), for: .touchUpInside )
    }
    
    @objc
    func nextPage() {
        self.props.nextPageCommand.perform(with: currentPage)
        switch currentPage {
        case 0:
            setContentOffset(subViews.page2.frame.origin, animated: true)
        default: break
        }
    }
    
    func styleSubviews() {
        subViews.page2.backgroundColor = .white
        //UIColor.blueGreen
        
        subViews.mainLabelOne.font = UIFont.LucidaGrandeBold(ofSize: 30)
        subViews.mainLabelOne.textColor = .white
        subViews.subtitleOne.font = UIFont.LucidaGrandeRegular(ofSize: 16)
        subViews.subtitleOne.textColor = .white
        subViews.detailOne.font = UIFont.LucidaGrandeRegular(ofSize: 18)
        subViews.detailOne.textColor = UIColor.pine
        subViews.detailOne.textAlignment = .justified
        subViews.image1.contentMode = .scaleAspectFill
        subViews.image1.clipsToBounds = true
        
        subViews.mainLabelTwo.font = UIFont.LucidaGrandeBold(ofSize: 30)
        subViews.mainLabelTwo.textColor = .white
        subViews.subtitleTwo.font = UIFont.LucidaGrandeRegular(ofSize: 16)
        subViews.subtitleTwo.textColor = .white
        subViews.detailTwo.font = UIFont.LucidaGrandeRegular(ofSize: 18)
        subViews.detailTwo.textColor = UIColor.pine
        subViews.detailTwo.textAlignment = .justified
        subViews.image2.contentMode = .scaleAspectFill
        subViews.image2.clipsToBounds = true
        
        subViews.nextBtn.setTitleColor(UIColor.blueGreen, for: .normal)
        subViews.nextBtn.titleLabel?.font = UIFont.LucidaGrandeRegular(ofSize: 20)
        
        subViews.pageControl.currentPageIndicatorTintColor = UIColor.blueGreen
        subViews.pageControl.pageIndicatorTintColor = UIColor.pine.withAlphaComponent(0.5)
        subViews.pageControl.numberOfPages = 2
        subViews.pageControl.currentPage = 0

    }
    
    func renderConstantData() {
        subViews.image1.image = UIImage(named: "presentFirst")
        subViews.logo1.image  = UIImage(named: "logo")
        subViews.mainLabelOne.text = "Мы найдем вам\nпарковку"
        subViews.subtitleOne.text  = "AirBNB для парковок"
        subViews.detailOne.text    = "Вам нужно найти парковку возле своей работы? Или вы хотите сдать свою парковку, пока она пустует?"
        
        
        subViews.image2.image = UIImage(named: "tutoImage2")
        subViews.logo2.image  = UIImage(named: "logo")
        subViews.mainLabelTwo.text = "Мы найдем вам\nпарковку"
        subViews.subtitleTwo.text  = "AirBNB для парковок"
        subViews.detailTwo.text    = "Используйте turaQshare и Вы найдете лучшее решение своей проблемы!"
        
        subViews.nextBtn.setTitle("Далее", for: .normal)
    }
    
    func render(with props: TutView.Props) {
        self.props = props
    }
    
    func layout() {
        TutViewLayout(rootView: self).paint()
    }
    
}

extension TutView: UIScrollViewDelegate {
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let offset = contentOffset.x
        subViews.pageControl.currentPage = Int(offset/frame.size.width)
        self.currentPage = Int(offset/frame.size.width)
    }
}


/// View layout

struct TutViewLayout {
    
    var rootView: TutView
    var sv: TutView.Subviews
    
    init(rootView: TutView) {
        self.rootView = rootView
        self.sv = rootView.subViews
    }
    
    func paint() {
        addSubViews()
        addConstraints()
    }
    
    func addSubViews() {
        rootView.addSubview(rootStack)
        
        rootStack.addArrangedSubview(sv.page1)
        rootStack.addArrangedSubview(sv.page2)
        
        sv.page1.addSubview(sv.image1)
        sv.page1.addSubview(sv.mainLabelOne)
        sv.page1.addSubview(sv.subtitleOne)
        sv.page1.addSubview(sv.logo1)
        sv.page1.addSubview(sv.detailOne)
        
        sv.page2.addSubview(sv.image2)
        sv.page2.addSubview(sv.mainLabelTwo)
        sv.page2.addSubview(sv.subtitleTwo)
        sv.page2.addSubview(sv.logo2)
        sv.page2.addSubview(sv.detailTwo)
        
        rootView.addSubview(sv.nextBtn)
        rootView.addSubview(sv.pageControl)
    }
    
    func addConstraints() {
        rootStack.addConstraints(equalTo(superView: rootView))
        rootStack.addConstraints([
            equal(rootView, \UIView.heightAnchor)
        ])
        
        addPageOneConstraints()
        addPageTwoConstraints()
        
        if let superView = rootView.superview {
            sv.nextBtn.addConstraints([
                equal(superView, \UIView.safeAreaLayoutGuide.trailingAnchor, constant: -20),
                equal(sv.pageControl, \UIView.safeAreaLayoutGuide.centerYAnchor, constant: 0)
            ])
            sv.pageControl.addConstraints([
                equal(superView, \UIView.centerXAnchor),
                equal(superView,
                      \UIView.safeAreaLayoutGuide.centerYAnchor,
                      \UIView.safeAreaLayoutGuide.bottomAnchor,
                      constant: -20)
                
            ])
        }

        

    }
    
    func addPageTwoConstraints() {
        sv.page2.addConstraints([
            equal(rootView, \UIView.widthAnchor)
        ])
        
        sv.mainLabelTwo.addConstraints([
            equal(sv.page2, \UIView.leadingAnchor, constant: 20),
            equal(sv.page2, \UIView.trailingAnchor, constant: -20),
            equal(sv.page2, \UIView.topAnchor, constant: 100)
        ])
        
        sv.subtitleTwo.addConstraints([
            equal(sv.page2, \UIView.leadingAnchor, constant: 20),
            equal(sv.page2, \UIView.trailingAnchor, constant: -20),
            equal(sv.mainLabelTwo,
                  \UIView.topAnchor,
                  \UIView.bottomAnchor,
                  constant: 9)
        ])
        
        sv.logo2.addConstraints([
            equal(sv.page2, \UIView.leadingAnchor, constant: 20),
            equal(sv.image2,
                  \UIView.centerYAnchor,
                  \UIView.bottomAnchor,
                  constant: 0)
        ])
        
        sv.detailTwo.addConstraints([
            equal(sv.page2, \UIView.leadingAnchor, constant: 20),
            equal(sv.page2, \UIView.trailingAnchor, constant: -20),
            
            equal(sv.logo2,
                  \UIView.topAnchor,
                  \UIView.bottomAnchor,
                  constant: 20)
        ])
        
        sv.image2.addConstraints([
            equal(sv.page2, \UIView.leadingAnchor),
            equal(sv.page2, \UIView.trailingAnchor),
            equal(sv.page2, \UIView.topAnchor),
            equal(sv.page2,
                  \UIView.bottomAnchor,
                  \UIView.centerYAnchor,
                  constant: 0)
        ])
        
    }
    
    func addPageOneConstraints() {
        sv.page1.addConstraints([
            equal(rootView, \UIView.widthAnchor)
        ])
        
        sv.mainLabelOne.addConstraints([
            equal(sv.page1, \UIView.leadingAnchor, constant: 20),
            equal(sv.page1, \UIView.trailingAnchor, constant: -20),
            equal(sv.page1, \UIView.topAnchor, constant: 100)
        ])
        
        sv.subtitleOne.addConstraints([
            equal(sv.page1, \UIView.leadingAnchor, constant: 20),
            equal(sv.page1, \UIView.trailingAnchor, constant: -20),
            equal(sv.mainLabelOne,
                  \UIView.topAnchor,
                  \UIView.bottomAnchor,
                  constant: 9)
        ])
        
        sv.logo1.addConstraints([
            equal(sv.page1, \UIView.leadingAnchor, constant: 20),
            equal(sv.image1,
                  \UIView.centerYAnchor,
                  \UIView.bottomAnchor,
                  constant: 0)
        ])
        
        sv.detailOne.addConstraints([
            equal(sv.page1, \UIView.leadingAnchor, constant: 20),
            equal(sv.page1, \UIView.trailingAnchor, constant: -20),

            equal(sv.logo1,
                  \UIView.topAnchor,
                  \UIView.bottomAnchor,
                  constant: 20)
        ])
        
        sv.image1.addConstraints([
            equal(sv.page1, \UIView.leadingAnchor),
            equal(sv.page1, \UIView.trailingAnchor),
            equal(sv.page1, \UIView.topAnchor),
            equal(sv.page1,
                  \UIView.bottomAnchor,
                  \UIView.centerYAnchor,
                  constant: 0)
        ])
        
    }
    
    let rootStack = UIStackView() { stack in
        stack.axis = .horizontal
        stack.distribution = .fill
        stack.alignment = .fill
        stack.spacing = 0
    }
    
    
}
