//
//  TutorialTutorialViewController.swift
//  turaQshare
//
//  Created by Yesbol Kulanbekov on 24/12/2018.
//  Copyright © 2018 turaQshare. All rights reserved.
//

import UIKit
typealias TutVCProps = TutorialViewController.Props

class TutorialViewController: UIViewController {
    
    struct Props {
        let rootViewProps: TutProps
    }
    
    let spinnerView = UIActivityIndicatorView(style: .gray)
    let tutView     = TutView()
    
    var output: TutorialViewOutput!

    // MARK: Life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        output.viewIsReady()
    }

}

extension TutorialViewController:  TutorialViewInput {

    // MARK: TutorialViewInput
    func setupInitialState() {
        setupInitViewState()
    }
    
    func render(with props: TutVCProps) {
        tutView.render(with: props.rootViewProps)
    }

}

extension TutorialViewController {
    private func setupInitViewState() {
        view.backgroundColor = .white
        view.addSubview(tutView)
        tutView.setNeedsLayout()
        tutView.layout()
        tutView.addConstraints(equalToSafeArea(superView: view))
        view.addSubview(spinnerView)
        spinnerView.addConstraints([
            equal(view, \UIView.safeAreaLayoutGuide.centerXAnchor),
            equal(view, \UIView.safeAreaLayoutGuide.centerYAnchor)
        ])
    }
}
