//
//  TutorialTutorialRouter.swift
//  turaQshare
//
//  Created by Yesbol Kulanbekov on 24/12/2018.
//  Copyright © 2018 turaQshare. All rights reserved.
//
// TODO: change viper_temp to include UIKit in router

import UIKit

class TutorialRouter: TutorialRouterInput {
    func openLogin(from vc: UIViewController) {
        let loginVC = LoginViewModuleConfigurator().assembleModule()
        let navigationController = UINavigationController(rootViewController: loginVC)
        
        guard let window = UIApplication.shared.keyWindow else {
            return
        }
        
        let storage = UserDefaultsUserStorageService.shared
        storage.setDidShowTut(true)
        
        UIView.transition(with: window, duration: 0.3, options: .transitionCrossDissolve, animations: {
            window.rootViewController = navigationController
        }, completion: nil)
    }
}
