//
//  TutorialTutorialRouterInput.swift
//  turaQshare
//
//  Created by Yesbol Kulanbekov on 24/12/2018.
//  Copyright © 2018 turaQshare. All rights reserved.
//

import UIKit

protocol TutorialRouterInput {
    func openLogin(from vc: UIViewController)
}
