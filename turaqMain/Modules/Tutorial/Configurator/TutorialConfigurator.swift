//
//  TutorialTutorialConfigurator.swift
//  turaQshare
//
//  Created by Yesbol Kulanbekov on 24/12/2018.
//  Copyright © 2018 turaQshare. All rights reserved.
//

import UIKit

struct TutorialModuleConfigurator {

    func assembleModule() -> TutorialViewController {
        let viewController = TutorialViewController()
        configure(viewController: viewController)
        return viewController
    }

    private func configure(viewController: TutorialViewController) {

        let router = TutorialRouter()

        let presenter = TutorialPresenter()
        presenter.view = viewController
        presenter.router = router

        let interactor = TutorialInteractor()
        interactor.output = presenter

        presenter.interactor = interactor
        viewController.output = presenter
    }

}
