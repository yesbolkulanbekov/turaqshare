//
//  EmailRegEmailRegConfigurator.swift
//  turaQshare
//
//  Created by Yesbol Kulanbekov on 20/12/2018.
//  Copyright © 2018 turaQshare. All rights reserved.
//

import UIKit

struct EmailRegModuleConfigurator {

    func assembleModule() -> EmailRegViewController {
        let viewController = EmailRegViewController()
        configure(viewController: viewController)
        return viewController
    }

    private func configure(viewController: EmailRegViewController) {

        let router = EmailRegRouter()

        let presenter = EmailRegPresenter()
        presenter.view = viewController
        presenter.router = router

        let interactor = EmailRegInteractor()
        interactor.output = presenter

        presenter.interactor = interactor
        viewController.output = presenter
    }

}
