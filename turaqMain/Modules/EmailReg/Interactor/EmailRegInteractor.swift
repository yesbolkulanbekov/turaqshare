//
//  EmailRegEmailRegInteractor.swift
//  turaQshare
//
//  Created by Yesbol Kulanbekov on 20/12/2018.
//  Copyright © 2018 turaQshare. All rights reserved.
//

import Foundation

class EmailRegInteractor: EmailRegInteractorInput {

    let api = TuraqAPI.shared
    
    let ref  = FirebaseServices.shared.ref
    let auth = FirebaseServices.shared.auth

    weak var output: EmailRegInteractorOutput!
    
    func register(with newUser: User.New) {
        api.register(newUser, onSuccess: { [weak self] (user: User) in
            guard let strongSelf = self else { return }
            guard let output = strongSelf.output else { return }
            
            DispatchQueue.main.async {
                output.didRegisterSuccessfully()
            }
            
            }, onFailure: { [weak self] (error) in
                guard let strongSelf = self else { return }
                guard let output = strongSelf.output else { return }
                
                DispatchQueue.main.async {
                    output.didFailRegistering(with: error)
                }
        })
    }
    
}

extension EmailRegInteractor {

    func enterData(of user: User) {
        
        guard let uid = auth.currentUser?.uid else {
            // TODO: handle no user id
            return
        }

        let usersRef = self.ref.child("users").child(uid)
        let values = ["car_number": "user.carNumber",
                      "name": "user.name",
                      "email": user.email ?? ""]

        usersRef.updateChildValues(values, withCompletionBlock: { [weak self] (error, ref) in
            guard let strongSelf = self else { return }
            guard let output = strongSelf.output else { return }

            guard let error = error else {
                output.updateDidSucceed()
                return
            }
            
            output.updateDidFail(with: error)

        })
        
    }

}
