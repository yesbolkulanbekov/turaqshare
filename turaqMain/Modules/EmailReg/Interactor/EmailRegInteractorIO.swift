//
//  EmailRegEmailRegInteractorIO.swift
//  turaQshare
//
//  Created by Yesbol Kulanbekov on 20/12/2018.
//  Copyright © 2018 turaQshare. All rights reserved.
//

import Foundation

protocol EmailRegInteractorInput {
    func enterData(of user: User)
    func register(with newUser: User.New)

}

protocol EmailRegInteractorOutput: class {
    func updateDidFail(with error: Error)
    func updateDidSucceed()
    
    func didRegisterSuccessfully()
    func didFailRegistering(with error: TuraqError)
}
