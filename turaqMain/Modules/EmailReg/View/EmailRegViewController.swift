//
//  EmailRegEmailRegViewController.swift
//  turaQshare
//
//  Created by Yesbol Kulanbekov on 20/12/2018.
//  Copyright © 2018 turaQshare. All rights reserved.
//

import UIKit

typealias RegVCProps = EmailRegViewController.Props

class EmailRegViewController: UIViewController {
    
    struct Props {
        let rootViewProps: EmailViewProps
    }
    
    let spinnerView   = UIActivityIndicatorView(style: .gray)
    let emailRegView  = EmailRegView()

    var output: EmailRegViewOutput!

    // MARK: Life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        output.viewIsReady()
    }
    
    var isDataEnteringInProgress: Bool = false {
        willSet {
            switch newValue {
            case true:
                spinnerView.startAnimating()
            case false:
                spinnerView.stopAnimating()
            }
        }
    }

}

extension EmailRegViewController:  EmailRegViewInput {

    // MARK: EmailRegViewInput
    func setupInitialState() {
        setupInitViewState()
    }
    
    func renderProps(with props: Props) {
        emailRegView.render(with: props.rootViewProps)
        
        let enterDataAction = props.rootViewProps.enterDataAction
        self.isDataEnteringInProgress = enterDataAction.isInProgress
        
    }

}

extension EmailRegViewController {
    private func setupInitViewState() {
        title = "Регистрация"
        navigationController?.isNavigationBarHidden = false
        view.backgroundColor = .white
        view.addSubview(emailRegView)
        emailRegView.addConstraints(equalToSafeArea(superView: view))
        view.addSubview(spinnerView)
        spinnerView.addConstraints([
            equal(view, \UIView.safeAreaLayoutGuide.centerXAnchor),
            equal(view, \UIView.safeAreaLayoutGuide.centerYAnchor)
        ])
    }
}
