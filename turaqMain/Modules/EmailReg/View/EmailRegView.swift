//
//  EmailRegView.swift
//  turaQshare
//
//  Created by Yesbol Kulanbekov on 12/20/18.
//  Copyright © 2018 turaQshare. All rights reserved.
//



import UIKit


class EmailRegView: UIScrollView {
    
    var props: Props = Props() {
        didSet {
            let enterDataAction = props.enterDataAction
            let isPossible = enterDataAction.possible != nil
            
            self.subViews.enterBTN.isEnabled    = isPossible
            self.subViews.nameTF.isEnabled      = isPossible
            self.subViews.emailTF.isEnabled     = isPossible
            self.subViews.carNumberTF.isEnabled = isPossible

            self.alpha = enterDataAction.isInProgress ? 0.3 : 1
        }
    }
    
    struct Subviews {
        let contentView          = UIView()
        let turaqLogo            = UIImageView()
        let backGroundView       = UIImageView()
        
        let nameTF            = UITextField()
        let secondNameTF      = UITextField()
        let phone             = UITextField()
        let passwordTF        = UITextField()
        let confirmPasswordTF = UITextField()
        let carNumberTF       = UITextField()
        let emailTF           = UITextField()
        let enterBTN          = UIButton(type: .system)
    }
    
    let subViews = EmailRegView.Subviews()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupSelf()
        configureSubViews()
        styleSubviews()
        renderConstantData()
        render(with: Props())
        layout()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
    }
}


/// Props configuration and init

typealias EmailViewProps = EmailRegView.Props
typealias EnterDataAction = EmailRegView.Props.EnterDataAction

extension EmailRegView {
    struct Props {
        let enterDataAction: EnterDataAction

        enum EnterDataAction {
            case disabled
            case inProgress
            case possible(CommandWith<User.New>)
            
            var possible: CommandWith<User.New>? {
                guard case let .possible(command) = self else { return nil }
                return command
            }
            
            var isDisabled: Bool {
                guard case .disabled = self else { return false }
                return true
            }
            
            var isInProgress: Bool {
                guard case .inProgress = self else { return false }
                return true
            }
        }
    }
}

extension EmailRegView.Props {
    init() {
        self.enterDataAction = .possible(CommandWith<User.New> { _ in })
    }
}

/// View configuration, content and styling

extension EmailRegView: UITextFieldDelegate {
    
    private func setupSelf() {
        translatesAutoresizingMaskIntoConstraints = false
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(dissMissKeyboard))
        addGestureRecognizer(tapGesture)
        alwaysBounceVertical = true
    }
    
    private func configureSubViews() {
        
        subViews.nameTF.inputAccessoryView = keyboardToolbar()
        subViews.secondNameTF.inputAccessoryView = keyboardToolbar()
        subViews.phone.inputAccessoryView = keyboardToolbar()
        subViews.emailTF.inputAccessoryView = keyboardToolbar()
        subViews.passwordTF.inputAccessoryView = keyboardToolbar()
        subViews.confirmPasswordTF.inputAccessoryView = keyboardToolbar()
        subViews.carNumberTF.inputAccessoryView = keyboardToolbar()
        
        subViews.nameTF.delegate = self
        subViews.secondNameTF.delegate = self
        subViews.phone.delegate = self
        subViews.emailTF.delegate = self
        subViews.passwordTF.delegate = self
        subViews.confirmPasswordTF.delegate = self
        subViews.carNumberTF.delegate = self
        
        subViews.phone.keyboardType = .phonePad
        subViews.emailTF.keyboardType = .emailAddress
        subViews.passwordTF.isSecureTextEntry = true
        subViews.confirmPasswordTF.isSecureTextEntry = true
        subViews.enterBTN.addTarget(self, action: #selector(enterData), for: .touchUpInside)
        
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        self.contentInset.bottom = 300
    }
    

    
    func rectangle() -> UIView {
        let leftViewPhone = UIView(frame: CGRect(x: 0, y: 0, width: 15, height: 15))
        leftViewPhone.backgroundColor = .whiteTwo
        return leftViewPhone
    }
    
    func styleSubviews() {
        subViews.nameTF.backgroundColor            = UIColor.whiteTwo
        subViews.secondNameTF.backgroundColor      = UIColor.whiteTwo
        subViews.phone.backgroundColor             = UIColor.whiteTwo
        subViews.emailTF.backgroundColor           = UIColor.whiteTwo
        subViews.passwordTF.backgroundColor        = UIColor.whiteTwo
        subViews.confirmPasswordTF.backgroundColor = UIColor.whiteTwo
        subViews.carNumberTF.backgroundColor       = UIColor.whiteTwo
        
        subViews.nameTF.font            = UIFont.LucidaGrandeRegular(ofSize: 16)
        subViews.secondNameTF.font      = UIFont.LucidaGrandeRegular(ofSize: 16)
        subViews.phone.font             = UIFont.LucidaGrandeRegular(ofSize: 16)
        subViews.emailTF.font           = UIFont.LucidaGrandeRegular(ofSize: 16)
        subViews.passwordTF.font        = UIFont.LucidaGrandeRegular(ofSize: 16)
        subViews.confirmPasswordTF.font = UIFont.LucidaGrandeRegular(ofSize: 16)
        subViews.carNumberTF.font       = UIFont.LucidaGrandeRegular(ofSize: 16)
        

        subViews.nameTF.textColor            = UIColor.pine
        subViews.secondNameTF.textColor      = UIColor.pine
        subViews.phone.textColor             = UIColor.pine
        subViews.emailTF.textColor           = UIColor.pine
        subViews.passwordTF.textColor        = UIColor.pine
        subViews.confirmPasswordTF.textColor = UIColor.pine
        subViews.carNumberTF.textColor       = UIColor.pine
        
        subViews.nameTF.leftView            = rectangle()
        subViews.secondNameTF.leftView      = rectangle()
        subViews.phone.leftView             = rectangle()
        subViews.emailTF.leftView           = rectangle()
        subViews.passwordTF.leftView        = rectangle()
        subViews.confirmPasswordTF.leftView = rectangle()
        subViews.carNumberTF.leftView       = rectangle()
        
        
        subViews.nameTF.leftViewMode            = .always
        subViews.secondNameTF.leftViewMode      = .always
        subViews.phone.leftViewMode             = .always
        subViews.emailTF.leftViewMode           = .always
        subViews.passwordTF.leftViewMode        = .always
        subViews.confirmPasswordTF.leftViewMode = .always
        subViews.carNumberTF.leftViewMode       = .always



        subViews.enterBTN.setTitleColor(UIColor.denim, for: .normal)
        subViews.enterBTN.titleLabel?.font = UIFont.LucidaGrandeBold(ofSize: 16)
        subViews.enterBTN.backgroundColor = UIColor.blueGreen.withAlphaComponent(0.08)
    }
    
    func render(with props: EmailRegView.Props) {
        self.props = props
    }
    
    func renderConstantData() {
        subViews.turaqLogo.image      = UIImage(named: "logo")
        subViews.backGroundView.image = UIImage(named: "backgroundfig")
        
        subViews.nameTF.placeholder            = "Ваше имя"
        subViews.secondNameTF.placeholder      = "Ваша фамилия"
        subViews.phone.placeholder             = "+ 7 (70X) XXX XX XX"
        subViews.emailTF.placeholder           = "enter@youremail.com"
        subViews.passwordTF.placeholder        = "Пароль"
        subViews.confirmPasswordTF.placeholder = "Подтвердить пароль"
        subViews.carNumberTF.placeholder       = "001 | KLM | 01"
        
        subViews.enterBTN.setTitle("Зарегистрироваться", for: .normal)
    }
    
    func layout() {
        EmailRegViewLayout(rootView: self).paint()
    }
    
}


extension EmailRegView {
    
    func keyboardToolbar() -> UIToolbar {
        let keyboardToolbar = UIToolbar()
        keyboardToolbar.sizeToFit()
        keyboardToolbar.isTranslucent = false
        keyboardToolbar.barTintColor = UIColor.white
        
        
        let addButton = UIBarButtonItem (
            title: "Готово",
            style: .done,
            target: self,
            action: #selector(doneEditing)
        )
        
        addButton.tintColor = UIColor.black
        
        let flexibleSpace = UIBarButtonItem(barButtonSystemItem: .flexibleSpace,
                                            target: self,
                                            action: nil)
        
        keyboardToolbar.setItems([flexibleSpace,addButton], animated: true)
        return keyboardToolbar
    }
    
    @objc func doneEditing() {
        self.endEditing(true)
        self.contentInset.bottom = 0
    }
    
    @objc
    func dissMissKeyboard() {
        self.endEditing(true)
        self.contentInset.bottom = 0
    }
    
    @objc
    func enterData() {

        
        let newUser = User.New.init(firstName: subViews.nameTF.text!,
                                    secondName: subViews.secondNameTF.text!,
                                    phone: subViews.phone.text!,
                                    email: subViews.emailTF.text!,
                                    password: subViews.passwordTF.text!,
                                    confirmPassword: subViews.confirmPasswordTF.text!)
        
        guard let enterUser = props.enterDataAction.possible else { return }
        enterUser.perform(with: newUser)
    }
    
}


/// View layout

struct EmailRegViewLayout {
    
    var rootView: EmailRegView
    var sv: EmailRegView.Subviews
    
    init(rootView: EmailRegView) {
        self.rootView = rootView
        self.sv = rootView.subViews
    }
    
    func paint() {
        addSubViews()
        addConstraints()
    }
    
    func addSubViews() {
        rootView.addSubview(sv.contentView)
        sv.contentView.addSubview(sv.turaqLogo)
        sv.contentView.addSubview(sv.backGroundView)
        sv.contentView.addSubview(regStack)
        
        regStack.addArrangedSubview(sv.nameTF)
        regStack.addArrangedSubview(sv.secondNameTF)
        regStack.addArrangedSubview(sv.phone)
        regStack.addArrangedSubview(sv.emailTF)
        regStack.addArrangedSubview(sv.passwordTF)
        regStack.addArrangedSubview(sv.confirmPasswordTF)
    
        sv.contentView.addSubview(sv.enterBTN)

    }
    
    func addConstraints() {
        sv.contentView.addConstraints(equalTo(superView: rootView))
        
        sv.contentView.addConstraints([
            equal(rootView, \UIView.widthAnchor),
            //equal(\.heightAnchor, to: UIScreen.main.bounds.height + 110)
            equal(sv.enterBTN,
                  \UIView.bottomAnchor,
                  \UIView.bottomAnchor,
                  constant: 40)
        ])
        
        sv.turaqLogo.addConstraints([
            equal(sv.contentView, \UIView.topAnchor, constant: 30),
            equal(sv.contentView, \UIView.leadingAnchor, constant: 20)
        ])
        
        sv.backGroundView.addConstraints([
            equal(sv.contentView, \UIView.topAnchor, constant: 23),
            equal(sv.contentView, \UIView.trailingAnchor),
        ])
        
        regStack.addConstraints([
            equal(sv.contentView, \UIView.safeAreaLayoutGuide.leadingAnchor, constant: 20),
            equal(sv.contentView, \UIView.safeAreaLayoutGuide.trailingAnchor, constant: -20),
            equal(sv.turaqLogo,
                  \UIView.safeAreaLayoutGuide.topAnchor,
                  \UIView.safeAreaLayoutGuide.bottomAnchor,
                  constant: 40)
        ])
        sv.nameTF.addConstraints([
            equal(\UIView.heightAnchor, to: 46)
        ])

        sv.enterBTN.addConstraints([
            equal(sv.contentView, \UIView.centerXAnchor),
            equal(\.widthAnchor, to: 180),
            equal(\.heightAnchor, to: 46),
            equal(regStack,
                  \UIView.topAnchor,
                  \UIView.bottomAnchor,
                  constant: 38)
        ])
        
    
    }
    
    let regStack = UIStackView() { stack in
        stack.axis = .vertical
        stack.distribution = .fillEqually
        stack.alignment = .fill
        stack.spacing = 15
    }
    
    
}
