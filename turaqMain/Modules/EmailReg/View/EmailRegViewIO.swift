//
//  EmailRegEmailRegViewIO.swift
//  turaQshare
//
//  Created by Yesbol Kulanbekov on 20/12/2018.
//  Copyright © 2018 turaQshare. All rights reserved.
//

protocol EmailRegViewOutput {

    /**
        @author Yesbol Kulanbekov
        Notify presenter that view is ready
    */

    func viewIsReady()
}


protocol EmailRegViewInput: class, DisplaysAlert, HasViewController {

    /**
        @author Yesbol Kulanbekov
        Setup initial state of the view
    */
    func renderProps(with props: RegVCProps) 

    func setupInitialState()
}
