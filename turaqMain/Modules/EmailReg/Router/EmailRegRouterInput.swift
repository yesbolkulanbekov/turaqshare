//
//  EmailRegEmailRegRouterInput.swift
//  turaQshare
//
//  Created by Yesbol Kulanbekov on 20/12/2018.
//  Copyright © 2018 turaQshare. All rights reserved.
//

import UIKit

protocol EmailRegRouterInput {
    func openTabBar(from vc: UIViewController)
}
