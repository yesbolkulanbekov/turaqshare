//
//  EmailRegEmailRegRouter.swift
//  turaQshare
//
//  Created by Yesbol Kulanbekov on 20/12/2018.
//  Copyright © 2018 turaQshare. All rights reserved.
//
import UIKit

class EmailRegRouter: EmailRegRouterInput {
    func openTabBar(from vc: UIViewController) {
        let tabBarVC = TabBarViewController()
        let delegate = UIApplication.shared.delegate as! AppDelegate
        delegate.window?.rootViewController = tabBarVC
    }
}
