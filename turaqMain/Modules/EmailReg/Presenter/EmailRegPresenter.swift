//
//  EmailRegEmailRegPresenter.swift
//  turaQshare
//
//  Created by Yesbol Kulanbekov on 20/12/2018.
//  Copyright © 2018 turaQshare. All rights reserved.
//

class EmailRegPresenter: EmailRegModuleInput {

    weak var view: EmailRegViewInput!
    var interactor: EmailRegInteractorInput!
    var router: EmailRegRouterInput!

}

extension EmailRegPresenter: EmailRegViewOutput {
    func viewIsReady() {
        view.setupInitialState()
        view.renderProps(with: actionPossibleProps())
    }
}

extension EmailRegPresenter: EmailRegInteractorOutput {
    func didRegisterSuccessfully() {
        print("Registration did succeed")
        view.renderProps(with: actionPossibleProps())
        router.openTabBar(from: view.getVC())
    }
    
    func didFailRegistering(with error: TuraqError) {
        view.renderProps(with: actionPossibleProps())
        view.displayAlert(with: error.localizedDescription)
    }
    
    func updateDidFail(with error: Error) {
        view.renderProps(with: actionPossibleProps())
        view.displayAlert(with: error.localizedDescription)
    }
    
    func updateDidSucceed() {
        print("Update did succeed")
        view.renderProps(with: actionPossibleProps())
        router.openTabBar(from: view.getVC())
    }

}

// TODO: Add validator

extension EmailRegPresenter {
    
    
    func actionPossibleProps() -> RegVCProps {
        let enterDataCommand = CommandWith { (newUser: User.New) in
            
            // Validate
            switch "" {
            case newUser.firstName,
                 newUser.secondName,
                 newUser.phone,
                 newUser.email,
                 newUser.password,
                 newUser.confirmPassword:
                self.view.displayAlert(with: "Пожалуйста, заполните все поля")
                return
            default:
                break
            }
            
            self.view.renderProps(with: self.actionInProgressProps())
            self.interactor.register(with: newUser)
        }
        
        // TODO: change call order in all commands:
        // first render the call async interactor
        
        let enterDataAction = EnterDataAction.possible(enterDataCommand)
        let subviewProps = EmailViewProps(enterDataAction: enterDataAction)
        let vcProps = RegVCProps(rootViewProps: subviewProps)
        return vcProps
    }
    
    
    func actionInProgressProps() -> RegVCProps {
        let enterInProgress = EnterDataAction.inProgress
        let subviewProps = EmailViewProps(enterDataAction: enterInProgress)
        let vcProps = RegVCProps(rootViewProps: subviewProps)
        return vcProps
    }
    
}
