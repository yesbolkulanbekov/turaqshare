//
//  TSettingsTSettingsPresenter.swift
//  turaQshare
//
//  Created by Yesbol Kulanbekov on 01/02/2019.
//  Copyright © 2019 turaQshare. All rights reserved.
//

class TSettingsPresenter: TSettingsModuleInput {

    weak var view: TSettingsViewInput!
    var interactor: TSettingsInteractorInput!
    var router: TSettingsRouterInput!

}

extension TSettingsPresenter: TSettingsViewOutput {
    func viewIsReady() {
        view.setupInitialState()
        let empty = settingsVCProps()
        view.render(with: empty)

        interactor.getUser()
    }
    
    func viewWillAppear() {
        interactor.getUser()
    }
}

extension TSettingsPresenter: TSettingsInteractorOutput {
    func didLoad(_ user: User) {
        view.render(with: settingsVCProps(with: user))

    }
    
    func didLoadUserFail(with error: Error) {
        view.displayAlert(with: error.localizedDescription)
    }
    
    
    func settingsVCProps(with user: User! = nil) -> SettingsVCProps {
        
        let onSelTerms = Command {
            self.router.openPDF(from: self.view.getVC())
        }
        
        let onSelSignout = Command {
            self.router.signOut(from: self.view.getVC())
        }
        

        var secondSectionCells = [
            //SettingsTVCellProps(field: "Номер телефона"),
            //SettingsTVCellProps(field: "Адрес"),
            //SettingsTVCellProps(field: "Payments"),
            //SettingsTVCellProps(field: ""),
            //SettingsTVCellProps(field: "Support"),
            //SettingsTVCellProps(field: "Share"),
            SettingsTVCellProps(field: "Условия и положения", selectAct: onSelTerms),
            SettingsTVCellProps(field: "Выйти", selectAct: onSelSignout)

        ]
        
        var title = ""

        if let user = user {
            title = user.firstName + " " + user.secondName + "\n" + user.email
            let numberCell = SettingsTVCellProps(field: "Номер телефона", detail:user.phone)
            secondSectionCells.insert(numberCell, at: 0)
        }
        
        let firstSectionCells = [SettingsTVCellProps(field: title)]

        
        let sections = [
            SettingsTVSectionProps(
                title: "Section1",
                cells: firstSectionCells
            ),
            SettingsTVSectionProps(
                title: "Section2",
                cells: secondSectionCells
            )
        ]
        let tvProps = SettingsTVProps(sections: sections)
        let vcProps = SettingsVCProps(tableProps: tvProps)
        return vcProps
    }
    
}
