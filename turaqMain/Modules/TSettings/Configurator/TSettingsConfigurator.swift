//
//  TSettingsTSettingsConfigurator.swift
//  turaQshare
//
//  Created by Yesbol Kulanbekov on 01/02/2019.
//  Copyright © 2019 turaQshare. All rights reserved.
//

import UIKit

struct TSettingsModuleConfigurator {

    func assembleModule() -> TSettingsViewController {
        let viewController = TSettingsViewController()
        configure(viewController: viewController)
        return viewController
    }

    private func configure(viewController: TSettingsViewController) {

        let router = TSettingsRouter()

        let presenter = TSettingsPresenter()
        presenter.view = viewController
        presenter.router = router

        let interactor = TSettingsInteractor()
        interactor.output = presenter

        presenter.interactor = interactor
        viewController.output = presenter
    }

}
