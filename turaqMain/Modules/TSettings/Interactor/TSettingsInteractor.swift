//
//  TSettingsTSettingsInteractor.swift
//  turaQshare
//
//  Created by Yesbol Kulanbekov on 01/02/2019.
//  Copyright © 2019 turaQshare. All rights reserved.
//

import Foundation

class TSettingsInteractor: TSettingsInteractorInput {

    weak var output: TSettingsInteractorOutput!
    
    
    func getUser() {
        
        let onSuccess = {(user: User) in
            DispatchQueue.main.async {
                self.output.didLoad(user)
            }
        }
        
        let onFailure = { (error: Error) in
            DispatchQueue.main.async {
                self.output.didLoadUserFail(with: error)
            }
        }
        
        TuraqAPI.shared.getUser(onSuccess: onSuccess, onFailure: onFailure)
    }

}
