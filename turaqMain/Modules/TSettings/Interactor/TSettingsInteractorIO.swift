//
//  TSettingsTSettingsInteractorIO.swift
//  turaQshare
//
//  Created by Yesbol Kulanbekov on 01/02/2019.
//  Copyright © 2019 turaQshare. All rights reserved.
//

import Foundation

protocol TSettingsInteractorInput {
    func getUser()
}

protocol TSettingsInteractorOutput: class {
    func didLoad(_ user: User)
    func didLoadUserFail(with error: Error)
}


