//
//  TSettingsTSettingsRouterInput.swift
//  turaQshare
//
//  Created by Yesbol Kulanbekov on 01/02/2019.
//  Copyright © 2019 turaQshare. All rights reserved.
//

import UIKit

protocol TSettingsRouterInput {
    func openPDF(from vc: UIViewController)
    func signOut(from vc: UIViewController)
}
