//
//  TSettingsTSettingsRouter.swift
//  turaQshare
//
//  Created by Yesbol Kulanbekov on 01/02/2019.
//  Copyright © 2019 turaQshare. All rights reserved.
//

import UIKit

class TSettingsRouter: TSettingsRouterInput {
    func openPDF(from vc: UIViewController) {
        let pdfVC = PDFViewController()
        vc.navigationController?.pushViewController(pdfVC, animated: true)
    }
    
    func signOut(from vc: UIViewController) {
        
        
        UserDefaultsUserStorageService.shared.removeToken()
        
        let loginVC = LoginViewModuleConfigurator().assembleModule()
        let navigationController = UINavigationController(rootViewController: loginVC)
        
        guard let window = UIApplication.shared.keyWindow else {
            return
        }
        
        window.rootViewController = navigationController
        
        
        
        
        
    }
}
