//
//  TSettingsTSettingsViewIO.swift
//  turaQshare
//
//  Created by Yesbol Kulanbekov on 01/02/2019.
//  Copyright © 2019 turaQshare. All rights reserved.
//

protocol TSettingsViewOutput {

    /**
        @author Yesbol Kulanbekov
        Notify presenter that view is ready
    */

    func viewIsReady()
    func viewWillAppear()
}


protocol TSettingsViewInput: class, HasViewController, DisplaysAlert {

    /**
        @author Yesbol Kulanbekov
        Setup initial state of the view
    */

    func setupInitialState()
    func render(with props: SettingsVCProps)
}
