//
//  TSettingsTSettingsViewController.swift
//  turaQshare
//
//  Created by Yesbol Kulanbekov on 01/02/2019.
//  Copyright © 2019 turaQshare. All rights reserved.
//

import UIKit

typealias SettingsVCProps = TSettingsViewController.Props
typealias SettingsTVProps = TSettingsViewController.SettingsTableProps
typealias SettingsTVSectionProps = TSettingsViewController.SettingsTableProps.Section
typealias SettingsTVCellProps = TSettingsViewController.SettingsTableProps.Cell


extension TSettingsViewController {
    struct Props {
        let tableProps: SettingsTableProps
    }
    
    struct SettingsTableProps {
        
        let sections: [Section]
        
        struct Section {
            let title: String
            let cells: [Cell]
        }
        
        struct Cell {
            let field: String
            let detailText: String
            let selectAct: Command
        }
        
        static func emptyTable() -> TSettingsViewController.SettingsTableProps {
            let emptyCells = [TSettingsViewController.SettingsTableProps.Cell(field: "-")]
            let emptySections = [TSettingsViewController.SettingsTableProps.Section(
                title: "-",
                cells: emptyCells
                )]
            let props = TSettingsViewController.SettingsTableProps(sections: emptySections)
            return props
        }
    
    }
    
    func render(with props: SettingsVCProps) {
        self.tableProps = props.tableProps
    }
}

extension TSettingsViewController.SettingsTableProps.Cell {
    init(field: String) {
        self.init(field: field, detailText: "", selectAct: .nop)
    }
    
    init(field: String, detail: String) {
        self.init(field: field, detailText: detail, selectAct: .nop)
    }
    
    init(field: String, selectAct: Command) {
        self.init(field: field, detailText: "", selectAct: selectAct)
    }
}

class TSettingsViewController: UIViewController {
    
    var tableProps = SettingsTVProps.emptyTable() {
        didSet {
            settingsTV.reloadData()
        }
    }

    var output: TSettingsViewOutput!
    
    let settingsTV  = UITableView(frame: .zero, style: .plain)
    let spinnerView = UIActivityIndicatorView(style: .gray)

    // MARK: Life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .white
        output.viewIsReady()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        output.viewWillAppear()
    }

}


extension TSettingsViewController:  TSettingsViewInput {

    // MARK: TSettingsViewInput
    func setupInitialState() {
        setupInitViewState()
        setupTableView()
    }

}


extension TSettingsViewController {
    
    func setupInitViewState() {
        navigationItem.title = "Настройки"
        view.backgroundColor = .white
        view.addSubview(settingsTV)
        settingsTV.addConstraints(equalToSafeArea(superView: view, with: .zero))
        
        view.addSubview(spinnerView)
        spinnerView.addConstraints([
            equal(view, \UIView.safeAreaLayoutGuide.centerXAnchor),
            equal(view, \UIView.safeAreaLayoutGuide.centerYAnchor)]
        )
        
    }

    private func setupTableView() {
        settingsTV.dataSource = self
        settingsTV.delegate = self
        
        settingsTV.tableFooterView = UIView()
    }
}

extension TSettingsViewController: UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return tableProps.sections.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return tableProps.sections[section].cells.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let
        cell = UITableViewCell(style: UITableViewCell.CellStyle.value1, reuseIdentifier: "")
        cell.textLabel?.numberOfLines = 0
        
        let sepStyle = UITableViewCell.SeparatorStyle.singleLine
        tableView.separatorStyle = sepStyle
        cell.separatorInset = UIEdgeInsets(top: 0, left: 20, bottom: 0, right: 20)
        cell.selectionStyle = .none
        
        cell.contentView.layoutMargins = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 20)
        let field = tableProps.sections[indexPath.section].cells[indexPath.row].field
        var endString = NSAttributedString(string: field)
        
        if field == "Условия и положения" {
            cell.accessoryType = .disclosureIndicator
        }
        
        let secondSection = 1
        let lastRowInSecondSection = (tableView.numberOfRows(inSection: secondSection) - 1 )
        if indexPath.row == lastRowInSecondSection {
            let atrs = [NSAttributedString.Key.foregroundColor: UIColor.red]
            endString = NSAttributedString(string: field, attributes: atrs)
        }

        cell.textLabel?.attributedText = endString

        let phone = tableProps.sections[indexPath.section].cells[indexPath.row].detailText
        if !phone.isEmpty {
            let atrs = [NSAttributedString.Key.foregroundColor: UIColor.blueGreen]
            let atrPhone = NSAttributedString(string: phone, attributes: atrs)
            cell.detailTextLabel?.attributedText = atrPhone
        }

        if indexPath.section ==  0 {
            cell.imageView?.image =  UIImage(named: "tick")
        }
        
        
        
        return cell
    }
    
    
}

extension TSettingsViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        let backView = UIView()
        backView.backgroundColor = UIColor.pine.withAlphaComponent(0.1)
        return backView
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return (section == 0) ? 30 : 0
    }

    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let cell = tableProps.sections[indexPath.section].cells[indexPath.row]
        cell.selectAct.perform()
    }
}
