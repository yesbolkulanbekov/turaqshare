//
//  PDFViewController.swift
//  turaQshare
//
//  Created by Yesbol Kulanbekov on 3/9/19.
//  Copyright © 2019 turaQshare. All rights reserved.
//

import UIKit
import PDFKit

class PDFViewController: UIViewController {
    
    var pdfView = PDFView()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationItem.title = "Условия и положения"
        view.addSubview(pdfView)
        pdfView.addConstraints(equalToSafeArea(superView: view))
        
        if let path = Bundle.main.path(forResource: "terms", ofType: "pdf") {
            if let pdfDocument = PDFDocument(url: URL(fileURLWithPath: path)) {
                pdfView.displayMode = .singlePageContinuous
                pdfView.autoScales = true
                pdfView.displayDirection = .vertical
                pdfView.document = pdfDocument
            }
        }
    }
}
