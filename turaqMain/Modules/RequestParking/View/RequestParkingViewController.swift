//
//  RequestParkingRequestParkingViewController.swift
//  turaQshare
//
//  Created by Yesbol Kulanbekov on 09/01/2019.
//  Copyright © 2019 turaQshare. All rights reserved.
//

import UIKit

class RequestParkingViewController: UIViewController {

    var output: RequestParkingViewOutput!
    
    // MARK: Subviews
    let spinnerView = UIActivityIndicatorView(style: .gray)
    let requestParkingView = RequestParkView()

    // MARK: Life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .white
        output.viewIsReady()
    }

}

extension RequestParkingViewController:  RequestParkingViewInput {

    // MARK: RequestParkingViewInput
    func setupInitialState() {
        setupInitViewState()
    }

}


extension RequestParkingViewController {
    private func setupInitViewState() {
        navigationItem.title = "Марат #245"
        view.backgroundColor = .white
        view.addSubview(requestParkingView)
        requestParkingView.addConstraints(equalToSafeArea(superView: view, with: .zero))
        
        view.addSubview(spinnerView)
        spinnerView.addConstraints([
            equal(view, \UIView.safeAreaLayoutGuide.centerXAnchor),
            equal(view, \UIView.safeAreaLayoutGuide.centerYAnchor)
            ])
    }
    
}
