//
//  RequestParkView.swift
//  turaQshare
//
//  Created by Yesbol Kulanbekov on 1/9/19.
//  Copyright © 2019 turaQshare. All rights reserved.
//

import UIKit

class RequestParkView: UIScrollView {
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupSelf()
        configureSubViews()
        styleSubviews()
        renderConstantData()
        layout()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
    }
    
    /// Properties
    
    var sv = RequestParkView.Subviews()
    
    var props = Props() {
        didSet {
            
        }
    }

    
    
    @objc func injected() {
        #if DEBUG
        
                
        sv.contentView.removeFromSuperview()
        sv.chooseDateInfoLbl.removeFromSuperview()
        sv.slider.removeFromSuperview()
        sv.datesLbl.removeFromSuperview()
        sv.totalSumInfoLbl.removeFromSuperview()
        sv.totalSumLbl.removeFromSuperview()

        sv.addressInfoLbl.removeFromSuperview()
        sv.addressLbl.removeFromSuperview()

        sv.toEmailBtn.removeFromSuperview()
        sv.payBtn.removeFromSuperview()

        sv.totalSumStack.removeFromSuperview()
        sv.bonusStack1.removeFromSuperview()
        sv.bonusStack2.removeFromSuperview()
        sv.tarifStack.removeFromSuperview()
        sv.tableStack.removeFromSuperview()

        sv.addressStack.removeFromSuperview()
        sv.buttonStack.removeFromSuperview()
        
        subviews.forEach{ $0.removeFromSuperview() }
        
        sv = RequestParkView.Subviews()
        styleSubviews()
        renderConstantData()
        layout()
        #endif
        
    }
    
}

/// View configuration, content and styling

extension RequestParkView {
    
    struct Props {
        
    }
    
    struct Subviews {
        let contentView       = UIView()
        let chooseDateInfoLbl = UILabel()
        let slider            = UISlider()
        let datesLbl          = UIButton(type: .system)
        let totalSumInfoLbl   = UILabel()
        let totalSumLbl       = UILabel()
        
        let addressInfoLbl    = UILabel()
        let addressLbl        = UILabel()
        
        let toEmailBtn        = UIButton(type: .system)
        let payBtn            = UIButton(type: .system)
        
        var totalSumStack     = UIStackView()
        var bonusStack1       = ThreeLabelStack()
        var bonusStack2       = ThreeLabelStack()
        var tarifStack        = ThreeLabelStack()
        let tableStack        = UIStackView()
        
        let addressStack      = UIStackView()
        let buttonStack       = UIStackView()
    }
    
    func renderConstantData() {
        sv.chooseDateInfoLbl.text = "Выберите даты и время"
        sv.datesLbl.setTitle("12 Октябрь 2019", for: .normal)
        sv.totalSumInfoLbl.text   = "Общий счет"
        sv.addressInfoLbl.text    = "Адрес"
        sv.addressLbl.text        = "Достык, 13\nЕсиль район, Астана, Z05H9H8"
        sv.totalSumLbl.text       = "T3220"
        
        sv.bonusStack1.setTexts("Бонус", "5 часов", "T200")
        sv.bonusStack2.setTexts("Бонус", "2 дня", "T200")
        sv.tarifStack.setTexts("Тариф", "5 часов", "3920")
        
        sv.toEmailBtn.setTitle("Счет на почту", for: .normal)
        sv.payBtn.setTitle("Оплатить", for: .normal)
    }
    
    func styleSubviews() {
        
        sv.slider.minimumTrackTintColor = .blueGreen
        sv.slider.thumbTintColor = .blueGreen

        sv.chooseDateInfoLbl.textColor = .pine
        sv.chooseDateInfoLbl.font = UIFont.LucidaGrandeBold(ofSize: 18)
        
        sv.datesLbl.titleLabel?.textAlignment = .center
        sv.datesLbl.setTitleColor(UIColor.pine, for: .normal)
        sv.datesLbl.titleLabel?.font = UIFont.LucidaGrandeBold(ofSize: 14)
        sv.datesLbl.backgroundColor = .white
        
        sv.totalSumInfoLbl.textColor = .pine
        sv.totalSumInfoLbl.font = UIFont.LucidaGrandeBold(ofSize: 18)
        sv.totalSumLbl.textColor = UIColor.blueGreen

        sv.addressInfoLbl.textColor = .pine
        sv.addressInfoLbl.font = UIFont.LucidaGrandeBold(ofSize: 18)

        sv.addressLbl.numberOfLines = 0
        sv.addressLbl.textColor = .pine
        sv.addressLbl.font = UIFont.NunitoLight(ofSize: 14)
        
        sv.toEmailBtn.setTitleColor(UIColor.pine, for: .normal)
        sv.toEmailBtn.titleLabel?.font = UIFont.LucidaGrandeBold(ofSize: 18)
        sv.toEmailBtn.backgroundColor = UIColor.pine.withAlphaComponent(0.08)
        
        sv.payBtn.setTitleColor(UIColor.blueGreen, for: .normal)
        sv.payBtn.titleLabel?.font = UIFont.LucidaGrandeBold(ofSize: 18)
        sv.payBtn.backgroundColor = UIColor.blueGreen.withAlphaComponent(0.08)
    }
    
    private func configureSubViews() {
        sv.slider.maximumValue = 100
        sv.slider.minimumValue = 0
        sv.slider.setValue(50, animated: false)
    }
    
    private func setupSelf() {
        translatesAutoresizingMaskIntoConstraints = false
    }
}


extension RequestParkView {
    
    func render(with props: RequestParkView.Props) {
        self.props = props
    }
    
    func layout() {
        RequestParkViewLayout(for: self).paint()
    }
    
}



/// View layout

struct RequestParkViewLayout {
    
    var rootView: RequestParkView
    var sv: RequestParkView.Subviews
    var conv: UIView
    
    init(for rootView: RequestParkView) {
        self.rootView = rootView
        self.sv = rootView.sv
        self.conv = rootView.sv.contentView
    }
    
    func paint() {
        configureStacks()
        addSubViews()
        addConstraints()
    }
    
    func configureStacks() {
        sv.totalSumStack.axis = .horizontal
        sv.totalSumStack.distribution = .equalSpacing
        
        sv.tableStack.axis = .vertical
        sv.tableStack.distribution = .fill
        sv.tableStack.spacing = 10
        
        sv.addressStack.axis = .vertical
        sv.addressStack.distribution = .fill
        sv.addressStack.alignment = .leading
        sv.addressStack.spacing = 10
        
        sv.buttonStack.axis = .horizontal
        sv.buttonStack.distribution = .fillEqually
        sv.buttonStack.alignment = .fill
        sv.buttonStack.spacing = 9.5
    
    }

    func addSubViews() {
        rootView.addSubview(sv.contentView)
        
        conv.addSubview(sv.chooseDateInfoLbl)
        conv.addSubview(sv.slider)
        conv.addSubview(sv.datesLbl)

        conv.addSubview(sv.totalSumStack)
        sv.totalSumStack.addArrangedSubview(sv.totalSumInfoLbl)
        sv.totalSumStack.addArrangedSubview(sv.totalSumLbl)
        
        conv.addSubview(sv.tableStack)
        sv.tableStack.addArrangedSubview(sv.bonusStack1)
        sv.tableStack.addArrangedSubview(sv.bonusStack2)
        sv.tableStack.addArrangedSubview(sv.tarifStack)
        
        conv.addSubview(sv.addressStack)
        sv.addressStack.addArrangedSubview(sv.addressInfoLbl)
        sv.addressStack.addArrangedSubview(sv.addressLbl)
        
        conv.addSubview(sv.buttonStack)
        sv.buttonStack.addArrangedSubview(sv.toEmailBtn)
        sv.buttonStack.addArrangedSubview(sv.payBtn)
    }
    
    func addConstraints() {
        sv.contentView.addConstraints(equalTo(superView: rootView, with: .zero))
        sv.contentView.addConstraints([
            equal(rootView, \.widthAnchor),
            equal(\.heightAnchor, to: 700)
            ])
        
        sv.chooseDateInfoLbl.addConstraints([
            equal(conv, \.topAnchor, constant: 20),
            equal(conv, \.leadingAnchor, constant: 20),
            equal(conv, \.trailingAnchor, constant: -20)
            ])
        
        sv.slider.addConstraints([
            equal(conv, \.leadingAnchor, constant: 20),
            equal(conv, \.trailingAnchor, constant: -20),
            equal(sv.chooseDateInfoLbl, \.topAnchor, \.bottomAnchor, constant: 20)
            ])
        
        sv.datesLbl.addConstraints([
            equal(conv, \.leadingAnchor, constant: 20),
            equal(conv, \.trailingAnchor, constant: -20),
            equal(sv.slider, \.topAnchor, \.bottomAnchor, constant: 37)

            ])
        
        sv.totalSumStack.addConstraints([
            equal(conv, \.leadingAnchor, constant: 20),
            equal(conv, \.trailingAnchor, constant: -20),
            equal(sv.datesLbl, \.topAnchor, \.bottomAnchor, constant: 30)
            
            ])
        
        sv.tableStack.addConstraints([
            equal(conv, \.leadingAnchor, constant: 20),
            equal(conv, \.trailingAnchor, constant: -20),
            equal(sv.totalSumStack, \.topAnchor, \.bottomAnchor, constant: 20)
            ])
        
        sv.addressStack.addConstraints([
            equal(conv, \.leadingAnchor, constant: 20),
            equal(conv, \.trailingAnchor, constant: -20),
            equal(sv.tableStack, \.topAnchor, \.bottomAnchor, constant: 30)
            ])
        
        sv.buttonStack.addConstraints([
            equal(conv, \.leadingAnchor, constant: 20),
            equal(conv, \.trailingAnchor, constant: -20),
            equal(sv.addressStack, \.topAnchor, \.bottomAnchor, constant: 33),
            equal(\.heightAnchor, to: 44)
            ])
    }
    
    
}
