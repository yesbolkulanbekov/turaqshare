//
//  RequestParkingRequestParkingViewIO.swift
//  turaQshare
//
//  Created by Yesbol Kulanbekov on 09/01/2019.
//  Copyright © 2019 turaQshare. All rights reserved.
//

protocol RequestParkingViewOutput {

    /**
        @author Yesbol Kulanbekov
        Notify presenter that view is ready
    */

    func viewIsReady()
}


protocol RequestParkingViewInput: class {

    /**
        @author Yesbol Kulanbekov
        Setup initial state of the view
    */

    func setupInitialState()
}
