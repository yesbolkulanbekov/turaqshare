//
//  ThreeLabelStack.swift
//  turaQshare
//
//  Created by Yesbol Kulanbekov on 1/9/19.
//  Copyright © 2019 turaQshare. All rights reserved.
//

import UIKit

class ThreeLabelStack: UIStackView {
    
    let label1 = UILabel()
    let label2 = UILabel()
    let label3 = UILabel()
    
    let centerView = UIView()
    
    //MARK: Initialization
    
    func setTexts(_ text1: String, _ text2: String, _ text3: String ) {
        
        label1.text = text1
        label2.text = text2
        label3.text = text3
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        axis = .horizontal
        distribution = .fillEqually
        alignment = .fill
        
        label1.textColor = .pine
        label1.font = UIFont.LucidaGrandeRegular(ofSize: 14)
        
        label2.textColor = .pine
        label2.font = UIFont.LucidaGrandeRegular(ofSize: 14)
        
        label3.textColor = .pine
        label3.font = UIFont.LucidaGrandeRegular(ofSize: 14)

        label2.textAlignment = .left
        label3.textAlignment = .right
        
        
        addArrangedSubview(label1)
        addArrangedSubview(label2)
        addArrangedSubview(label3)
        
        
    }
    
    required init(coder: NSCoder) {
        super.init(coder: coder)
    }
}
