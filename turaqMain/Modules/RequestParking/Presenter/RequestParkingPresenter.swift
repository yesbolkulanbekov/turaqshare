//
//  RequestParkingRequestParkingPresenter.swift
//  turaQshare
//
//  Created by Yesbol Kulanbekov on 09/01/2019.
//  Copyright © 2019 turaQshare. All rights reserved.
//

class RequestParkingPresenter: RequestParkingModuleInput {

    weak var view: RequestParkingViewInput!
    var interactor: RequestParkingInteractorInput!
    var router: RequestParkingRouterInput!

}

extension RequestParkingPresenter: RequestParkingViewOutput {
    func viewIsReady() {
        view.setupInitialState()
    }
}

extension RequestParkingPresenter: RequestParkingInteractorOutput {

}
