//
//  RequestParkingRequestParkingConfigurator.swift
//  turaQshare
//
//  Created by Yesbol Kulanbekov on 09/01/2019.
//  Copyright © 2019 turaQshare. All rights reserved.
//

import UIKit

struct RequestParkingModuleConfigurator {

    func assembleModule() -> RequestParkingViewController {
        let viewController = RequestParkingViewController()
        configure(viewController: viewController)
        return viewController
    }

    private func configure(viewController: RequestParkingViewController) {

        let router = RequestParkingRouter()

        let presenter = RequestParkingPresenter()
        presenter.view = viewController
        presenter.router = router

        let interactor = RequestParkingInteractor()
        interactor.output = presenter

        presenter.interactor = interactor
        viewController.output = presenter
    }

}
