//
//  BuildingParkingListBuildingParkingListPresenter.swift
//  turaQshare
//
//  Created by Yesbol Kulanbekov on 08/01/2019.
//  Copyright © 2019 turaQshare. All rights reserved.
//

import Foundation

class BuildingParkingListPresenter: BuildingParkingListModuleInput {

    weak var view: BuildingParkingListViewInput!
    var interactor: BuildingParkingListInteractorInput!
    var router: BuildingParkingListRouterInput!

    var zone: Zone!
}

extension BuildingParkingListPresenter: BuildingParkingListViewOutput {
    func viewIsReady() {
        view.setupInitialState()
        self.getPublicFreetimes()

    }
    
    func initView(with zone: Zone) {
        self.zone = zone
    }
    
}

extension BuildingParkingListPresenter: BuildingParkingListInteractorOutput {

}


extension BuildingParkingListPresenter {
//    func parkingsLoadedProps() -> BuildingParkingListViewController.Props {
//        let cell1 = BPListTVProps.CellProps(name: "#245     ЖК “Нурсая-1”", select: Command {
//            self.router.openRequestParking(from: self.view.getVC())
//        })
//        let cell2 = BPListTVProps.CellProps(name: "#246     ЖК “Нурсая-2”", select: Command {
//            self.router.openRequestParking(from: self.view.getVC())
//        })
//        let cells = [cell1, cell2]
//        let tableProps = BPListTVProps(cells: cells)
//
//        let props = BPListVCProps(rootTableProps: tableProps)
//        return props
//    }
    
//    func parkingsLoaded(with places: [ParkingPlace] ) -> BuildingParkingListViewController.Props {
//
//        let cells:[BPTVCellProps] = places.map { (place: ParkingPlace) in
//
//            var statusStr = ""
//            if let status = place.status {
//                statusStr = String(status)
//            }
//            let name = "Место: "
//            + place.placeNumber
//            + "  Зона: "
//            + String(place.parkingZone)
//            + "     Статус " + statusStr
//
//            let cell = NewFreetimeCell.Props(text: name, subtitle: statusStr, isParent: false)
////            (name: name, select: Command {
////                self.router.openPostBookVC(with: place, from: self.view.getVC())
////            })
//
//            return cell
//        }
//
//
//        let tableProps = BPListTVProps(cells: cells)
//
//        let props = BPListVCProps(rootTableProps: tableProps)
//        return props
//    }
//
    
    func parkingsLoaded(with freetimes: [FreeTime] ) -> BuildingParkingListViewController.Props {
        
        let cells:[BPTVCellProps] = freetimes.map { (freetime: FreeTime) in
        
            let beg = freetime.startTime?.formattedISODate ?? ""
            let end = freetime.repeatEndTime?.formattedISODate ?? ""
            let freq = (Freq.instance(from: freetime.frequency)?.rawValue) ?? ""
            
            let cell = NewFreetimeCell.Props(
                beg: beg,
                end: end,
                freq: freq,
                placeNumber: freetime.parkingPlaceNumber,
                onSelect: Command {
                self.router.openPostBookVC(with: freetime, from: self.view.getVC())
            })
            
            return cell
        }
        
        
        let tableProps = BPListTVProps(cells: cells)
        
        let props = BPListVCProps(rootTableProps: tableProps)
        return props
    }
}


extension BuildingParkingListPresenter {
    func getAllPlaces() {
        let onReturn = { (result: AllPlacesResult) in
            
            switch result {
            case let .success(places) :
                print("All places :", places)
                
                DispatchQueue.main.async {
//                    let props = self.parkingsLoaded(with: places)
//                    self.view.render(with: props)
                }
                
            case let .failure(error):
                
                DispatchQueue.main.async {
                    self.view.displayAlert(with: error.localizedDescription)
                }
                
            }
        }
        
        
        let req = AllPlaces.init(onReturn: onReturn)
        req.dispatch()
    }
    
    func getPublicFreetimes() {
        let freetimes = GetPublicFreeTimes(zoneID: self.zone.id) { (result) in
            
            switch result {
            case .success(let freetimes):
                
                DispatchQueue.main.async {
                    let props = self.parkingsLoaded(with: freetimes)
                    self.view.render(with: props)
                }
                
            case .failure(let error):
                
                DispatchQueue.main.async {
                    self.view.displayAlert(with: error.localizedDescription)
                }
            }
        }
        
        freetimes.dispatch()
    }
}
