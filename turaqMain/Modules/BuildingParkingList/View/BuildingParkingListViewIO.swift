//
//  BuildingParkingListBuildingParkingListViewIO.swift
//  turaQshare
//
//  Created by Yesbol Kulanbekov on 08/01/2019.
//  Copyright © 2019 turaQshare. All rights reserved.
//

protocol BuildingParkingListViewOutput {

    /**
        @author Yesbol Kulanbekov
        Notify presenter that view is ready
    */

    func viewIsReady()
    func initView(with zone: Zone)
}


protocol BuildingParkingListViewInput: class, HasViewController,DisplaysAlert {

    /**
        @author Yesbol Kulanbekov
        Setup initial state of the view
    */
    func render(with props: BuildingParkingListViewController.Props)
    func setupInitialState()
}
