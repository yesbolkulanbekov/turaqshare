//
//  BuildingParkingListBuildingParkingListViewController.swift
//  turaQshare
//
//  Created by Yesbol Kulanbekov on 08/01/2019.
//  Copyright © 2019 turaQshare. All rights reserved.
//

import UIKit

typealias BPListVCProps = BuildingParkingListViewController.Props
typealias BPListTVProps = BuildingParkingListViewController.ParkingTableProps
typealias BPTVCellProps = NewFreetimeCell.Props

class BuildingParkingListViewController: UIViewController {

    struct Props {
        let rootTableProps: ParkingTableProps
    }
    
    struct ParkingTableProps {
        let cells: [NewFreetimeCell.Props]
        
        static let EmptyTable = ParkingTableProps(cells: [NewFreetimeCell.Props]())
    }
    
    var props = Props(rootTableProps: ParkingTableProps.EmptyTable ) {
        didSet {
            
        }
    }
    
    
    var output: BuildingParkingListViewOutput!
    
    
    var parkListTV = UITableView()
    let spinnerView   = UIActivityIndicatorView(style: .gray)
    let newParkingBtn = UIButton(type: .system)

    // MARK: Life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .white
        output.viewIsReady()
    }

}

extension BuildingParkingListViewController:  BuildingParkingListViewInput {

    // MARK: BuildingParkingListViewInput
    func setupInitialState() {
        setupInitViewState()
        setupTableView()
    }

    func render(with props: BuildingParkingListViewController.Props) {
        self.props = props
        parkListTV.reloadData()
    }
}


extension BuildingParkingListViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return props.rootTableProps.cells.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: NewFreetimeCell.cellID, for: indexPath) as! NewFreetimeCell
        let freetime = props.rootTableProps.cells[indexPath.row]
        cell.props = freetime
        return cell
    }
}


extension BuildingParkingListViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let freetime = props.rootTableProps.cells[indexPath.row]
        freetime.onSelect.perform()
    }
}


extension BuildingParkingListViewController {
    

    private func setupTableView() {
        parkListTV.register(UITableViewHeaderFooterView.self, forHeaderFooterViewReuseIdentifier: "ParkingHeaderView")
        parkListTV.register(NewFreetimeCell.self, forCellReuseIdentifier: NewFreetimeCell.cellID)
        parkListTV.dataSource = self
        parkListTV.delegate = self
        parkListTV.tableFooterView = UIView()
        
        let parkingImageView = UIImageView()
        let myImage: UIImage = UIImage(named: "novimir")!
        parkingImageView.image = myImage
        parkingImageView.frame.size = parkingImageView.intrinsicContentSize
        parkingImageView.contentMode = .scaleAspectFill
        parkListTV.tableHeaderView = parkingImageView
        
    }
    
    private func setupInitViewState() {
        navigationItem.title = "Свободные Парковки"
        view.backgroundColor = .white
        view.addSubview(parkListTV)
        parkListTV.addConstraints(equalToSafeArea(superView: view, with: .zero))
        view.addSubview(spinnerView)
        spinnerView.addConstraints([
            equal(view, \UIView.safeAreaLayoutGuide.centerXAnchor),
            equal(view, \UIView.safeAreaLayoutGuide.centerYAnchor)
            ])
    }
}
