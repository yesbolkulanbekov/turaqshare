//
//  NewFreetimeCell.swift
//  turaQshare
//
//  Created by Yesbol Kulanbekov on 4/7/19.
//  Copyright © 2019 turaQshare. All rights reserved.
//

import UIKit

class NewFreetimeCell: UITableViewCell {
    
    static let cellID = "NewFreetimeCellIdentifier"
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setupSelf()
        configureSubViews()
        styleSubviews()
        sv.render(with: Props.zero)
        sv.renderConstantData()
        NewFreetimeCellLayout(for: self).paint()
        
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
    }
    
    /// Properties
    
    let sv = Subviews()
    
    var props = Props.zero {
        didSet {
            sv.render(with: props)
            
        }
    }
    
}

/// View configuration, content and styling

extension NewFreetimeCell {
    
    struct Props {
        
        let beg: String
        let end: String
        let freq: String
        let placeNumber: String
        let onSelect: Command
        
        
        static let zero = Props(
            beg: "вск, Фев 17 - 09:00 ",
            end: "вск 18:00",
            freq: "Каждый день",
            placeNumber: "42",
            onSelect: Command.nop
        )
    }
    
    struct Subviews {
        
        let backView = UIView()
        let initials = UIImageView()
        let userNameLbl = UILabel()
        let userNumberLbl = UILabel()
        
        let beginIcon = UIImageView()
        let endIcon   = UIImageView()
        let freqIcon  = UIImageView()
        
        let begLbl    = UILabel()
        let endLbl    = UILabel()
        let freqLbl   = UILabel()
        
        let begDateLb = UILabel()
        let endDateLb = UILabel()
        let freqValueLb = UILabel()
        
        func render(with props: Props) {
            userNameLbl.text = "User"
            userNumberLbl.text = "#\(props.placeNumber)"
            
            begDateLb.text = props.beg
            endDateLb.text = props.end
            freqValueLb.text = props.freq
        }
        
        func renderConstantData() {
            initials.image = UIImage(named: "user")
            beginIcon.image = UIImage(named: "iconClock")
            endIcon.image = UIImage(named: "endClock")
            freqIcon.image = UIImage(named: "repeat1")
            
            begLbl.text = "Начало"
            endLbl.text = "Конец"
            freqLbl.text = "Повтор"
        }
    }
    
    func styleSubviews() {
        sv.backView.layer.shadowOffset = CGSize(width: 0, height: 2)
        sv.backView.layer.shadowColor = UIColor(red: 25/255, green: 54/255, blue: 40/255, alpha: 0.08).cgColor
        sv.backView.layer.shadowOpacity = 1
        sv.backView.layer.shadowRadius = 4
        sv.backView.backgroundColor = UIColor(red: 1, green: 1, blue: 1, alpha: 1)
        sv.backView.layer.cornerRadius = 4
        
        sv.initials.contentMode = .scaleAspectFit
        
        sv.userNameLbl.font = UIFont.NunitoRegular(ofSize: 14)
        sv.userNameLbl.textColor = UIColor.pine
        sv.userNameLbl.textAlignment = .center
        
        sv.userNumberLbl.font = UIFont.NunitoRegular(ofSize: 14)
        sv.userNumberLbl.textColor = UIColor.teal
        sv.userNumberLbl.textAlignment = .center
        
        sv.begDateLb.textAlignment = .right
        sv.begDateLb.font = UIFont.NunitoSemiBold(ofSize: 14)
        
        sv.endDateLb.textAlignment = .right
        sv.endDateLb.font = UIFont.NunitoSemiBold(ofSize: 14)
    
        sv.freqValueLb.textAlignment = .right
        sv.freqValueLb.font = UIFont.NunitoSemiBold(ofSize: 14)
        
        sv.beginIcon.contentMode = .scaleAspectFit
        
        sv.endIcon.contentMode = .scaleAspectFit
        
        sv.freqIcon.contentMode = .scaleAspectFit
        
        sv.begLbl.styleFreetimeCell()
        
        sv.endLbl.styleFreetimeCell()
        
        sv.freqLbl.styleFreetimeCell()
        
    }
    
    private func style(_ lbl: UILabel) {
        sv.begLbl.font = UIFont.NunitoRegular(ofSize: 16)
        sv.begLbl.textColor = UIColor.black.withAlphaComponent(0.3)
    }
    
    private func configureSubViews() {
        
    }
    
    private func setupSelf() {
        selectionStyle = .none
        backgroundColor = .clear
        translatesAutoresizingMaskIntoConstraints = false
    }
}

extension UILabel {
    func styleFreetimeCell() {
        self.font = UIFont.NunitoRegular(ofSize: 16)
        self.textColor = UIColor.black.withAlphaComponent(0.3)
    }
}


/// View layout

struct NewFreetimeCellLayout {
    
    var sv: NewFreetimeCell.Subviews
    var contentView: UIView
    
    
    init(for rootView: NewFreetimeCell) {
        self.sv = rootView.sv
        self.contentView = rootView.contentView
    }
    
    func paint() {
        addSubViews()
        addConstraints()
    }
    
    
    private func addSubViews() {
        sv.initials.setContentHuggingPriority(.defaultHigh, for: .horizontal)
        
        sv.beginIcon.setContentHuggingPriority(.defaultHigh, for: .horizontal)
        sv.endIcon.setContentHuggingPriority(.defaultHigh, for: .horizontal)
        sv.freqIcon.setContentHuggingPriority(.defaultHigh, for: .horizontal)
        
        contentView.addSubview(rootStack)
        rootStack.insertSubview(sv.backView, at: 0)
        
        rootStack.addArrangedSubview(iconStack)
        rootStack.addArrangedSubview(allLabels)
    
        iconStack.addArrangedSubview(sv.initials)
        iconStack.addArrangedSubview(sv.userNameLbl)
        iconStack.addArrangedSubview(sv.userNumberLbl)
        
        
        labelStack1.addArrangedSubview(sv.beginIcon)
        labelStack1.addArrangedSubview(sv.begLbl)
        labelStack1.addArrangedSubview(sv.begDateLb)
        
        labelStack2.addArrangedSubview(sv.endIcon)
        labelStack2.addArrangedSubview(sv.endLbl)
        labelStack2.addArrangedSubview(sv.endDateLb)
        
        labelStack3.addArrangedSubview(sv.freqIcon)
        labelStack3.addArrangedSubview(sv.freqLbl)
        labelStack3.addArrangedSubview(sv.freqValueLb)
        
        allLabels.addArrangedSubview(labelStack1)
        allLabels.addArrangedSubview(labelStack2)
        allLabels.addArrangedSubview(labelStack3)
    }
    
    private func addConstraints() {
        let insets = UIEdgeInsets(top: 12, left: 8, bottom: 12, right: 8)
        rootStack.addConstraints(equalTo(superView: contentView, with: insets))
        sv.backView.addConstraints(equalTo(superView: rootStack))
        
        sv.initials.addConstraints([
            equal(\.widthAnchor, to: 42),
            equal(sv.initials, \.widthAnchor, \.heightAnchor)
        ])
    
    }
    
    let rootStack = UIStackView() { stack in
        stack.axis = .horizontal
        stack.spacing = 0
        stack.distribution = .fill
        stack.alignment = UIStackView.Alignment.fill
        stack.isLayoutMarginsRelativeArrangement = true
        stack.layoutMargins = UIEdgeInsets(top: 2, left: 2, bottom: 2, right: 2)
    }
    
    let labelStack1 = UIStackView() { stack in
        stack.axis = .horizontal
        stack.distribution = .fill
        stack.spacing = 4
    }
    
    let labelStack2 = UIStackView() { stack in
        stack.axis = .horizontal
        stack.distribution = .fill
        stack.spacing = 4
    }
    
    let labelStack3 = UIStackView() { stack in
        stack.axis = .horizontal
        stack.distribution = .fill
        stack.spacing = 4
    }
    
    let allLabels = UIStackView() { stack in
        stack.axis = .vertical
        stack.spacing = 8
        stack.distribution = .equalSpacing
        stack.isLayoutMarginsRelativeArrangement = true
        stack.layoutMargins = UIEdgeInsets(top: 4, left: 4, bottom: 4, right: 4)
    }
    
    let iconStack = UIStackView() { stack in
        stack.axis = .vertical
        stack.spacing = 4
        stack.distribution = .fill
        stack.isLayoutMarginsRelativeArrangement = true
        stack.layoutMargins = UIEdgeInsets(top: 4, left: 4, bottom: 4, right: 4)
    }
    
    private func defaultInsets() -> UIEdgeInsets {
        return UIEdgeInsets(top: 4, left: 4, bottom: 4, right: 4)
    }
}


