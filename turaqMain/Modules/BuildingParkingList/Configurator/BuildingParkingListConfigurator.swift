//
//  BuildingParkingListBuildingParkingListConfigurator.swift
//  turaQshare
//
//  Created by Yesbol Kulanbekov on 08/01/2019.
//  Copyright © 2019 turaQshare. All rights reserved.
//

import UIKit

struct BuildingParkingListModuleConfigurator {

    func assembleModule() -> BuildingParkingListViewController {
        let viewController = BuildingParkingListViewController()
        configure(viewController: viewController)
        return viewController
    }

    private func configure(viewController: BuildingParkingListViewController) {

        let router = BuildingParkingListRouter()

        let presenter = BuildingParkingListPresenter()
        presenter.view = viewController
        presenter.router = router

        let interactor = BuildingParkingListInteractor()
        interactor.output = presenter

        presenter.interactor = interactor
        viewController.output = presenter
    }

}
