//
//  BuildingParkingListBuildingParkingListRouterInput.swift
//  turaQshare
//
//  Created by Yesbol Kulanbekov on 08/01/2019.
//  Copyright © 2019 turaQshare. All rights reserved.
//

import UIKit

protocol BuildingParkingListRouterInput {
    func openRequestParking(from vc: UIViewController)
    func openPostBookVC(with parkingPlace: ParkingPlace, from vc: UIViewController)
    func openPostBookVC(with freetime: FreeTime, from vc: UIViewController)

}
