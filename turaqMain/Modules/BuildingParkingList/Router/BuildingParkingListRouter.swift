//
//  BuildingParkingListBuildingParkingListRouter.swift
//  turaQshare
//
//  Created by Yesbol Kulanbekov on 08/01/2019.
//  Copyright © 2019 turaQshare. All rights reserved.
//

import UIKit

class BuildingParkingListRouter: BuildingParkingListRouterInput {
    func openRequestParking(from vc: UIViewController) {
        let reqParkingVC = RequestParkingModuleConfigurator().assembleModule()
        vc.navigationController?.pushViewController(reqParkingVC, animated: true)
    }
    
    func openPostBookVC(with parkingPlace: ParkingPlace, from vc: UIViewController){
        let postBookVC = PostBookingModuleConfigurator().assembleModule()
        postBookVC.output.initView(with: parkingPlace)
        vc.navigationController?.pushViewController(postBookVC, animated: true)
    }

    func openPostBookVC(with freetime: FreeTime, from vc: UIViewController) {
        let postBookVC = PostBookingModuleConfigurator().assembleModule()
        postBookVC.output.initView(with: freetime)
        vc.navigationController?.pushViewController(postBookVC, animated: true)
    }

}
