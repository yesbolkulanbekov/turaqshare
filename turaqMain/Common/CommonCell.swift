//
//  CommonCell.swift
//  turaQshare
//
//  Created by Yesbol Kulanbekov on 3/11/19.
//  Copyright © 2019 turaQshare. All rights reserved.
//

import UIKit

// TODO: add this to shared code

class CommonCell: UITableViewCell {
    
    static let id = "CommonCellReuseIdentifier"
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setupSelf()
        configureSubViews()
        styleSubviews()
        sv.render(with: Props.zero)
        sv.renderConstantData()
        CommonCellLayout(for: self).paint()
    }
    
  
    required init?(coder: NSCoder) {
        super.init(coder: coder)
    }
    
    /// Properties
    
    let sv = Subviews()
    
    var props = Props.zero {
        didSet {
            sv.render(with: props)
        }
    }
    
}

/// View configuration, content and styling

extension CommonCell {
    
    struct Props {
        
        static let zero = Props(
            
        )
    }
    
    struct Subviews {
        
        let title = UILabel()
        let subTitle = UILabel()
        
        func render(with props: Props) {
            
        }
        
        func renderConstantData() {
            
        }
    }
    
    func styleSubviews() {
        
    }
    
    private func configureSubViews() {
        
    }
    
    private func setupSelf() {
        translatesAutoresizingMaskIntoConstraints = false
    }
}



/// View layout

struct CommonCellLayout {
    
    var rootView: CommonCell
    var contentView: UIView
    var sv: CommonCell.Subviews
    
    init(for rootView: CommonCell) {
        self.rootView = rootView
        self.contentView = rootView.contentView
        self.sv = rootView.sv
    }
    
    func paint() {
        addSubViews()
        addConstraints()
    }
    
    func addSubViews() {
        rootView.addSubview(rootStack)
        
    }
    
    func addConstraints() {
        rootStack.addConstraints(equalToSafeArea(superView: rootView))
    }
    
    let rootStack = UIStackView() { stack in
        stack.axis = .vertical
        stack.distribution = .fill
        stack.alignment = .fill
        stack.spacing = 8
    }
    
}




