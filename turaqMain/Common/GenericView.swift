//
//  GenericView.swift
//  turaQshare
//
//  Created by Yesbol Kulanbekov on 12/11/18.
//  Copyright © 2018 Galym Anuarbek. All rights reserved.
//

import Foundation



import UIKit


class GenericView: UIView {
    
    struct Subviews {
        
    }
    
    let subViews = GenericView.Subviews()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupSelf()
        configureSubViews()
        styleSubviews()
        render(with: Props())
        layout()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
    }
}


/// Props configuration and init

extension GenericView {
    struct Props {
        
    }
}

extension GenericView.Props {
    
}

/// View configuration, content and styling

extension GenericView {
    
    private func setupSelf() {
        translatesAutoresizingMaskIntoConstraints = false
    }
    
    private func configureSubViews() {
        
    }
    
    func styleSubviews() {
        
    }
    
    func render(with props: GenericView.Props) {
        
    }
    
    func layout() {
        //GenericViewLayout(rootView: self).paint()
    }
    
}


/// View layout

struct GenericViewLayout {
    
    var rootView: GenericView
    var subViews: GenericView.Subviews
    
    init(rootView: GenericView) {
        self.rootView = rootView
        self.subViews = rootView.subViews
    }
    
    func paint() {
        addSubViews()
        addConstraints()
    }
    
    func addSubViews() {
        rootView.addSubview(rootStack)

    }
    
    func addConstraints() {
        rootStack.addConstraints(equalToSafeArea(superView: rootView))
    }
    
    let rootStack = UIStackView() { stack in
        stack.axis = .vertical
        stack.distribution = .fill
        stack.alignment = .fill
        stack.spacing = 8
    }
    
    
}



class GenericViewB: UIView {
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupSelf()
        configureSubViews()
        styleSubviews()
        renderConstantData()
        layout()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
    }
    
    /// Properties
    
    let sv = GenericViewB.Subviews()
    
    var props = Props() {
        didSet {
            
        }
    }
    
}

/// View configuration, content and styling

extension GenericViewB {
    
    struct Props {
        
    }
    
    struct Subviews {

    }
    
    func renderConstantData() {
        
    }
    
    func styleSubviews() {
        
    }
    
    private func configureSubViews() {
        
    }
    
    private func setupSelf() {
        translatesAutoresizingMaskIntoConstraints = false
    }
}


extension GenericViewB {
    
    func render(with props: GenericViewB.Props) {
        self.props = props
    }
    
    func layout() {
        GenericViewBLayout(for: self).paint()
    }
    
}



/// View layout

struct GenericViewBLayout {
    
    var rootView: GenericViewB
    var sv: GenericViewB.Subviews
    
    init(for rootView: GenericViewB) {
        self.rootView = rootView
        self.sv = rootView.sv
    }
    
    func paint() {
        addSubViews()
        addConstraints()
    }
    
    func addSubViews() {
        rootView.addSubview(rootStack)
        
    }
    
    func addConstraints() {
        rootStack.addConstraints(equalToSafeArea(superView: rootView))
    }
    
    let rootStack = UIStackView() { stack in
        stack.axis = .vertical
        stack.distribution = .fill
        stack.alignment = .fill
        stack.spacing = 8
    }
    
}


extension UIStackView {
    @nonobjc public convenience init(style: ((UIStackView) -> Void)...) {
        self.init(frame: .zero)
        style.forEach { $0(self) }
    }
}

extension UIStackView {
    @nonobjc public convenience init(style: ((UIStackView) -> Void)..., views: [UIView]) {
        self.init(arrangedSubviews: views)
        style.forEach { $0(self) }
    }
}



// TODO: add this to shared code

class GenericViewC: UIView {
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupSelf()
        configureSubViews()
        styleSubviews()
        sv.render(with: Props.zero)
        sv.renderConstantData()
        GenericViewCLayout(for: self).paint()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
    }
    
    /// Properties
    
    let sv = Subviews()
    
    var props = Props.zero {
        didSet {
            sv.render(with: props)
        }
    }
    
}

/// View configuration, content and styling

extension GenericViewC {
    
    struct Props {
        
        static let zero = Props(
            
        )
    }
    
    struct Subviews {
        func render(with props: Props) {
            
        }
        
        func renderConstantData() {
            
        }
    }
    
    func styleSubviews() {
        
    }
    
    private func configureSubViews() {
        
    }
    
    private func setupSelf() {
        translatesAutoresizingMaskIntoConstraints = false
    }
}



/// View layout

struct GenericViewCLayout {
    
    var rootView: GenericViewC
    var sv: GenericViewC.Subviews
    
    init(for rootView: GenericViewC) {
        self.rootView = rootView
        self.sv = rootView.sv
    }
    
    func paint() {
        addSubViews()
        addConstraints()
    }
    
    func addSubViews() {
        rootView.addSubview(rootStack)
        
    }
    
    func addConstraints() {
        rootStack.addConstraints(equalToSafeArea(superView: rootView))
    }
    
    let rootStack = UIStackView() { stack in
        stack.axis = .vertical
        stack.distribution = .fill
        stack.alignment = .fill
        stack.spacing = 8
    }
    
}





