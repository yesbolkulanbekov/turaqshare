//
//  DatePickerPresenter.swift
//  turaQshare
//
//  Created by Yesbol Kulanbekov on 2/26/19.
//  Copyright © 2019 turaQshare. All rights reserved.
//

import UIKit

struct DatePickerPresenter {

}

extension Date {
    func adding(minutes: Int) -> Date {
        return Calendar.current.date(byAdding: .minute, value: minutes, to: self)!
    }
}



class MonsterDatePicker: UIViewController {
    
    var datePicked = CommandWith<Date> { _ in }
    var selectedDate: Date!
    var minDate: Date!
    
    private let saveButton = UIButton(type: .system)
    private let datePicker = UIDatePicker()
    private let dateTitle = UILabel()

    
    override func viewDidLoad() {
        
        layout()
        showDatePicker()
        
        view.backgroundColor = UIColor.gray.withAlphaComponent(0.8)
        let gestRecognizer = UITapGestureRecognizer(target: self, action: #selector(tap))
        view.addGestureRecognizer(gestRecognizer)
    }
    
    @objc
    func tap() {
        dismiss(animated: true, completion: nil)
    }

    
    @objc func done() {
        dismiss(animated: true, completion: {
            self.datePicked.perform(with: self.selectedDate)
        })
    }
    
    func layout() {
        
        dateTitle.text = "Выберите дату"
        dateTitle.backgroundColor = UIColor.blueGreen
        dateTitle.textColor = .white
        dateTitle.font = UIFont.systemFont(ofSize: 20)
        dateTitle.textAlignment = .center
        
        saveButton.setTitle("Сохранить", for: .normal)
        saveButton.setTitleColor(.white, for: .normal)
        saveButton.titleLabel?.font = UIFont.LucidaGrandeBold(ofSize: 14)
        saveButton.backgroundColor = UIColor.blueGreen
        saveButton.titleLabel?.textAlignment = .center
        saveButton.addTarget(self, action: #selector(done), for: .touchUpInside)
        
        view.addSubview(stack)
        stack.addConstraints([
            equal(view, \UIView.centerXAnchor),
            equal(view, \UIView.centerYAnchor),
        ])
        
        stack.addArrangedSubview(dateTitle)
        stack.addArrangedSubview(datePicker)
        stack.addArrangedSubview(saveButton)
        
        dateTitle.addConstraints([equal(\UIView.heightAnchor, to: 54)])
        saveButton.addConstraints([equal(\UIView.heightAnchor, to: 44)])

    }
    
    func showDatePicker() {
        
        datePicker.minimumDate = minDate
        datePicker.timeZone = NSTimeZone.local
        datePicker.datePickerMode = .dateAndTime
        datePicker.locale = Locale(identifier: "ru_RU")
        datePicker.backgroundColor = UIColor.white
        datePicker.layer.borderWidth = 0.5
        datePicker.layer.borderColor = UIColor.blueGreen.cgColor
        datePicker.date = selectedDate
        
        datePicker.addTarget(self, action: #selector(datePickerValueChanged(_:)), for: .valueChanged)
        
    }
    
    let stack = UIStackView { stack in
        stack.axis = .vertical
        stack.distribution = .fill
        stack.alignment = .fill
        stack.spacing = 0
        stack.isLayoutMarginsRelativeArrangement = true
        stack.layoutMargins = UIEdgeInsets(top: 10, left: 10, bottom: 10, right: 10)
    }
    
    @objc func datePickerValueChanged(_ sender: UIDatePicker){
        
        selectedDate = sender.date
        
        let calendar = Calendar.current
        let newYear = calendar.component(.year, from: sender.date)
        let newDay = calendar.component(.day, from: sender.date)
        
        let form = DateFormatter()
        form.locale = Locale(identifier: "ru_RU")
        form.dateFormat = "MMMM"
        
        let fullMonth = form.string(from: sender.date)
        let newMonth  = String(fullMonth.prefix(3))
        
        let day = "\(newDay) "
        let month = newMonth
        let year = " \(newYear)"
        title = day + month + year
    }
    
}


