//
//  CustomPicker.swift
//  turaQshare
//
//  Created by Yesbol Kulanbekov on 3/3/19.
//  Copyright © 2019 turaQshare. All rights reserved.
//

import UIKit

enum Freq: String {
    case day = "Ежедневно"
    case weekly = "Недельно"
    case once = "Один раз"
    
    var intForm: Int {
        switch self {
        case .day: return 1
        case .weekly: return 2
        case .once: return 3
        }
    }
    
    static func instance(from number: Int) -> Freq? {
        switch number {
        case 1: return Freq.day
        case 2: return Freq.weekly
        case 3: return Freq.once
        default: return nil
        }
    }
    
    var isOnce: Bool {
        guard case .once = self else { return false }
        return true
    }
    
    var isWeekly: Bool {
        guard case .weekly = self else { return false }
        return true
    }
}

enum WeekDay: String, CustomStringConvertible, CaseIterable {
    case mon = "Понедельник"
    case tue = "Вторник"
    case wed = "Среда"
    case thu = "Четверг"
    case fri = "Пятница"
    case sat = "Суббота"
    case sun = "Воскресенье"
    
    var description: String {
        switch self {
        case .mon: return "Monday"
        case .tue: return "Tuesday"
        case .wed: return "Wednesday"
        case .thu: return "Thursday"
        case .fri: return "Friday"
        case .sat: return "Saturday"
        case .sun: return "Sunday"
        }
    }
}

typealias OnPick = CommandWith<Freq>

class FreqPicker: UIViewController {
    
    var onPick = CommandWith<Freq> { _ in }
    var selectedFreq = Freq.day
    
    
    private let saveButton = UIButton(type: .system)
    private let picker = UIPickerView()
    private let dateTitle = UILabel()
    
    var choices = [Freq.once, Freq.day, Freq.weekly]
    

    
    override func viewDidLoad() {
        
        layout()
        showPicker()
        picker.dataSource = self
        picker.delegate = self
        let row = choices.firstIndex(of: selectedFreq)!
        picker.selectRow(row, inComponent: 0, animated: false)
        
        view.backgroundColor = UIColor.gray.withAlphaComponent(0.8)
        let gestRecognizer = UITapGestureRecognizer(target: self, action: #selector(tap))
        view.addGestureRecognizer(gestRecognizer)
    }
    
    @objc
    func tap() {
        dismiss(animated: true, completion: nil)
    }
    
    
    @objc func done() {
        dismiss(animated: true, completion: {
            self.onPick.perform(with: self.selectedFreq)
        })
    }
    
    func layout() {
        
        dateTitle.text = "Выберите частоту"
        dateTitle.backgroundColor = UIColor.blueGreen
        dateTitle.textColor = .white
        dateTitle.font = UIFont.systemFont(ofSize: 20)
        dateTitle.textAlignment = .center
        
        saveButton.setTitle("Сохранить", for: .normal)
        saveButton.setTitleColor(.white, for: .normal)
        saveButton.titleLabel?.font = UIFont.LucidaGrandeBold(ofSize: 14)
        saveButton.backgroundColor = UIColor.blueGreen
        saveButton.titleLabel?.textAlignment = .center
        saveButton.addTarget(self, action: #selector(done), for: .touchUpInside)
        
        view.addSubview(stack)
        stack.addConstraints([
            equal(view, \UIView.centerXAnchor),
            equal(view, \UIView.centerYAnchor),
            ])
        
        stack.addArrangedSubview(dateTitle)
        stack.addArrangedSubview(picker)
        stack.addArrangedSubview(saveButton)
        
        dateTitle.addConstraints([equal(\UIView.heightAnchor, to: 54)])
        saveButton.addConstraints([equal(\UIView.heightAnchor, to: 44)])
        
    }
    
    func showPicker() {

        picker.backgroundColor = UIColor.white
        picker.layer.borderWidth = 0.5
        picker.layer.borderColor = UIColor.blueGreen.cgColor
        
    }
    
    let stack = UIStackView { stack in
        stack.axis = .vertical
        stack.distribution = .fill
        stack.alignment = .fill
        stack.spacing = 0
        stack.isLayoutMarginsRelativeArrangement = true
        stack.layoutMargins = UIEdgeInsets(top: 10, left: 10, bottom: 10, right: 10)
    }
    

    
}


extension FreqPicker: UIPickerViewDataSource, UIPickerViewDelegate {
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return choices.count
    }
    
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return choices.map{$0.rawValue} [row]
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        selectedFreq = choices[row]
    }
    
    
}
