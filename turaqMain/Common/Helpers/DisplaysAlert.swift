//
//  DisplaysAlert.swift
//  MonsterTours
//
//  Created by Yesbol Kulanbekov on 6/12/18.
//  Copyright © 2018 Monster Tours. All rights reserved.
//

import UIKit

protocol DisplaysAlert {
    func displayAlert(with message: String)
    func disPlayAlert(with message: String, action: @escaping () -> Void )

}

extension DisplaysAlert where Self : UIViewController {
    func displayAlert(with message: String) {
        let alertPresenter = AlertPresenter(message: message,
                                            acceptTitle: "Ok",
                                            handler: { })
        alertPresenter.present(in: self)
        
    }
    
    func disPlayAlert(with message: String, action: @escaping () -> Void ) {
        let alertPresenter = AlertPresenter(message: message,
                                            acceptTitle: "Ok",
                                            handler: action)
        alertPresenter.present(in: self)
    }

}
