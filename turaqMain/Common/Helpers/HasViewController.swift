//
//  HasViewController.swift
//  turaQshare
//
//  Created by Yesbol Kulanbekov on 12/15/18.
//  Copyright © 2018 Galym Anuarbek. All rights reserved.
//

import UIKit

protocol HasViewController {
    func getVC() -> UIViewController
}

extension HasViewController {
    func getVC() -> UIViewController {
        return self as! UIViewController
    }
}
