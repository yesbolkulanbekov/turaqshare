//
//  AlertPresenter.swift
//  turaQshare
//
//  Created by Yesbol Kulanbekov on 12/14/18.
//  Copyright © 2018 Galym Anuarbek. All rights reserved.
//

import UIKit

// TODO: Add this to shared

struct AlertPresenter {
    
    let message: String
    
    let acceptTitle: String

    let handler: () -> Void
    
    func present(in viewController: UIViewController) {
        let alert = UIAlertController(
            title: nil,
            message: message,
            preferredStyle: .alert
        )
    
        let readTheMessageAction = UIAlertAction(title: acceptTitle, style: .default) { _ in
            self.handler()
        }
        
        alert.addAction(readTheMessageAction)
        
        viewController.present(alert, animated: true)
    }
}

enum Outcome {
    case accepted
}
