//
//  DateFormat.swift
//  turaQshare
//
//  Created by Yesbol Kulanbekov on 3/6/19.
//  Copyright © 2019 turaQshare. All rights reserved.
//

import Foundation

//extension ISO8601DateFormatter {
//    convenience init(_ formatOptions: Options) {
//        self.init()
//        self.formatOptions = formatOptions
//        let seconds = TimeZone.current.secondsFromGMT()
//        self.timeZone = TimeZone(secondsFromGMT: seconds)!
//    }
//}
//extension Formatter {
//    static let iso8601 = ISO8601DateFormatter([])
//}

extension Date {
    var iso8601: String {
        return Formatter.iso8601.string(from: self)
    }
}
extension String {
    var iso8601: Date? {
        return Formatter.iso8601.date(from: self)
    }
    
    var formattedISODate: String {
        let date = Formatter.iso8601.date(from: self)!
        
        var calendar = Calendar.current
        calendar.timeZone = TimeZone(secondsFromGMT: 0)!
        calendar.locale = Locale(identifier: "ru_RU")
        
        let day = calendar.component(.day, from: date)
        let hourComp = calendar.component(.hour, from: date)
        let hour = String(format: "%.2d", hourComp)
        let minComp  = calendar.component(.minute, from: date)
        let min = String(format: "%.2d", minComp)
        let weekday = calendar.component(.weekday, from: date)
        let monthInt = calendar.component(.month, from: date)

        let form = DateFormatter()
        form.locale = Locale(identifier: "ru_RU")
        let month = form.shortMonthSymbols[monthInt - 1].capitalized
        let weekStr = form.shortWeekdaySymbols[weekday-1]
        
        let final = "\(weekStr),\(month) \(day) - \(hour):\(min)"
        return final
    }
}


extension Formatter {
    static let iso8601: DateFormatter = {
        let formatter = DateFormatter()
        formatter.calendar = Calendar(identifier: .iso8601)
        formatter.locale = Locale(identifier: "ru_RU")
        formatter.timeZone = TimeZone(secondsFromGMT: 0)!
        formatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss'Z'"
        return formatter
    }()
}


extension Date {
    var secondsInADay: Int {
        var calendar = Calendar.current
        calendar.timeZone = TimeZone(secondsFromGMT: 0)!
        let timeComponents = calendar.dateComponents([.hour,.minute, .second], from: self)
        return timeComponents.hour! * 3600 + timeComponents.minute! * 60 + timeComponents.second!
    }
}

extension Int {
    var dateComps: DateComponents {
        let date = Date.init(timeIntervalSinceReferenceDate: TimeInterval(self))
        var calendar = Calendar.current
        calendar.timeZone = TimeZone(secondsFromGMT: 0)!
        let components = calendar.dateComponents([.hour, .minute, .second], from: date)
        return components
    }
}

extension Date {
    public func setTime(comps: DateComponents) -> Date? {
        let x: Set<Calendar.Component> = [.year, .month, .day, .hour, .minute, .second]
        var cal = Calendar.current
        cal.timeZone = TimeZone(secondsFromGMT: 0)!
        var components = cal.dateComponents(x, from: self)
        
        components.timeZone = TimeZone(secondsFromGMT: 0)!
        components.hour = comps.hour
        components.minute = comps.minute
        components.second = comps.second
        
        return cal.date(from: components)
    }
}

extension Date {
    static func dates(from fromDate: Date, to toDate: Date) -> [Date] {
        var dates: [Date] = []
        var date = fromDate
        
        while date <= toDate {
            dates.append(date)
            var calendar = Calendar.current
            calendar.timeZone = TimeZone(secondsFromGMT: 0)!
            guard let newDate = calendar.date(byAdding: .day, value: 1, to: date) else { break }
            date = newDate
        }
        return dates
    }
}

extension Date {
    var formatted: String {
        
        let calendar = Calendar.current
        let newYear = calendar.component(.year, from: self)
        let newDay = calendar.component(.day, from: self)
        let hour = " \(calendar.component(.hour, from: self)):"
        let min  = "\(calendar.component(.minute, from: self))"
        
        let form = DateFormatter()
        form.locale = Locale(identifier: "ru_RU")
        form.dateFormat = "MMMM"
        
        let fullMonth = form.string(from: self)
        let newMonth  = String(fullMonth.prefix(3))
        
        let day = "\(newDay) "
        let month = newMonth
        let year = " \(newYear)"
        let title = day + month + year + hour + min
        return title
    }
}


protocol HasMiddleValue {
    
    associatedtype ItemType
    func middle() -> [ItemType]
    
}

extension Array: HasMiddleValue {
    
    typealias ItemType = Iterator.Element
    
    func middle() -> [ItemType] {
        guard count > 0 else {
            return [ItemType]()
        }
        
        let middleIndex = count / 2
        let middleArray: [ItemType]
        
        if count % 2 != 0 {
            middleArray = [self[middleIndex]]
        } else {
            middleArray = [self[middleIndex - 1], self[middleIndex]]
        }
        
        return middleArray
    }
    
}


extension Date {
    // Convert UTC (or GMT) to local time
    
    func toLocalTime() -> Date {
        
        let timezone = TimeZone.current
        
        let seconds = TimeInterval(timezone.secondsFromGMT(for: self))
        
        return Date(timeInterval: seconds, since: self)
        
    }
    
    // Convert local time to UTC (or GMT)
    
    func toGlobalTime() -> Date {
        
        let timezone = TimeZone.current
        
        let seconds = -TimeInterval(timezone.secondsFromGMT(for: self))
        
        return Date(timeInterval: seconds, since: self)
        
    }
    
}
