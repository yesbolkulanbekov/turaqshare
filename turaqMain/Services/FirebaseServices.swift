//
//  FirebaseServices.swift
//  turaQshare
//
//  Created by Yesbol Kulanbekov on 12/7/18.
//  Copyright © 2018 Galym Anuarbek. All rights reserved.
//

import Foundation
import FirebaseAuth
import FirebaseDatabase


class FirebaseServices { 
    static let shared = FirebaseServices()
    
    let auth: Auth = Auth.auth()
    let phoneProvider = PhoneAuthProvider.provider()
    let ref = Database.database().reference(fromURL: "https://turaqshare-1234a.firebaseio.com/")
}
