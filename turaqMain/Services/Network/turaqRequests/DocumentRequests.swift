//
//  DocumentRequests.swift
//  turaQshare
//
//  Created by Yesbol Kulanbekov on 1/28/19.
//  Copyright © 2019 turaQshare. All rights reserved.
//


import Moya
import ObjectMapper
import Result

typealias DocRequestResult = Result<String, MoyaError>


struct GetDocRequest: BaseService {
    
    let placeNumber: Int
    let parkingZone: Int
    let documentID: Int?
    
    var path: String  {
        var docID = ""
        if let unWDoc = documentID {
            docID = String(unWDoc)
        }
        return "/me/places/\(parkingZone)/\(placeNumber)/documents/" + docID
    }
    
    var onReturn: (DocRequestResult) -> Void = { _ in  }
    
    func onMoyaSuccess(_ json: Any) {
        let result = DocRequestResult.success("Success")
        self.onReturn(result)
    }
    
    func onMoyaFailure(_ error: Any) {
//        let result = DocRequestResult.failure(error)
//        self.onReturn(result)
    }
    
}

struct PostDocRequest: BaseService {
    
    let placeNumber: String
    let parkingZone: Int
    let fileURL: URL
    
    var path: String  {
        return "/me/places/\(parkingZone)/\(placeNumber)/documents"
    }
    
    var method: Moya.Method {
        return .post
    }
    
    var task: Task {
        let provider = MultipartFormData.FormDataProvider.file(fileURL)
        let formData = MultipartFormData.init(provider: provider, name: "document")
        return Task.uploadMultipart([formData])
    }
    
    var onReturn: (DocRequestResult) -> Void = { _ in  }
    
    func onMoyaSuccess(_ json: Any) {
        let result = DocRequestResult.success("Upload Success")
        self.onReturn(result)
    }
    
    func onMoyaFailure(_ error: Any) {
//        let result = DocRequestResult.failure(error)
//        self.onReturn(result)
    }
    
}



struct DeleteDocRequest: BaseService {
    
    let placeNumber: Int
    let parkingZone: Int
    let fileNumber: Int
    
    var path: String  {
        return "/me/places/\(parkingZone)/\(placeNumber)/documents/" + "\(fileNumber)"
    }
    
    var method: Moya.Method {
        return .delete
    }
    
    var onReturn: (DocRequestResult) -> Void = { _ in  }
    
    func onMoyaSuccess(_ json: Any) {
        let result = DocRequestResult.success("Delete Success")
        self.onReturn(result)
    }
    
    func onMoyaFailure(_ error: Any) {
//        let result = DocRequestResult.failure(error)
//        self.onReturn(result)
    }
    
}
