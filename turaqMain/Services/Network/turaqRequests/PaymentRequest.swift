//
//  PaymentRequest.swift
//  turaQshare
//
//  Created by Yesbol Kulanbekov on 1/18/19.
//  Copyright © 2019 turaQshare. All rights reserved.
//

import Moya
import ObjectMapper
import Result

typealias PaymentResult = Result<String, MoyaError>

struct PaymentRequest: BaseService {
    
    var baseURL: URL {
        return URL(string:"https://testpay.kkb.kz/jsp/process/logon.jsp")!
    }
    
    var path: String {
        return ""
    }
    
    var method: Moya.Method {
        return .post
    }
    
    var parameters: [String: Any] {
        let params: [String:Any] = [
            "Signed_Order_B64": "PGRvY3VtZW50PjxtZXJjaGFudCBjZXJ0X2lkPSIwMGMxODNkNzBiIiBuYW1lPSJOZXcgRGVtbyBTaG9wIj48b3JkZXIgb3JkZXJfaWQ9IjAxMjIxNjM4NDciIGFtb3VudD0iNTAiIGN1cnJlbmN5PSIzOTgiPjxkZXBhcnRtZW50IG1lcmNoYW50X2lkPSI5MjA2MTEwMyIgYW1vdW50PSI1MCIvPjwvb3JkZXI+PC9tZXJjaGFudD48bWVyY2hhbnRfc2lnbiB0eXBlPSJSU0EiPnNmMDFzN1VxSHdhQnRsbjFUbTNXVkJSeUhuWkl1VkRlMlVhRmJOVkFmUWpyQXJMcC9vSHVCeTRWQ0VBSjRGK3pMUEV0T3ZZOEMzY1I0aVdwcWdNZFJDOHJMVVhsTnAvbXcvc0p1UzlzWmZBNVArdG5oY1dSNVBBUE1OUlcydXprcXl3dnhma2NtOUI3MjE0RkN2RmVZeUlNMnplMW1wTWJqMTJQbXk1WFlXbz08L21lcmNoYW50X3NpZ24+PC9kb2N1bWVudD4=",
            "email": "SeFrolov@kkb.kz",
            "BackLink": "https://testpay.kkb.kz/jsp/client/pay.jsp",
            "FailureBackLink": "https://testpay.kkb.kz/jsp/client/pay.jsp",
            "PostLink": "https://testpay.kkb.kz/jsp/client/pl.jsp"
        ]
        return params
    }
    
    var task: Task {
        return Task.requestParameters(parameters: parameters, encoding: JSONEncoding.default)
    }
    
    var onReturn: (PaymentResult) -> Void = { _ in  }
    
    func onMoyaSuccess(_ json: Any) {
        print("Response is ", json)
        let result = PaymentResult.success("")
        self.onReturn(result)
    }
    
    func onMoyaFailure(_ error: Any) {
//        let result = PaymentResult.failure(error)
//        self.onReturn(result)
    }
    
}
