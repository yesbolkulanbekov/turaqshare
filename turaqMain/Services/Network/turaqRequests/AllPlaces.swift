//
//  AllPlaces.swift
//  turaQshare
//
//  Created by Yesbol Kulanbekov on 3/7/19.
//  Copyright © 2019 turaQshare. All rights reserved.
//

import Foundation
import Result
import ObjectMapper

typealias AllPlacesResult = Result<[ParkingPlace], TuraqError>

struct AllPlaces: BaseService {
    
    var path: String {
        return "/places"
    }
    
    var onReturn: (AllPlacesResult) -> Void = { _ in }
    
    func onMoyaSuccess(_ json: Any) {
        let rootParkingObject = Mapper<RootObjectParking>().map(JSONObject: json)
        
        let resultWithRoot = Result(rootParkingObject, failWith: TuraqError.objectMapperFailure("GetPlacesRequest"))
        
        let placesResult = resultWithRoot.analysis(
            ifSuccess: { PlacesRequestResult.success($0.places) } ,
            ifFailure: { PlacesRequestResult.failure($0)}
        )
        
        self.onReturn(placesResult)
    }

    
    func onMoyaFailure(_ error: Any) {
        let turaqError = RootError.map(error)
        onReturn(AllPlacesResult.failure(turaqError))
    }
    
    
}
