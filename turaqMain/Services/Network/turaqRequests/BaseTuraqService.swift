//
//  MoyaAltUsage.swift
//  turaQshare
//
//  Created by Yesbol Kulanbekov on 1/17/19.
//  Copyright © 2019 turaQshare. All rights reserved.
//

import Moya
import ObjectMapper
import Result


let multiProvider = MoyaProvider<MultiTarget>()


protocol BaseService: TargetType {
    func onMoyaSuccess(_ json: Any)
    func onMoyaFailure(_ error: Any)
    func onMoyaFailure(_ moyaError: MoyaError)
}

extension BaseService {
    var baseURL: URL {
        return URL(string:"\(turaqURL.base)")!
    }
    
    var path: String  {
        return ""
    }
    
    var method: Moya.Method {
        return .get
    }
    
    var task: Task {
        return .requestPlain
    }
    
    var headers: [String : String]? {
        let localStorage = UserDefaultsUserStorageService.shared
        let token = localStorage.getToken() ?? ""
        print(token)
        return ["Authorization": token ]
    }
    
    var validationType: ValidationType {
        return .successCodes
    }
    
    var sampleData: Data {
        switch self {
        default: return "Sample Data".utf8Encoded
        }
    }
    
}


extension BaseService {
        
    func handle(_ data: Data, with onSerialize: (Any) -> Void) {
        do {
            let json = try JSONSerialization.jsonObject(with: data, options: [])
            print("Json object from data is ", json)
            onSerialize(json)
        } catch {
            print("Data can't be parsed into json")
            return
        }
    }
    
    func onMoyaRequestCompletion(_ result: Result<Moya.Response, MoyaError>) {
        switch result {
        case let .success(response):
            handle(response.data) { self.onMoyaSuccess($0) }
            
        case let .failure(error):
            guard let resp = error.response else { return }
            handle(resp.data) { self.onMoyaFailure($0) }
            onMoyaFailure(error)
        }
    }
    
    func dispatch() {
        let target = MultiTarget(self)
        multiProvider.request(target, completion: self.onMoyaRequestCompletion)
    }
    
    
}

// MARK: Default Implementations

extension BaseService {
    
    func onMoyaFailure(_ moyaError: MoyaError) {
        print("Moya error is: ", moyaError.localizedDescription)
    }
    
    func onMoyaFailure(_ error: Any) {
        
    }

    func onMoyaSuccess(_ json: Any) {
        
    }

}


