//
//  GetFreeTimes.swift
//  turaQshare
//
//  Created by Yesbol Kulanbekov on 3/7/19.
//  Copyright © 2019 turaQshare. All rights reserved.
//

import Foundation
import Result
import ObjectMapper
import Moya

typealias GetFreeTimesResult = Result<[FreeTime], TuraqError>

struct GetFreeTimes: BaseService {
    
    var path: String {
        return "/me/free-times"
    }
    
    var onReturn: (GetFreeTimesResult) -> Void = { _ in }
    
    func onMoyaSuccess(_ json: Any) {
        
        let rootObject = Mapper<RootObjFreeTimes>().map(JSONObject: json)
        
        let resultWithRoot = Result(rootObject, failWith: TuraqError.objectMapperFailure("GetFreeTimes"))
        
        let freetimesResult = resultWithRoot.analysis(
            ifSuccess: { GetFreeTimesResult.success($0.freetimes) } ,
            ifFailure: { GetFreeTimesResult.failure($0)}
        )
        
        self.onReturn(freetimesResult)
    }
    
    
    func onMoyaFailure(_ error: Any) {
        let turaqError = RootError.map(error)
        onReturn(GetFreeTimesResult.failure(turaqError))
    }
    
    
}


struct RootObjFreeTimes {
    var message: String!
    var freetimes: [FreeTime]!
}

extension RootObjFreeTimes: Mappable {
    init?(map: Map) {
        
    }
    
    mutating func mapping(map: Map) {
        message <- map["message"]
        freetimes  <- map["freeTimes"]
    }
    
}



struct GetPublicFreeTimes: BaseService {
    
    // MARK: Public

    var zoneID: Int

    var onReturn: (GetFreeTimesResult) -> Void = { _ in }


    // MARK: Private
    
    var path: String {
        return "/free-times"
    }
    
    var task: Task {
        let params = ["zone-id": zoneID]
        return .requestParameters(parameters: params, encoding: URLEncoding.default)
    }
    
    func onMoyaSuccess(_ json: Any) {
        
        let rootObject = Mapper<RootObjFreeTimes>().map(JSONObject: json)
        
        let resultWithRoot = Result(rootObject, failWith: TuraqError.objectMapperFailure("GetFreeTimes"))
        
        let freetimesResult = resultWithRoot.analysis(
            ifSuccess: { GetFreeTimesResult.success($0.freetimes) } ,
            ifFailure: { GetFreeTimesResult.failure($0)}
        )
        
        self.onReturn(freetimesResult)
    }
    
    
    func onMoyaFailure(_ error: Any) {
        let turaqError = RootError.map(error)
        onReturn(GetFreeTimesResult.failure(turaqError))
    }
    
    
}
