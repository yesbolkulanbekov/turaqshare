//
//  PlacesRequests.swift
//  turaQshare
//
//  Created by Yesbol Kulanbekov on 1/24/19.
//  Copyright © 2019 turaQshare. All rights reserved.
//

import Moya
import ObjectMapper
import Result

typealias PlacesRequestResult = Result<[ParkingPlace], TuraqError>

struct GetPlacesRequest: BaseService {
        
    var path: String  {
        return "/me/places"
    }
    
    var onReturn: (PlacesRequestResult) -> Void = { _ in  }
    
    func onMoyaSuccess(_ json: Any) {
        let rootParkingObject = Mapper<RootObjectParking>().map(JSONObject: json)
        
        let resultWithRoot = Result(rootParkingObject, failWith: TuraqError.objectMapperFailure("GetPlacesRequest"))
        
        let placesResult = resultWithRoot.analysis(
            ifSuccess: { PlacesRequestResult.success($0.places) } ,
            ifFailure: { PlacesRequestResult.failure($0)}
        )
        
        self.onReturn(placesResult)
    }
    
    func onMoyaFailure(_ error: Any) {
        var turaqError = TuraqError.objectMapperFailure("GetPlacesRequest")
        guard let rootParkingObject = Mapper<RootObjectParking>().map(JSONObject: error) else {
            self.onReturn(PlacesRequestResult.failure(turaqError))
            return
        }
        turaqError = TuraqError.serverMessage(rootParkingObject.message)
        
        let result = PlacesRequestResult.failure(turaqError)
        self.onReturn(result)
    }
    
}

typealias PostPlacesReqRes = Result<String, TuraqError>

struct PostPlacesRequest: BaseService {
    
    let placeNumber: String
    let parkingZone: Int
    
    var path: String  {
        return "/me/places"
    }
    
    var method: Moya.Method {
        return .post
    }
    
    var params: [String: Any] {
        return [
            "PlaceNumber": placeNumber,
            "ParkingZone": parkingZone
        ]
    }
    
    var headers: [String : String]? {
        let localStorage = UserDefaultsUserStorageService.shared
        let token = localStorage.getToken() ?? ""
        print(token)
        return ["Content-type": "application/x-www-form-urlencoded",
                "Authorization": token ]
    }
    
    var task: Task {
        return Task.requestParameters(parameters: params, encoding: URLEncoding.default)
    }
    
    
    var onReturn: (PostPlacesReqRes) -> Void = { _ in  }
    
    func onMoyaSuccess(_ json: Any) {
        let rootParkingObject = Mapper<RootObjectParking>().map(JSONObject: json)
        let resultWithRoot = Result(rootParkingObject, failWith: TuraqError.objectMapperFailure("PostPlacesRequest"))
        
        let postPlacesResult = resultWithRoot.analysis(
            ifSuccess: { PostPlacesReqRes.success($0.message) } ,
            ifFailure: { PostPlacesReqRes.failure($0)}
        )
        
        self.onReturn(postPlacesResult)

    }
    
    func onMoyaFailure(_ error: Any) {
        var turaqError = TuraqError.objectMapperFailure("PostPlacesRequest")
        guard let rootParkingObject = Mapper<RootObjectParking>().map(JSONObject: error) else {
            self.onReturn(PostPlacesReqRes.failure(turaqError))
            return
        }
        turaqError = TuraqError.serverMessage(rootParkingObject.message)
        let result = PostPlacesReqRes.failure(turaqError)
        self.onReturn(result)
    }
    
}


struct DeletePlaceRequest: BaseService {
    
    let placeNumber: Int
    let parkingZone: Int
    
    var path: String  {
        return "/me/places/\(parkingZone)/\(placeNumber)"
    }
    
    var method: Moya.Method {
        return .delete
    }
    
    var onReturn: (PlacesRequestResult) -> Void = { _ in  }
    
    func onMoyaSuccess(_ json: Any) {
//        let result = PlacesRequestResult.success("Deleted")
//        self.onReturn(result)
    }
    
    func onMoyaFailure(_ error: Any) {
//        let result = PlacesRequestResult.failure(error)
//        self.onReturn(result)
    }
    
}

