//
//  Zones.swift
//  turaQshare
//
//  Created by Yesbol Kulanbekov on 2/21/19.
//  Copyright © 2019 turaQshare. All rights reserved.
//

import Moya
import ObjectMapper
import Result


struct Zones: BaseService {
    
    var path: String {
        return "/zones"
    }
    
    var onReturn: (Result<[Zone], TuraqError>) -> Void = { _ in }
    
    func onMoyaSuccess(_ json: Any) {
        
        let rootObject = RootObjectZones.map(json)
        let zonesJson = rootObject.zones
        let zones = Zone.map(zonesJson)
        let result = Result<[Zone], TuraqError>.success(zones)
        self.onReturn(result)
    }
    
    func onMoyaFailure(_ error: Any) {
        var turaqError = TuraqError.objectMapperFailure("GetPlacesRequest")
        guard let rootZonesObject = Mapper<RootObjectZones>().map(JSONObject: error) else {
            self.onReturn(Result.failure(turaqError))
            return
        }
        turaqError = TuraqError.serverMessage(rootZonesObject.message)
        
        let result = Result<[Zone], TuraqError>.failure(turaqError)
        self.onReturn(result)
    }
    
}
