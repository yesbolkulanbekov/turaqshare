//
//  GetPublicSchedules.swift
//  turaQshare
//
//  Created by Yesbol Kulanbekov on 3/23/19.
//  Copyright © 2019 turaQshare. All rights reserved.
//

import Foundation
import Result
import ObjectMapper
import Moya

typealias GetPublicSchedulesResult = Result<[Schedule], TuraqError>

struct GetPublicSchedules: BaseService {

    // MARK: Public
    var onReturn: (GetPublicSchedulesResult) -> Void = { _ in }

    var freetimeID: String
    
    // MARK: Private
    
    var path: String {
        return "/schedules"
    }
    
    var task: Task {
        let params = ["free-time": freetimeID]
        return .requestParameters(parameters: params, encoding: URLEncoding.default)
    }
    
    
    func onMoyaSuccess(_ json: Any) {
        
        let rootObject = Mapper<RootObjSchedules>().map(JSONObject: json)
        
        let resultWithRoot = Result(rootObject, failWith: TuraqError.objectMapperFailure("GetPublicSchedules"))
        
        let freetimesResult = resultWithRoot.analysis(
            ifSuccess: { GetPublicSchedulesResult.success($0.publicSchedules) } ,
            ifFailure: { GetPublicSchedulesResult.failure($0)}
        )
        
        self.onReturn(freetimesResult)
    }
    
    
    func onMoyaFailure(_ error: Any) {
        let turaqError = RootError.map(error)
        onReturn(GetPublicSchedulesResult.failure(turaqError))
    }
    
    
}


struct RootObjSchedules {
    var message: String!
    var publicSchedules: [Schedule]!
}

extension RootObjSchedules: Mappable {
    init?(map: Map) {
        
    }
    
    mutating func mapping(map: Map) {
        message <- map["message"]
        publicSchedules  <- map["publicSchedules"]
    }
    
}



struct Schedule {
    var status: BookingStatus!
    var startTime: Date!
    var endTime: Date!
}

extension Schedule: Mappable {
    init?(map: Map) { }
    
    mutating func mapping(map: Map) {
        status <- map["Status"]
        startTime <- (map["StartTime"], ISO8601DateTransform() )
        endTime <- (map["EndTime"], ISO8601DateTransform())
    }
    
}


enum BookingStatus: String {
    case free = "Free"
    case booked = "Booked"
}
