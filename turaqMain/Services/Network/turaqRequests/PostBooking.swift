//
//  PostBooking.swift
//  turaQshare
//
//  Created by Yesbol Kulanbekov on 3/10/19.
//  Copyright © 2019 turaQshare. All rights reserved.
//


import Moya
import ObjectMapper
import Result

typealias PostBookingResult = Result<String, TuraqError>

struct PostBookingReq: BaseService {
    
    let booking: Booking
    
    var onReturn: (PostBookingResult) -> Void = { _ in  }
    
    var path: String {
        return "/me/bookings"
    }
    
    var method: Moya.Method {
        return .post
    }
    
    var headers: [String : String]? {
        let localStorage = UserDefaultsUserStorageService.shared
        let token = localStorage.getToken() ?? ""
        
        return ["Content-type": "application/x-www-form-urlencoded",
                "Authorization": token ]
    }
    
    private var parameters: [String: Any] {
        
        var prms: [String: Any] = [
            "CarNumber": booking.carNumber,
            "ParkingPlaceNumber": booking.parkingPlaceNumber!,
            "ParkingZone": booking.parkingZone!,
            "StartTime": booking.startTime!,
            "EndTime": booking.endTime!,
            "Frequency": booking.frequency!,
            "RepeatStartTime": booking.repeatStartTime!,
            "RepeatEndTime": booking.repeatEndTime!,
            ]
        
        if let weekDays = booking.weekDays {
            for day in weekDays {
                prms[day] = true
            }
        }


        return prms
        
    }
    
    var task: Task {
        return .requestParameters(parameters: parameters, encoding: URLEncoding.default)
    }
    
    func onMoyaSuccess(_ json: Any) {
        let result = PostBookingResult.success("Success post Bookings")
        self.onReturn(result)
    }
    
    func onMoyaFailure(_ error: Any) {
        let turaqError = RootError.map(error)
        onReturn(PostBookingResult.failure(turaqError))
    }
    
}


struct Booking {
    
    var uuid: String!
    var customer: String!
    var owner: String!
    
    var parkingPlaceNumber: String!
    var parkingZone: Int!
    var startTime: String!
    var endTime: String!
    var status: String!
    var frequency: Int!
    var repeatStartTime: String!
    var repeatEndTime: String!

    var weekDays: [String]?
    var carNumber: String!

}


extension Booking: Mappable {
    
    init?(map: Map) {

    }

    mutating func mapping(map: Map) {
        parkingPlaceNumber <- map["ParkingPlaceNumber"]
        parkingZone        <- map["ParkingZone"]
        startTime          <- map["StartTime"]
        endTime            <- map["EndTime"]
        frequency          <- map["Frequency"]
        repeatStartTime    <- map["RepeatStartTime"]
        repeatEndTime      <- map["RepeatEndTime"]
    }

}

