//
//  PostFreeTimesReq.swift
//  turaQshare
//
//  Created by Yesbol Kulanbekov on 2/24/19.
//  Copyright © 2019 turaQshare. All rights reserved.
//

import Moya
import ObjectMapper
import Result

typealias PostFreeTimesResult = Result<String, TuraqError>

struct PostFreeTimesReq: BaseService {

    let freetime: FreeTime
    
    var onReturn: (PostFreeTimesResult) -> Void = { _ in  }

    var path: String {
        return "/me/free-times"
    }
    
    var method: Moya.Method {
        return .post
    }
    
    var headers: [String : String]? {
        let localStorage = UserDefaultsUserStorageService.shared
        let token = localStorage.getToken() ?? ""
        
        return ["Content-type": "application/x-www-form-urlencoded",
                "Authorization": token ]
    }
    
    private var parameters: [String: Any] {
        
    
        var prms: [String: Any] = [
            "ParkingPlaceNumber": freetime.parkingPlaceNumber!,
            "ParkingZone": freetime.parkingZone!,
            "StartTime": freetime.startTime!,
            "EndTime": freetime.endTime!,
            "Frequency": freetime.frequency!,
            "RepeatStartTime": freetime.repeatStartTime!,
            "RepeatEndTime": freetime.repeatEndTime!,
        ]
        if let weekDays = freetime.weekDays {
            for day in weekDays {
                prms[day] = true
            }
        }
        
        
        return prms
    }
    
    var task: Task {
        return .requestParameters(parameters: parameters, encoding: URLEncoding.default)
    }
    
    
    func onMoyaSuccess(_ json: Any) {
        let result = PostFreeTimesResult.success("Success")
        self.onReturn(result)
    }
    
    func onMoyaFailure(_ error: Any) {
        let turaqError = RootError.map(error)
        onReturn(PostFreeTimesResult.failure(turaqError))
    }
    
}


struct FreeTime {
    var uuid: String!
    var parkingPlaceNumber: String!
    var parkingZone: Int!
    var startTime: String!
    var endTime: String!
    var frequency: Int!
    var repeatStartTime: String!
    var repeatEndTime: String!
    
    var weekDays: [String]?

}


extension FreeTime: Mappable {
    init?(map: Map) {
        
    }
    
    mutating func mapping(map: Map) {
        uuid               <- map["UUID"]
        parkingPlaceNumber <- map["ParkingPlaceNumber"]
        parkingZone        <- map["ParkingZone"]
        startTime          <- map["StartTime"]
        endTime            <- map["EndTime"]
        frequency          <- map["Frequency"]
        repeatStartTime    <- map["RepeatStartTime"]
        repeatEndTime      <- map["RepeatEndTime"]
    }
    
}
