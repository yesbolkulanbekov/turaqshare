//
//  NetworkError.swift
//  turaQshare
//
//  Created by Yesbol Kulanbekov on 1/16/19.
//  Copyright © 2019 turaQshare. All rights reserved.
//

import Foundation

enum NetworkError: Error {
    case serverFailure(String)
    
    var localizedDescription: String {
        switch self {
        case .serverFailure(let message):
            return "Server failure " + message
        }
    }
}
