//
//  StructMoyaExample.swift
//  turaQshare
//
//  Created by Yesbol Kulanbekov on 1/28/19.
//  Copyright © 2019 turaQshare. All rights reserved.
//

import Moya
import Result
import ObjectMapper


/// Example

struct NewService: BaseService {
    
    let id: Int
    var path: String {
        return "/people/\(id)"
    }
    
    var onReturn: (Result<Person, MoyaError>) -> Void = { _ in }
    
    func onMoyaSuccess(_ json: Any) {
        let person = Mapper<Person>().map(JSONObject: json)!
        let result = Result<Person, MoyaError>.success(person)
        self.onReturn(result)
    }
    
    func onMoyaFailure(_ error: Any) {
//        let result = Result<Person, MoyaError>.failure(error)
//        self.onReturn(result)
    }
    
}



class Person: Mappable {
    var eye_color: String?
    
    
    func mapping(map: Map) {
        eye_color        <- map["eye_color"]
    }
    
    required init?(map: Map) {}
    
}
