//
//  Provider.swift
//  turaQshare
//
//  Created by Yesbol Kulanbekov on 1/15/19.
//  Copyright © 2019 turaQshare. All rights reserved.
//

import Moya

enum turaqURL {
    static let base = "http://35.158.124.136/api/v1"
}

let provider = MoyaProvider<TuraqService>()

enum TuraqService{
    case getUser
    case register(User.New)
    case login(User.Login)
    case refresh(String)
}


extension TuraqService: TargetType {
    
    var baseURL: URL {
        return URL(string:"\(turaqURL.base)")!
    }
    
    var path: String {
        switch self {
        case .getUser:
            return "/me"
        case .register(_):
            return "/register"
        case .login:
            return "/login"
        case .refresh:
            return "/refresh"
        }
    }
    
    var method: Moya.Method {
        switch self {
        case .getUser:
            return .get
            
        case .register(_), .login(_), .refresh(_):
            return .post

        }
    }
    
    var task: Task {
        switch self {
        case .getUser:
            return .requestPlain
        case .register(let newUser):
            return .requestParameters(parameters: newUser.postParameters, encoding: URLEncoding.default)
        case .login(let login):
            return .requestParameters(parameters: login.postParameters, encoding: URLEncoding.default)
        case .refresh(let token):
            return .requestParameters(parameters: ["Refresh": token], encoding: URLEncoding.default)
        }
        
    }
    
    /// Header and Sample data
    
    var headers: [String : String]? {
        switch self {
        case .getUser:
            let localStorage = UserDefaultsUserStorageService.shared
            let token = localStorage.getToken() ?? ""
            return ["Content-type": "application/json",
                    "Authorization": token ]
            
        case .register(_), .login(_), .refresh(_):
            return ["Content-type": "application/x-www-form-urlencoded"]
        }
        

    }
    
    var validationType: ValidationType {
        return .successCodes
    }
    
    var sampleData: Data {
        switch self {
        default: return "Sample Data".utf8Encoded
        }
    }
    
}



// MARK: - Helpers
extension String {
    var urlEscaped: String {
        return addingPercentEncoding(withAllowedCharacters: .urlHostAllowed)!
    }
    
    var utf8Encoded: Data {
        return data(using: .utf8)!
    }
}
