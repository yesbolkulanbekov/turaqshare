//
//  TuraqAPI.swift
//  turaQshare
//
//  Created by Yesbol Kulanbekov on 1/15/19.
//  Copyright © 2019 turaQshare. All rights reserved.
//

import Moya
import ObjectMapper

public class TuraqAPI {
    
    
    public static let shared: TuraqAPI = TuraqAPI()
    


    func getUser(onSuccess: @escaping (User) -> Void,
                 onFailure: @escaping (Error) -> Void)
    {
        executeRequest(.getUser, onSuccess: { (response) in
            let json = try! JSONSerialization.jsonObject(with: response.data, options: [])
            let userRootObject = Mapper<UserRootObject>().map(JSONObject: json)!
            onSuccess(userRootObject.user)
        }, onFailure: { (error) in
            onFailure(error)
        })
    }
    
    
    func register(_ newUser: User.New,
                  onSuccess: @escaping (User) -> Void,
                  onFailure: @escaping (TuraqError) -> Void)
    {
        executeRequest(.register(newUser), onSuccess: { (response) in
            let json = try! JSONSerialization.jsonObject(with: response.data, options: [])
            let userRootObject = Mapper<UserRootObject>().map(JSONObject: json)!
            let localStorage = UserDefaultsUserStorageService.shared
            localStorage.setToken(userRootObject.user.accessToken)
            onSuccess(userRootObject.user)
        }, onFailure: { (error) in
            guard let resp = error.response else { return }
            let json = try! JSONSerialization.jsonObject(with: resp.data, options: [])
            let turaqError = RootError.map(json)
            onFailure(turaqError)
        })
    }

    
    func login(_ login: User.Login,
               onSuccess: @escaping (User) -> Void,
               onFailure: @escaping (Error) -> Void)
    {
        executeRequest(.login(login), onSuccess: { (response) in
            let json = try! JSONSerialization.jsonObject(with: response.data, options: [])
            let userRootObject = Mapper<UserRootObject>().map(JSONObject: json)!
            let localStorage = UserDefaultsUserStorageService.shared
            localStorage.setToken(userRootObject.user.accessToken)
            onSuccess(userRootObject.user)
        }, onFailure: { (error) in
            onFailure(error)
        })
    }
    
    func refresh(_ accessToken: String,
                 onSuccess: @escaping () -> Void,
                 onFailure: @escaping (Error) -> Void)
    {
        executeRequest(.refresh(accessToken), onSuccess: { (response) in
            let json = try! JSONSerialization.jsonObject(with: response.data, options: [])
            let token = Mapper<AccessToken>().map(JSONObject: json)!
            let localStorage = UserDefaultsUserStorageService.shared
            localStorage.setToken(token.token)
            onSuccess()
        }, onFailure: { (error) in
            onFailure(error)
        })
    }
    
    
    
    /// Private methods
    
    private func executeRequest(_ service: TuraqService,
                                onSuccess: @escaping (Response) -> Void,
                                onFailure: @escaping (MoyaError) -> Void) {
        provider.request(service) { (result) in
            
            switch result {
            case let .success(response):
                
                self.debug(response)

                guard (200...201).contains(response.statusCode) else {
//                    onFailure(NetworkError.serverFailure("Status code: \(response.statusCode)"))
                    return
                }
                
                onSuccess(response)
                
            case let .failure(error):
                print("Moya error is: ", error.localizedDescription)
                self.debug(error.response)
//                onFailure(NetworkError.serverFailure(error.localizedDescription))
                onFailure(error)
            }
        }
    }
    
    private func debug(_ response: Response?) {
        guard let response = response else {
            print("Data response is nil")
            return
        }
        print("Debug Status code is ", response.statusCode)
        let json = try! JSONSerialization.jsonObject(with: response.data, options: [])
        print("Debug Response is ", json)
    }
    
}
