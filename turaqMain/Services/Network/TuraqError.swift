//
//  TuraqError.swift
//  turaQshare
//
//  Created by Yesbol Kulanbekov on 1/28/19.
//  Copyright © 2019 turaQshare. All rights reserved.
//

import Foundation

enum TuraqError: Error {
    case serverFailure
    case objectMapperFailure(String)
    case serverMessage(String)
    
    var localizedDescription: String {
        switch self {
        case .serverFailure:
            return "Server failure"
        case .objectMapperFailure(let object):
            return "Mapping objects failed for object: \(object)"
        case .serverMessage(let message):
            return message
        }
    }
}
