//
//  UserDefaultsStora.swift
//  turaQshare
//
//  Created by Yesbol Kulanbekov on 12/14/18.
//  Copyright © 2018 Galym Anuarbek. All rights reserved.
//

import Foundation

// TODO: Add this to shared


fileprivate struct Config {
    static let phone = "phoneFireBaseAuthVerificationID"
    static let tutorial = "didShowTutorial"
    static let token = "token"
}


class UserDefaultsUserStorageService {
    private var defaults = UserDefaults.standard
    
    static let shared = UserDefaultsUserStorageService()
 
    func setPhoneAuthID(_ id: String?) {
        defaults.set(id, forKey: Config.phone)
    }
    
    func getPhoneAuthID() -> String? {
        return defaults.string(forKey: Config.phone)
    }
    
    func setDidShowTut(_ didShow: Bool) {
        defaults.set(didShow, forKey: Config.tutorial)
    }
    
    func getDidShowTut() -> Bool {
        return defaults.bool(forKey: Config.tutorial)
    }
    
    
    public func setToken(_ token: String) {
        defaults.set(token, forKey: Config.token)
    }
    
    public func getToken() -> String? {
        return defaults.string(forKey: Config.token)
    }
    
    public func removeToken() {
        defaults.removeObject(forKey: Config.token)
    }

}
