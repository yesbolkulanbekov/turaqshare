//
//  ParkingPlace.swift
//  turaQshare
//
//  Created by Yesbol Kulanbekov on 1/28/19.
//  Copyright © 2019 turaQshare. All rights reserved.
//

import ObjectMapper


struct ParkingPlace {
    var placeNumber: String!
    var parkingZone: Int!
    var owner: String!
    var status: Int!
}


extension ParkingPlace: Mappable {
    init?(map: Map) {
        
    }
    
    mutating func mapping(map: Map) {
        placeNumber <- map["PlaceNumber"]
        parkingZone <- map["ParkingZone"]
        owner       <- map["Owner"]
        status      <- map["Status"]
    }
    
}


struct RootObjectParking {
    var message: String!
    var places: [ParkingPlace]!
}

extension RootObjectParking: Mappable {
    init?(map: Map) {
        
    }
    
    mutating func mapping(map: Map) {
        message <- map["message"]
        places  <- map["places"]
    }
    
}
