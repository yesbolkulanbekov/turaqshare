//
//  Zones.swift
//  turaQshare
//
//  Created by Yesbol Kulanbekov on 2/21/19.
//  Copyright © 2019 turaQshare. All rights reserved.
//

import ObjectMapper


struct Zone {
    var id: Int!
    var name: String!
    var address: String!
    var longitude: Double!
    var latitude: Double!
    var photoPath: String!
    
    static var mpr = Mapper<Zone>()
    
    static func map(_ json: Any) -> [Zone] {
        return Zone.mpr.mapArray(JSONObject: json)!
    }
}


extension Zone: Mappable {
    init?(map: Map) {
        
    }
    
    mutating func mapping(map: Map) {
        id          <- map["ID"]
        name        <- map["Name"]
        address     <- map["Address"]
        longitude   <- map["Longitude"]
        latitude    <- map["Latitude"]
        photoPath   <- map["PhotoPath"]
    }
    
}



struct RootObjectZones {
    var message: String!
    var zones: Any!
    
    static var mpr = Mapper<RootObjectZones>()
    
    static func map(_ json: Any) -> RootObjectZones {
        return RootObjectZones.mpr.map(JSONObject: json)!
    }
}

extension RootObjectZones: Mappable {
    init?(map: Map) {
        
    }
    
    mutating func mapping(map: Map) {
        message <- map["message"]
        zones   <- map["zones"]
    }
    
}
