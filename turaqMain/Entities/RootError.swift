//
//  File.swift
//  turaQshare
//
//  Created by Yesbol Kulanbekov on 2/24/19.
//  Copyright © 2019 turaQshare. All rights reserved.
//

import ObjectMapper

struct RootError {
    var message: String!
    
    static func map(_ json: Any) -> TuraqError {
        
        guard let rootError = Mapper<RootError>().map(JSONObject: json) else {
            return TuraqError.objectMapperFailure("RootError's object mapping failure")
        }
        
        return TuraqError.serverMessage(rootError.message)
    }
}

extension RootError: Mappable {
    init?(map: Map) {
        
    }
    
    mutating func mapping(map: Map) {
        message <- map["message"]
    }
    
}
