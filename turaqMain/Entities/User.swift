//
//  User.swift
//  turaQshare
//
//  Created by Yesbol Kulanbekov on 12/24/18.
//  Copyright © 2018 turaQshare. All rights reserved.
//

import ObjectMapper


struct User  {
    var uuID: String
    var firstName: String
    var secondName: String
    var phone: String
    var email: String
    var accessToken: String
    var refreshToken: String
}

extension User: Mappable {
    
    init?(map: Map) {
        self.init(uuID: "", firstName: "", secondName: "", phone: "", email: "", accessToken: "", refreshToken: "")
    }
    
    mutating func mapping(map: Map) {
        uuID          <- map["UUID"]
        firstName     <- map["FirstName"]
        secondName    <- map["SecondName"]
        phone         <- map["Phone"]
        email         <- map["Email"]
        accessToken         <- map["AccessToken"]
        refreshToken        <- map["RefreshToken"]
    }
}


extension User {
    
    struct New {
        var firstName: String
        var secondName: String
        var phone: String
        var email: String
        var password: String
        var confirmPassword: String
        
        var postParameters: [String: Any] {
            return [
                "FirstName": self.firstName,
                "SecondName": self.secondName,
                "Phone": self.phone,
                "Email": self.email,
                "Password": self.password,
                "ConfirmPassword": self.confirmPassword
            ]
        }
    }
    
    struct Login {
        let phone: String
        let password: String
        
        var postParameters: [String: Any] {
            return [
                "Phone":    self.phone,
                "Password": self.password
            ]
        }
    }
    
}



class UserRootObject: Mappable {
    var message: String?
    var user: User!
    
    func mapping(map: Map) {
        message        <- map["message"]
        user           <- map["user"]
    }
    
    
    required init?(map: Map) {}
    
}
