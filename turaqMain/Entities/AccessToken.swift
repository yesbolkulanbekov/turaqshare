//
//  AccessToken.swift
//  turaQshare
//
//  Created by Yesbol Kulanbekov on 1/16/19.
//  Copyright © 2019 turaQshare. All rights reserved.
//

import ObjectMapper

class AccessToken: Mappable {
    var token: String!
    
    func mapping(map: Map) {
        token        <- map["AccessToken"]
    }
    
    required init?(map: Map) {}
    
}
