//
//  AppDelegate.swift
//  turaQshare
//
//  Created by Galym Anuarbek on 10/2/18.
//  Copyright © 2018 Galym Anuarbek. All rights reserved.
//

import UIKit
import Firebase
import IQKeyboardManager

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?

    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        
        FirebaseApp.configure()
        
        IQKeyboardManager.shared().isEnabled = true

        
        UINavigationBar.appearance().titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.denim]
        UINavigationBar.appearance().largeTitleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.denim]
        UINavigationBar.appearance().tintColor = UIColor.denim
        
//        let BarButtonItemAppearance = UIBarButtonItem.appearance()

//        let attributes = [
//            NSAttributedString.Key.font: UIFont(name: "Helvetica-Bold", size: 0.1)!,
//            NSAttributedString.Key.foregroundColor: UIColor.clear
//        ]
        
//        BarButtonItemAppearance.setTitleTextAttributes(attributes, for: .normal)
//        BarButtonItemAppearance.setTitleTextAttributes(attributes, for: .highlighted)
        
        window = UIWindow(frame: UIScreen.main.bounds)
        ensureRootViewController()
        window?.makeKeyAndVisible()

        
        #if DEBUG
        Bundle(path: "/Applications/InjectionIII.app/Contents/Resources/iOSInjection10.bundle")?.load()
        #endif
        
        return true
    }

    func ensureRootViewController() {

//        let firstVC = TSettingsModuleConfigurator().assembleModule()
//        window?.rootViewController = firstVC
//        return

        let localStorage = UserDefaultsUserStorageService.shared
        let didShow = localStorage.getDidShowTut()
        
        guard didShow else {
            let tutVC = TutorialModuleConfigurator().assembleModule()
            window?.rootViewController = tutVC
            return
        }
        
        
        let token = localStorage.getToken()
    
        if token == nil  {
            let loginVC = LoginViewModuleConfigurator().assembleModule()
            let navigationController = UINavigationController(rootViewController: loginVC)
            window?.rootViewController = navigationController
        }
        else {
            let tabbarVC = TabBarViewController()
            window?.rootViewController = tabbarVC
        }
    
        
    }


}



// TODO: Show server error when registering and logging in
// TODO: Use SFSafariViewController

