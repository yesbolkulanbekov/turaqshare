//
//  User.swift
//  turaQshare
//
//  Created by Galym Anuarbek on 10/13/18.
//  Copyright © 2018 Galym Anuarbek. All rights reserved.
//

import UIKit
import FirebaseAuth
import FirebaseDatabase

class UserOld: NSObject {
    
    var uid: String
    var phone: String
    var name: String
    var carNumber: String
    var image: UIImage
    
    init(uid: String, phone: String, name: String, carNumber: String, image: UIImage) {
        self.uid = uid
        self.phone = phone
        self.name = name
        self.carNumber = carNumber
        self.image = image
    }
    
    class func create(uid: String?, carNumber: String, name: String, phone: String) {
        if uid != nil {
            var ref: DatabaseReference!
            ref = Database.database().reference()
            ref.child("users").child(uid!).setValue(["car_number": carNumber, "name": name, "phone": phone])
        }
    }
    
    class func userExists(uid: String, completion: @escaping (Bool) -> Swift.Void) {
        Database.database().reference().child("users").observeSingleEvent(of: DataEventType.value) { (snapshot) in
            if snapshot.hasChild(uid) {
                completion(true)
            } else {
                completion(false)
            }
        }
    }
    
    class func getme(completion: @escaping (UserOld) -> Swift.Void) {
        Database.database().reference().child("users").child(UserDefaults.standard.string(forKey: "token")!).observe(DataEventType.value) { (snapshot) in
            if let userData = snapshot.value as? [String:String] {
                completion(UserOld(uid: UserDefaults.standard.string(forKey: "token")!, phone: userData["phone"] ?? "", name: userData["name"] ?? "", carNumber: userData["car_number"] ?? "", image: UIImage()))
            }
        }
    }
    
}
