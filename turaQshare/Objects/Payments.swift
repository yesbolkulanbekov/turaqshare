//
//  Payments.swift
//  turaQshare
//
//  Created by Galym Anuarbek on 10/13/18.
//  Copyright © 2018 Galym Anuarbek. All rights reserved.
//

import UIKit

class Payments: NSObject {
    
    var status: PaymentStatus
    var date: Date
    var quantity: Int
    var price: Int
    var detail: String
    
    let paidImage = UIImage(named: "paidStatus")!
    let rejectedImage = UIImage(named: "rejectedStatus")!
    let reservedImage = UIImage(named: "reservedStatus")!
    
    init(status: PaymentStatus, date: Date, quantity: Int, price: Int, detail: String) {
        self.status = status
        self.date = date
        self.quantity = quantity
        self.price = price
        self.detail = detail
    }
    
    func getStatusImage() -> UIImage {
        switch status {
        case .paid:
            return paidImage
        case .reserved:
            return reservedImage
        case .rejected:
            return rejectedImage
        }
    }
    
}
