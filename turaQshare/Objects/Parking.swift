//
//  Parking.swift
//  turaQshare
//
//  Created by Galym Anuarbek on 10/17/18.
//  Copyright © 2018 Galym Anuarbek. All rights reserved.
//

import UIKit
import FirebaseDatabase

class ParkingOld: NSObject {
    
    var owner: UserOld
    var startTime: Date
    var endTime: Date
    var index: String
    
    init(owner: UserOld, startTime: Date, endTime: Date, index: String) {
        self.owner = owner
        self.startTime = startTime
        self.endTime = endTime
        self.index = index
    }
    
    class func getMyLots(token: String, completion: @escaping ([ParkingOld]) -> Swift.Void) {
        var parkings: [ParkingOld] = []
        Database.database().reference().child("parkings").child("ЖК Новый Мир").child(token).observeSingleEvent(of: DataEventType.value) { (snapshot) in
            if let datas = snapshot.value as? [String:Any] {
                for data in datas {
                    if let parking = data.value as? [String:Any] {
                        UserOld.getme(completion: { (user) in
                            let this_parking = ParkingOld(owner: user, startTime: Date(timeIntervalSince1970: TimeInterval(exactly: parking["start"] as! Int) ?? 0), endTime: Date(timeIntervalSince1970: TimeInterval(exactly: parking["end"] as! Int) ?? 0), index: data.key)
                            parkings.append(this_parking)
                            DispatchQueue.main.async {
                                completion(parkings)
                            }
                        })
                    } else {
                        completion(parkings)
                    }
                }
            } else {
               completion(parkings)
            }
        }
    }
    
    class func saveLot(parkingIndex: String, frequencyString: String, begin: Date, end: Date) {
        Database.database().reference().child("parkings").child("ЖК Новый Мир").child(UserDefaults.standard.string(forKey: "token")!).child(parkingIndex).updateChildValues([
            "address" : "ЖК Новый Мир, ул.Д.Кунаева, 35",
            "latitude" : 51.130185,
            "longitude" : 71.436364,
            "number" : parkingIndex,
            "status" : "onVerification",
            "frequency" : frequencyString,
            "start" : begin.millisecondsSince1970,
            "end" : end.millisecondsSince1970
            ])
    }
    
    func getMyLotData(completion: @escaping (ParkingOld) -> Swift.Void) {
        Database.database().reference().child("parkings").child("ЖК Новый Мир").child(UserDefaults.standard.string(forKey: "token")!).child(self.index).observeSingleEvent(of: DataEventType.value) { (snapshot) in
            if let data = snapshot.value as? [String:Any] {
                let owner = UserOld(uid: UserDefaults.standard.string(forKey: "token")!, phone: "", name: "", carNumber: "", image: UIImage())
                let this_parking = ParkingOld(owner: owner, startTime: Date(timeIntervalSince1970: TimeInterval(exactly: data["start"] as! Int) ?? 0), endTime: Date(timeIntervalSince1970: TimeInterval(exactly: data["end"] as! Int) ?? 0), index: snapshot.key)
                completion(this_parking)
            }
        }
    }
    
    func deleteMe() {
        Database.database().reference().child("parkings").child("ЖК Новый Мир").child(UserDefaults.standard.string(forKey: "token")!).child(self.index).removeValue()
    }
    
}
