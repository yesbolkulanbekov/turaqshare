//
//  House.swift
//  turaQshare
//
//  Created by Galym Anuarbek on 10/16/18.
//  Copyright © 2018 Galym Anuarbek. All rights reserved.
//

import UIKit
import FirebaseDatabase

class House: NSObject {
    
    var lat: Double
    var lon: Double
    var name: String
    var photo: UIImage?
    
    init(lat: Double, lon: Double, name: String, photo: UIImage?) {
        self.lat = lat
        self.lon = lon
        self.name = name
        self.photo = photo
    }
    
    func getParkings(completion: @escaping ([ParkingOld]) -> Swift.Void) {
        var allParkings: [ParkingOld] = []
        Database.database().reference().child("parkings").child("ЖК Новый Мир").observeSingleEvent(of: DataEventType.value) { (snapshot) in
            guard let data = snapshot.value as? [String: Any] else { return }
            for owners in data.keys {
                Database.database().reference().child("users").child(owners).observeSingleEvent(of: DataEventType.value, with: { (userSnapshot) in
                    guard let userData = userSnapshot.value as? [String: Any] else { return }
                    let owner = UserOld(uid: owners, phone: userData["phone"] as? String ?? "", name: userData["name"] as? String ?? "", carNumber: userData["car_number"] as? String ?? "", image: #imageLiteral(resourceName: "tick"))
                    guard let parkings = data[owners] as? [String: Any] else { return }
                    for place in parkings.keys {
                        guard let parking = parkings[place] as? [String: Any] else { return }
                        let park = ParkingOld(owner: owner, startTime: Date(timeIntervalSinceNow: 0), endTime: Date(timeIntervalSinceNow: 0), index: place)
                        allParkings.append(park)
                    }
                    DispatchQueue.main.async {
                        completion(allParkings)
                    }
                })
            }
        }
    }
    
    class func getHouses() -> [House] {
//        let park = ParkingOld(owner: User(uid: "", phone: "", name: "Galym", carNumber: "", image: UIImage(named: "tick")!), startTime: Date.init(), endTime: Date.init(), index: 0)
        return [House(lat: 51.130280, lon: 71.436910, name: "ЖК Новый Мир", photo: nil)]
        
    }
    
    
    
}
