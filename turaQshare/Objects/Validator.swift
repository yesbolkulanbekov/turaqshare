//
//  Validator.swift
//  turaQshare
//
//  Created by Galym Anuarbek on 10/16/18.
//  Copyright © 2018 Galym Anuarbek. All rights reserved.
//

import UIKit

class Validator: NSObject {
    class func validatePhone(value: String?) -> (Bool, String) {
        if value != nil {
            let formatedString = value!.replacingOccurrences(of:"[^0-9]", with: "", options: .regularExpression)
            if formatedString.count == 11 {
                let areFirstTwo = formatedString.getSubstring(from: 0, to: 1) == "77" || formatedString.getSubstring(from: 0, to: 1) == "87"
                let formatedPhone = "+77" + formatedString.getSubstring(from: 2, to: 10)
                return (areFirstTwo, formatedPhone)
            } else {
                return (false, "")
            }
        } else {
            return (false, "")
        }
    }
    
    class func validateCar(value: String?) -> Bool {
        if value != nil {
            let formatedString = value!.replacingOccurrences(of:"|", with: "").replacingOccurrences(of:" ", with: "")
            if formatedString.count == 8 {
                let numb = Int(formatedString.getSubstring(from: 0, to: 2)) ?? 0
                let town = Int(formatedString.getSubstring(from: 6, to: 7)) ?? 0
                let series = formatedString.getSubstring(from: 3, to: 5)
                let isNumb = numb > 0 && numb < 1000
                let isTown = town > 0 && town < 18
                let isSeries = series.replacingOccurrences(of:"[^a-zA-Z]", with: "", options: .regularExpression) == series
                return isNumb && isTown && isSeries
            } else if formatedString.count == 7 {
                let numb = Int(formatedString.getSubstring(from: 0, to: 2)) ?? 0
                let town = Int(formatedString.getSubstring(from: 5, to: 6)) ?? 0
                let series = formatedString.getSubstring(from: 3, to: 4)
                let isNumb = numb > 0 && numb < 1000
                let isTown = town > 0 && town < 18
                let isSeries = series.replacingOccurrences(of:"[^a-zA-Z]", with: "", options: .regularExpression) == series
                return isNumb && isTown && isSeries
            } else {
                return false
            }
        } else {
            return false
        }
    }
}
