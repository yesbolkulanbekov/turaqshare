//
//  RegistrationVIew.swift
//  turaQshare
//
//  Created by Galym Anuarbek on 10/6/18.
//  Copyright © 2018 Galym Anuarbek. All rights reserved.
//

import UIKit
import EasyPeasy

class RegistrationView: UIView {
    
    let offerButton = RegistrationButtons()
    let findButton = RegistrationButtons()
    let representButton = RegistrationButtons()
    let logoBackground = UIImageView()
    let welcomeLabel = UILabel()
    let descriptionLabel = UILabel()
    let backgroundfig = UIImageView()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setup() {
        self.backgroundColor = UIColor.white
        
        self.addSubview(backgroundfig)
        backgroundfig.image = UIImage(named: "backgroundfig")
        backgroundfig.easy.layout([
            Top(43.2),
            Width(134.3),
            Height(361),
            Right(0)
            ])
        
        self.addSubview(logoBackground)
        logoBackground.image = UIImage(named: "logo")
        logoBackground.easy.layout([
            Height(43),
            Width(115),
            Top(50),
            Left(20)
            ])
        
        self.addSubview(welcomeLabel)
        var attributedString = NSMutableAttributedString(string: "Регистрация",
                                                         attributes: [NSAttributedString.Key.font : UIFont.LucidaGrandeBold(ofSize: 30),
                                                                      NSAttributedString.Key.foregroundColor : UIColor.pine])
        let attributedString2 = NSMutableAttributedString(string: "...",
                                                          attributes: [NSAttributedString.Key.font : UIFont.NunitoBold(ofSize: 30),
                                                                       NSAttributedString.Key.foregroundColor : UIColor.pine])
        attributedString.append(attributedString2)
        welcomeLabel.attributedText = attributedString
        welcomeLabel.numberOfLines = 0
        welcomeLabel.easy.layout([
            Top(76).to(logoBackground),
            Left(20),
            Right(20)
            ])
        
        self.addSubview(descriptionLabel)
        descriptionLabel.font = UIFont.LucidaGrandeRegular(ofSize: 14)
        descriptionLabel.textColor = UIColor.pine
        descriptionLabel.text = "Выберите опцию…"
        descriptionLabel.numberOfLines = 0
        descriptionLabel.easy.layout([
            Top(15).to(welcomeLabel),
            Left(20),
            Right(54)
            ])
        
        offerButton.setup(customTitle: "Предложить парковку", info: "Сейчас ищут парковку - 22")
        offerButton.clipsToBounds = false
        self.addSubview(offerButton)
        offerButton.easy.layout([
            Height(78),
            Width(335),
            CenterX(),
            Top(15).to(descriptionLabel)
            ])
        
        findButton.setup(customTitle: "Найти парковку", info: "Сейчас предлагают парковку - 13")
        self.addSubview(findButton)
        findButton.easy.layout([
            Height(78),
            Width(335),
            CenterX(),
            Top(27).to(offerButton)
            ])
        
        representButton.setup(customTitle: "Представитель ЖК", info: "")
        self.addSubview(representButton)
        representButton.easy.layout([
            Height(78),
            Width(335),
            CenterX(),
            Top(32).to(findButton)
            ])
    }
    
}
