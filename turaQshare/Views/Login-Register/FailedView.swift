//
//  FailedView.swift
//  turaQshare
//
//  Created by Galym Anuarbek on 10/10/18.
//  Copyright © 2018 Galym Anuarbek. All rights reserved.
//

import UIKit
import EasyPeasy

class FailedView: UIView {
    
    let backgroundfig = UIImageView()
    let failedView = UIImageView()
    let failedLabel = UILabel()
    let commetaries = UILabel()
    let tryAgain = UIButton()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init coder(decoder:) has not been implemented")
    }
    
    func setup() {
        self.backgroundColor = UIColor.white
        
        self.addSubview(backgroundfig)
        backgroundfig.image = UIImage(named: "backgroundfig")
        backgroundfig.easy.layout([
            Top(43.2),
            Width(134.3),
            Height(361),
            Right(0)
            ])
        
        self.addSubview(failedView)
        failedView.image = UIImage(named: "danger")
        failedView.easy.layout([
            Top(216),
            CenterX(),
            Width(41),
            Height(35)
            ])
        
        self.addSubview(failedLabel)
        failedLabel.text = "Что-то пошло не так"
        failedLabel.font = UIFont.LucidaGrandeRegular(ofSize: 16)
        failedLabel.textColor = UIColor.pine
        failedLabel.textAlignment = NSTextAlignment.center
        failedLabel.easy.layout([
            Top(30).to(failedView),
            CenterX()
            ])
        
        self.addSubview(commetaries)
        commetaries.text = "Nulla vitae elit libero, a pharetra augue. Donec id elit non mi porta gravida at eget metus. Donec ullamcorper nulla non metus auctor fringilla."
        commetaries.font = UIFont.NunitoRegular(ofSize: 14)
        commetaries.textAlignment = NSTextAlignment.center
        commetaries.numberOfLines = 0
        commetaries.easy.layout([
            Left(20),
            Right(20),
            Top(10).to(failedLabel)
            ])
        
        self.addSubview(tryAgain)
        tryAgain.setTitle("Повторить", for: UIControl.State.normal)
        tryAgain.setTitleColor(UIColor.red, for: UIControl.State.normal)
        tryAgain.easy.layout([
            CenterX(),
            Top(46).to(commetaries)
            ])
    }
    
}
