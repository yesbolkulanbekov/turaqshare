//
//  TermsNConditionsView.swift
//  turaQshare
//
//  Created by Galym Anuarbek on 10/10/18.
//  Copyright © 2018 Galym Anuarbek. All rights reserved.
//

import UIKit
import EasyPeasy

class TermsNConditionsView: UIView {
    
    let termsView = UITextView()
    let continueButton = UIButton()
    let welcomeLabel = UILabel()
    let backgroundfig = UIImageView()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init coder: failed")
    }
    
    func setup() {
        self.backgroundColor = UIColor.white
        
        self.addSubview(backgroundfig)
        backgroundfig.image = UIImage(named: "backgroundfig")
        backgroundfig.easy.layout([
            Top(43.2),
            Width(134.3),
            Height(361),
            Right(0)
            ])
        
        self.addSubview(welcomeLabel)
        var attr = NSMutableAttributedString(string: "Правила ", attributes: [NSAttributedString.Key.font : UIFont.LucidaGrandeBold(ofSize: 24), NSAttributedString.Key.foregroundColor : UIColor.pine])
        let attr2 = NSMutableAttributedString(string: "&", attributes: [NSAttributedString.Key.font : UIFont.NunitoBold(ofSize: 24), NSAttributedString.Key.foregroundColor : UIColor.pine])
        let attr3 = NSMutableAttributedString(string: " Условия", attributes: [NSAttributedString.Key.font : UIFont.LucidaGrandeBold(ofSize: 24), NSAttributedString.Key.foregroundColor : UIColor.pine])
        attr.append(attr2)
        attr.append(attr3)
        welcomeLabel.attributedText = attr
        welcomeLabel.easy.layout([
            Top(43),
            CenterX()
            ])
        
        self.addSubview(continueButton)
        continueButton.backgroundColor = UIColor.blueGreen
        continueButton.setTitle("Далее", for: UIControl.State.normal)
        continueButton.titleLabel?.font = UIFont.LucidaGrandeBold(ofSize: 18)
        continueButton.setTitleColor(UIColor.denim, for: UIControl.State.normal)
        continueButton.easy.layout([
            Bottom(71),
            CenterX(),
            Width(180),
            Height(44)
            ])
        
        self.addSubview(termsView)
        termsView.font = UIFont.NunitoRegular(ofSize: 16)
        termsView.backgroundColor = UIColor.whiteTwo
        termsView.contentInset = UIEdgeInsets(top: 39, left: 12, bottom: 12, right: 9)
        termsView.text = "Donec sed odio dui. Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum. Donec sed odio dui. Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum. Donec sed odio dui. Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum. Donec sed odio dui. Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum. Donec sed odio dui. Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum. Donec sed odio dui. Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum."
        termsView.textAlignment = NSTextAlignment.left
        termsView.isEditable = false
        termsView.easy.layout([
            Left(20),
            Right(20),
            Bottom(20).to(continueButton),
            Top(36).to(welcomeLabel)
            ])
    }
}
