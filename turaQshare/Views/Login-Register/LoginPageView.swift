//
//  LoginPageView.swift
//  turaQshare
//
//  Created by Galym Anuarbek on 10/2/18.
//  Copyright © 2018 Galym Anuarbek. All rights reserved.
//

import UIKit
import EasyPeasy
import FirebaseAuth


class LoginPageView: UIView {
    
    let welcomeLabel = UILabel()
    let donecLabel = UILabel()
    let registerLabel = UILabel()
    let phoneField = UITextField()
    let passwordField = UITextField()
    let forgotPassword = UIButton()
    let loginButton = UIButton()
    let registerButton = UIButton()
    let logoBackground = UIImageView()
    let backgroundfig = UIImageView()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setup() {
        self.backgroundColor = .white
        
        self.addSubview(backgroundfig)
        backgroundfig.image = UIImage(named: "backgroundfig")
        backgroundfig.easy.layout([
            Top(43.2),
            Width(134.3),
            Height(361),
            Right(0)
            ])
        
        self.addSubview(logoBackground)
        logoBackground.image = UIImage(named: "logo")
        logoBackground.easy.layout([
                Height(43),
                Width(115),
                Top(50),
                Left(20)
            ])
        

        self.addSubview(welcomeLabel)
        var attributedString = NSMutableAttributedString(string: "Добро пожаловать",
                                                         attributes: [NSAttributedString.Key.font : UIFont.LucidaGrandeBold(ofSize: 30),
                                                                      NSAttributedString.Key.foregroundColor : UIColor.pine])
        var attributedString2 = NSMutableAttributedString(string: "!",
                                                          attributes: [NSAttributedString.Key.font : UIFont.NunitoBold(ofSize: 30),
                                                                       NSAttributedString.Key.foregroundColor : UIColor.pine])
        attributedString.append(attributedString2)
        welcomeLabel.attributedText = attributedString
        welcomeLabel.numberOfLines = 0
        welcomeLabel.easy.layout([
                Top(70).to(logoBackground),
                Left(20),
                Right(20)
            ])
        
        
        self.addSubview(donecLabel)
        donecLabel.font = UIFont.NunitoLight(ofSize: 14)
        donecLabel.textColor = UIColor.pine
        donecLabel.text = "Donec sed odio dui. Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum"
        donecLabel.numberOfLines = 0
        donecLabel.easy.layout([
            Top(120).to(logoBackground),
            Left(20),
            Right(54)
            ])
        
        
        self.addSubview(phoneField)
        phoneField.easy.layout([
            Top(24).to(donecLabel),
            Left(20),
            Right(20),
            Height(46)
            ])
        phoneField.font = UIFont.NunitoRegular(ofSize: 16)
        phoneField.textColor = UIColor.pine
        phoneField.borderStyle = UITextField.BorderStyle.none
        phoneField.backgroundColor = UIColor.whiteTwo
        phoneField.placeholder = "Номер телефона"
        phoneField.setLeftPaddingPoints(15)
        phoneField.setRightPaddingPoints(15)
        
//        self.addSubview(passwordField)
//        passwordField.easy.layout([
//            Top(15).to(phoneField),
//            Left(20),
//            Right(20),
//            Height(46)
//            ])
//        passwordField.font = UIFont.NunitoRegular(ofSize: 16)
//        passwordField.textColor = UIColor.pine
//        passwordField.borderStyle = UITextField.BorderStyle.none
//        passwordField.backgroundColor = UIColor.whiteTwo
//        passwordField.placeholder = "Пароль"
//        passwordField.autocapitalizationType = UITextAutocapitalizationType.none
//        passwordField.autocorrectionType = UITextAutocorrectionType.no
//        passwordField.isSecureTextEntry = true
//        passwordField.setLeftPaddingPoints(15)
//        passwordField.setRightPaddingPoints(15)
        

//        self.addSubview(forgotPassword)
//        attributedString = NSMutableAttributedString(string: "Забыли пароль",
//                                                     attributes: [NSAttributedString.Key.font : UIFont.LucidaGrandeRegular(ofSize: 16),
//                                                                  NSAttributedString.Key.foregroundColor : UIColor.pine])
//        attributedString2 = NSMutableAttributedString(string: "?",
//                                                      attributes: [NSAttributedString.Key.font : UIFont.NunitoRegular(ofSize: 16),
//                                                                   NSAttributedString.Key.foregroundColor : UIColor.pine])
//        attributedString.append(attributedString2)
//        forgotPassword.setAttributedTitle(attributedString, for: UIControl.State.normal)
//        forgotPassword.setTitleColor(UIColor.pine, for: UIControl.State.normal)
//        forgotPassword.easy.layout([
//                Right(24),
//                Top(30).to(passwordField)
//            ])
        
        
        self.addSubview(loginButton)
        loginButton.backgroundColor = UIColor.blueGreen
        attributedString = NSMutableAttributedString(string: "Войти",
                                                     attributes: [NSAttributedString.Key.font : UIFont.LucidaGrandeBold(ofSize: 18),
                                                                  NSAttributedString.Key.foregroundColor : UIColor.denim])
        loginButton.setAttributedTitle(attributedString, for: UIControl.State.normal)
        loginButton.setTitleColor(UIColor.denim, for: UIControl.State.normal)
        loginButton.easy.layout([
            Width(180),
            CenterX(),
            Top(30).to(phoneField),
            Height(44)
            ])
        
        
        self.addSubview(registerButton)
        registerButton.backgroundColor = UIColor.denim
        attributedString = NSMutableAttributedString(string: "Зарегистрироваться",
                                                     attributes: [NSAttributedString.Key.font : UIFont.LucidaGrandeBold(ofSize: 18),
                                                                  NSAttributedString.Key.foregroundColor : UIColor.white])
        registerButton.setAttributedTitle(attributedString, for: UIControl.State.normal)
        registerButton.setTitleColor(UIColor.white, for: UIControl.State.normal)
        registerButton.easy.layout([
            Right(0),
            Left(0),
            Bottom(0),
            Height(50)
            ])
        
        
        self.addSubview(registerLabel)
        attributedString = NSMutableAttributedString(string: "Нужен аккаунт",
                                                     attributes: [NSAttributedString.Key.font : UIFont.LucidaGrandeRegular(ofSize: 16),
                                                                  NSAttributedString.Key.foregroundColor : UIColor.pine])
        attributedString.append(NSMutableAttributedString(string: "?",
                                                          attributes: [NSAttributedString.Key.font : UIFont.NunitoRegular(ofSize: 16),
                                                                       NSAttributedString.Key.foregroundColor : UIColor.pine]))
        registerLabel.attributedText = attributedString
        registerLabel.easy.layout([
            CenterX(),
            Bottom(23).to(registerButton)
            ])
    }
    
}
