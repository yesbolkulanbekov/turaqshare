//
//  PresentFirstView.swift
//  turaQshare
//
//  Created by Galym Anuarbek on 10/10/18.
//  Copyright © 2018 Galym Anuarbek. All rights reserved.
//

import UIKit
import EasyPeasy

class PresentFirstView: UIView {
    
    let topImage = UIImageView()
    let logo = UIImageView()
    let mainLabel = UILabel()
    let secondLabel = UILabel()
    let details = UILabel()
    let continueButton = UIButton()
    let ovals = Ovals()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init coder(decoder:) has not been implemented")
    }
    
    func setup() {
        self.backgroundColor = UIColor.white
        
        self.addSubview(topImage)
        topImage.contentMode = UIView.ContentMode.scaleAspectFit
        topImage.image = UIImage(named: "presentFirst")
        topImage.easy.layout([
            Top(0),
            Left(0),
            Right(0),
            Height(377)
            ])
        
        self.addSubview(mainLabel)
        mainLabel.text = "Мы найдем вам парковку"
        mainLabel.font = UIFont.LucidaGrandeBold(ofSize: 30)
        mainLabel.numberOfLines = 0
        mainLabel.textColor = UIColor.white
        mainLabel.easy.layout([
            Top(111),
            Left(20),
            Right(20)
            ])
        
        self.addSubview(secondLabel)
        secondLabel.text = "AirBNB для парковок"
        secondLabel.textColor = UIColor.white
        secondLabel.numberOfLines = 0
        secondLabel.font = UIFont.NunitoRegular(ofSize: 16)
        secondLabel.easy.layout([
            Top(9).to(mainLabel),
            Left(20),
            Right(20)
            ])
        
        self.addSubview(logo)
        logo.image = UIImage(named: "logo")
        logo.easy.layout([
            Left(14),
            Height(49),
            Width(154),
            Top(-24.5).to(topImage)
            ])
        
        self.addSubview(details)
        let attr = NSMutableAttributedString(string: "Вам нужно найти парковку возле своей работы", attributes: [NSAttributedString.Key.font : UIFont.LucidaGrandeRegular(ofSize: 18), NSAttributedString.Key.foregroundColor : UIColor.pine])
        attr.append(NSMutableAttributedString(string: "?", attributes: [NSAttributedString.Key.font : UIFont.NunitoRegular(ofSize: 18), NSAttributedString.Key.foregroundColor : UIColor.pine]))
        attr.append(NSMutableAttributedString(string: " Или вы хотите сдать свою парковку, пока она пустует", attributes: [NSAttributedString.Key.font : UIFont.LucidaGrandeRegular(ofSize: 18), NSAttributedString.Key.foregroundColor : UIColor.pine]))
        attr.append(NSMutableAttributedString(string: "?", attributes: [NSAttributedString.Key.font : UIFont.NunitoRegular(ofSize: 18), NSAttributedString.Key.foregroundColor : UIColor.pine]))
        details.attributedText = attr
        details.numberOfLines = 0
        details.easy.layout([
            Top(44).to(topImage),
            Left(20),
            Right(20)
            ])
        
        self.addSubview(ovals)
        ovals.easy.layout([
            CenterX(-CGFloat(Double(ovals.ovals.count) * 5.5)),
            Bottom(29)
            ])
        ovals.ovals[0].backgroundColor = UIColor.denim
        
        self.addSubview(continueButton)
        continueButton.setTitle("Далее", for: UIControl.State.normal)
        continueButton.setTitleColor(UIColor.denim, for: UIControl.State.normal)
        continueButton.easy.layout([
            Bottom(11),
            Right(20)
            ])
    }
}
