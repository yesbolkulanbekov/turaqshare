//
//  SignUpView.swift
//  turaQshare
//
//  Created by Galym Anuarbek on 10/3/18.
//  Copyright © 2018 Galym Anuarbek. All rights reserved.
//

import UIKit
import EasyPeasy

class SignUpView: UIView {
    
    let logoBackground = UIImageView()
    let nameField = UITextField()
    let phoneField = UITextField()
    let carNumbField = UITextField()
    let registerButton = UIButton()
    let loginButton = UIButton()
    let loginLabel = UILabel()
    let welcomeLabel = UILabel()
    let backgroundfig = UIImageView()
    let bgView = UIView()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setup() {
        self.backgroundColor = .white
        
        self.addSubview(backgroundfig)
        backgroundfig.image = UIImage(named: "backgroundfig")
        backgroundfig.easy.layout([
            Top(43.2),
            Width(134.3),
            Height(361),
            Right(0)
            ])
        
        self.addSubview(logoBackground)
        logoBackground.image = UIImage(named: "logo")
        logoBackground.easy.layout([
            Height(43),
            Width(115),
            Top(50),
            Left(20)
            ])
        
        
        self.addSubview(welcomeLabel)
        var attributedString = NSMutableAttributedString(string: "Регистрация",
                                                         attributes: [NSAttributedString.Key.font : UIFont.LucidaGrandeBold(ofSize: 30),
                                                                      NSAttributedString.Key.foregroundColor : UIColor.pine])
        let attributedString2 = NSMutableAttributedString(string: "...",
                                                          attributes: [NSAttributedString.Key.font : UIFont.NunitoBold(ofSize: 30),
                                                                       NSAttributedString.Key.foregroundColor : UIColor.pine])
        attributedString.append(attributedString2)
        welcomeLabel.attributedText = attributedString
        welcomeLabel.numberOfLines = 0
        welcomeLabel.easy.layout([
            Top(90).to(logoBackground),
            Left(20),
            Right(20)
            ])
        
        
        self.addSubview(nameField)
        nameField.easy.layout([
            Top(32).to(welcomeLabel),
            Left(20),
            Right(20),
            Height(46)
            ])
        nameField.font = UIFont.NunitoRegular(ofSize: 16)
        nameField.textColor = UIColor.pine
        nameField.borderStyle = UITextField.BorderStyle.none
        nameField.backgroundColor = UIColor.whiteTwo
        nameField.placeholder = "Имя"
        nameField.setLeftPaddingPoints(15)
        nameField.setRightPaddingPoints(15)
        
        
        self.addSubview(phoneField)
        phoneField.easy.layout([
            Top(15).to(nameField),
            Left(20),
            Right(20),
            Height(46)
            ])
        phoneField.font = UIFont.NunitoRegular(ofSize: 16)
        phoneField.textColor = UIColor.pine
        phoneField.borderStyle = UITextField.BorderStyle.none
        phoneField.backgroundColor = UIColor.whiteTwo
        phoneField.placeholder = "Номер телефона"
        phoneField.autocapitalizationType = UITextAutocapitalizationType.none
        phoneField.autocorrectionType = UITextAutocorrectionType.no
        phoneField.setLeftPaddingPoints(15)
        phoneField.setRightPaddingPoints(15)
        phoneField.keyboardType = UIKeyboardType.phonePad
        
        
        self.addSubview(carNumbField)
        carNumbField.easy.layout([
            Top(15).to(phoneField),
            Left(20),
            Right(20),
            Height(46)
            ])
        carNumbField.font = UIFont.NunitoRegular(ofSize: 16)
        carNumbField.textColor = UIColor.pine
        carNumbField.borderStyle = UITextField.BorderStyle.none
        carNumbField.backgroundColor = UIColor.whiteTwo
        carNumbField.placeholder = "Номер машины"
        carNumbField.autocapitalizationType = UITextAutocapitalizationType.none
        carNumbField.autocorrectionType = UITextAutocorrectionType.no
        carNumbField.setLeftPaddingPoints(15)
        carNumbField.setRightPaddingPoints(15)
        
        self.addSubview(registerButton)
        registerButton.backgroundColor = UIColor.blueGreen
        attributedString = NSMutableAttributedString(string: "Регистрация",
                                                     attributes: [NSAttributedString.Key.font : UIFont.LucidaGrandeBold(ofSize: 18),
                                                                  NSAttributedString.Key.foregroundColor : UIColor.denim])
        registerButton.setAttributedTitle(attributedString, for: UIControl.State.normal)
        registerButton.setTitleColor(UIColor.denim, for: UIControl.State.normal)
        registerButton.easy.layout([
            Width(180),
            CenterX(),
            Top(40).to(carNumbField),
            Height(44)
            ])
        
        
        self.addSubview(loginButton)
        loginButton.backgroundColor = UIColor.denim
        attributedString = NSMutableAttributedString(string: "Войти",
                                                     attributes: [NSAttributedString.Key.font : UIFont.LucidaGrandeBold(ofSize: 18),
                                                                  NSAttributedString.Key.foregroundColor : UIColor.white])
        loginButton.setAttributedTitle(attributedString, for: UIControl.State.normal)
        loginButton.setTitleColor(UIColor.white, for: UIControl.State.normal)
        loginButton.easy.layout([
            Right(0),
            Left(0),
            Bottom(0),
            Height(50)
            ])
        
        
        self.addSubview(loginLabel)
        attributedString = NSMutableAttributedString(string: "Уже имеется аккаунт",
                                                     attributes: [NSAttributedString.Key.font : UIFont.LucidaGrandeRegular(ofSize: 16),
                                                                  NSAttributedString.Key.foregroundColor : UIColor.pine])
        attributedString.append(NSMutableAttributedString(string: "?",
                                                          attributes: [NSAttributedString.Key.font : UIFont.NunitoRegular(ofSize: 16),
                                                                       NSAttributedString.Key.foregroundColor : UIColor.pine]))
        loginLabel.font = UIFont.LucidaGrandeRegular(ofSize: 16)
        loginLabel.attributedText = attributedString
        loginLabel.easy.layout([
            CenterX(),
            Bottom(23).to(loginButton)
            ])
        
        bgView.backgroundColor = UIColor.pine.withAlphaComponent(0.5)
        self.addSubview(bgView)
        bgView.easy.layout([
            Top(UIScreen.main.bounds.height),
            CenterX(),
            Width(UIScreen.main.bounds.width),
            Height(UIScreen.main.bounds.height)
            ])
    }
    
}
