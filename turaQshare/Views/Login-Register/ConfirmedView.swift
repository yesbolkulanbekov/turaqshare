//
//  ConfirmedView.swift
//  turaQshare
//
//  Created by Galym Anuarbek on 10/10/18.
//  Copyright © 2018 Galym Anuarbek. All rights reserved.
//

import UIKit
import EasyPeasy

class ConfirmedView: UIView {
    
    let backgroundfig = UIImageView()
    let tickView = UIImageView()
    let parkingConfirmed = UILabel()
    let commetaries = UILabel()
    let closeButton = UIButton()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init coder: failed")
    }
    
    func setup() {
        self.backgroundColor = UIColor.white
        
        self.addSubview(backgroundfig)
        backgroundfig.image = UIImage(named: "backgroundfig")
        backgroundfig.easy.layout([
            Top(43.2),
            Width(134.3),
            Height(361),
            Right(0)
            ])
        
        self.addSubview(tickView)
        tickView.image = UIImage(named: "tick")
        tickView.easy.layout([
            Top(216),
            CenterX(),
            Width(45),
            Height(45)
            ])
        
        self.addSubview(parkingConfirmed)
        parkingConfirmed.text = "Номер успешно подтвержден"
        parkingConfirmed.font = UIFont.LucidaGrandeRegular(ofSize: 16)
        parkingConfirmed.textColor = UIColor.pine
        parkingConfirmed.textAlignment = NSTextAlignment.center
        parkingConfirmed.easy.layout([
            Top(30).to(tickView),
            CenterX()
            ])
        
        self.addSubview(commetaries)
        commetaries.text = "Nulla vitae elit libero, a pharetra augue. Donec id elit non mi porta gravida at eget metus. Donec ullamcorper nulla non metus auctor fringilla."
        commetaries.font = UIFont.NunitoRegular(ofSize: 14)
        commetaries.textAlignment = NSTextAlignment.center
        commetaries.numberOfLines = 0
        commetaries.easy.layout([
            Left(20),
            Right(20),
            Top(10).to(parkingConfirmed)
            ])
        
        self.addSubview(closeButton)
        closeButton.setTitle("Close", for: UIControl.State.normal)
        closeButton.setTitleColor(UIColor.red, for: UIControl.State.normal)
        closeButton.easy.layout([
            CenterX(),
            Top(46).to(commetaries)
            ])
    }
}
