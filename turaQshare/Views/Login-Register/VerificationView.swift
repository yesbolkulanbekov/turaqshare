//
//  VerificationView.swift
//  turaQshare
//
//  Created by Galym Anuarbek on 10/3/18.
//  Copyright © 2018 Galym Anuarbek. All rights reserved.
//

import UIKit
import EasyPeasy

class VerificationViewOld: UIView {
    
    let logoBackground = UIImageView()
    let welcomeLabel = UILabel()
    let descriptionLabel = UILabel()
    let codeLabel = UILabel()
    let phoneImageView = UIImageView()
    let backgroundfig = UIImageView()
    let codeField = VerificationField()
    let resendButton = UIButton()
    let confirmButton = UIButton()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setup() {
        self.backgroundColor = .white
        
        self.addSubview(logoBackground)
        logoBackground.image = UIImage(named: "logo")
        logoBackground.easy.layout([
            Height(43),
            Width(115),
            Top(50),
            Left(20)
            ])
        
        self.addSubview(backgroundfig)
        backgroundfig.image = UIImage(named: "backgroundfig")
        backgroundfig.easy.layout([
            Top(43.2),
            Width(134.3),
            Height(361),
            Right(0)
            ])
        
        self.addSubview(welcomeLabel)
        var attributedString = NSMutableAttributedString(string: "Верификация",
                                                         attributes: [NSAttributedString.Key.font : UIFont.LucidaGrandeBold(ofSize: 30),
                                                                      NSAttributedString.Key.foregroundColor : UIColor.pine])
        let attributedString2 = NSMutableAttributedString(string: "...",
                                                          attributes: [NSAttributedString.Key.font : UIFont.NunitoBold(ofSize: 30),
                                                                       NSAttributedString.Key.foregroundColor : UIColor.pine])
        attributedString.append(attributedString2)
        welcomeLabel.attributedText = attributedString
        welcomeLabel.numberOfLines = 0
        welcomeLabel.easy.layout([
            Top(35).to(logoBackground),
            Left(20),
            Right(20)
            ])
        
        
        self.addSubview(descriptionLabel)
        attributedString = NSMutableAttributedString(string: "Введите ",
                                                     attributes: [NSAttributedString.Key.font : UIFont.LucidaGrandeRegular(ofSize: 14),
                                                                  NSAttributedString.Key.foregroundColor : UIColor.pine])
        attributedString.append(NSMutableAttributedString(string: "4-",
                                                          attributes: [NSAttributedString.Key.font : UIFont.NunitoLight(ofSize: 14),
                                                                       NSAttributedString.Key.foregroundColor : UIColor.pine]))
        attributedString.append(NSMutableAttributedString(string: "х значный код, высланный вам по СМС.",
                                                          attributes: [NSAttributedString.Key.font : UIFont.LucidaGrandeRegular(ofSize: 14),
                                                                       NSAttributedString.Key.foregroundColor : UIColor.pine]))
        descriptionLabel.attributedText = attributedString
        descriptionLabel.textColor = UIColor.pine
        descriptionLabel.numberOfLines = 0
        descriptionLabel.easy.layout([
            Top(5).to(welcomeLabel),
            Left(20),
            Right(54)
            ])
        
        
        self.addSubview(phoneImageView)
        phoneImageView.contentMode = UIView.ContentMode.scaleAspectFit
        phoneImageView.image = UIImage(named: "verification phone")
        phoneImageView.easy.layout([
            Top(20).to(descriptionLabel),
            Right(88),
            Height(157),
            Width(118)
            ])
        
        
        self.addSubview(codeLabel)
        codeLabel.text = "Код подтверждения"
        codeLabel.font = UIFont.LucidaGrandeBold(ofSize: 22)
        codeLabel.addCharacterSpacing(kernValue: 4)
        codeLabel.textAlignment = NSTextAlignment.center
        codeLabel.easy.layout([
            Top(35).to(phoneImageView),
            CenterX()
            ])
        
        
        codeField.setup(backgroundColor: UIColor.white, textHolderColor: UIColor.whiteTwo, fieldWidth: 46.4, fieldHeight: 50, spacing: 10)
        self.addSubview(codeField)
        codeField.easy.layout([
            Top(10).to(codeLabel),
            Width(329),
            Height(50),
            CenterX()
            ])
        
        self.addSubview(resendButton)
        attributedString = NSMutableAttributedString(string: "Переотправить\n60 секунд",
                                                     attributes: [NSAttributedString.Key.font : UIFont.LucidaGrandeRegular(ofSize: 16),
                                                                  NSAttributedString.Key.foregroundColor : UIColor.denim])
        resendButton.setAttributedTitle(attributedString, for: UIControl.State.normal)
        resendButton.titleLabel?.textAlignment = NSTextAlignment.center
        resendButton.titleLabel?.numberOfLines = 0
        resendButton.easy.layout([
            Top(10).to(codeField),
            Right(14),
            Width(148)
            ])
        
        self.addSubview(confirmButton)
        confirmButton.backgroundColor = UIColor.blueGreen
        attributedString = NSMutableAttributedString(string: "Подтведить",
                                                     attributes: [NSAttributedString.Key.font : UIFont.LucidaGrandeBold(ofSize: 18),
                                                                  NSAttributedString.Key.foregroundColor : UIColor.denim])
        confirmButton.setAttributedTitle(attributedString, for: UIControl.State.normal)
        
        confirmButton.setTitleColor(UIColor.denim, for: UIControl.State.normal)
        confirmButton.easy.layout([
            Top(72).to(codeField),
            Height(44),
            Width(180),
            CenterX()
            ])
    }
    
}
