//
//  PresentSecondView.swift
//  turaQshare
//
//  Created by Galym Anuarbek on 10/10/18.
//  Copyright © 2018 Galym Anuarbek. All rights reserved.
//

import UIKit
import EasyPeasy

class PresentSecondView: UIView {
    
    let topImage = UIImageView()
    let logo = UIImageView()
    let mainLabel = UILabel()
    let secondLabel = UILabel()
    let details = UILabel()
    let continueButton = UIButton()
    let ovals = Ovals()
    let triangle = UIImageView()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init coder(decoder:) has not been implemented")
    }
    
    func setup() {
        self.backgroundColor = UIColor.denim
        
        self.addSubview(topImage)
        topImage.contentMode = UIView.ContentMode.scaleAspectFit
        topImage.image = UIImage(named: "presentFirst")
        topImage.easy.layout([
            Top(0),
            Left(0),
            Right(0),
            Height(377)
            ])
        
        topImage.addSubview(triangle)
        triangle.image = UIImage(named: "triangle")
        triangle.contentMode = UIView.ContentMode.scaleToFill
        triangle.easy.layout([
            Height(130),
            Left(0),
            Right(0),
            Bottom(0)
            ])
        
        self.addSubview(mainLabel)
        mainLabel.text = "Мы найдем вам парковку"
        mainLabel.numberOfLines = 0
        mainLabel.font = UIFont.LucidaGrandeBold(ofSize: 30)
        mainLabel.textColor = UIColor.white
        mainLabel.easy.layout([
            Top(53),
            Left(20),
            Right(20)
            ])
        
        self.addSubview(secondLabel)
        secondLabel.text = "AirBNB для парковок"
        secondLabel.numberOfLines = 0
        secondLabel.textColor = UIColor.white
        secondLabel.font = UIFont.NunitoRegular(ofSize: 16)
        secondLabel.easy.layout([
            Top(7).to(mainLabel),
            Left(20),
            Right(20)
            ])
        
        self.addSubview(logo)
        logo.image = UIImage(named: "logow")
        logo.easy.layout([
            Left(14),
            Height(49),
            Width(154),
            Top(-24.5).to(topImage)
            ])
        
        self.addSubview(details)
        let attr = NSMutableAttributedString(string: "Используйте ", attributes: [NSAttributedString.Key.font : UIFont.LucidaGrandeRegular(ofSize: 16), NSAttributedString.Key.foregroundColor : UIColor.white])
        attr.append(NSMutableAttributedString(string: "turaQshare", attributes: [NSAttributedString.Key.font : UIFont.NunitoLight(ofSize: 16), NSAttributedString.Key.foregroundColor : UIColor.white]))
        attr.append(NSMutableAttributedString(string: " и Вы найдете лучшее решение своей проблемы!", attributes: [NSAttributedString.Key.font : UIFont.LucidaGrandeRegular(ofSize: 16), NSAttributedString.Key.foregroundColor : UIColor.white]))
        details.attributedText = attr
        details.numberOfLines = 0
        details.easy.layout([
            Top(50).to(logo),
            Left(20),
            Right(20)
            ])
        
        self.addSubview(ovals)
        ovals.easy.layout([
            CenterX(-CGFloat(Double(ovals.ovals.count) * 5.5)),
            Bottom(29)
            ])
        ovals.ovals[0].backgroundColor = UIColor.pine
        ovals.ovals[1].backgroundColor = UIColor.white
        
        self.addSubview(continueButton)
        continueButton.setTitle("Далее", for: UIControl.State.normal)
        continueButton.setTitleColor(UIColor.white, for: UIControl.State.normal)
        continueButton.easy.layout([
            Bottom(11),
            Right(20)
            ])
    }
}
