//
//  ShareParkingView.swift
//  turaQshare
//
//  Created by Galym Anuarbek on 10/7/18.
//  Copyright © 2018 Galym Anuarbek. All rights reserved.
//

import UIKit
import EasyPeasy

class ShareParkingView: UIView {
    
    let welcomeLabel = UILabel()
    let backgroundfig = UIImageView()
    let parkingIndexField = UITextField()
    let addressField = UITextView()
    let addDocumentLabel = UILabel()
    let addDocumentButton = UIButton()
    let helpButton = UIButton()
    let checkmark = Checkmark()
    let pinButton = UIButton()
    let showOnMap = UIButton()
    let acceptRules = UIButton()
    let continueButton = UIButton()
    let termsAndConditions = UIButton()
    let backButton = UIButton()
    
    var documentImage: UIImage? = nil
    
    let popup = PopUpHelp(frame: CGRect(origin: CGPoint(), size: CGSize(width: 235, height: 161.3)))
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setup() {
        self.backgroundColor = .white
        
        self.addSubview(backgroundfig)
        backgroundfig.image = UIImage(named: "backgroundfig")
        backgroundfig.easy.layout([
            Top(43.2),
            Width(134.3),
            Height(361),
            Right(0)
            ])
        
        self.addSubview(backButton)
        backButton.setImage(UIImage(named: "left-arrow"), for: UIControl.State.normal)
        backButton.easy.layout([
                Top(43),
                Left(20),
                Width(20.9),
                Height(13.9)
            ])
        
        self.addSubview(welcomeLabel)
        welcomeLabel.font = UIFont.HelveticaRegular(ofSize: 30)
        welcomeLabel.numberOfLines = 0
        welcomeLabel.text = "Укажите ваше парковочное место"
        welcomeLabel.easy.layout([
                Left(20),
                Right(20),
                Top(8).to(backButton)
            ])
        
        self.addSubview(parkingIndexField)
        parkingIndexField.font = UIFont.LucidaGrandeRegular(ofSize: 16)
        parkingIndexField.textColor = UIColor.pine
        parkingIndexField.borderStyle = UITextField.BorderStyle.none
        parkingIndexField.backgroundColor = UIColor.whiteTwo
        parkingIndexField.attributedPlaceholder = NSAttributedString(string: "Введите номер парковочного места", attributes: [NSAttributedString.Key.font : UIFont.LucidaGrandeRegular(ofSize: 16), NSAttributedString.Key.foregroundColor : UIColor.lightGray])
        parkingIndexField.setLeftPaddingPoints(15)
        parkingIndexField.setRightPaddingPoints(15)
        parkingIndexField.easy.layout([
            Top(20).to(welcomeLabel),
            Left(20),
            Right(20),
            Height(46)
            ])
        
        self.addSubview(addressField)
        addressField.delegate = self
        addressField.font = UIFont.LucidaGrandeRegular(ofSize: 16)
        addressField.textColor = UIColor.lightGray
        addressField.text = "Адрес (Бета тест: ЖК Новый Мир)"
        addressField.isEditable = false
        addressField.layer.borderWidth = 0
        addressField.layer.borderColor = UIColor(white: 1, alpha: 0).cgColor
        addressField.backgroundColor = UIColor.whiteTwo
        addressField.contentInset = UIEdgeInsets(top: 9, left: 9, bottom: 9, right: 9)
        addressField.easy.layout([
            Top(17).to(parkingIndexField),
            Left(20),
            Right(20),
            Height(98)
            ])
        
        self.addSubview(showOnMap)
        self.addSubview(pinButton)
        showOnMap.titleLabel?.font = UIFont.LucidaGrandeRegular(ofSize: 16)
        showOnMap.setTitleColor(UIColor.denim, for: UIControl.State.normal)
        showOnMap.setTitle("указать на карте", for: UIControl.State.normal)
        pinButton.setImage(UIImage(named: "locationGrid"), for: UIControl.State.normal)
        showOnMap.easy.layout([
            Right(30),
            Top(12).to(addressField)
            ])
        pinButton.easy.layout([
            Top(0).to(addressField),
            Right(0).to(showOnMap),
            Width(44),
            Height(44)
            ])
        
        self.addSubview(addDocumentLabel)
        addDocumentLabel.text = "Прикрепите офицальный документ…"
        addDocumentLabel.font = UIFont.LucidaGrandeRegular(ofSize: 14)
        addDocumentLabel.textColor = UIColor.pine
        addDocumentLabel.textAlignment = NSTextAlignment.left
        addDocumentLabel.easy.layout([
            Top(11).to(pinButton),
            Left(23),
            Right(23)
            ])
        
        self.addSubview(addDocumentButton)
        addDocumentButton.backgroundColor = UIColor.blueGreen
        addDocumentButton.setImage(UIImage(named: "file"), for: UIControl.State.normal)
        addDocumentButton.easy.layout([
            Top(12).to(addDocumentLabel),
            Left(27),
            Height(44),
            Width(180)
            ])
        
        self.addSubview(helpButton)
        helpButton.setImage(UIImage(named: "information"), for: UIControl.State.normal)
        helpButton.easy.layout([
            Top(17).to(addDocumentLabel),
            Left(10).to(addDocumentButton),
            Width(32),
            Height(32)
            ])
        
        self.addSubview(checkmark)
        checkmark.easy.layout([
            Top(26).to(helpButton),
            Left(20),
            Height(25),
            Width(25)
            ])
        
        self.addSubview(acceptRules)
        var attrib = NSMutableAttributedString(string: "Принять Правила ", attributes: [NSAttributedString.Key.font : UIFont.LucidaGrandeRegular(ofSize: 16), NSAttributedString.Key.foregroundColor : UIColor.pine])
        attrib.append(NSMutableAttributedString(string: "&", attributes: [NSAttributedString.Key.font : UIFont.NunitoRegular(ofSize: 16), NSAttributedString.Key.foregroundColor : UIColor.pine]))
        attrib.append(NSMutableAttributedString(string: " Условия", attributes: [NSAttributedString.Key.font : UIFont.LucidaGrandeRegular(ofSize: 16), NSAttributedString.Key.foregroundColor : UIColor.pine]))
        acceptRules.setAttributedTitle(attrib, for: UIControl.State.normal)
        acceptRules.setTitleColor(UIColor.pine, for: UIControl.State.normal)
        acceptRules.easy.layout([
            Left(15).to(checkmark),
            CenterY().to(checkmark)
            ])
        
        self.addSubview(continueButton)
        continueButton.backgroundColor = UIColor.blueGreen
        continueButton.setTitle("Далее", for: UIControl.State.normal)
        continueButton.titleLabel?.font = UIFont.LucidaGrandeBold(ofSize: 18)
        continueButton.setTitleColor(UIColor.denim, for: UIControl.State.normal)
        continueButton.easy.layout([
            Top(39).to(acceptRules),
            CenterX(),
            Width(180),
            Height(44)
            ])
        
        self.addSubview(termsAndConditions)
        attrib = NSMutableAttributedString(string: "Правила ", attributes: [NSAttributedString.Key.font : UIFont.LucidaGrandeRegular(ofSize: 16), NSAttributedString.Key.foregroundColor : UIColor.denim])
        attrib.append(NSMutableAttributedString(string: "&", attributes: [NSAttributedString.Key.font : UIFont.NunitoRegular(ofSize: 16), NSAttributedString.Key.foregroundColor : UIColor.denim]))
        attrib.append(NSMutableAttributedString(string: " Условия", attributes: [NSAttributedString.Key.font : UIFont.LucidaGrandeRegular(ofSize: 16), NSAttributedString.Key.foregroundColor : UIColor.denim]))
        termsAndConditions.setAttributedTitle(attrib, for: UIControl.State.normal)
        termsAndConditions.easy.layout([
            CenterX(),
            Top(50).to(continueButton)
            ])
        
        self.addSubview(popup)
        popup.isHidden = true
        popup.easy.layout([
            CenterX().to(helpButton),
            Bottom(15).to(helpButton),
            Width(235),
            Height(161.3)
            ])
    }
}

extension ShareParkingView: UITextViewDelegate {
    
    func textViewShouldBeginEditing(_ textView: UITextView) -> Bool {
        if textView.textColor == UIColor.lightGray {
            textView.text = nil
            textView.textColor = UIColor.pine
        }
        return true
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text.isEmpty {
            textView.text = "Адрес"
            textView.textColor = UIColor.lightGray
        }
    }
}
