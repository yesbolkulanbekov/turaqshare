//
//  MyLotsListView.swift
//  turaQshare
//
//  Created by Galym Anuarbek on 10/22/18.
//  Copyright © 2018 Galym Anuarbek. All rights reserved.
//

import UIKit
import EasyPeasy

class MyLotsListView: UIView {
    
    lazy var listTableView: UITableView = {
        let tableView = UITableView()
        tableView.separatorInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        tableView.rowHeight = 60
        tableView.allowsSelection = false
        tableView.backgroundColor = UIColor.white
        tableView.register(LotsListTableViewCell.self, forCellReuseIdentifier: "cell")
        tableView.register(LotsTableHeader.self, forCellReuseIdentifier: "header")
        tableView.register(LotsTableFooter.self, forCellReuseIdentifier: "footer")
        return tableView
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setup() {
        self.backgroundColor = UIColor.white
        
        self.addSubview(listTableView)
        listTableView.backgroundColor = UIColor.white
        listTableView.easy.layout(Edges(0))
    }
}
