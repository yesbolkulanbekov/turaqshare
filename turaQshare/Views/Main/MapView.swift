//
//  MapView.swift
//  turaQshare
//
//  Created by Galym Anuarbek on 10/10/18.
//  Copyright © 2018 Galym Anuarbek. All rights reserved.
//

import UIKit
import EasyPeasy
import MapKit
import CoreLocation

class MapView: UIView, UITextFieldDelegate, CLLocationManagerDelegate {
    
    let mapView = MKMapView()
    let searchBar = UITextField()
    let barBackground = UIView()
    let barLabel = UILabel()
//    let doneButton = UIButton()
    let pinView = UIImageView()
    var locationManager: CLLocationManager = CLLocationManager()
    lazy var geocoder = CLGeocoder()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setup() {
        self.backgroundColor = UIColor.white
        
        self.addSubview(barBackground)
        barBackground.backgroundColor = UIColor.white
        barBackground.easy.layout([
            Top(0),
            Right(0),
            Left(0),
            Height(137)
            ])
        
        self.addSubview(barLabel)
        barLabel.font = UIFont.LucidaGrandeBold(ofSize: 18)
        barLabel.textColor = UIColor.pine
        barLabel.text = "Выберите локацию"
        barLabel.easy.layout([
            Top(38),
            Left(57)
            ])
        barLabel.textAlignment = NSTextAlignment.left
        
        barBackground.addSubview(searchBar)
        searchBar.backgroundColor = UIColor.whiteTwo
        searchBar.tintColor = UIColor.pine
        searchBar.layer.borderWidth = 0
        searchBar.layer.borderColor = UIColor(white: 1, alpha: 0).cgColor
        searchBar.setLeftPaddingPoints(44)
        searchBar.setRightPaddingPoints(15)
        searchBar.easy.layout([
            Top(17).to(barLabel),
            Left(20),
            Right(20),
            Height(46)
            ])
        searchBar.delegate = self
        
        searchBar.addSubview(pinView)
        pinView.image = UIImage(named: "searchPin")
        pinView.easy.layout([
            Top(11),
            Left(11),
            Bottom(11),
            Width(18)
            ])
        
        self.addSubview(mapView)
        locationManager.delegate = self
        locationManager.requestWhenInUseAuthorization()
        mapView.showsUserLocation = true
        mapView.userTrackingMode = .none
        mapView.showsPointsOfInterest = true
        
        
        mapView.easy.layout([
            Top(0).to(barBackground),
            Bottom(43),
            Right(0),
            Left(0)
            ])
        
        
        locationManager.requestWhenInUseAuthorization()
        locationManager.delegate = self
        let userLat = locationManager.location?.coordinate.latitude
        let userLon = locationManager.location?.coordinate.longitude
        print(userLat, userLon)
        if userLat != nil && userLon != nil {
            let center = CLLocationCoordinate2D(latitude: userLat!, longitude: userLon!)
            var region = MKCoordinateRegion(center: center, span: MKCoordinateSpan(latitudeDelta: 0.3, longitudeDelta: 0.3))
            region.center = center
            mapView.setRegion(region, animated: true)
        }
        
        
        
//        self.addSubview(doneButton)
//        doneButton.setTitle("Готово", for: UIControl.State.normal)
//        doneButton.setTitleColor(UIColor.white, for: UIControl.State.normal)
//        doneButton.backgroundColor = UIColor.denim
//        doneButton.titleLabel?.font = UIFont.LucidaGrandeBold(ofSize: 18)
//        doneButton.easy.layout([
//            Bottom(0),
//            Left(0),
//            Right(0),
//            Height(43)
//            ])
    }
    
    @objc func btnAction() {
        print("lol")
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.endEditing(true)
        return false
    }
    
}
