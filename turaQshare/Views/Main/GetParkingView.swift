//
//  GetParkingView.swift
//  turaQshare
//
//  Created by Galym Anuarbek on 11/2/18.
//  Copyright © 2018 Galym Anuarbek. All rights reserved.
//

import UIKit
import EasyPeasy

class GetParkingView: UIView {
    
    let backButton = UIButton()
    let titleLabel = UILabel()
    let chooseLabel = UILabel()
    let beginButton = CalendarButtons()
    let endButton = CalendarButtons()
    let sumLabel = UILabel()
    let sumInfoLabel = UILabel()
    let tarifView = BillInfoView()
    let addressLabel = UILabel()
    let addressInfoLabel = UILabel()
    let buttonHolders = UIView()
    let emailBillButton = UIButton()
    let payButton = UIButton()
    
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setup() {
        self.backgroundColor = UIColor.white
        
        self.addSubview(backButton)
        backButton.setImage(UIImage(named: "left-arrow"), for: UIControl.State.normal)
        backButton.contentEdgeInsets = UIEdgeInsets(top: 8.3, left: 5, bottom: 8.3, right: 5)
        backButton.easy.layout([
            Width(30),
            Height(30),
            Top(30),
            Left(15)
            ])
        
        self.addSubview(titleLabel)
        titleLabel.font = UIFont.LucidaGrandeBold(ofSize: 14)
        titleLabel.textColor = UIColor.pine
        titleLabel.easy.layout([
            CenterX(),
            Top(36)
            ])
        
        self.addSubview(chooseLabel)
        chooseLabel.font = UIFont.LucidaGrandeBold(ofSize: 18)
        chooseLabel.textColor = UIColor.pine
        chooseLabel.easy.layout([
            Top(28).to(titleLabel),
            Left(20)
            ])
        
        self.addSubview(beginButton)
        beginButton.title.text = "Начало"
        beginButton.easy.layout([
            Top(11).to(chooseLabel),
            Left(20),
            Right(20),
            Height(70)
            ])
        
        self.addSubview(endButton)
        endButton.title.text = "Конец"
        endButton.easy.layout([
            Top(20).to(beginButton),
            Left(20),
            Right(20),
            Height(70)
            ])
        
        self.addSubview(sumLabel)
        sumLabel.font = UIFont.LucidaGrandeBold(ofSize: 18)
        sumLabel.textColor = UIColor.pine
        sumLabel.text = "Общий счет"
        sumLabel.easy.layout([
            Left(20),
            Top(34).to(endButton)
            ])
        
        self.addSubview(sumInfoLabel)
        sumInfoLabel.font = UIFont.NunitoSemiBold(ofSize: 18)
        sumInfoLabel.textColor = UIColor.denim
        sumInfoLabel.textAlignment = NSTextAlignment.right
        sumInfoLabel.text = "T1337"
        sumInfoLabel.easy.layout([
            Right(20),
            Top(34).to(endButton)
            ])
        
        self.addSubview(tarifView)
        tarifView.titleLabel.text = "Тариф"
        tarifView.priceLabel.text = "Т3920"
        tarifView.timeLabel.text = "56 часов"
        tarifView.easy.layout([
            Top(17).to(sumLabel),
            Height(50),
            Left(20),
            Right(20)
            ])
        
        self.addSubview(buttonHolders)
        buttonHolders.backgroundColor = UIColor.clear
        buttonHolders.easy.layout([
            CenterX(),
            Width(1),
            Height(44)
            ])
        
        self.addSubview(emailBillButton)
        emailBillButton.backgroundColor = UIColor.pine.withAlphaComponent(0.08)
        emailBillButton.setTitle("Счет на почту", for: UIControl.State.normal)
        emailBillButton.setTitleColor(UIColor.pine, for: UIControl.State.normal)
        emailBillButton.easy.layout([
            Bottom(25.5),
            Right(4.75).to(buttonHolders),
            Left(20),
            Width(162.5),
            Height(44)
            ])
        
        self.addSubview(payButton)
        payButton.backgroundColor = UIColor.blueGreen
        payButton.setTitle("Оплатить", for: UIControl.State.normal)
        payButton.setTitleColor(UIColor.denim, for: UIControl.State.normal)
        payButton.easy.layout([
            Bottom(25.5),
            Left(4.75).to(buttonHolders),
            Right(20),
            Width(162.5),
            Height(44)
            ])
        
        self.addSubview(addressInfoLabel)
        addressInfoLabel.font = UIFont.LucidaGrandeRegular(ofSize: 14)
        addressInfoLabel.numberOfLines = 0
        addressInfoLabel.textColor = UIColor.pine
        addressInfoLabel.text = "Достык, 13\nЕсиль район, Астана, Z05H9H8"
        addressInfoLabel.easy.layout([
            Left(20),
            Right(20),
            Bottom(11).to(emailBillButton)
            ])
        
        self.addSubview(addressLabel)
        addressLabel.font = UIFont.LucidaGrandeBold(ofSize: 18)
        addressLabel.numberOfLines = 0
        addressLabel.textColor = UIColor.pine
        addressLabel.text = "Адрес"
    }
}
