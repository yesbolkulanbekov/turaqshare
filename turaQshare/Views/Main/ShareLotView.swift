//
//  ShareLotView.swift
//  turaQshare
//
//  Created by Galym Anuarbek on 10/10/18.
//  Copyright © 2018 Galym Anuarbek. All rights reserved.
//

import UIKit
import EasyPeasy

class ShareLotView: UIView {
    
    let titleLabel = UILabel()
    let hexLabel = UILabel()
    let houseLabel = UILabel()
    let bellButton = UIButton()
    let detailLabel = UILabel()
    let beginButton = CalendarButtons()
    let endButton = CalendarButtons()
    let frequencyButton = FrequencyButtons()
    let doneButton = UIButton()
    let note = UILabel()
    let noteDetails = UILabel()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setup() {
        self.backgroundColor = UIColor.white
        
        self.addSubview(titleLabel)
        titleLabel.text = "Сдать место"
        titleLabel.textColor = UIColor.pine
        titleLabel.font = UIFont.HelveticaRegular(ofSize: 30)
        titleLabel.easy.layout([
            Top(50),
            Left(20)
            ])
        
        self.addSubview(bellButton)
        bellButton.setImage(UIImage(named: "tick"), for: UIControl.State.normal)
        bellButton.easy.layout([
            Right(20.2),
            Top(59),
            Height(23.9),
            Width(21.7)
            ])
        
        self.addSubview(hexLabel)
        hexLabel.font = UIFont.LucidaGrandeRegular(ofSize: 14)
        hexLabel.textColor = UIColor.denim
        hexLabel.textAlignment = NSTextAlignment.center
        hexLabel.easy.layout([
            Top(0).to(titleLabel),
            CenterX()
            ])
        
        self.addSubview(houseLabel)
        houseLabel.font = UIFont.LucidaGrandeRegular(ofSize: 14)
        houseLabel.textColor = UIColor.pine
        houseLabel.textAlignment = NSTextAlignment.center
        houseLabel.easy.layout([
            Top(0).to(hexLabel),
            CenterX()
            ])
        
        self.addSubview(detailLabel)
        detailLabel.font = UIFont.HelveticaRegular(ofSize: 14)
        detailLabel.textColor = UIColor.pine
        detailLabel.easy.layout([
            Top(8).to(houseLabel),
            Left(20)
            ])
        
        self.addSubview(beginButton)
        beginButton.title.text = "Начало"
        beginButton.easy.layout([
            Top(11).to(detailLabel),
            Left(20),
            Right(20),
            Height(70)
            ])
        
        self.addSubview(endButton)
        endButton.title.text = "Конец"
        endButton.easy.layout([
            Top(20).to(beginButton),
            Left(20),
            Right(20),
            Height(70)
            ])
        
        self.addSubview(frequencyButton)
        frequencyButton.title.text = "Частота"
        frequencyButton.easy.layout([
            Top(20).to(endButton),
            Left(20),
            Right(20),
            Height(70)
            ])
        
        
        self.addSubview(doneButton)
        doneButton.backgroundColor = UIColor.blueGreen
        doneButton.titleLabel?.font = UIFont.LucidaGrandeBold(ofSize: 18)
        doneButton.setTitle("Подтвердить", for: UIControl.State.normal)
        doneButton.setTitleColor(UIColor.denim, for: UIControl.State.normal)
        doneButton.easy.layout([
            Width(180),
            CenterX(),
            Height(44),
            Top(35).to(frequencyButton)
            ])
        
        self.addSubview(note)
        note.font = UIFont.NunitoSemiBold(ofSize: 14)
        note.textColor = UIColor.pine
        note.text = "Note:"
        note.easy.layout([
            Top(39).to(doneButton),
            Left(20)
            ])
        
        self.addSubview(noteDetails)
        noteDetails.font = UIFont.NunitoRegular(ofSize: 14)
        noteDetails.textColor = UIColor.pine
        noteDetails.numberOfLines = 0
        noteDetails.text = "Price is decided by the pickup man at the time of collecting your clothes"
        noteDetails.easy.layout([
            Top(0).to(note),
            Right(20),
            Left(20)
            ])
    }
}
