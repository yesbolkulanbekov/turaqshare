//
//  ParkingsView.swift
//  turaQshare
//
//  Created by Galym Anuarbek on 10/17/18.
//  Copyright © 2018 Galym Anuarbek. All rights reserved.
//

import UIKit
import EasyPeasy

class ParkingView: UIView {
    
    lazy var layout: UICollectionViewFlowLayout = {
        let layout = UICollectionViewFlowLayout()
        layout.minimumLineSpacing = 16
        layout.sectionInset = UIEdgeInsets(top: 0.01, left: 0.01, bottom: 15, right: 0.01)
        layout.minimumInteritemSpacing = 16
        let size: CGFloat = (UIScreen.main.bounds.width)
        layout.itemSize = CGSize(width: size - 40, height: 78)
        layout.headerReferenceSize = CGSize(width: size, height: 266)
        return layout
    }()
    
    lazy var historyCollectionView: UICollectionView = {
        let collectionView = UICollectionView(frame: .zero, collectionViewLayout: self.layout)
        collectionView.register(ParkingCollectionCell.self, forCellWithReuseIdentifier: "cell")
        collectionView.register(ParkingCollectionHeader.self, forSupplementaryViewOfKind: UICollectionView.elementKindSectionHeader, withReuseIdentifier: "header")
        collectionView.backgroundColor = UIColor.white
        return collectionView
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setup() {
        self.addSubview(historyCollectionView)
        historyCollectionView.easy.layout(Edges(0))
//        historyCollectionView.backgroundColor = UIColor.white
        self.backgroundColor = UIColor.white
        
//        backButton.setImage(UIImage(named: "left-arrow"), for: UIControl.State.normal)
//        backButton.contentEdgeInsets = UIEdgeInsets(top: 8.3, left: 5, bottom: 8.3, right: 5)
//        backButton.contentMode = UIView.ContentMode.scaleAspectFit
//        self.addSubview(backButton)
//        backButton.easy.layout([
//            Top(30),
//            Left(15),
//            Height(30),
//            Width(30)
//            ])
    }
}
