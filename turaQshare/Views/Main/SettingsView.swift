//
//  SettingsView.swift
//  turaQshare
//
//  Created by Galym Anuarbek on 10/10/18.
//  Copyright © 2018 Galym Anuarbek. All rights reserved.
//

import UIKit
import EasyPeasy
import FirebaseAuth

class SettingsView: UIView {
    
    lazy var layout: UICollectionViewFlowLayout = {
        let layout = UICollectionViewFlowLayout()
        layout.minimumLineSpacing = 1
        layout.sectionInset = UIEdgeInsets(top: 1, left: 1, bottom: 10, right: 1)
        layout.minimumInteritemSpacing = 1
        let size: CGFloat = (UIScreen.main.bounds.width)
        layout.itemSize = CGSize(width: size, height: 50)
        layout.headerReferenceSize = CGSize(width: size, height: 158)
        return layout
    }()
    
    lazy var settingsCollectionView: UICollectionView = {
        let collectionView = UICollectionView(frame: .zero, collectionViewLayout: self.layout)
        collectionView.register(SettingsCollectionViewCell.self, forCellWithReuseIdentifier: "cell")
        collectionView.register(EmptyCell.self, forCellWithReuseIdentifier: "ecell")
        collectionView.register(SettingsCollectionViewHeader.self, forSupplementaryViewOfKind: UICollectionView.elementKindSectionHeader, withReuseIdentifier: "header")
        collectionView.backgroundColor = UIColor.white
        return collectionView
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setup() {
        settingsCollectionView.alwaysBounceVertical = true
        self.backgroundColor = UIColor.white
        
        self.addSubview(settingsCollectionView)
        settingsCollectionView.easy.layout([
            Edges(0)
            ])
    }
}


