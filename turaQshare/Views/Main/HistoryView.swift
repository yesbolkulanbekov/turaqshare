//
//  HistoryView.swift
//  turaQshare
//
//  Created by Galym Anuarbek on 10/10/18.
//  Copyright © 2018 Galym Anuarbek. All rights reserved.
//

import UIKit
import EasyPeasy

class HistoryView: UIView {
    
    var chosenIndex = 0
    
    var history: [Payments] = [Payments(status: .rejected, date: Date(timeIntervalSinceNow: 0), quantity: 8, price: 0, detail: ""),
                               Payments(status: .paid, date: Date(timeIntervalSinceNow: 0), quantity: 8, price: 1000, detail: ""),
                               Payments(status: .reserved, date: Date(timeIntervalSinceNow: 0), quantity: 8, price: 0, detail: ""),
                               Payments(status: .rejected, date: Date(timeIntervalSinceNow: 0), quantity: 8, price: 0, detail: ""),
                               Payments(status: .rejected, date: Date(timeIntervalSinceNow: 0), quantity: 8, price: 0, detail: ""),
                               Payments(status: .rejected, date: Date(timeIntervalSinceNow: 0), quantity: 8, price: 0, detail: "")]
    
    lazy var layout: UICollectionViewFlowLayout = {
        let layout = UICollectionViewFlowLayout()
        layout.minimumLineSpacing = 20
        layout.sectionInset = UIEdgeInsets(top: 0.01, left: 0.01, bottom: 15, right: 0.01)
        layout.minimumInteritemSpacing = 20
        let size: CGFloat = (UIScreen.main.bounds.width)
        layout.itemSize = CGSize(width: size - 40, height: 78)
        layout.headerReferenceSize = CGSize(width: size, height: 70)
        return layout
    }()
    
    lazy var historyCollectionView: UICollectionView = {
        let collectionView = UICollectionView(frame: .zero, collectionViewLayout: self.layout)
        collectionView.register(HistoryAllCell.self, forCellWithReuseIdentifier: "vsecell")
        collectionView.register(HistoryObrCell.self, forCellWithReuseIdentifier: "obrabcell")
        collectionView.register(HistoryArchCell.self, forCellWithReuseIdentifier: "archcell")
        collectionView.register(HistoryCollectionHeader.self, forSupplementaryViewOfKind: UICollectionView.elementKindSectionHeader, withReuseIdentifier: "header")
        collectionView.dataSource = self
        collectionView.delegate = self
        collectionView.backgroundColor = UIColor.white
        return collectionView
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setup() {
        self.backgroundColor = UIColor.white
        
        self.addSubview(historyCollectionView)
        historyCollectionView.easy.layout(Edges(0))
    }
    
    @objc func indexChanged(_ sender: UISegmentedControl) {
        chosenIndex = sender.selectedSegmentIndex
        historyCollectionView.reloadData()
    }
}

extension HistoryView: UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if chosenIndex == 0 {
            return history.count
        } else if chosenIndex == 1 {
            return 0
        } else {
            return 0
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if chosenIndex == 0 {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "vsecell", for: indexPath) as! HistoryAllCell
            cell.layer.shadowColor = UIColor.pine.cgColor
            cell.layer.shadowOpacity = 0.08
            cell.layer.shadowOffset = CGSize(width: 0, height: 2)
            cell.dateLabel.text = history[indexPath.row].date.toString()
            cell.quantityLabel.text = String(history[indexPath.row].quantity) + " Quantity"
            cell.statusView.image = history[indexPath.row].getStatusImage()
            
            if history[indexPath.row].status == .paid {
                cell.totalLabel.text = "Total"
                cell.priceLabel.text = "₸" + String(history[indexPath.row].price)
            } else {
                cell.totalLabel.text = ""
                cell.priceLabel.text = ""
            }
            
            return cell
        } else if chosenIndex == 1 {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "obrabcell", for: indexPath) as! HistoryObrCell
            return cell
        } else {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "archcell", for: indexPath) as! HistoryArchCell
            return cell
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        if kind == UICollectionView.elementKindSectionHeader {
            let view = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: "header", for: indexPath) as! HistoryCollectionHeader
            view.segmentControl.addTarget(self, action: #selector(indexChanged(_:)), for: UIControl.Event.valueChanged)
            return view
        }
        return UICollectionReusableView()
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if chosenIndex == 0 {
            if history[indexPath.row].status == .paid {
                return CGSize(width: 335, height: 120)
            } else {
                return CGSize(width: 335, height: 78)
            }
        } else if chosenIndex == 1 {
            return CGSize(width: 335, height: 120)
        } else {
            return CGSize(width: 335, height: 120)
        }
    }
}
