//
//  DatePickerView.swift
//  turaQshare
//
//  Created by Galym Anuarbek on 10/13/18.
//  Copyright © 2018 Galym Anuarbek. All rights reserved.
//

import UIKit
import EasyPeasy

class DatePickerView: UIView {
    
    let datePicker = UIDatePicker()
    
    override init(frame: CGRect) {
        super.init(frame: CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: 216))
        setup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setup() {
        self.addSubview(datePicker)
        datePicker.easy.layout([
            CenterX(),
            CenterY()
            ])
    }
}
