//
//  CalendarButtons.swift
//  turaQshare
//
//  Created by Galym Anuarbek on 10/13/18.
//  Copyright © 2018 Galym Anuarbek. All rights reserved.
//

import UIKit
import EasyPeasy

class CalendarButtons: UIView {
    
    let button = UIButton()
    let dateField = UITextField()
    var date = Date(timeIntervalSince1970: 0)
    let title = UILabel()
    let titleBackground = UIView()
    let dateBackground = UIView()
    let timepicker = UIDatePicker()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setup() {
        self.backgroundColor = UIColor.clear
        
        self.addSubview(dateBackground)
        dateBackground.backgroundColor = UIColor.whiteTwo
        dateBackground.easy.layout([
            Left(0),
            Right(0),
            Bottom(0),
            Height(60)
            ])
        
        self.addSubview(titleBackground)
        titleBackground.backgroundColor = UIColor.denim
        titleBackground.easy.layout([
            Top(0),
            Left(10),
            Width(65),
            Height(20)
            ])
        
        
        timepicker.datePickerMode = UIDatePicker.Mode.dateAndTime
        timepicker.minimumDate = Date(timeIntervalSinceNow: TimeInterval(exactly: 0)!)
        
        dateBackground.addSubview(dateField)
        dateField.font = UIFont.NunitoLight(ofSize: 14)
        dateField.textColor = UIColor.pine
        dateField.setLeftPaddingPoints(15)
        dateField.setRightPaddingPoints(15)
        dateField.inputView = timepicker
        dateField.contentVerticalAlignment = UIControl.ContentVerticalAlignment.center
        dateField.textAlignment = NSTextAlignment.left
        dateField.easy.layout([
            Edges(0)
            ])
        
        
        
        titleBackground.addSubview(title)
        title.font = UIFont.LucidaGrandeRegular(ofSize: 14)
        title.textColor = UIColor.white
        title.easy.layout([
            CenterX(),
            CenterY()
            ])
        
//        self.addSubview(button)
//        button.backgroundColor = UIColor.clear
//        button.easy.layout(Edges(0))
    }
    
}
