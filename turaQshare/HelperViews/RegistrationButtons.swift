//
//  RegistrationButtonsView.swift
//  turaQshare
//
//  Created by Galym Anuarbek on 10/6/18.
//  Copyright © 2018 Galym Anuarbek. All rights reserved.
//

import UIKit
import EasyPeasy

class RegistrationButtons: UIView {
    
    let customTitleLabel = UILabel()
    let infoLabel = UILabel()
    let arrow = UIImageView()
    let button = UIButton()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setup(customTitle: String, info: String) {
        self.layer.cornerRadius = 4
        self.backgroundColor = UIColor.white
        self.layer.shadowOffset = CGSize(width: 1, height: 2)
        self.layer.shadowOpacity = 0.5
        self.layer.shadowColor = UIColor.black.cgColor
        self.layer.cornerRadius = 4
        self.layer.shadowRadius = 4
        
        
        self.addSubview(customTitleLabel)
        var attributedText = NSAttributedString(string: customTitle,
                                                attributes: [NSAttributedString.Key.font : UIFont.LucidaGrandeRegular(ofSize: 22),
                                                             NSAttributedString.Key.foregroundColor : UIColor.pine])
        customTitleLabel.attributedText = attributedText
        customTitleLabel.easy.layout([
            Top(14),
            Left(10)
            ])
        
        self.addSubview(infoLabel)
        attributedText = NSAttributedString(string: info,
                                            attributes: [NSAttributedString.Key.font : UIFont.LucidaGrandeRegular(ofSize: 14),
                                                         NSAttributedString.Key.foregroundColor : UIColor.pine])
        infoLabel.attributedText = attributedText
        infoLabel.easy.layout([
            Bottom(5),
            Right(9)
            ])
        infoLabel.textAlignment = NSTextAlignment.right
        
        self.addSubview(arrow)
        arrow.image = UIImage(named: "register arrow")
        arrow.easy.layout([
            Top(17),
            Right(17),
            Width(20.9),
            Height(33.5)
            ])
        
        self.addSubview(button)
        button.easy.layout([
            Edges(0)
            ])
        button.backgroundColor = UIColor(white: 1, alpha: 0)
    }
}
