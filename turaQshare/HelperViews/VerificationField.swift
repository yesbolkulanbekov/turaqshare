//
//  VerificationField.swift
//  turaQshare
//
//  Created by Galym Anuarbek on 10/5/18.
//  Copyright © 2018 Galym Anuarbek. All rights reserved.
//

import UIKit
import EasyPeasy

class VerificationField: UIView {
    
    let firstText = CustomUITextField()
    let secondText = CustomUITextField()
    let thirdText = CustomUITextField()
    let fourthText = CustomUITextField()
    let fifthText = CustomUITextField()
    let sixthText = CustomUITextField()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        firstText.customDelegate = self
        secondText.customDelegate = self
        thirdText.customDelegate = self
        fourthText.customDelegate = self
        fifthText.customDelegate = self
        sixthText.customDelegate = self
        
        firstText.delegate = self
        secondText.delegate = self
        thirdText.delegate = self
        fourthText.delegate = self
        fifthText.delegate = self
        sixthText.delegate = self
        
        firstText.addTarget(self, action: #selector(textFieldChange(textField: )), for: UIControl.Event.editingChanged)
        secondText.addTarget(self, action: #selector(textFieldChange(textField: )), for: UIControl.Event.editingChanged)
        thirdText.addTarget(self, action: #selector(textFieldChange(textField: )), for: UIControl.Event.editingChanged)
        fourthText.addTarget(self, action: #selector(textFieldChange(textField: )), for: UIControl.Event.editingChanged)
        fifthText.addTarget(self, action: #selector(textFieldChange(textField: )), for: UIControl.Event.editingChanged)
        sixthText.addTarget(self, action: #selector(textFieldChange(textField: )), for: UIControl.Event.editingChanged)
        
        firstText.addTarget(self, action: #selector(startTyping), for: UIControl.Event.allTouchEvents)
        secondText.addTarget(self, action: #selector(startTyping), for: UIControl.Event.allTouchEvents)
        thirdText.addTarget(self, action: #selector(startTyping), for: UIControl.Event.allTouchEvents)
        fourthText.addTarget(self, action: #selector(startTyping), for: UIControl.Event.allTouchEvents)
        fifthText.addTarget(self, action: #selector(startTyping), for: UIControl.Event.allTouchEvents)
        sixthText.addTarget(self, action: #selector(startTyping), for: UIControl.Event.allTouchEvents)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setup(backgroundColor: UIColor, textHolderColor: UIColor, fieldWidth: CGFloat, fieldHeight: CGFloat, spacing: CGFloat) {
        self.backgroundColor = backgroundColor
        
        self.addSubview(firstText)
        firstText.borderStyle = UITextField.BorderStyle.none
        firstText.backgroundColor = textHolderColor
        firstText.easy.layout([
            Width(fieldWidth),
            Height(fieldHeight),
            Top(0),
            Left(0)
            ])
        
        self.addSubview(secondText)
        secondText.borderStyle = UITextField.BorderStyle.none
        secondText.backgroundColor = textHolderColor
        secondText.easy.layout([
            Width(fieldWidth),
            Height(fieldHeight),
            Top(0),
            Left(spacing).to(firstText)
            ])
        
        self.addSubview(thirdText)
        thirdText.borderStyle = UITextField.BorderStyle.none
        thirdText.backgroundColor = textHolderColor
        thirdText.easy.layout([
            Width(fieldWidth),
            Height(fieldHeight),
            Top(0),
            Left(spacing).to(secondText)
            ])
        
        self.addSubview(fourthText)
        fourthText.borderStyle = UITextField.BorderStyle.none
        fourthText.backgroundColor = textHolderColor
        fourthText.easy.layout([
            Width(fieldWidth),
            Height(fieldHeight),
            Top(0),
            Left(spacing).to(thirdText)
            ])
        
        self.addSubview(fifthText)
        fifthText.borderStyle = UITextField.BorderStyle.none
        fifthText.backgroundColor = textHolderColor
        fifthText.easy.layout([
            Width(fieldWidth),
            Height(fieldHeight),
            Top(0),
            Left(spacing).to(fourthText)
            ])
        
        self.addSubview(sixthText)
        sixthText.borderStyle = UITextField.BorderStyle.none
        sixthText.backgroundColor = textHolderColor
        sixthText.easy.layout([
            Width(fieldWidth),
            Height(fieldHeight),
            Top(0),
            Left(spacing).to(fifthText)
            ])
        
        firstText.textAlignment = NSTextAlignment.center
        secondText.textAlignment = NSTextAlignment.center
        thirdText.textAlignment = NSTextAlignment.center
        fourthText.textAlignment = NSTextAlignment.center
        fifthText.textAlignment = NSTextAlignment.center
        sixthText.textAlignment = NSTextAlignment.center
        
        firstText.keyboardType = UIKeyboardType.numberPad
        secondText.keyboardType = UIKeyboardType.numberPad
        thirdText.keyboardType = UIKeyboardType.numberPad
        fourthText.keyboardType = UIKeyboardType.numberPad
        fifthText.keyboardType = UIKeyboardType.numberPad
        sixthText.keyboardType = UIKeyboardType.numberPad
    }
    
    @objc func textFieldChange(textField: UITextField) {
        if textField.text?.count ?? 0 > 0 {
            switch textField {
            case firstText:
                secondText.becomeFirstResponder()
            case secondText:
                thirdText.becomeFirstResponder()
            case thirdText:
                fourthText.becomeFirstResponder()
            case fourthText:
                fifthText.becomeFirstResponder()
            case fifthText:
                sixthText.becomeFirstResponder()
            case sixthText:
                sixthText.resignFirstResponder()
            default:
                break
            }
        }
    }
    
    @objc func startTyping() {
        if firstText.text?.count == 0 {
            firstText.becomeFirstResponder()
        } else if secondText.text?.count == 0 {
            secondText.becomeFirstResponder()
        } else if thirdText.text?.count == 0 {
            thirdText.becomeFirstResponder()
        } else if fourthText.text?.count == 0 {
            fourthText.becomeFirstResponder()
        } else if fifthText.text?.count == 0 {
            fifthText.becomeFirstResponder()
        } else if sixthText.text?.count == 0 {
            sixthText.becomeFirstResponder()
        }
    }
    
    func getText() -> String {
        if firstText.text != nil && secondText.text != nil && thirdText.text != nil && fourthText.text != nil && fifthText.text != nil && sixthText.text != nil {
            let text = (firstText.text!) + (secondText.text!) + (thirdText.text!) + (fourthText.text!) + (fifthText.text!) + (sixthText.text!)
            return text
        } else {
            return ""
        }
    }
    
}

extension VerificationField: CustomUITextFieldDelegate, UITextFieldDelegate {
    
    func textFieldDidDelete() {
        if sixthText.text?.count ?? 0 > 0 {
            sixthText.becomeFirstResponder()
        } else if fifthText.text?.count ?? 0 > 0 {
            fifthText.becomeFirstResponder()
        } else if fourthText.text?.count ?? 0 > 0 {
            fourthText.becomeFirstResponder()
        } else if thirdText.text?.count ?? 0 > 0 {
            thirdText.becomeFirstResponder()
        } else if secondText.text?.count ?? 0 > 0 {
            secondText.becomeFirstResponder()
        } else if firstText.text?.count ?? 0 > 0 {
            firstText.becomeFirstResponder()
        }
    }
    
//    func textFieldWillDelete() {
//        if sixthText.text?.count ?? 0 > 0 {
//            fifthText.becomeFirstResponder()
//        } else if fifthText.text?.count ?? 0 > 0 {
//            fourthText.becomeFirstResponder()
//        } else if fourthText.text?.count ?? 0 > 0 {
//            thirdText.becomeFirstResponder()
//        } else if thirdText.text?.count ?? 0 > 0 {
//            secondText.becomeFirstResponder()
//        } else if secondText.text?.count ?? 0 > 0 {
//            firstText.becomeFirstResponder()
//        } else if firstText.text?.count ?? 0 > 0 {
//        }
//    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        guard let text = textField.text else { return true }
        let newLength = text.count + string.count - range.length
        return newLength <= 1 // Bool
    }
}

