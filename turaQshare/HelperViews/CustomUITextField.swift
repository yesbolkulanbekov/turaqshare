//
//  CustomUITextField.swift
//  turaQshare
//
//  Created by Galym Anuarbek on 10/6/18.
//  Copyright © 2018 Galym Anuarbek. All rights reserved.
//

import UIKit

protocol CustomUITextFieldDelegate {
    func textFieldDidDelete()
//    func textFieldWillDelete()
}

class CustomUITextField: UITextField {
    
    var customDelegate: CustomUITextFieldDelegate?
    
    override func deleteBackward() {
//        customDelegate?.textFieldWillDelete()
        super.deleteBackward()
        customDelegate?.textFieldDidDelete()
    }
    
}
