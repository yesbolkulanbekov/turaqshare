//
//  BillInfoView.swift
//  turaQshare
//
//  Created by Galym Anuarbek on 11/2/18.
//  Copyright © 2018 Galym Anuarbek. All rights reserved.
//

import UIKit
import EasyPeasy

class BillInfoView: UIView {
    
    let titleLabel = UILabel()
    let timeLabel = UILabel()
    let priceLabel = UILabel()
    let separator = UIView()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setup() {
        self.addSubview(titleLabel)
        titleLabel.font = UIFont.LucidaGrandeRegular(ofSize: 14)
        titleLabel.easy.layout([
            Left(0),
            CenterY()
            ])
        
        self.addSubview(timeLabel)
        timeLabel.font = UIFont.NunitoLight(ofSize: 14)
        timeLabel.easy.layout([
            CenterX(),
            CenterY()
            ])
        
        self.addSubview(priceLabel)
        priceLabel.font = UIFont.NunitoLight(ofSize: 14)
        priceLabel.easy.layout([
            Right(0),
            CenterY()
            ])
        
        self.addSubview(separator)
        separator.backgroundColor = UIColor.pine.withAlphaComponent(0.08)
        separator.easy.layout([
            Height(1),
            Right(0),
            Left(0)
            ])
    }
}
