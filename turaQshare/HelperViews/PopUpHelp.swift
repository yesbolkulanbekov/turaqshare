//
//  PopUpHelp.swift
//  turaQshare
//
//  Created by Galym Anuarbek on 10/9/18.
//  Copyright © 2018 Galym Anuarbek. All rights reserved.
//

import UIKit
import EasyPeasy

class PopUpHelp: UIImageView {
    
    let helpLabel = UILabel()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init coder: failed")
    }
    
    func setup() {
        self.addSubview(helpLabel)
        helpLabel.text = "Загрузите\nфото документа на\nпарковку"
        helpLabel.numberOfLines = 0
        helpLabel.font = UIFont.LucidaGrandeRegular(ofSize: 16)
        helpLabel.textColor = UIColor.pine
        helpLabel.easy.layout([
            CenterX(),
            Top(24)
            ])
        self.image = UIImage(named: "shape")
    }
    
    
    
}
