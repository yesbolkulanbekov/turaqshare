//
//  Сheckmark.swift
//  turaQshare
//
//  Created by Galym Anuarbek on 10/9/18.
//  Copyright © 2018 Galym Anuarbek. All rights reserved.
//

import UIKit

class Checkmark: UIButton {
    
    let empty = UIImage(named: "empty")
    let check = UIImage(named: "check")
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setup() {
        self.setImage(empty, for: UIControl.State.normal)
        self.addTarget(self, action: #selector(press), for: UIControl.Event.touchUpInside)
    }
    
    @objc func press() {
        if self.image(for: UIControl.State.normal) == empty {
            self.setImage(check, for: UIControl.State.normal)
        } else if self.image(for: UIControl.State.normal) == check {
            self.setImage(empty, for: UIControl.State.normal)
        }
        action(self)
    }
    
    func isMark() -> Bool {
        if self.image(for: UIControl.State.normal) == empty {
            return false
        } else if self.image(for: UIControl.State.normal) == check {
            return true
        } else {
            fatalError("Checkmark image is nil")
        }
    }
    
    var action: (Checkmark) -> Void = {_ in}
    
}
