//
//  Ovals.swift
//  turaQshare
//
//  Created by Galym Anuarbek on 10/10/18.
//  Copyright © 2018 Galym Anuarbek. All rights reserved.
//

import UIKit
import EasyPeasy

class Ovals: UIView {
    
    let ovals = [UIView(), UIView()]
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setup() {
        var x: CGFloat = 0
        for oval in ovals {
            self.addSubview(oval)
            oval.backgroundColor = UIColor.lightGray
            oval.easy.layout([
                Height(8),
                Width(8),
                Left((8+6)*x)
                ])
            oval.layer.cornerRadius = 4
            x = x + 1
        }
    }
}
