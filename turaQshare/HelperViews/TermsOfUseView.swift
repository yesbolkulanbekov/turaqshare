//
//  TermsOfUseView.swift
//  turaQshare
//
//  Created by Galym Anuarbek on 11/7/18.
//  Copyright © 2018 Galym Anuarbek. All rights reserved.
//

import UIKit
import EasyPeasy

class TermsOfUseView: UIView {
    
    let scrollView = UIScrollView()
    let termsLabel = UILabel()
    let agreeLabel = UILabel()
    let checkmark = Checkmark()
    let agreeButton = UIButton()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setup() {
        self.backgroundColor = UIColor.white
        
//        scrollView.backgroundColor = UIColor.white
//        self.addSubview(scrollView)
//        scrollView.easy.layout([
//            CenterX(),
//            Top(0),
//            Width(self.frame.width),
//            Height(self.frame.height - 100)
//            ])
//
//        termsLabel.numberOfLines = 0
//        scrollView.addSubview(termsLabel)
//        termsLabel.easy.layout([
//            CenterX(),
//            Top(10),
//            Width(self.frame.width - 20)
//            ])
//        termsLabel.attributedText = NSMutableAttributedString(string: "").setupTerms()
//        termsLabel.sizeToFit()
//        scrollView.contentSize = CGSize(width: self.frame.width, height: 10000)
//        print(termsLabel.bounds.size.height)
        
        if let pdfURL = Bundle.main.url(forResource: "terms", withExtension: "pdf", subdirectory: nil, localization: nil)  {
            do {
                let data = try Data(contentsOf: pdfURL)
                let webView = UIWebView()
                webView.backgroundColor = UIColor.white
                webView.layer.shadowColor = UIColor.clear.cgColor
                webView.layer.shadowOpacity = 0
                webView.load(data, mimeType: "application/pdf", textEncodingName:"", baseURL: pdfURL.deletingLastPathComponent())
                self.addSubview(webView)
                webView.easy.layout([
                    CenterX(),
                    Top(0),
                    Height(self.frame.size.height - 115),
                    Width(self.frame.size.width)
                    ])
            } catch {
                print("catch")
            }
        
        }
        
        self.addSubview(checkmark)
        checkmark.action = { checkmark in
            if checkmark.isMark() {
                self.agreeButton.isEnabled = true
            } else {
                self.agreeButton.isEnabled = false
            }
        }
        checkmark.easy.layout([
            Left(10),
            Height(25),
            Width(25),
            Bottom(85 - (25/2))
            ])
        
        agreeLabel.font = UIFont.LucidaGrandeBold(ofSize: 14)
        agreeLabel.numberOfLines = 0
        agreeLabel.textAlignment = NSTextAlignment.left
        agreeLabel.text = "Пользователь подтверждает, что ознакомлен со всеми пунктами настоящего соглашения и безусловно принимает их."
        self.addSubview(agreeLabel)
        agreeLabel.easy.layout([
            Left(20).to(checkmark),
            Right(20),
            CenterY().to(checkmark)
            ])
        
        agreeButton.backgroundColor = UIColor.denim
        agreeButton.setTitleColor(UIColor.white, for: UIControl.State.normal)
        agreeButton.setTitle("Принять", for: UIControl.State.normal)
        agreeButton.titleLabel?.font = UIFont.LucidaGrandeBold(ofSize: 18)
        agreeButton.isEnabled = false
        self.addSubview(agreeButton)
        agreeButton.easy.layout([
            Bottom(0),
            Left(0),
            Right(0),
            Top(10).to(agreeLabel)
            ])
    }
    
}
