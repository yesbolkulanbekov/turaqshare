//
//  FrequencyButton.swift
//  turaQshare
//
//  Created by Galym Anuarbek on 10/28/18.
//  Copyright © 2018 Galym Anuarbek. All rights reserved.
//

import UIKit
import EasyPeasy

class FrequencyButtons: UIView {
    
    let button = UIButton()
    let title = UILabel()
    let titleBackground = UIView()
    let dateBackground = UIView()
    
//    let pn = UIButton()
//    let vt = UIButton()
//    let sr = UIButton()
//    let ct = UIButton()
//    let pt = UIButton()
//    let sb = UIButton()
//    let vs = UIButton()
    let weekDays = ["пн", "вт", "ср", "чт", "пт", "сб", "вс"]
    let weekButtons = [UIButton(), UIButton(), UIButton(), UIButton(), UIButton(), UIButton(), UIButton()]
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setup() {
        self.backgroundColor = UIColor.clear
        
        self.addSubview(dateBackground)
        dateBackground.backgroundColor = UIColor.whiteTwo
        dateBackground.easy.layout([
            Left(0),
            Right(0),
            Bottom(0),
            Height(60)
            ])
        
        self.addSubview(titleBackground)
        titleBackground.backgroundColor = UIColor.denim
        titleBackground.easy.layout([
            Top(0),
            Left(10),
            Width(65),
            Height(20)
            ])
        
        titleBackground.addSubview(title)
        title.font = UIFont.LucidaGrandeRegular(ofSize: 14)
        title.textColor = UIColor.white
        title.easy.layout([
            CenterX(),
            CenterY()
            ])
        
        var i = 0
        for _ in weekButtons {
            self.dateBackground.addSubview(weekButtons[i])
            weekButtons[i].setTitle(weekDays[i], for: UIControl.State.normal)
            weekButtons[i].setTitleColor(UIColor.pine, for: UIControl.State.normal)
            weekButtons[i].titleLabel?.font = UIFont.NunitoBold(ofSize: 12)
            weekButtons[i].backgroundColor = UIColor.white
            weekButtons[i].addTarget(self, action: #selector(weekButtonAction(forButton:)), for: UIControl.Event.touchUpInside)
            let width = (UIScreen.main.bounds.size.width - 50)/7
            weekButtons[i].layer.cornerRadius = (width - 5)/2
            if i == 0 {
                weekButtons[i].easy.layout([
                    CenterY(),
                    Width(width - 5),
                    Height(width - 5),
                    Left(5)
                    ])
            } else {
                weekButtons[i].easy.layout([
                    CenterY(),
                    Width(width - 5),
                    Height(width - 5),
                    Left(5).to(weekButtons[i - 1])
                    ])
            }
            i = i + 1
        }
    }
    
    @objc func weekButtonAction(forButton: UIButton) {
        if forButton.backgroundColor == UIColor.white {
            forButton.backgroundColor = UIColor.denim
            forButton.setTitleColor(UIColor.white, for: UIControl.State.normal)
        } else {
            forButton.setTitleColor(UIColor.pine, for: UIControl.State.normal)
            forButton.backgroundColor = UIColor.white
        }
    }
    
    func getActiveDays() -> [Int] {
        var activeDays: [Int] = []
        var i = 0
        for _ in weekButtons {
            if weekButtons[i].backgroundColor == UIColor.denim {
                activeDays.append(1)
            } else {
                activeDays.append(0)
            }
            i = i + 1
        }
        return activeDays
    }
    
    func frequencyString() -> String {
        var string = ""
        let stringArray = getActiveDays()
        for strings in stringArray {
            string = string + String(strings) + ","
        }
        string = String(string.dropLast())
        return string
    }
    
}
