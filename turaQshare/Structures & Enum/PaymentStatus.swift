//
//  PaymentStatus.swift
//  turaQshare
//
//  Created by Galym Anuarbek on 10/13/18.
//  Copyright © 2018 Galym Anuarbek. All rights reserved.
//

import UIKit

enum PaymentStatus {
    case paid
    case rejected
    case reserved
}
