//
//  SignStatus.swift
//  turaQshare
//
//  Created by Galym Anuarbek on 10/16/18.
//  Copyright © 2018 Galym Anuarbek. All rights reserved.
//

import Foundation

enum SignStatus {
    case signUp
    case signIn
}
