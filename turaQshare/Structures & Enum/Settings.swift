//
//  Settings.swift
//  turaQshare
//
//  Created by Galym Anuarbek on 10/12/18.
//  Copyright © 2018 Galym Anuarbek. All rights reserved.
//

import UIKit

struct Settings {
    var title: String
    var titleColor: UIColor
    var detail: String
    var detailColor: UIColor
}
