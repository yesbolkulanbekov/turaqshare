//
//  MapViewController.swift
//  turaQshare
//
//  Created by Galym Anuarbek on 10/10/18.
//  Copyright © 2018 Galym Anuarbek. All rights reserved.
//

import UIKit
import MapKit
import CoreLocation

class MapViewControllerOld: UIViewController, MKMapViewDelegate {
    let customView = MapView()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setup()
    }
    
    func setup() {
        let houses = House.getHouses()
        for house in houses {
            let annotation = MKPointAnnotation()
            annotation.coordinate = CLLocationCoordinate2D(latitude: house.lat, longitude: house.lon)
            annotation.title = house.name
            customView.mapView.addAnnotation(annotation)
        }
        customView.mapView.delegate = self
        self.view = customView
        self.hideKeyboardWhenTappedAround()
    }
    
    func placeHouses() -> [House] {
        return House.getHouses()
    }
    
    @objc func btnAction() {
        
    }
    
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        guard !(annotation is MKUserLocation) else {
            return nil
        }
        let annotationIdentifier = "Identifier"
        var annotationView: MKAnnotationView?
        if let dequeuedAnnotationView = mapView.dequeueReusableAnnotationView(withIdentifier: annotationIdentifier) {
            annotationView = dequeuedAnnotationView
            annotationView?.annotation = annotation
        } else {
            annotationView = MKAnnotationView(annotation: annotation, reuseIdentifier: annotationIdentifier)
            annotationView?.rightCalloutAccessoryView = UIButton(type: .detailDisclosure)
        }
        
        if let annotationView = annotationView {
            annotationView.canShowCallout = true
            annotationView.image = UIImage(named: "mapPinYellow")
            let btn = UIButton(type: .detailDisclosure)
            btn.addTarget(self, action: #selector(btnAction), for: .touchUpInside)
            annotationView.rightCalloutAccessoryView = btn
        }
        return annotationView
    }
    
    
    func mapView(_ mapView: MKMapView, annotationView view: MKAnnotationView, calloutAccessoryControlTapped control: UIControl) {
        let vc = ParkingsViewController()
        self.present(vc, animated: true, completion: nil)
    }
    
//    func mapView(_ mapView: MKMapView, didSelect view: MKAnnotationView) {
//        print("lol")
//    }
    
}
