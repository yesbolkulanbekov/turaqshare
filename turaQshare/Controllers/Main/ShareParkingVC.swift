//
//  ShareParkingVC.swift
//  turaQshare
//
//  Created by Galym Anuarbek on 10/7/18.
//  Copyright © 2018 Galym Anuarbek. All rights reserved.
//

import UIKit

class ShareParkingViewController: UIViewController {
    
    let customView = ShareParkingView()
    var prevVC = UIViewController()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setup()
    }
    
//    override func dismiss(animated flag: Bool, completion: (() -> Void)? = nil) {
//        super.dismiss(animated: flag, completion: completion)
//        (prevVC as! MyLotsListViewController).browse()
//    }
    
    func setup() {
        customView.backButton.addTarget(self, action: #selector(back), for: UIControl.Event.touchUpInside)
        customView.helpButton.addTarget(self, action: #selector(help), for: UIControl.Event.touchUpInside)
        customView.termsAndConditions.addTarget(self, action: #selector(termsNConditions), for: UIControl.Event.touchUpInside)
        customView.continueButton.addTarget(self, action: #selector(continueButton), for: UIControl.Event.touchUpInside)
        customView.addDocumentButton.addTarget(self, action: #selector(uploadDoc), for: UIControl.Event.touchUpInside)
        self.view = customView
        self.hideKeyboardWhenTappedAround()
    }
    
    @objc func back() { self.dismiss(animated: true) }
    
    @objc func help() {
        if customView.popup.isHidden {
            customView.popup.isHidden = false
        } else {
            customView.popup.isHidden = true
        }
    }
    
    
    
    @objc func termsNConditions() {
        let vc = TermsNConditionsViewController()
        self.present(vc, animated: true, completion: nil)
    }
    
    @objc func continueButton() {
        if (self.view as! ShareParkingView).checkmark.isMark() && (self.view as! ShareParkingView).documentImage != nil && (self.view as! ShareParkingView).parkingIndexField.text != "" && (self.view as! ShareParkingView).parkingIndexField.text != nil {
            let vc = ShareLotViewController()
//            vc.parkingAdress =
            vc.parkingIndex = (self.view as! ShareParkingView).parkingIndexField.text!
            vc.documentImage = (self.view as! ShareParkingView).documentImage!
            vc.prevVC = self
            self.present(vc, animated: true, completion: nil)
        }
    }
    
}

extension ShareParkingViewController: UINavigationControllerDelegate, UIImagePickerControllerDelegate {
    @objc func uploadDoc() {
        let image = UIImagePickerController()
        image.delegate = self
        image.sourceType = UIImagePickerController.SourceType.photoLibrary
        image.allowsEditing = false
        self.present(image, animated: true) {
            
        }
    }
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        if let image = info[UIImagePickerController.InfoKey.originalImage] as? UIImage {
            (self.view as! ShareParkingView).documentImage = image
        } else {
            
        }
        self.dismiss(animated: true, completion: nil)
    }
}
