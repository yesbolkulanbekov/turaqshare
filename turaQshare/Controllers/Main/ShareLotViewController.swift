//
//  ShareLotViewController.swift
//  turaQshare
//
//  Created by Galym Anuarbek on 10/10/18.
//  Copyright © 2018 Galym Anuarbek. All rights reserved.
//

import UIKit
import FirebaseDatabase

class ShareLotViewController: UIViewController {
    
    let customView = ShareLotView()
    var documentImage = UIImage()
    var parkingIndex = ""
    var parkingAdress = "ЖК Новый Мир"
    var prevVC = UIViewController()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setup()
    }
    
    func setup() {
        customView.doneButton.addTarget(self, action: #selector(done), for: UIControl.Event.touchUpInside)
        customView.beginButton.timepicker.addTarget(self, action: #selector(handleTimePicker(picker:)), for: UIControl.Event.valueChanged)
        customView.endButton.timepicker.addTarget(self, action: #selector(handleTimePicker(picker:)), for: UIControl.Event.valueChanged)
        self.view = customView
        
        self.hideKeyboardWhenTappedAround()
    }
    
    @objc func done() {
        let beginExists = (self.view as! ShareLotView).beginButton.dateField.text != "" && (self.view as! ShareLotView).beginButton.dateField.text != nil
        let endExists = (self.view as! ShareLotView).endButton.dateField.text != "" && (self.view as! ShareLotView).endButton.dateField.text != nil
        let frequencyExists = (self.view as! ShareLotView).frequencyButton.getActiveDays() != []
        let tokenExists = UserDefaults.standard.string(forKey: "token") != nil && UserDefaults.standard.string(forKey: "token") != ""
        if beginExists && endExists && frequencyExists && tokenExists {
            ParkingOld.saveLot(parkingIndex: parkingIndex, frequencyString: (self.view as! ShareLotView).frequencyButton.frequencyString(), begin: (self.view as! ShareLotView).beginButton.date, end: (self.view as! ShareLotView).endButton.date)
            self.dismiss(animated: true) {
                self.prevVC.dismiss(animated: false)
            }
        }
    }
    
    
    
    @objc func handleTimePicker(picker: UIDatePicker) {
        let year = picker.date.getYear()
        let month = picker.date.getMonth()
        let day = picker.date.getDay()
        let hour = picker.date.getHour()
        let minutes = picker.date.getMinutes()
        switch picker {
        case (self.view as! ShareLotView).beginButton.timepicker:
            (self.view as! ShareLotView).beginButton.dateField.text = "\(day) \(Date.getMonthNames()[month - 1]) \(year), \(hour):\(minutes)"
            (self.view as! ShareLotView).beginButton.date = picker.date
            (self.view as! ShareLotView).endButton.timepicker.minimumDate = picker.date
        case (self.view as! ShareLotView).endButton.timepicker:
            (self.view as! ShareLotView).endButton.dateField.text = "\(day) \(Date.getMonthNames()[month - 1]) \(year), \(hour):\(minutes)"
            (self.view as! ShareLotView).endButton.date = picker.date
        default:
            print("default")
        }
    }
    
}
