//
//  SettingsViewController.swift
//  turaQshare
//
//  Created by Galym Anuarbek on 10/10/18.
//  Copyright © 2018 Galym Anuarbek. All rights reserved.
//

import UIKit
import FirebaseAuth

class SettingsViewController: UIViewController {
    
    let customView = SettingsView()
    var settingsList = [Settings(title: "Phone Number", titleColor: UIColor.pine, detail: "483-930-7078", detailColor: UIColor.pine),
                        Settings(title: "Address", titleColor: UIColor.pine, detail: "015 Rolfson Inlet Apt. 700,…", detailColor: UIColor.denim),
                        Settings(title: "Payments", titleColor: UIColor.pine, detail: "Axis Credit Card", detailColor: UIColor.denim),
                        Settings(title: "", titleColor: UIColor.clear, detail: "", detailColor: UIColor.clear),
                        Settings(title: "Support", titleColor: UIColor.pine, detail: "", detailColor: UIColor.clear),
                        Settings(title: "Share", titleColor: UIColor.pine, detail: "", detailColor: UIColor.clear),
                        Settings(title: "Terms & Condition", titleColor: UIColor.pine, detail: "", detailColor: UIColor.clear),
                        Settings(title: "Signout", titleColor: UIColor.red, detail: "", detailColor: UIColor.clear)]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setup()
    }
    
    func setup() {
        self.navigationItem.title = "Настройки"
        customView.settingsCollectionView.dataSource = self
        customView.settingsCollectionView.delegate = self
        self.view = customView
    }
}

extension SettingsViewController: UICollectionViewDataSource, UICollectionViewDelegate {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return settingsList.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if settingsList[indexPath.row].title != "" {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! SettingsCollectionViewCell
            cell.titleLabel.text = settingsList[indexPath.row].title
            cell.titleLabel.textColor = settingsList[indexPath.row].titleColor
            cell.detailLabel.text = settingsList[indexPath.row].detail
            cell.detailLabel.textColor = settingsList[indexPath.row].detailColor
            return cell
        } else {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ecell", for: indexPath) as! EmptyCell
            return cell
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        if kind == UICollectionView.elementKindSectionHeader {
            let view = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: "header", for: indexPath) as! SettingsCollectionViewHeader
            return view
        }
        return UICollectionReusableView()
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        switch indexPath.row {
        case 7:
            do {
//                try Auth.auth().signOut()
//                let vc = self.tabBarController as! TabbarController
//                vc.dismiss(animated: true, completion: nil)
//                vc.prevVC.dismiss(animated: false, completion: nil)
                
                UserDefaultsUserStorageService.shared.removeToken()
                
                let loginVC = LoginViewModuleConfigurator().assembleModule()
                let navigationController = UINavigationController(rootViewController: loginVC)
        
                guard let window = UIApplication.shared.keyWindow else {
                    return
                }
                
                window.rootViewController = navigationController
                
            } catch let signOutError as NSError {
                print ("Error signing out: %@", signOutError)
            }
        default:
            print(indexPath.row)
        }
    }
    
}
