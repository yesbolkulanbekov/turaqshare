//
//  HistoryViewController.swift
//  turaQshare
//
//  Created by Galym Anuarbek on 10/10/18.
//  Copyright © 2018 Galym Anuarbek. All rights reserved.
//

import UIKit

class HistoryViewController: UIViewController {
    
    let customView = HistoryView()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setup()
    }
    
    func setup() {
        self.view = customView
        self.navigationItem.title = "История"
    }
}
