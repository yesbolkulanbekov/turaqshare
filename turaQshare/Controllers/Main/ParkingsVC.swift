//
//  ParkingsVC.swift
//  turaQshare
//
//  Created by Galym Anuarbek on 10/17/18.
//  Copyright © 2018 Galym Anuarbek. All rights reserved.
//

import UIKit

class ParkingsViewController: UIViewController {
    
    let customView = ParkingView()
    var house = House.getHouses()[0]
    var parkings: [ParkingOld] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setup()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        browse()
    }
    
    func browse() {
        self.house.getParkings { (parkingss) in
            self.parkings = parkingss
            print(self.parkings.count)
            (self.view as! ParkingView).historyCollectionView.reloadData()
        }
    }
    
    func setup() {
        customView.historyCollectionView.dataSource = self
        customView.historyCollectionView.delegate = self
        self.view = customView
    }
    
    @objc func backButtonAction() {
        self.dismiss(animated: true, completion: nil)
    }
    
    
}

extension ParkingsViewController: UICollectionViewDelegate, UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.parkings.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! ParkingCollectionCell
        let attr = NSMutableAttributedString(string: parkings[indexPath.row].owner.name, attributes: [NSAttributedString.Key.font : UIFont.LucidaGrandeRegular(ofSize: 14), NSAttributedString.Key.foregroundColor : UIColor.pine])
        let attr2 = NSAttributedString(string: " #" + parkings[indexPath.row].index, attributes: [NSAttributedString.Key.font : UIFont.NunitoLight(ofSize: 14), NSAttributedString.Key.foregroundColor : UIColor.denim])
        attr.append(attr2)
        cell.nameLabel.attributedText = attr
        cell.date.text = parkings[indexPath.row].startTime.toString() + "\n" + parkings[indexPath.row].endTime.toString()
        cell.photoView.image = parkings[indexPath.row].owner.image
        cell.backgroundColor = UIColor.white
        cell.layer.shadowColor = UIColor.pine.cgColor
        cell.layer.shadowOpacity = 0.08
        cell.layer.shadowOffset = CGSize(width: 0, height: 2)
        cell.layer.cornerRadius = 4
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        if kind == UICollectionView.elementKindSectionHeader {
            let view = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: "header", for: indexPath) as! ParkingCollectionHeader
            view.image.image = UIImage(named: "novimir")
            view.backButton.addTarget(self, action: #selector(backButtonAction), for: UIControl.Event.touchUpInside)
            return view
        }
        return UICollectionReusableView()
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let vc = GetParkingViewController()
        vc.parking = self.parkings[indexPath.row]
        self.present(vc, animated: true, completion: nil)
    }
}

