//
//  MyLotsListViewController.swift
//  turaQshare
//
//  Created by Galym Anuarbek on 10/22/18.
//  Copyright © 2018 Galym Anuarbek. All rights reserved.
//

import UIKit

class MyLotsListViewController: UIViewController {
    
    let customView = MyLotsListView()
    var my_parkings: [ParkingOld] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setup()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        browse()
    }
    
    func setup() {
        customView.listTableView.dataSource = self
        customView.listTableView.delegate = self
        customView.listTableView.tableFooterView = UIView()
        self.view = self.customView
    }
    
    func browse() {
        ParkingOld.getMyLots(token: UserDefaults.standard.string(forKey: "token")!) { (parkings) in
            print("browse")
            self.my_parkings = parkings
            (self.view as! MyLotsListView).listTableView.reloadData()
        }
    }
    
    @objc func newAdress() {
        let vc = ShareParkingViewController()
        vc.prevVC = self
        self.present(vc, animated: true, completion: nil)
    }
    
    func editAct(row: Int) {
        let vc = ShareParkingViewController()
        (vc.customView as! ShareParkingView).parkingIndexField.isUserInteractionEnabled = false
        (vc.customView as! ShareParkingView).parkingIndexField.text = self.my_parkings[row].index
//        (vc.customView as! ShareParkingView).
        self.present(vc, animated: true, completion: nil)
    }
}

extension MyLotsListViewController: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.my_parkings.count + 2
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch indexPath.row {
        case 0:
            let header = tableView.dequeueReusableCell(withIdentifier: "header", for: indexPath) as! LotsTableHeader
            return header
        case self.my_parkings.count + 1:
            let footer = tableView.dequeueReusableCell(withIdentifier: "footer", for: indexPath) as! LotsTableFooter
            footer.newLotButton.addTarget(self, action: #selector(newAdress), for: UIControl.Event.touchUpInside)
            footer.separatorInset = UIEdgeInsets(top: 0, left: 1000, bottom: 0, right: 0)
            return footer
        default:
            let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! LotsListTableViewCell
            if my_parkings.count > 0 {
                cell.hashLabel.text = self.my_parkings[indexPath.row - 1].index
                cell.nameLabel.text = "ЖК Новый Мир"
            }
            return cell
        }
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        switch indexPath.row {
        case 0:
            return 80
        case self.my_parkings.count + 1:
            return 118
        default:
            return 60
        }
    }
    
//    @available(iOS 10.0, *)
    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
        let edit = UITableViewRowAction(style: .normal, title: "Edit") { (action, indexPath) in
            self.editAct(row: indexPath.row - 1)
        }
        
        let delete = UITableViewRowAction(style: .normal, title: "Delete") { (action, indexPath) in
            DispatchQueue.main.async {
                tableView.beginUpdates()
                self.my_parkings[indexPath.row - 1].deleteMe()
                self.my_parkings.remove(at: indexPath.row - 1)
                tableView.deleteRows(at: [indexPath], with: UITableView.RowAnimation.left)
                tableView.endUpdates()
                print(indexPath.row, self.my_parkings.count)
            }
        }
        edit.backgroundColor = UIColor(red: 0, green: 85/255, blue: 77/255, alpha: 1)
        delete.backgroundColor = UIColor.denim
        
        
        if indexPath.row == 0 || indexPath.row == self.my_parkings.count + 1 {
            return []
        } else {
            return [delete, edit]
        }
    }
    
    @available(iOS 11.0, *)
    func tableView(_ tableView: UITableView, trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
        let edit = UIContextualAction(style: .normal, title:  "", handler: { (ac:UIContextualAction, view:UIView, success:(Bool) -> Void) in
            self.editAct(row: indexPath.row - 1)
            success(true)
        })
        edit.image = UIImage(named: "pencil")
        edit.backgroundColor = UIColor(red: 0, green: 85/255, blue: 77/255, alpha: 1)
        
        let delete = UIContextualAction(style: .normal, title:  "", handler: { (ac:UIContextualAction, view:UIView, success:(Bool) -> Void) in
            DispatchQueue.main.async {
                tableView.beginUpdates()
                self.my_parkings[indexPath.row - 1].deleteMe()
                self.my_parkings.remove(at: indexPath.row - 1)
                tableView.deleteRows(at: [indexPath], with: UITableView.RowAnimation.left)
                tableView.endUpdates()
                tableView.reloadData()
            }
            success(true)
        })
        delete.image = UIImage(named: "trash")
        delete.backgroundColor = UIColor.denim
        
        if indexPath.row == 0 || indexPath.row == self.my_parkings.count + 1 {
            return UISwipeActionsConfiguration(actions: [])
        } else {
            return UISwipeActionsConfiguration(actions: [delete, edit])
        }
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 1
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let v = UIView()
        v.backgroundColor = UIColor.clear
        return v
    }
}
