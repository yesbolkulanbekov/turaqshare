//
//  GetParkingViewController.swift
//  turaQshare
//
//  Created by Galym Anuarbek on 11/2/18.
//  Copyright © 2018 Galym Anuarbek. All rights reserved.
//

import UIKit
import FirebaseDatabase

class GetParkingViewController: UIViewController {
    
    let customView = GetParkingView()
    var parking = ParkingOld(owner: UserOld(uid: "", phone: "", name: "", carNumber: "", image: UIImage()), startTime: Date(), endTime: Date(), index: "")
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setup()
    }
    
    func setup() {
        customView.backButton.addTarget(self, action: #selector(backButtonAction), for: UIControl.Event.touchUpInside)
        customView.emailBillButton.addTarget(self, action: #selector(billToEmailAction), for: UIControl.Event.touchUpInside)
        customView.payButton.addTarget(self, action: #selector(payAction), for: UIControl.Event.touchUpInside)
        customView.beginButton.timepicker.addTarget(self, action: #selector(handleTimePicker(picker:)), for: UIControl.Event.valueChanged)
        customView.endButton.timepicker.addTarget(self, action: #selector(handleTimePicker(picker:)), for: UIControl.Event.valueChanged)
        self.view = customView
        self.hideKeyboardWhenTappedAround()
    }
    
    @objc func payAction() {
        Database.database().reference().child("parkings").child("ЖК Новый Мир").child(parking.owner.uid).child(parking.index).child("status").observe(DataEventType.value) { (snapshot) in
            if snapshot.value as? String != "Taken" {
                Database.database().reference().child("parkings").child("ЖК Новый Мир").child(self.parking.owner.uid).child(self.parking.index).updateChildValues([
                    "taken_by" : UserDefaults.standard.string(forKey: "token")!,
                    "status" : "Taken"
                    ])
            }
        }
        
    }
    
    @objc func billToEmailAction() {
        
    }
    
    @objc func backButtonAction() {
        self.dismiss(animated: true)
    }
    
    @objc func handleTimePicker(picker: UIDatePicker) {
        let year = picker.date.getYear()
        let month = picker.date.getMonth()
        let day = picker.date.getDay()
        let hour = picker.date.getHour()
        let minutes = picker.date.getMinutes()
        switch picker {
        case (self.view as! GetParkingView).beginButton.timepicker:
            (self.view as! GetParkingView).beginButton.dateField.text = "\(day) \(Date.getMonthNames()[month - 1]) \(year), \(hour):\(minutes)"
            (self.view as! GetParkingView).beginButton.date = picker.date
            (self.view as! GetParkingView).endButton.timepicker.minimumDate = picker.date
        case (self.view as! GetParkingView).endButton.timepicker:
            (self.view as! GetParkingView).endButton.dateField.text = "\(day) \(Date.getMonthNames()[month - 1]) \(year), \(hour):\(minutes)"
            (self.view as! GetParkingView).endButton.date = picker.date
        default:
            print("default")
        }
    }
}
