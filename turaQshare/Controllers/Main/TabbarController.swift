//
//  TabbarController.swift
//  turaQshare
//
//  Created by Galym Anuarbek on 10/10/18.
//  Copyright © 2018 Galym Anuarbek. All rights reserved.
//

import UIKit

class TabbarController: UITabBarController, UITabBarControllerDelegate {
    
    var prevVC = ConfirmedViewController()
    let share = MyLotsListViewController()
    let map = MapViewController()
    let history = HistoryViewController()
    let settings = SettingsViewController()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setup()
    }
    
    func setup() {
//        self.tabBar.
        delegate = self
        
        map.tabBarItem = UITabBarItem(title: "", image: UIImage(named: "icon1")?.withRenderingMode(UIImage.RenderingMode.alwaysOriginal), selectedImage: UIImage(named: "icon1a")?.withRenderingMode(UIImage.RenderingMode.alwaysOriginal))
        
        share.tabBarItem = UITabBarItem(title: "", image: UIImage(named: "icon2")?.withRenderingMode(UIImage.RenderingMode.alwaysOriginal), selectedImage: UIImage(named: "icon2a")?.withRenderingMode(UIImage.RenderingMode.alwaysOriginal))
        
        
        history.tabBarItem = UITabBarItem(title: "", image: UIImage(named: "icon3")?.withRenderingMode(UIImage.RenderingMode.alwaysOriginal), selectedImage: UIImage(named: "icon3a")?.withRenderingMode(UIImage.RenderingMode.alwaysOriginal))
        
        settings.tabBarItem = UITabBarItem(title: "", image: UIImage(named: "icon4")?.withRenderingMode(UIImage.RenderingMode.alwaysOriginal), selectedImage: UIImage(named: "icon4a")?.withRenderingMode(UIImage.RenderingMode.alwaysOriginal))
        
        let tabBarList = [map, share, history, settings]
        
        viewControllers = tabBarList
        
        self.tabBar.barTintColor = UIColor(white: 1, alpha: 0.8)
        self.tabBar.tintColor = UIColor.denim
//        self.tabBar.unselectedItemTintColor = UIColor
//        self.tabBar.tint
    }
    
//    override func tabBar(_ tabBar: UITabBar, didSelect item: UITabBarItem) {
//        if item == share.tabBarItem {
//            share.browse()
//        }
//    }
}
