//
//  VerificationVC.swift
//  turaQshare
//
//  Created by Galym Anuarbek on 10/3/18.
//  Copyright © 2018 Galym Anuarbek. All rights reserved.
//

import UIKit
import FirebaseAuth

class VerificationViewControllerOld: UIViewController {
    
    let customView = VerificationViewOld()
    var prevVC = true
    var status: SignStatus = .signUp
    var verificationID = ""
    var carNumber = ""
    var name = ""
    var phone = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setup()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        if prevVC {
            
        } else {
            self.dismiss(animated: false, completion: nil)
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
    }
    
    func setup() {

        customView.confirmButton.addTarget(self, action: #selector(confirm), for: UIControl.Event.touchUpInside)
        
        self.view = customView
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(keyboardWillShow),
            name: UIResponder.keyboardWillShowNotification,
            object: nil
        )
        
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(keyboardWillHide),
            name: UIResponder.keyboardWillHideNotification,
            object: nil
        )
        
        self.hideKeyboardWhenTappedAround()
    }
    
    @objc func keyboardWillShow(_ notification: Notification) {
        if let keyboardFrame: NSValue = notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue {
            if keyboardFrame.cgRectValue.minY < (self.view as! VerificationViewOld).codeField.frame.maxY {
                UIView.animate(withDuration: 0.2, delay: 0, options: [], animations: {
                    self.view.center.y -= ((self.view as! VerificationViewOld).codeField.frame.maxY - keyboardFrame.cgRectValue.minY) + 10
                })
            }
        }
    }
    
    @objc func keyboardWillHide(_ notification: Notification) {
        if self.view.frame.maxY < self.view.frame.height {
            UIView.animate(withDuration: 0.2, delay: 0, options: [], animations: {
                self.view.center.y += self.view.frame.height - self.view.frame.maxY
            })
        }
        confirm()
    }
    
    @objc func confirm() {

        if (self.view as! VerificationViewOld).codeField.getText().count == 6 {
            let verificationCode = (self.view as! VerificationViewOld).codeField.getText()
            let credential = PhoneAuthProvider.provider().credential(withVerificationID: verificationID, verificationCode: verificationCode)
            Auth.auth().signIn(with: credential) { (user, error) in
                print(error.debugDescription)
                if user?.uid != nil {
                    self.prevVC = false
                    print(user?.uid)
                    print(self.verificationID)
                    UserDefaults.standard.set(user?.uid, forKey: "token")
                    if self.status == SignStatus.signUp { UserOld.create(uid: user?.uid, carNumber: self.carNumber, name: self.name, phone: self.phone) }
                    UserOld.userExists(uid: (user?.uid)!, completion: { (exists) in
                        if exists {
                            let vc = ConfirmedViewController()
                            self.present(vc, animated: true)
                        } else {
                            let vc = MissingDataViewController()
                            vc.userPhone = self.phone
                            vc.uid = (user?.uid)!
                            self.present(vc, animated: true, completion: nil)
                        }
                    })
                } else {
                    //phone already taken
                }
            }
        }
    }
    
}
