//
//  MissingDataVC.swift
//  turaQshare
//
//  Created by Galym Anuarbek on 10/28/18.
//  Copyright © 2018 Galym Anuarbek. All rights reserved.
//

import UIKit

class MissingDataViewController: UIViewController {
    
    let customView = MissingDataView()
    var userPhone = ""
    var uid = ""
    var isPrev = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setup()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        if isPrev {
            self.dismiss(animated: false, completion: nil)
        }
    }
    
    func setup() {
        customView.phoneField.text = userPhone
        customView.registerButton.addTarget(self, action: #selector(registerButton), for: UIControl.Event.touchUpInside)
        self.view = customView
    }
    
    @objc func registerButton() {
        if (self.view as! MissingDataView).carNumbField.text != "" && (self.view as! MissingDataView).carNumbField.text != nil && (self.view as! MissingDataView).nameField.text != "" && (self.view as! MissingDataView).nameField.text != nil {
            UserOld.create(uid: uid, carNumber: (self.view as! MissingDataView).carNumbField.text!, name: (self.view as! MissingDataView).nameField.text!, phone: userPhone)
            let vc = TabbarController()
            self.isPrev = true
            self.present(vc, animated: true, completion: nil)
        }
    }
}
