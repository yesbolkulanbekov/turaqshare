//
//  ConfirmedViewController.swift
//  turaQshare
//
//  Created by Galym Anuarbek on 10/10/18.
//  Copyright © 2018 Galym Anuarbek. All rights reserved.
//

import UIKit

class ConfirmedViewController: UIViewController {
    
    let customView = ConfirmedView()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setup()
    }
    
    func setup() {
        customView.closeButton.addTarget(self, action: #selector(close), for: UIControl.Event.touchUpInside)
        self.view = customView
        self.hideKeyboardWhenTappedAround()
    }
    
    @objc func close() {
        let vc = TabbarController()
        vc.prevVC = self
        self.present(vc, animated: true)
    }
}
