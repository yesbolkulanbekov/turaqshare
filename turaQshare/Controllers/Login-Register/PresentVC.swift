//
//  PresentViewController.swift
//  turaQshare
//
//  Created by Galym Anuarbek on 10/10/18.
//  Copyright © 2018 Galym Anuarbek. All rights reserved.
//

import UIKit

class PresentViewController: UIViewController {
    
    let customViews = [PresentFirstView(), PresentSecondView()]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setup()
    }
    
    func setup() {
        (customViews[0] as! PresentFirstView).continueButton.addTarget(self, action: #selector(nextView), for: UIControl.Event.touchUpInside)
        (customViews[1] as! PresentSecondView).continueButton.addTarget(self, action: #selector(continueButton), for: UIControl.Event.touchUpInside)
        self.view = customViews[0]
    }
    
    @objc func nextView() {
        self.view = customViews[1]
    }
    
    @objc func continueButton() {
        print("lalsjd")
    }
}
