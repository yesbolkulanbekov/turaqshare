//
//  RegistrationVC.swift
//  turaQshare
//
//  Created by Galym Anuarbek on 10/6/18.
//  Copyright © 2018 Galym Anuarbek. All rights reserved.
//

import UIKit

class RegistrationViewController: UIViewController {
    
    let customView = RegistrationView()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setup()
    }
    
    func setup() {
        customView.offerButton.button.addTarget(self, action: #selector(offer), for: UIControl.Event.touchUpInside)
        customView.findButton.button.addTarget(self, action: #selector(find), for: UIControl.Event.touchUpInside)
        customView.representButton.button.addTarget(self, action: #selector(represent), for: UIControl.Event.touchUpInside)
        self.view = customView
        self.hideKeyboardWhenTappedAround()
    }
    
    @objc func offer() {
        let vc = SignUpViewController()
        self.show(vc, sender: nil)
    }
    
    @objc func find() {
        let vc = VerificationViewControllerOld()
        self.show(vc, sender: nil)
    }
    
    @objc func represent() {
        self.dismiss(animated: true, completion: nil)
    }
}
