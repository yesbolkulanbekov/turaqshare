//
//  SignUpVC.swift
//  turaQshare
//
//  Created by Galym Anuarbek on 10/3/18.
//  Copyright © 2018 Galym Anuarbek. All rights reserved.
//

import UIKit
import FirebaseAuth
import EasyPeasy

class SignUpViewController: UIViewController {
    
    let customView = SignUpView()
    let termsView = TermsOfUseView(frame: CGRect(x: 20, y: UIScreen.main.bounds.height + 50, width: UIScreen.main.bounds.width - 40, height: UIScreen.main.bounds.height - 100))
    let bgView = UIView()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setup()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    func setup() {
        customView.registerButton.addTarget(self, action: #selector(registerAction), for: UIControl.Event.touchUpInside)
        customView.loginButton.addTarget(self, action: #selector(loginAction), for: UIControl.Event.touchUpInside)
        self.view = customView
        self.view.addSubview(bgView)
        self.view.addSubview(termsView)
        bgView.easy.layout([
            Size(UIScreen.main.bounds.size),
            Top(UIScreen.main.bounds.height)
            ])
        self.hideKeyboardWhenTappedAround()
    }
    
    @objc func registerAction(sender: UIButton!) {
        let carNumb = (self.view as! SignUpView).carNumbField.text ?? ""
        let phoneNumb = (self.view as! SignUpView).phoneField.text ?? ""
        let validate: Bool
        let formatedPhone: String
        (validate, formatedPhone) = Validator.validatePhone(value: phoneNumb)
        if validate && Validator.validateCar(value: carNumb) && (self.view as! SignUpView).nameField.text != nil && (self.view as! SignUpView).nameField.text! != "" {
            
            bgView.backgroundColor = UIColor.black
            bgView.alpha = 0
            bgView.easy.layout(Edges(0))
            
            
            termsView.isHidden = false
            termsView.layer.cornerRadius = 10
            termsView.clipsToBounds = true
            termsView.agreeButton.addTarget(self, action: #selector(agree), for: UIControl.Event.touchUpInside)
            
            UIView.animate(withDuration: 1) {
                self.termsView.center.y -= UIScreen.main.bounds.height
                self.bgView.alpha += 0.8
            }
            
        } else {
            print("error")
        }
    }
    
    @objc func agree() {
        let vc = VerificationViewControllerOld()
        let validate: Bool
        let formatedPhone: String
        let carNumb = (self.view as! SignUpView).carNumbField.text ?? ""
        let phoneNumb = (self.view as! SignUpView).phoneField.text ?? ""
        (validate, formatedPhone) = Validator.validatePhone(value: phoneNumb)
        self.present(vc, animated: true, completion: {
            PhoneAuthProvider.provider().verifyPhoneNumber(formatedPhone, uiDelegate: nil, completion: { (verificationID, error) in
                if error != nil { return }
                guard let verificationID = verificationID else { return }
                vc.verificationID = verificationID
                vc.phone = formatedPhone
                vc.carNumber = carNumb
                vc.name = (self.view as! SignUpView).nameField.text!
            })
        })
    }
    
    @objc func loginAction(sender: UIButton!) {
        self.dismiss(animated: false, completion: nil)
    }
    
    
    
}
