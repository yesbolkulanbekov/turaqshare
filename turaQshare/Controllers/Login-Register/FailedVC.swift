//
//  FailedViewController.swift
//  turaQshare
//
//  Created by Galym Anuarbek on 10/10/18.
//  Copyright © 2018 Galym Anuarbek. All rights reserved.
//

import UIKit

class FailedViewController: UIViewController {
    
    let customView = FailedView()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setup()
    }
    
    func setup() {
        customView.tryAgain.addTarget(self, action: #selector(tryAgain), for: UIControl.Event.touchUpInside)
        self.view = customView
    }
    
    @objc func tryAgain() {
        self.dismiss(animated: true, completion: nil)
    }
    
}
