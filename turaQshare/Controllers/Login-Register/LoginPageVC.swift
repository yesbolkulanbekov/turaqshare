//
//  LoginPageViewController.swift
//  turaQshare
//
//  Created by Galym Anuarbek on 10/2/18.
//  Copyright © 2018 Galym Anuarbek. All rights reserved.
//

import UIKit
import FirebaseAuth

class LoginPageViewController: UIViewController {
    
    let customView = LoginPageView()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setup()
        loginedUser()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    func loginedUser() {
        Auth.auth().addStateDidChangeListener { (auth, user) in
            if let user = user {
                UserDefaults.standard.set(user.uid, forKey: "token")
                let vc = TabbarController()
                self.present(vc, animated: true)
            } else {
                return
            }
        }
    }
    
    func setup() {
        customView.registerButton.addTarget(self, action: #selector(registerAction), for: UIControl.Event.touchUpInside)
        customView.loginButton.addTarget(self, action: #selector(loginAction), for: UIControl.Event.touchUpInside)
        customView.forgotPassword.addTarget(self, action: #selector(forgotAction), for: UIControl.Event.touchUpInside)
        self.view = customView
        self.hideKeyboardWhenTappedAround()
    }
    
    @objc func registerAction(sender: UIButton!) {
        let vc = SignUpViewController()
        self.present(vc, animated: false, completion: nil)
    }
    
    @objc func loginAction(sender: UIButton!) {
        let phone = (self.view as! LoginPageView).phoneField.text
        let formatedPhone: String
        let validate: Bool
        (validate, formatedPhone) = Validator.validatePhone(value: phone)
        if validate {
            PhoneAuthProvider.provider().verifyPhoneNumber(formatedPhone, uiDelegate: nil) { (verificationID, error) in
                if error != nil { return }
                guard let verificationID = verificationID else { return }
                UserDefaults.standard.set(verificationID, forKey: "authVerificationID")
                let vc = VerificationViewControllerOld()
                vc.verificationID = verificationID
                vc.phone = formatedPhone
                vc.status = SignStatus.signIn
                self.present(vc, animated: true, completion: nil)
            }
    //        let vc = TabbarController()
    //        self.present(vc, animated: true, completion: nil)
        }
    }
    
    @objc func forgotAction(sender: UIButton!) {
        print("forgot")
    }
    
}
