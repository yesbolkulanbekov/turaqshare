//
//  TermsNConditionsVC.swift
//  turaQshare
//
//  Created by Galym Anuarbek on 10/10/18.
//  Copyright © 2018 Galym Anuarbek. All rights reserved.
//

import UIKit

class TermsNConditionsViewController: UIViewController {
    
    let customView = TermsNConditionsView()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setup()
    }
    
    func setup() {
        customView.continueButton.addTarget(self, action: #selector(continueAction), for: UIControl.Event.touchUpInside)
        self.view = customView
        self.hideKeyboardWhenTappedAround()
    }
    
    @objc func continueAction() {
        self.dismiss(animated: true, completion: nil)
    }
    
}
