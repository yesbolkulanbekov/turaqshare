//
//  ParkingCollectionHeader.swift
//  turaQshare
//
//  Created by Galym Anuarbek on 10/17/18.
//  Copyright © 2018 Galym Anuarbek. All rights reserved.
//

import UIKit
import EasyPeasy

class ParkingCollectionHeader: UICollectionReusableView {
    
    let image = UIImageView()
    let backButton = UIButton()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setup() {
        self.addSubview(image)
        image.easy.layout([
            Top(0),
            Left(0),
            Right(0),
            Bottom(16)
            ])
        
        backButton.setImage(UIImage(named: "left-arrow"), for: UIControl.State.normal)
        backButton.contentEdgeInsets = UIEdgeInsets(top: 8.3, left: 5, bottom: 8.3, right: 5)
        backButton.contentMode = UIView.ContentMode.scaleAspectFit
        self.addSubview(backButton)
        backButton.easy.layout([
            Top(10),
            Left(15),
            Height(30),
            Width(30)
            ])
    }
}
