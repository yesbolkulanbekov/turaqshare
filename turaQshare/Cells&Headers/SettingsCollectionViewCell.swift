//
//  UICollectionViewCell.swift
//  turaQshare
//
//  Created by Galym Anuarbek on 10/10/18.
//  Copyright © 2018 Galym Anuarbek. All rights reserved.
//

import UIKit
import EasyPeasy

class SettingsCollectionViewCell: UICollectionViewCell {
    
    let titleLabel = UILabel()
    let detailLabel = UILabel()
    let separator = UIView()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setup() {
        self.addSubview(titleLabel)
        titleLabel.easy.layout([
            Top(16),
            Bottom(15),
            Left(20),
            Width(130)
            ])
        titleLabel.font = UIFont.NunitoLight(ofSize: 14)
        titleLabel.textColor = UIColor.pine
        titleLabel.numberOfLines = 1
        
        self.addSubview(detailLabel)
        detailLabel.easy.layout([
            Top(16),
            Bottom(15),
            Right(20),
            Width(180)
            ])
        detailLabel.font = UIFont.NunitoRegular(ofSize: 14)
        detailLabel.textColor = UIColor.denim
        detailLabel.numberOfLines = 1
        detailLabel.textAlignment = NSTextAlignment.right
        
        self.addSubview(separator)
        separator.easy.layout([
            Left(20),
            Right(20),
            Bottom(0),
            Height(1)
            ])
        separator.backgroundColor = UIColor.darkGray.withAlphaComponent(0.1)
    }
    
}


class EmptyCell: UICollectionViewCell {
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
