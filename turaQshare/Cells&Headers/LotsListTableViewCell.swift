//
//  LotsListTableViewCell.swift
//  turaQshare
//
//  Created by Galym Anuarbek on 10/22/18.
//  Copyright © 2018 Galym Anuarbek. All rights reserved.
//

import UIKit
import EasyPeasy

class LotsListTableViewCell: UITableViewCell {
    
    let hashLabel = UILabel()
    let nameLabel = UILabel()
    let statusView = UIImageView()
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setup() {
        self.addSubview(hashLabel)
        hashLabel.textColor = UIColor.denim
        hashLabel.font = UIFont.NunitoRegular(ofSize: 14)
        hashLabel.textAlignment = NSTextAlignment.left
        hashLabel.easy.layout([
            Left(53),
            CenterY()
            ])
        
        self.addSubview(nameLabel)
        nameLabel.textColor = UIColor.pine
        nameLabel.font = UIFont.LucidaGrandeRegular(ofSize: 14)
        nameLabel.textAlignment = NSTextAlignment.left
        nameLabel.easy.layout([
            Left(110),
            CenterY()
            ])
        
        self.addSubview(statusView)
        statusView.contentMode = UIView.ContentMode.scaleAspectFit
        statusView.image = UIImage(named: "sandClock")
        statusView.easy.layout([
            CenterY(),
            Right(19)
            ])
    }
}
