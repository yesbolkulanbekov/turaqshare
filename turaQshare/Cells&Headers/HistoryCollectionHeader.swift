//
//  HistoryCollectionHeader.swift
//  turaQshare
//
//  Created by Galym Anuarbek on 10/13/18.
//  Copyright © 2018 Galym Anuarbek. All rights reserved.
//

import UIKit
import EasyPeasy

class HistoryCollectionHeader: UICollectionReusableView {
    
    let titleLabel = UILabel()
    let detailLabel = UILabel()
    let segmentControl = UISegmentedControl(items: ["Все", "В обработке", "Архив"])
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setup() {
//        self.addSubview(titleLabel)
//        titleLabel.font = UIFont.LucidaGrandeBold(ofSize: 22)
//        titleLabel.textColor = UIColor.pine
//        titleLabel.text = "История"
//        titleLabel.easy.layout([
//            Top(12),
//            CenterX()
//            ])
//
//        self.addSubview(detailLabel)
//        detailLabel.font = UIFont.NunitoLight(ofSize: 14)
//        detailLabel.textColor = UIColor.pine
//        detailLabel.numberOfLines = 0
//        detailLabel.text = "Donec sed odio dui. Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum."
//        detailLabel.easy.layout([
//            Top(38).to(titleLabel),
//            Left(20),
//            Right(20)
//            ])
        
        self.addSubview(segmentControl)
        segmentControl.selectedSegmentIndex = 0
        segmentControl.tintColor = UIColor.denim
        segmentControl.layer.cornerRadius = 0
        segmentControl.layer.borderColor = UIColor.denim.cgColor
        segmentControl.layer.borderWidth = 1.0
        segmentControl.layer.masksToBounds = true
        segmentControl.easy.layout([
//            Top(15).to(detailLabel),
            Top(20),
            Left(20),
            Right(20),
            Height(40)
            ])
    }
}
