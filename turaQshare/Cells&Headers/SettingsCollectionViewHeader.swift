//
//  SettingsCollectionViewHeader.swift
//  turaQshare
//
//  Created by Galym Anuarbek on 10/10/18.
//  Copyright © 2018 Galym Anuarbek. All rights reserved.
//

import UIKit
import EasyPeasy

class SettingsCollectionViewHeader: UICollectionReusableView {
    
    //let titleLabel = UILabel()
    let avatarView = UIImageView()
    let nameLabel = UILabel()
    let viewProfile = UIButton()
    let bellView = UIButton()
    let separator = UIView()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setup() {
//        self.addSubview(titleLabel)
//        titleLabel.font = UIFont.LucidaGrandeBold(ofSize: 22)
//        titleLabel.textColor = UIColor.pine
//        titleLabel.text = "Настройки"
//        titleLabel.easy.layout([
//            Top(23),
//            CenterX()
//            ])
        
        self.addSubview(avatarView)
        avatarView.easy.layout([
            Height(60),
            Width(60),
            Left(20),
//            Top(10).to(titleLabel),
            Top(10)
            ])
        avatarView.contentMode = UIView.ContentMode.scaleAspectFit
        avatarView.image = UIImage(named: "tick")
        avatarView.layer.cornerRadius = 30
        
        self.addSubview(nameLabel)
        nameLabel.font = UIFont.NunitoRegular(ofSize: 20)
        nameLabel.textColor = UIColor.pine
        nameLabel.text = "Default Default"
        nameLabel.easy.layout([
            Left(16).to(avatarView),
            Top(17),//.to(titleLabel),
            Width(220)
            ])
        
        self.addSubview(viewProfile)
        viewProfile.setTitle("Edit profile", for: UIControl.State.normal)
        viewProfile.setTitleColor(UIColor.denim, for: UIControl.State.normal)
        viewProfile.easy.layout([
            Top(0).to(nameLabel),
            Left(16).to(avatarView)
            ])
        
        self.addSubview(separator)
        separator.backgroundColor = UIColor.pine.withAlphaComponent(0.1)
        separator.easy.layout([
            Top(21).to(avatarView),
            Right(0),
            Left(0),
            Height(10)
            ])
        
//        self.addSubview(bellView)
//        
    }
    
}
