//
//  HistoryAllCell.swift
//  turaQshare
//
//  Created by Galym Anuarbek on 10/13/18.
//  Copyright © 2018 Galym Anuarbek. All rights reserved.
//

import UIKit
import EasyPeasy

class HistoryAllCell: UICollectionViewCell {
    
    let dateLabel = UILabel()
    let quantityLabel = UILabel()
    let statusView = UIImageView()
    let priceLabel = UILabel()
    let totalLabel = UILabel()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setup() {
        self.backgroundColor = UIColor.white
        
        self.addSubview(dateLabel)
        dateLabel.font = UIFont.NunitoLight(ofSize: 14)
        dateLabel.textColor = UIColor.pine
        dateLabel.easy.layout([
            Top(15),
            Left(10)
            ])
        
        self.addSubview(quantityLabel)
        quantityLabel.font = UIFont.NunitoBold(ofSize: 14)
        quantityLabel.textColor = UIColor.pine
        quantityLabel.easy.layout([
            Top(10).to(dateLabel),
            Left(10)
            ])
        
        self.addSubview(statusView)
        statusView.easy.layout([
            Right(11),
            CenterY(),
            Height(43),
            Width(109)
            ])
        
        self.addSubview(totalLabel)
        totalLabel.font = UIFont.NunitoBold(ofSize: 16)
        totalLabel.textColor = UIColor.pine.withAlphaComponent(0.5)
        totalLabel.easy.layout([
            Top(20).to(quantityLabel),
            Left(10)
            ])
        
        self.addSubview(priceLabel)
        priceLabel.font = UIFont.NunitoBold(ofSize: 16)
        priceLabel.textColor = UIColor.pine
        priceLabel.easy.layout([
            CenterY().to(totalLabel),
            Left(20).to(totalLabel)
            ])
    }
}
