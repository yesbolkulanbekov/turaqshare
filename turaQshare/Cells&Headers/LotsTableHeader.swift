//
//  LotsTableHeader.swift
//  turaQshare
//
//  Created by Galym Anuarbek on 10/22/18.
//  Copyright © 2018 Galym Anuarbek. All rights reserved.
//

import UIKit
import EasyPeasy

class LotsTableHeader: UITableViewCell {
    
    let titleLabel = UILabel()
    let bg = UIView()
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setup() {
        self.backgroundColor = UIColor.white
        
        self.addSubview(bg)
        bg.backgroundColor = UIColor.white
        bg.easy.layout(Edges(0))
        
        self.addSubview(titleLabel)
        titleLabel.text = "Выберите парковку"
        titleLabel.textColor = UIColor.pine
        titleLabel.font = UIFont.LucidaGrandeBold(ofSize: 22)
        titleLabel.easy.layout([
            CenterX(),
            Top(32)
            ])
    }
    
}
