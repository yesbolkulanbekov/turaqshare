//
//  LotsTableFooter.swift
//  turaQshare
//
//  Created by Galym Anuarbek on 10/22/18.
//  Copyright © 2018 Galym Anuarbek. All rights reserved.
//

import UIKit
import EasyPeasy

class LotsTableFooter: UITableViewCell {
    
    let newLotButton = UIButton()
    let bg = UIView()
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setup() {
        self.backgroundColor = UIColor.white
        
        self.addSubview(bg)
        bg.backgroundColor = UIColor.white
        bg.easy.layout(Edges(0))
        
        self.addSubview(newLotButton)
        newLotButton.backgroundColor = UIColor.blueGreen
        newLotButton.setTitleColor(UIColor.denim, for: UIControl.State.normal)
        newLotButton.setTitle("+ Новый адрес", for: UIControl.State.normal)
        newLotButton.titleLabel?.font = UIFont.LucidaGrandeBold(ofSize: 18)
        newLotButton.easy.layout([
            Top(67),
            CenterX(),
            Width(206),
            Height(44)
            ])
    }
    
}
