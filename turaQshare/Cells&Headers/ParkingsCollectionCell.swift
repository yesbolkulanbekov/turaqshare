//
//  ParkingsCollectionCell.swift
//  turaQshare
//
//  Created by Galym Anuarbek on 10/17/18.
//  Copyright © 2018 Galym Anuarbek. All rights reserved.
//

import UIKit
import EasyPeasy

class ParkingCollectionCell: UICollectionViewCell {
    
    let photoView = UIImageView()
    let nameLabel = UILabel()
    let date = UILabel()
    let arrow = UIImageView()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setup() {
        self.addSubview(photoView)
        photoView.layer.cornerRadius = 30
        photoView.easy.layout([
            Left(10),
            CenterY(),
            Height(60),
            Width(60)
            ])
        
        self.addSubview(nameLabel)
        nameLabel.font = UIFont.LucidaGrandeRegular(ofSize: 14)
        nameLabel.textColor = UIColor.pine
        nameLabel.easy.layout([
            Left(7).to(photoView),
            CenterY()
            ])
        
        self.addSubview(arrow)
        arrow.image = UIImage(named: "register arrow")
        arrow.contentMode = UIView.ContentMode.scaleToFill
        arrow.easy.layout([
            CenterY(),
            Right(9),
            Width(20.9),
            Height(33.3)
            ])
        
        self.addSubview(date)
        date.font = UIFont.NunitoLight(ofSize: 14)
        date.textColor = UIColor.pine
        date.numberOfLines = 0
        date.textAlignment = NSTextAlignment.center
        date.easy.layout([
            Right(22).to(arrow),
            Width(96),
            CenterY()
            ])
    }
}
