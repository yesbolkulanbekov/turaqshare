//
//  UILabelExtension.swift
//  turaQshare
//
//  Created by Galym Anuarbek on 10/3/18.
//  Copyright © 2018 Galym Anuarbek. All rights reserved.
//

import UIKit

extension UILabel {
    func addCharacterSpacing(kernValue: Double) {
        if let labelText = text, labelText.count > 0 {
            let attributedString = NSMutableAttributedString(string: labelText)
            attributedString.addAttribute(NSAttributedString.Key.kern, value: kernValue, range: NSRange(location: 0, length: attributedString.length - 1))
            attributedText = attributedString
        }
    }
}
