//
//  StringExtension.swift
//  turaQshare
//
//  Created by Galym Anuarbek on 10/13/18.
//  Copyright © 2018 Galym Anuarbek. All rights reserved.
//

import UIKit

extension String {
    func getSubstring(from: Int, to: Int) -> String{
        let startIndex = self.index(self.startIndex, offsetBy: from)
        let endIndex = self.index(startIndex, offsetBy: to - from + 1)
        return String(self[startIndex..<endIndex])
    }
}
