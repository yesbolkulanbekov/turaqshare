//
//  UIFontExtension.swift
//  turaQshare
//
//  Created by Galym Anuarbek on 10/2/18.
//  Copyright © 2018 Galym Anuarbek. All rights reserved.
//

import UIKit

extension UIFont {
    static func LucidaGrandeBold(ofSize size: CGFloat) -> UIFont {
        if let font = UIFont(name: "LucidaGrande-Bold", size: size) {
            return font
        }
        return UIFont.systemFont(ofSize: size)
    }
    
    static func NunitoBold(ofSize size: CGFloat) -> UIFont {
        if let font = UIFont(name: "Nunito-Bold", size: size) {
            return font
        }
        return UIFont.systemFont(ofSize: size)
    }
    
    static func NunitoLight(ofSize size: CGFloat) -> UIFont {
        if let font = UIFont(name: "Nunito-Light", size: size) {
            return font
        }
        return UIFont.systemFont(ofSize: size)
    }
    
    static func NunitoItalic(ofSize size: CGFloat) -> UIFont {
        if let font = UIFont(name: "Nunito-Italic", size: size) {
            return font
        }
        return UIFont.systemFont(ofSize: size)
    }
    
    static func NunitoRegular(ofSize size: CGFloat) -> UIFont {
        if let font = UIFont(name: "Nunito-Regular", size: size) {
            return font
        }
        return UIFont.systemFont(ofSize: size)
    }
    
    static func LucidaGrandeRegular(ofSize size: CGFloat) -> UIFont {
        if let font = UIFont(name: "LucidaGrande", size: size) {
            return font
        }
        return UIFont.systemFont(ofSize: size)
    }
    
    static func HelveticaRegular(ofSize size: CGFloat) -> UIFont {
        if let font = UIFont(name: "Helvetica", size: size) {
            return font
        }
        return UIFont.systemFont(ofSize: size)
    }
    
    static func NunitoSemiBold(ofSize size: CGFloat) -> UIFont {
        if let font = UIFont(name: "Nunito-SemiBold", size: size) {
            return font
        }
        return UIFont.systemFont(ofSize: size)
    }
}
