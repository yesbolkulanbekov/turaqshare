//
//  NSDateExtension.swift
//  turaQshare
//
//  Created by Galym Anuarbek on 10/13/18.
//  Copyright © 2018 Galym Anuarbek. All rights reserved.
//

import UIKit

extension Date {
    func toString() -> String {
        let formatter = DateFormatter()
        formatter.dateFormat = "dd MMM yyyy, HH:mm"
        return formatter.string(from: self)
    }
    
    func getHour() -> Int {
        let calendar = Calendar.current
        let hour = calendar.component(.hour, from: self)
        return hour
    }
    
    func getMinutes() -> Int {
        let calendar = Calendar.current
        let minutes = calendar.component(.minute, from: self)
        return minutes
    }
    
    func getDay() -> Int {
        let calendar = Calendar.current
        let day = calendar.component(.day, from: self)
        return day
    }
    
    func getMonth() -> Int {
        let calendar = Calendar.current
        let year = calendar.component(.month, from: self)
        return year
    }
    
    func getYear() -> Int {
        let calendar = Calendar.current
        let year = calendar.component(.year, from: self)
        return year
    }
    
    func getSeconds() -> Int {
        let calendar = Calendar.current
        let seconds = calendar.component(.second, from: self)
        return seconds
    }
    
    static func getMonthNames() -> [String] {
        let months: [String] = ["Января", "Февраля", "Марта", "Апреля", "Мая", "Июня", "Июля", "Августа", "Сентября", "Октября", "Ноября", "Декабря"]
        return months
    }
    
    var millisecondsSince1970:Int {
        return Int((self.timeIntervalSince1970 * 1000.0).rounded())
    }
    
    init(milliseconds:Int) {
        self = Date(timeIntervalSince1970: TimeInterval(milliseconds) / 1000)
    }
}
