//
//  UIColorExtension.swift
//  turaQshare
//
//  Created by Galym Anuarbek on 10/2/18.
//  Copyright © 2018 Galym Anuarbek. All rights reserved.
//

import UIKit

extension UIColor {
//    static let denim = UIColor(red: 0, green: 136/255, blue: 122/255, alpha: 1)
//    static let pine = UIColor(red: 25/255, green: 54/255, blue: 40/255, alpha: 1)
//    static let whiteTwo = UIColor(red: 244/255, green: 244/255, blue: 244/255, alpha: 1)
//    static let blueGreen = UIColor(red: 237/255, green: 246/255, blue: 245/255, alpha: 1)
}

extension UIColor {
    @nonobjc class var teal: UIColor {
        return UIColor(red: 0.0, green: 139.0 / 255.0, blue: 122.0 / 255.0, alpha: 1.0)
    }
    
    @nonobjc class var pine: UIColor {
        return UIColor(red: 25.0 / 255.0, green: 54.0 / 255.0, blue: 40.0 / 255.0, alpha: 1.0)
    }
    
    @nonobjc class var shamrockGreen: UIColor {
        return UIColor(red: 0.0, green: 196.0 / 255.0, blue: 100.0 / 255.0, alpha: 1.0)
    }
    
    @nonobjc class var white: UIColor {
        return UIColor(white: 1.0, alpha: 1.0)
    }
    
    @nonobjc class var tomatoRed: UIColor {
        return UIColor(red: 1.0, green: 19.0 / 255.0, blue: 19.0 / 255.0, alpha: 1.0)
    }
    
    @nonobjc class var whiteTwo: UIColor {
        return UIColor(white: 244.0 / 255.0, alpha: 1.0)
    }
    
    @nonobjc class var denim: UIColor {
        return UIColor(red: 54.0 / 255.0, green: 113.0 / 255.0, blue: 107.0 / 255.0, alpha: 1.0)
    }
    
    @nonobjc class var blueGreen: UIColor {
        return UIColor(red: 0.0, green: 136.0 / 255.0, blue: 122.0 / 255.0, alpha: 1.0)
    }
    
}
